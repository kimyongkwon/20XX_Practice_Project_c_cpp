#pragma once

#include "stdafx.h"
#include "DataStructure.h"

typedef struct Point
{
    int x;
    int y;

    bool operator==(Point &data)
    {
        if (this->x == data.x && this->y == data.y)
            return true;
        return false;
    };
}Point;

//typedef int LData;
typedef Point LData;

//하나의 배열 리스트를 클래스로 표현한다.
class CArrayList
{
protected:
    LData                   ArrayList[ARRAY_NUM];
    uint32_t                curPosition;
    uint32_t                numOfData;
public:
    CArrayList();
    ~CArrayList();

    void Insert(LData &data);
    void PrintDataNum(void) const;
    bool PrintFirst(LData &data);
    bool PrintNext(LData &data);
    void PrintData(LData &data);
    bool CompData(LData &Data, LData &RemoveData);
    void RemoveData(void);

    void InitCurPosition(void);
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//배열리스트 파트
class CArrayListPart : public CDataStructure
{
protected:
    // CDataStructure의 멤버변수를 쓰는게 아니라 멤버변수를 새롭게 정의한다. 
    CArrayList                m_ArrayList;
public:
    CArrayListPart();
    ~CArrayListPart();
    void ShowCommand(void);
    virtual bool Update(void) override;
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//원형큐 파트

#define MAX_QUEUE_NUM 4   

//여기서 큐는 배열 기반으로 되어 있다.
class CArrayListForQueue
{
    int             ArrayList[MAX_QUEUE_NUM];
    uint32_t        front;
    uint32_t        rear;
public:
    CArrayListForQueue();
    ~CArrayListForQueue();
    void Enqueue(int data);
    void Dequeue(void);
    int NextPosIdx(int);
    bool QueueIsEmpty(void);
    bool QueueIsFull(void);
};

class CCircularQueuePart : public CDataStructure
{
    CArrayListForQueue m_ArrayList;
public:
    CCircularQueuePart();
    ~CCircularQueuePart();
    void ShowCommand(void);
    virtual bool Update(void) override;
};