#include "SimpleHeap.h"

//void HeapInit(Heap * ph)
//{
//	ph->numOfData = 0;
//}
//
//int HIsEmpty(Heap * ph)
//{
//	if(ph->numOfData == 0)
//		return TRUE;
//	else
//		return FALSE;
//}
//
//int GetParentIDX(int idx) 
//{ 
//	return idx/2; 
//}
//
//int GetLChildIDX(int idx) 
//{ 
//	return idx*2; 
//}
//
//int GetRChildIDX(int idx) 
//{ 
//	return GetLChildIDX(idx)+1; 
//}
//
//int GetHiPriChildIDX(Heap * ph, int idx)
//{
//	if(GetLChildIDX(idx) > ph->numOfData)    // 자식 노드가 없다면
//		return 0;
//
//	else if(GetLChildIDX(idx) == ph->numOfData)    // 왼쪽 자식 노드가 마지막 노드라면
//		return GetLChildIDX(idx);
//
//	else   // 왼쪽 자식 노드와 오른쪽 자식 노드의 우선순위를 비교
//	{
//		if(ph->heapArr[GetLChildIDX(idx)].pr 
//						> ph->heapArr[GetRChildIDX(idx)].pr)
//			return GetRChildIDX(idx);
//		else
//			return GetLChildIDX(idx);
//	}
//}
//
//void HInsert(Heap * ph, HData data, Priority pr)
//{
//	int idx = ph->numOfData+1;
//	HeapElem nelem = {pr, data}; 
//
//	while(idx != 1)
//	{
//		if(pr < (ph->heapArr[GetParentIDX(idx)].pr))
//		{
//			ph->heapArr[idx] = ph->heapArr[GetParentIDX(idx)];
//			idx = GetParentIDX(idx);
//		}
//		else
//			break;
//	}
//	
//	ph->heapArr[idx] = nelem;
//	ph->numOfData += 1;
//}
//
//HData HDelete(Heap * ph)
//{
//	HData retData = (ph->heapArr[1]).data;    // 삭제할 데이터 임시 저장
//	HeapElem lastElem = ph->heapArr[ph->numOfData];
//
//	int parentIdx = 1;    // 루트 노드의 Index
//	int childIdx;
//
//	while(childIdx = GetHiPriChildIDX(ph, parentIdx))
//	{
//		if(lastElem.pr <= ph->heapArr[childIdx].pr)
//			break;
//
//		ph->heapArr[parentIdx] = ph->heapArr[childIdx];
//		parentIdx = childIdx;
//	}
//
//	ph->heapArr[parentIdx] = lastElem;
//	ph->numOfData -= 1;
//	return retData;
//}

#ifdef UsefulHeap

void HeapInit(Heap * ph, PriorityComp pc)
{
    ph->numOfData = 0;
    ph->comp = pc;
}

int HIsEmpty(Heap * ph)
{
    if (ph->numOfData > 0)
        return FALSE;
    return TRUE;
}

int GetParentIdx(int childidx)
{
    return childidx / 2;
}

int GetLeftChildIdx(int parentidx)
{
    return parentidx * 2;
}

int GetRightChildIdx(int parentidx)
{
    return (parentidx * 2) + 1;
}

void HInsert(Heap * ph, HData data)
{
    int idx = ph->numOfData + 1;

    //i가 1일때까지 for문이 순회한다.(i가 1이라는 뜻은 루트노드의 배열인덱스를 나타낸다)
    while(idx != 1 )
    {
        if (ph->comp(data, ph->heapArr[GetParentIdx(idx)]) > 0)
        {
            ph->heapArr[idx] = ph->heapArr[GetParentIdx(idx)];
            idx = GetParentIdx(idx);
        }
        else
            break;
    }

    ph->heapArr[idx] = data;
    ph->numOfData += 1;
}

int GetHiPriChildIDX(Heap * ph, int idx)
{
    //경우는 총 3가지
    //자식노드가 없거나, 왼쪽 자식노드만 있거나, 양쪽 자식노드가 다 있거나
    

    // ★((현재 저장되있는 노드의 수 = 힙의 마지막 인덱스))★

    //다음은 자식 노드가 없는 경우다. 왼쪽 자식노드 인덱스가 현재 저장되있는 노드의 수(numOfData)보다 크기때문에
    if (GetLeftChildIdx(idx) > ph->numOfData)
        return 0;

    //왼쪽 자식노드만 있는 경우다. 왜냐하면 왼쪽자식노드 인덱스와 현재 저장되있는 노드의 수(numOfData)와 같기때문에
    else if (GetLeftChildIdx(idx) == ph->numOfData)
        return GetLeftChildIdx(idx);

    //자식노드가 둘다 있는 경우다
    else
    {
        //왼쪽 자식노드의 우선순위가 오른쪽 자식노드의 우선순위보다 높다면
        if(ph->comp(ph->heapArr[GetLeftChildIdx(idx)], ph->heapArr[GetRightChildIdx(idx)]) < 0 )
            return GetRightChildIdx(idx);

        //왼쪽 자식 노드의 우선순위가 높다면
        else
            return GetLeftChildIdx(idx);
    }
}

HData HDelete(Heap * ph)
{
    //힙의 마지막 데이터를 저장해둔다.
    HData LastElem = ph->heapArr[ph->numOfData];
    HData data = ph->heapArr[1];

    //루트노드부터 순회하기때문에 ParentIDX는 1이다.
    int ParentIDX = 1;
    int childIdx;

    //GetHiPriChildIDX함수는 자식 인덱스를 구하는 함수다.
    while (childIdx = GetHiPriChildIDX(ph, ParentIDX))
    {
        //만약 마지막 데이터의 우선순위가 자식노드의 우선순위보다 더 크다면(pr이 작을 것이다)
        //while문을 빠져나간다. 즉 값을 바꾼다는 뜻
        if (ph->comp(LastElem, ph->heapArr[childIdx]) >= 0)
            break;

        //마지막 데이터의 우선순위가 자식노드의 우선순위보다 크지 않다면(pr이 크다면)
        ph->heapArr[ParentIDX] = ph->heapArr[childIdx];
        ParentIDX = childIdx;
    }

    ph->heapArr[ParentIDX] = LastElem;
    ph->numOfData--;
    return data;
}

#else

void HeapInit(Heap * ph)
{
    ph->numOfData = 0;
}

int HIsEmpty(Heap * ph)
{
    if (ph->numOfData > 0)
        return FALSE;
    return TRUE;
}

int GetParentIdx(int childidx)
{
    return childidx / 2;
}

int GetLeftChildIdx(int parentidx)
{
    return parentidx * 2;
}

int GetRightChildIdx(int parentidx)
{
    return (parentidx * 2) + 1;
}

void HInsert(Heap * ph, HData data, Priority pr)
{
    HeapElem newNode;
    newNode.data = data;
    newNode.pr = pr;

    //if (!HIsEmpty(ph))

    //가장 마지막 위치에 데이터를 둔다.
    //배열의 인덱스는 1부터다.
    ph->heapArr[++ph->numOfData] = newNode;

    //i가 1일때까지 for문이 순회한다.(i가 1이라는 뜻은 루트노드의 배열인덱스를 나타낸다)
    for (int idx = ph->numOfData; idx > 0; idx--)
    {
        //부모의 인덱스를 구한다.
        int ParentIdx = GetParentIdx(idx);
        //부모의 인덱스 자리에 있는 원소의 우선순위와 지금 나의 원소의 우선순위를 비교한다.
        //pr이 낮을수록 우선순위가 높으므로 내 우선순위가 더 높으면 부모 원소와 자리를 바꾼다.
        if (ph->heapArr[ParentIdx].pr > ph->heapArr[idx].pr)
        {
            newNode = ph->heapArr[ParentIdx];
            ph->heapArr[ParentIdx] = ph->heapArr[idx];
            ph->heapArr[idx] = newNode;
        }
    }
}

int GetHiPriChildIDX(Heap * ph, int idx)
{
    //경우는 총 3가지
    //자식노드가 없거나, 왼쪽 자식노드만 있거나, 양쪽 자식노드가 다 있거나


    // ★((현재 저장되있는 노드의 수 = 힙의 마지막 인덱스))★

    //다음은 자식 노드가 없는 경우다. 왼쪽 자식노드 인덱스가 현재 저장되있는 노드의 수(numOfData)보다 크기때문에
    if (GetLeftChildIdx(idx) > ph->numOfData)
        return 0;

    //왼쪽 자식노드만 있는 경우다. 왜냐하면 왼쪽자식노드 인덱스와 현재 저장되있는 노드의 수(numOfData)와 같기때문에
    else if (GetLeftChildIdx(idx) == ph->numOfData)
        return GetLeftChildIdx(idx);

    //자식노드가 둘다 있는 경우다
    else
    {
        //왼쪽 자식노드의 우선순위가 오른쪽 자식노드의 우선순위보다 높다면
        if (ph->heapArr[GetLeftChildIdx(idx)].pr > ph->heapArr[GetRightChildIdx(idx)].pr)
            return GetRightChildIdx(idx);

        //왼쪽 자식 노드의 우선순위가 높다면
        else
            return GetLeftChildIdx(idx);
    }
}

HData HDelete(Heap * ph)
{
    //힙의 마지막 데이터를 저장해둔다.
    HeapElem LastElem = ph->heapArr[ph->numOfData];
    HData data = ph->heapArr[1].data;

    //루트노드부터 순회하기때문에 ParentIDX는 1이다.
    int ParentIDX = 1;
    int childIdx;

    //GetHiPriChildIDX함수는 자식 인덱스를 구하는 함수다.
    while (childIdx = GetHiPriChildIDX(ph, ParentIDX))
    {
        //만약 마지막 데이터의 우선순위가 자식노드의 우선순위보다 더 크다면(pr이 작을 것이다)
        //while문을 빠져나간다. 즉 값을 바꾼다는 뜻
        if (LastElem.pr <= ph->heapArr[childIdx].pr)
            break;

        //마지막 데이터의 우선순위가 자식노드의 우선순위보다 크지 않다면(pr이 크다면)
        ph->heapArr[ParentIDX] = ph->heapArr[childIdx];
        ParentIDX = childIdx;
    }

    ph->heapArr[ParentIDX] = LastElem;
    ph->numOfData--;
    return data;
}

#endif