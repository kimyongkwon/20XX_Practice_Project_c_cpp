#pragma once
#include "stdafx.h"
#include "DataStructure.h"

typedef uint32_t DATA;
    
typedef struct NODE
{
    DATA Data;
    struct NODE * next;
    struct NODE * prev;
}Node;

class DBLinkedList
{
protected:
    Node * head;
    Node * cur;
    int numOfData;
public:
    void Init(void);
    void Insert(DATA & data);
    void PrintDataNum(void);
    bool PrintFirst(DATA & data);
    bool PrintNext(DATA & data);
    void PrintData(DATA & data);
    void Remove(void);
};


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//양방향 연결리스트 파트
class CDBLinkedListPart : public CDataStructure
{
    //리스트를 멤버변수로 가지고 있다.
    DBLinkedList m_LinkedList;
public:
    virtual bool Update(void) override;
    virtual void InitData(void) override;
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//스택 파트

class LinkedListForStack : public DBLinkedList
{
public:
    bool IsEmpty(void);
    void Push(DATA & data);
    void Pop(void);
    DATA Peek(void);
};

class CStackPart : public CDataStructure
{
    LinkedListForStack m_LinkedList;
public:
    virtual bool Update(void) override;
    virtual void InitData(void) override;
};