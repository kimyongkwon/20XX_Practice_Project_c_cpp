#pragma once

const int ARRAY_NUM = 10000;

class CDataStructure
{
public:
    CDataStructure();
    ~CDataStructure();
    //uint32_t & GetArrData() const;

    //공통된 작업을 수행한다.
    virtual void Common(void);
    virtual void InitData(void);
    virtual void SortData(void);

    virtual bool Update(void) { return 0; };
};

