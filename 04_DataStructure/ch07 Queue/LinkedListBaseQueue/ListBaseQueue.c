#include <stdio.h>
#include <stdlib.h>
#include "ListBaseQueue.h"

//void QueueInit(Queue * pq)
//{
//	pq->front = NULL;
//	pq->rear = NULL;
//}
//
//int QIsEmpty(Queue * pq)
//{
//	if(pq->front == NULL)
//		return TRUE;
//	else
//		return FALSE;
//}
//
//void Enqueue(Queue * pq, Data data)
//{
//	Node * newNode = (Node*)malloc(sizeof(Node));
//	newNode->next = NULL;
//	newNode->data = data;
//
//	if(QIsEmpty(pq))
//	{
//		pq->front = newNode;
//		pq->rear = newNode;
//	}
//	else
//	{
//		pq->rear->next = newNode;
//		pq->rear = newNode;
//	}
//}
//
//Data Dequeue(Queue * pq)
//{
//	Node * delNode;
//	Data retData;
//
//	if(QIsEmpty(pq))
//	{
//		printf("Queue Memory Error!");
//		exit(-1);
//	}
//
//	delNode = pq->front;
//	retData = delNode->data;
//	pq->front = pq->front->next;
//
//	free(delNode);
//	return retData;
//}
//
//Data QPeek(Queue * pq)
//{
//	if(QIsEmpty(pq))
//	{
//		printf("Queue Memory Error!");
//		exit(-1);
//	}
//
//	return pq->front->data;
//}


void QueueInit(Queue * pq)
{
    pq->front = NULL;
    pq->rear = NULL;
}

void Enqueue(Queue * pq, Data data)
{   
    Node * newNode = (Node*)malloc(sizeof(Node));
    newNode->data = data;
    newNode->next = NULL;

    //만약 큐가 비어있다면 front, rear가 새로운노드를 가리키게 한다.
    if (QIsEmpty(pq))
    {
        pq->rear = newNode;
        pq->front = newNode;
    }
    //큐가 비어있지않다면 rear->next에 새로운 노드를 만들고 그 노드를 rear가 가리키게 한다.
    else
    {
        pq->rear->next = newNode;
        pq->rear = newNode;
    }
}

int QIsEmpty(Queue * pq)
{
    if (pq->front == NULL)
        return TRUE;
    return FALSE;
}

Data Dequeue(Queue * pq)
{
    Node * tmp = pq->front;
    Data data = tmp->data;

    //front는 다음 노드를 가리키게 한다. 만약 다음 노드가 없다면
    //front는 NULL을 가리키게 될것이다. front가 NULL을 가리킨다는 의미는 큐가 비어있다는 의미다.
    pq->front = pq->front->next;

    free(tmp);
    return data;

}