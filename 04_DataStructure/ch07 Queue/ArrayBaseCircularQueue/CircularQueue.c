#include <stdio.h>
#include <stdlib.h>
#include "CircularQueue.h"

//void QueueInit(Queue * pq)
//{
//	pq->front = 0;
//	pq->rear = 0;
//}
//
//int QIsEmpty(Queue * pq)
//{
//	if(pq->front == pq->rear)
//		return TRUE;
//	else
//		return FALSE;
//}
//
//int NextPosIdx(int pos)
//{
//	if(pos == QUE_LEN-1)
//		return 0;
//	else
//		return pos+1;
//}
//
//void Enqueue(Queue * pq, Data data)
//{
//	if(NextPosIdx(pq->rear) == pq->front)
//	{
//		printf("Queue Memory Error!");
//		exit(-1);
//	}
//
//	pq->rear = NextPosIdx(pq->rear);
//	pq->queArr[pq->rear] = data;
//}
//
//Data Dequeue(Queue * pq)
//{
//	if(QIsEmpty(pq))
//	{
//		printf("Queue Memory Error!");
//		exit(-1);
//	}
//
//	pq->front = NextPosIdx(pq->front);
//	return pq->queArr[pq->front];
//}
//
//Data QPeek(Queue * pq)
//{
//	if(QIsEmpty(pq))
//	{
//		printf("Queue Memory Error!");
//		exit(-1);
//	}
//
//	return pq->queArr[NextPosIdx(pq->front)];
//}

void QueueInit(Queue * pq)
{
    pq->front = 0;
    pq->rear = 0;
}

int QIsEmpty(Queue * pq)
{
    if (pq->front == pq->rear)
        return TRUE;
    return FALSE;
}

void Enqueue(Queue * pq, Data data)
{
    //데이터를 Enqueue할때 NextPos의 리턴값과 front가 같다는 의미는 큐가 꽉 찼다는 의미이기때문에
    //프로그램을 종료한다.
    if (NextPos(pq->rear) == pq->front)
    {
        printf("배열이 꽉찻음, 배열 칸 좀 더 키워임마 ");
        exit(-1);
    }

    //위의 조건이 성립하지 않으면 NextPos함수를 통해 rear의 인덱스 한칸 이동
    pq->rear = NextPos(pq->rear);
    pq->queArr[pq->rear] = data;
}

Data Dequeue(Queue * pq)
{
    //NextPos함수를 통해 front의 인덱스 한칸 이동
    pq->front = NextPos(pq->front);
    return pq->queArr[pq->front];
}

int NextPos(int pos)
{
    //pos의 위치가 배열의 끝과 같다면 0을 리턴(= 배열의 맨처음으로 인덱스를 이동한다)
    if (pos == QUE_LEN - 1)
        return 0;
    //pos의 위치가 배열의 끝이 아니라면 앞으로 한칸 전진
    else
        return pos + 1;
}