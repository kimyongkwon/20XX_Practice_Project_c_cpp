#include <stdio.h>
#include <stdlib.h>
#include "DLinkedList.h"

//void ListInit(List * plist)
//{
//	plist->head = (Node*)malloc(sizeof(Node));
//	plist->head->next = NULL;
//	plist->comp = NULL;
//	plist->numOfData = 0;
//}
//
//void FInsert(List * plist, LData data)
//{
//	Node * newNode = (Node*)malloc(sizeof(Node));
//	newNode->data = data;
//
//	newNode->next = plist->head->next;
//	plist->head->next = newNode;
//
//	(plist->numOfData)++;
//}
//
//void SInsert(List * plist, LData data)
//{
//	Node * newNode = (Node*)malloc(sizeof(Node));
//	Node * pred = plist->head;
//	newNode->data = data;
//
//	while(pred->next != NULL &&
//		plist->comp(data, pred->next->data) != 0)
//	{
//		pred = pred->next;
//	}
//
//	newNode->next = pred->next;
//	pred->next = newNode;
//
//	(plist->numOfData)++;
//}
//
//
//void LInsert(List * plist, LData data)
//{
//	if(plist->comp == NULL)
//		FInsert(plist, data);
//	else
//		SInsert(plist, data);
//}
//
//int LFirst(List * plist, LData * pdata)
//{
//	if(plist->head->next == NULL)
//		return FALSE;
//
//	plist->before = plist->head;
//	plist->cur = plist->head->next;
//
//	*pdata = plist->cur->data;
//	return TRUE;
//}
//
//int LNext(List * plist, LData * pdata)
//{
//	if(plist->cur->next == NULL)
//		return FALSE;
//
//	plist->before = plist->cur;
//	plist->cur = plist->cur->next;
//
//	*pdata = plist->cur->data;
//	return TRUE;
//}
//
//LData LRemove(List * plist)
//{
//	Node * rpos = plist->cur;
//	LData rdata = rpos->data;
//
//	plist->before->next = plist->cur->next;
//	plist->cur = plist->before;
//
//	free(rpos);
//	(plist->numOfData)--;
//	return rdata;
//}
//
//int LCount(List * plist)
//{
//	return plist->numOfData;
//}
//
//void SetSortRule(List * plist, int (*comp)(LData d1, LData d2))
//{
//	plist->comp = comp;
//}

void ListInit(List * plist)
{
    //더미노드를 head가 가리킨다.
    plist->head = (Node*)malloc(sizeof(Node));
    plist->head->next = NULL;
    plist->cur = NULL;
    plist->before = NULL;
    plist->numOfData = 0;
    plist->comp = NULL;
}

void LInsert(List * plist, LData data)
{
    if (plist->comp == NULL) 
        FInsert(plist, data);
    else 
        SInsert(plist, data);
}

void FInsert(List * plist, LData data)
{
    Node * newNode = (Node*)malloc(sizeof(Node));
    newNode->data = data;
    newNode->next = NULL;

    //case1
    ////리스트에 데이터가 없으면 바로 새로운노드를 연결
    //if (plist->head->next == NULL)
    //    plist->head->next = newNode;
    ////리스트에 데이터가 있다면 새로운노드에 원래 있는 노드를 연결하고
    ////더미노드에 새로운 노드를 연결한다.
    //else
    //{
    //    newNode->next = plist->head->next;
    //    plist->head->next = newNode;
    //}

    //case2
    //별도의 분기없이 모든 경우에 삽입과정은 동일하다.
    //이것이 더미노드기반의 연결리스트이다.
    newNode->next = plist->head->next;
    plist->head->next = newNode;

    plist->numOfData += 1;
}

void SInsert(List * plist, LData data)
{
    Node * newNode = (Node*)malloc(sizeof(Node));
    newNode->data = data;
    newNode->next = NULL;
    
    Node * pred = plist->head;

    //pred->next가 NULL이거나 comp함수의 리턴값이 0 (새로운 데이터값이 pred다음에 
    //노드에 있는 데이터값보다 작을경우) 일 때까지 pred포인터를 계속 뒤로 이동한다.
    while (pred->next != NULL && plist->comp(data, pred->next->data) != 0)
    {
        pred = pred->next;
    }
                                           
    newNode->next = pred->next;
    pred->next = newNode;

    plist->numOfData += 1;
}

int LCount(List * plist)
{
    return plist->numOfData;
}

int LFirst(List * plist, LData * data)
{   
    //데이터가 하나도 저장되지않았으면 false 리턴(더미노드의 next가 NULL)
    if (plist->head->next == NULL) 
        return FALSE;

    //데이터를 지우려면 before가 있어야할것이다.
    plist->before = plist->head;
    //cur포인터는 더미노드 다음 노드를 가리킨다. 
    plist->cur = plist->head->next;
    *data = plist->cur->data;
    return TRUE;
}

int LNext(List * plist, LData * data)
{
    if (plist->cur->next == NULL) return FALSE;

    ////case1
    ////1. 데이터를 data에 저장하고
    //*data = plist->cur->next->data;
    //plist->before = plist->cur;
    ////2. cur포인터를 다음칸으로 옮긴다.
    //plist->cur = plist->cur->next;

    //case2
    plist->before = plist->cur;
    //2. cur포인터를 다음칸으로 옮긴다.
    plist->cur = plist->cur->next;
    *data = plist->cur->data;

    //case1과 2가 다른점은 
    //case1 : cur포인터를 옮기기전에 data를 참조하느냐 아니면
    //case2 : cur포인터를 옮겨놓고 data를 참조하느냐 차이가있다 
    //둘다 가능한 방법이지만 case2가 의미상 더 괜찮음.
    return TRUE;
}

LData LRemove(List * plist)
{
    ////방법1
    //LData tmp = plist->cur->data;
    //plist->before->next = plist->cur->next;
    //free(plist->cur);
    //plist->numOfData -= 1;
    //plist->cur = plist->before;
    //return tmp;

    //방법2
    Node * rpos = plist->cur;
    LData rdata = rpos->data;
    plist->before->next = plist->cur->next;
    plist->cur = plist->before;
    free(rpos);
    (plist->numOfData)--;
    return rdata;
}

void SetSortRule(List * plist, int(*comp)(LData d1, LData d2))
{
    plist->comp = comp;
}
