#include "LinkedListPart.h"

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
void DBLinkedList::Init(void)
{
   head = NULL;
   numOfData = 0;
}

void DBLinkedList::Insert(DATA & data)
{
    Node * newNode = new Node;
    newNode->Data = data;
    newNode->next = NULL;
    newNode->prev = NULL;

    newNode->next = head;
    if (head != NULL)
        head->prev = newNode;
    head = newNode;

    numOfData++;
}

void DBLinkedList::PrintDataNum(void)
{
    cout << "현재 총 " << numOfData << "개의 데이터를 가지고 있습니다." << endl;
}

bool DBLinkedList::PrintFirst(DATA & data)
{
    if (numOfData == 0)
    {
        system("cls");
        puts("데이터가 하나도 없어. 데이터부터 저장해라");
        return false;
    }

    cur = head;
    data = cur->Data;

    return true;
}

bool DBLinkedList::PrintNext(DATA & data)
{
    if (cur->next == NULL)
        return false;

    cur = cur->next;
    data = cur->Data;

    return true;
}

void DBLinkedList::PrintData(DATA & data)
{
    cout << data << " " << endl;
}

void DBLinkedList::Remove(void)
{
    cout << cur->Data << "을 삭제하였습니다" << endl;

    Node * tmp = cur;

    //양방향연결리스트를 사용하면 삭제할때 필요한 before포인터가 필요가 없다. 
    cur->prev->next = cur->next;
    cur->next->prev = cur->prev;
    cur = cur->prev;

    numOfData--;

    delete tmp;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//연결리스트 파트
void CDBLinkedListPart::InitData(void)
{
    m_LinkedList.Init();
}

bool CDBLinkedListPart::Update(void)
{
    this->InitData();
    
    while (1)
    {
        cout << "연결리스트에서 무엇을 하실건가요?" << endl;
        cout << "1. Insert" << endl;
        cout << "2. PrintDataNum" << endl;
        cout << "3. ShowDatas" << endl;
        cout << "4. Remove" << endl;
        cout << "0. 뒤로가기" << endl;
        int cmd;
        cin >> cmd;
        
        switch (cmd)
        {
        case 1:
        {
            DATA Data;
            system("cls");
            cout << "어떤값을 삽입하시겠습니까? " << endl;
            cin >> Data;
            //데이터는 머리(head)에 추가된다.
            m_LinkedList.Insert(Data);
            break;
        }
        case 2:
        {
            system("cls");
            m_LinkedList.PrintDataNum();
            break;
        }
        case 3:
        {
            system("cls");
            DATA Data;
            if (m_LinkedList.PrintFirst(Data))
            {
                m_LinkedList.PrintData(Data);

                while (m_LinkedList.PrintNext(Data))
                {
                    m_LinkedList.PrintData(Data);
                }
            }
            break;
        }
        case 4:
        {
            system("cls");
            DATA Data; DATA RemoveData;
            cout << "어떤값을 삭제하시겠습니까? " << endl;
            cin >> RemoveData;
            if (m_LinkedList.PrintFirst(Data))
            {
                if (Data == RemoveData)
                    m_LinkedList.Remove();
                while (m_LinkedList.PrintNext(Data))
                {
                    if (Data == RemoveData)
                        m_LinkedList.Remove();
                }
            }
            break;
        }
        case 0:
        {
            return 0;
        }
        default:
            break;
        }
    }
    return 0;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//스택 파트
void LinkedListForStack::Push(DATA & data)
{
    Node * newNode = new Node;
    newNode->Data = data;
    newNode->next = NULL;
    newNode->prev = NULL;

    newNode->next = head;
    if (head != NULL)
        head->prev = newNode;
    head = newNode;
    
    numOfData++;
}

void LinkedListForStack::Pop(void)
{
    cur = head;
    Node * tmp = cur;
    DATA data = tmp->Data;
    head = cur->next;

    if (cur->next != NULL)
        cur->next->prev = NULL;

    delete tmp;
    
    cout << data << "가 Pop되었습니다" << endl;

    numOfData--;
}

bool LinkedListForStack::IsEmpty(void)
{
    if (head == NULL)
        return true;
    return false;
}

void CStackPart::InitData(void)
{
    m_LinkedList.Init();
}

bool CStackPart::Update(void)
{
    this->InitData();

    while (1)
    {
        cout << "스택에서 무엇을 하실건가요?" << endl;
        cout << "1. Push" << endl;
        cout << "2. PrintDataNum" << endl;
        cout << "3. ShowDatas" << endl;
        cout << "4. Pop" << endl;
        cout << "0. 뒤로가기" << endl;
        int cmd;
        cin >> cmd;

        switch (cmd)
        {
        case 1:
        {
            DATA Data;
            system("cls");
            cout << "어떤값을 Push하시겠습니까? " << endl;
            cin >> Data;
            m_LinkedList.Push(Data);
            break;
        }
        case 2:
        {
            system("cls");
            m_LinkedList.PrintDataNum();
            break;
        }
        case 3:
        {
            system("cls");
            DATA Data;
            if (m_LinkedList.PrintFirst(Data))
            {
                m_LinkedList.PrintData(Data);
                while (m_LinkedList.PrintNext(Data))
                {
                    m_LinkedList.PrintData(Data);
                }
            }
            break;
        }
        case 4:
        {
            system("cls");
            if(!m_LinkedList.IsEmpty())         //노드가 있으면 그 노드를 Pop한다.
                m_LinkedList.Pop();             
            else
            {                                  
                system("cls");
                puts("데이터가 하나도 없어. 데이터부터 저장해라");
            }
            break;
        }
        case 0:
        {
            return 0;
        }
        default:
            break;
        }
    }
    return 0;
}


