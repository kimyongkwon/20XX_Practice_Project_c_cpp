#include "ArrayListPart.h"

CArrayList::CArrayList(void)
{
    curPosition = -1;
    numOfData = 0;
}

CArrayList::~CArrayList(void)
{
}

void CArrayList::Insert(LData &data)
{
    assert(numOfData <= ARRAY_NUM && "배열의 크기를 넘어갔습니다. 너 추가시키지마");

    ArrayList[numOfData] = data;
    numOfData++;

    system("cls");
    cout << "**삽입완료**" << endl;
}

void CArrayList::PrintDataNum(void) const
{
    cout << "현재 총 " << numOfData << "개의 데이터를 가지고 있습니다." << endl;
}

bool CArrayList::PrintFirst(LData &data)
{
    if (numOfData == 0) 
    {
        system("cls");
        puts("데이터가 하나도 없어. 데이터부터 저장해라");
        return false;
    }

    curPosition = 0;
    data = ArrayList[curPosition];
    return true;
}

void CArrayList::PrintData(LData &data)
{
    cout << "x : " << data.x << " y : " << data.y << endl;
}

bool CArrayList::PrintNext(LData &data)
{
    if (curPosition >= numOfData - 1 ) return false;

    data = ArrayList[++curPosition];
    return true;
}

void CArrayList::InitCurPosition(void)
{
    curPosition = 0;
}

bool CArrayList::CompData(LData &data, LData &RemoveData)
{
    if (data == RemoveData) return true;
    else return false;
}

void CArrayList::RemoveData(void)
{
    //지우려고 하는 데이터 뒤에있는 데이터들을 앞으로 모두 땡긴다.
    //그래서 cPos는 현재지워야하는 데이터 뒤의 인덱스
    int cPos = curPosition + 1;

    for (cPos; cPos <= numOfData; cPos++){
        ArrayList[cPos -1] = ArrayList[cPos];
    }

    //데이터를 삭제하고 참조위치를 한 칸 앞으로 한다.
    //왜? 그렇지않으면 땡겨서 참조하지않은 데이터를 가리키고 있기때문에
    curPosition--;

    //현재 데이터의 갯수를 -1 한다.
    numOfData--;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//배열리스트 파트
CArrayListPart::CArrayListPart()
{
}

CArrayListPart::~CArrayListPart()
{
}

void CArrayListPart::ShowCommand(void)
{
    cout << "배열리스트에서 무엇을 하실건가요?" << endl;
    cout << "1. Insert" << endl;
    cout << "2. PrintDataNum" << endl;
    cout << "3. ShowDatas" << endl;
    cout << "4. Remove" << endl;
    cout << "0. 뒤로가기" << endl;
}

bool CArrayListPart::Update(void)
{
    while(1)
    {
        ShowCommand();
        int cmd;
        cin >> cmd;
        switch (cmd)
        {
        case 1:
        {
            system("cls");
            LData * pLData = new LData;
            cout << "어떤값을 삽입하시겠습니까? " << endl;
            cout << "x : "; cin >> pLData->x;
            cout << "y : "; cin >> pLData->y;
            m_ArrayList.Insert(*pLData);
            delete pLData;      //이렇게 delete해도 된다.이미 위의 자료구조 함수에서 값이 복사되었기때문에
            break;
        }
        case 2:
        {
            system("cls");
            m_ArrayList.PrintDataNum();
            break;
        }
        case 3:
        {
            system("cls");
            LData * pLData = new LData;
            if (m_ArrayList.PrintFirst(*pLData))
            {
                m_ArrayList.PrintData(*pLData);

                while (m_ArrayList.PrintNext(*pLData))
                {
                    m_ArrayList.PrintData(*pLData);
                }
            }
            m_ArrayList.InitCurPosition();
            delete pLData;
            break;
        }
        case 4:
        {
            system("cls");
            LData * pRemoveData = new LData;
            LData * pLData = new LData;
            cout << "어떤값을 삭제하시겠습니까? " << endl;
            cout << "x : "; cin >> pRemoveData->x;
            cout << "y : "; cin >> pRemoveData->y;
            if (m_ArrayList.PrintFirst(*pLData))
            {
                if (m_ArrayList.CompData(*pLData, *pRemoveData))
                    m_ArrayList.RemoveData();
                while (m_ArrayList.PrintNext(*pLData))
                {
                    if (m_ArrayList.CompData(*pLData, *pRemoveData))
                        m_ArrayList.RemoveData();
                }
            }
            m_ArrayList.InitCurPosition();
            delete pLData, delete pRemoveData;
            break;
        }
        case 0:
        {
            return 0;
        }
        default:
            break;
        }
    }
    return 0;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//원형큐 파트
CCircularQueuePart::CCircularQueuePart()
{
}

CCircularQueuePart::~CCircularQueuePart()
{
}

void CCircularQueuePart::ShowCommand(void)
{
    cout << "큐에서 무엇을 하실건가요?" << endl;
    cout << "1. Enqueue" << endl;
    cout << "2. Dequeue" << endl;
    cout << "0. 뒤로가기" << endl;
}

bool CCircularQueuePart::Update(void)
{
    while (1)
    {
        ShowCommand();
        int cmd;
        cin >> cmd;
        switch (cmd)
        {
        case 1:
        {
            system("cls");
            int Data;
            //Queue가 Full상태가 아니라면
            if (! m_ArrayList.QueueIsFull())
            {
                cout << "어떤값을 Enqueue하시겠습니까? " << endl;
                cin >> Data;
                m_ArrayList.Enqueue(Data);
            }
         
            break;
        }
        case 2:
        {
            system("cls");
            if (!(m_ArrayList.QueueIsEmpty()))
                m_ArrayList.Dequeue();
            break;
        }
        case 0:
        {
            return 0;
        }
        default:
            break;
        }
    }
    return 0;
}


CArrayListForQueue::CArrayListForQueue()
{
    front = 0; 
    rear = 0;
}

CArrayListForQueue::~CArrayListForQueue()
{
}

bool CArrayListForQueue::QueueIsEmpty(void)
{
    if (front == rear)
    {
        cout << "큐가 비어 있습니다.Enqueue먼저 해주세요" << endl;
        return true;
    }
    else
        return false;
}
bool CArrayListForQueue::QueueIsFull(void)
{
    if (NextPosIdx(rear) == front)
    {
        cout << "큐가 가득 차있습니다.Dequeue먼저 해주세요" << endl;
        return true;
    }
    else
        return false;
}
void CArrayListForQueue::Enqueue(int data)
{
    rear = NextPosIdx(rear);
    ArrayList[rear] = data;
}

void CArrayListForQueue::Dequeue(void)
{
    front = NextPosIdx(front);
    cout << ArrayList[front] << "가 Dequeue되었습니다" << endl;
}

int CArrayListForQueue::NextPosIdx(int pos)
{
    if (pos == MAX_QUEUE_NUM - 1)
        return 0;
    else
        return pos + 1;
}
