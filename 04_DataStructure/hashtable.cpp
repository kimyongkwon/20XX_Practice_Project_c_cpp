//18.7.25

#include <cstdio>
#include <stdlib.h>

typedef int KeyType;
typedef int ValueType;

typedef struct tagNode
{
	KeyType		Key;
	ValueType	Value;
}Node;

typedef struct tagHashTable
{
	int		TableSize;
	Node*	Table;
}HashTable;

HashTable* SHT_CreateHashTable(int TableSize)
{
	HashTable* HT = (HashTable*)malloc(sizeof(HashTable));
	HT->Table = (Node*)malloc(sizeof(Node)*TableSize);
	HT->TableSize = TableSize;

	return HT;
}

int SHT_Hash(KeyType Key, int TableSize)
{
	return Key % TableSize;
}

void SHT_Set(HashTable* HT, KeyType Key, ValueType Value)
{
	// Key를 사용해서 주소값을 구한다.
	int Address = SHT_Hash(Key, HT->TableSize);

	// 해당 테이블 주소값에 Key와 Value값을 넣는다.
	HT->Table[Address].Key = Key;
	HT->Table[Address].Value = Value;
}

ValueType SHT_Get(HashTable* HT, KeyType Key)
{
	// Key값을 통해서 주소를 반환하고
	int Address = SHT_Hash(Key, HT->TableSize);

	// 테이블 그 주소에 접근해서 Value를 return한다.
	return HT->Table[Address].Value;
}

void SHT_DestroyHashTable(HashTable* HT)
{
	free(HT->Table);
	free(HT);
}

int main()
{
	// 193크기를 갖는 해쉬테이블을 만든다.
	HashTable * HT = SHT_CreateHashTable( 193 );

	SHT_Set( HT, 418, 32114 );
	SHT_Set( HT, 9, 514 );
	SHT_Set( HT, 27, 8917 );
	SHT_Set( HT, 1031, 286 );

	printf("Key : %d, value : %d \n", 418, SHT_Get(HT, 418));
	printf("Key : %d, value : %d \n", 9, SHT_Get(HT, 9));
	printf("Key : %d, value : %d \n", 27, SHT_Get(HT, 27));
	printf("Key : %d, value : %d \n", 1031, SHT_Get(HT, 1031));

	SHT_DestroyHashTable(HT);
	return 0;
}