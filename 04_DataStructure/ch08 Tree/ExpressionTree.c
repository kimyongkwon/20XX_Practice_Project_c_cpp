#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "ListBaseStack.h"
#include "BinaryTree2.h"

//BTreeNode * MakeExpTree(char exp[])
//{
//	Stack stack;
//	BTreeNode * pnode;
//
//	int expLen = strlen(exp);
//	int i;
//
//	StackInit(&stack);
//
//	for(i=0; i<expLen; i++)
//	{
//		pnode = MakeBTreeNode();
//
//		if(isdigit(exp[i]))		// 피연산자라면...
//		{
//			SetData(pnode, exp[i]-'0');
//		}
//		else					// 연산자라면...
//		{
//			MakeRightSubTree(pnode, SPop(&stack));
//			MakeLeftSubTree(pnode, SPop(&stack));
//			SetData(pnode, exp[i]);
//		}
//
//		SPush(&stack, pnode);
//	}
//
//	return SPop(&stack);
//}
//
//int EvaluateExpTree(BTreeNode * bt)
//{
//	int op1, op2;
//
//	if(GetLeftSubTree(bt)==NULL && GetRightSubTree(bt)==NULL)
//		return GetData(bt);
//
//	op1 = EvaluateExpTree(GetLeftSubTree(bt));
//	op2 = EvaluateExpTree(GetRightSubTree(bt));
//
//	switch(GetData(bt))
//	{
//	case '+':
//		return op1+op2;
//	case '-':
//		return op1-op2;
//	case '*':
//		return op1*op2;
//	case '/':
//		return op1/op2;
//	}
//
//	return 0;
//}
//
//void ShowNodeData(int data)
//{
//	if(0<=data && data<=9)
//		printf("%d ", data);
//	else
//		printf("%c ", data);
//}
//
//void ShowPrefixTypeExp(BTreeNode * bt)
//{
//	PreorderTraverse(bt, ShowNodeData);
//}
//
//void ShowInfixTypeExp(BTreeNode * bt)
//{
//	InorderTraverse(bt, ShowNodeData);
//} 
//
//void ShowPostfixTypeExp(BTreeNode * bt)
//{
//	PostorderTraverse(bt, ShowNodeData);
//}


BTreeNode * MakeExpTree(char exp[])
{
    int len = strlen(exp);
    BTreeNode * bt;
    Stack stack;

    StackInit(&stack);

    int i;
    for ( i = 0 ; i < len; i++)
    {
        //for문이 돌때마다 BTreeNode를 하나씩 동적할당한다.
        bt = MakeBTreeNode();
        if (isdigit(exp[i])) //피연산자라면
        {
            SetData(bt, exp[i]);
        }
        else
        {
            MakeRightSubTree(bt, SPop(&stack));
            MakeLeftSubTree(bt,SPop(&stack));
            SetData(bt, exp[i]);
        }
        SPush(&stack, bt);
    }

    
    //return bt; //이렇게 해도 상관은 없지만 이러면 스택에 아직도 수식이 남아있을것이다.
   
    return SPop(&stack); //위와 같은 문제가 없으려면 이렇게 해야함
}

void ShowNodeData(int data)
{
    if (0 <= data && data <= 9)
        printf("%d ", data);
    else
        printf("%c ", data);
}

void ShowPrefixTypeExp(BTreeNode * bt)
{
    PreorderTraverse(bt, ShowNodeData);
}

void ShowInfixTypeExp(BTreeNode * bt)
{
    if (bt == NULL)
        return;

    if (bt->left != NULL || bt->right != NULL)
        printf(" ( ");

    ShowInfixTypeExp(bt->left);
    ShowNodeData(bt->data);
    ShowInfixTypeExp(bt->right);

    if (bt->left != NULL || bt->right != NULL)
        printf(" ) ");
    
    //InorderTraverse(bt, ShowNodeData);
}

void ShowPostfixTypeExp(BTreeNode * bt)
{
    PostorderTraverse(bt, ShowNodeData);
}

int EvaluateExpTree(BTreeNode * bt)
{
}