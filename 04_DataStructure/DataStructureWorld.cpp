#include "stdafx.h"
#include "DataStructure.h"
#include "Recursion.h"
#include "ArrayListPart.h"
#include "LinkedListPart.h"
#include "SortPart.h"

class CDataStructureWorld
{
    CDataStructure * DataStructure;
public:
    CDataStructureWorld();
    ~CDataStructureWorld();
    bool Update(void);
};

CDataStructureWorld::CDataStructureWorld()
{
    DataStructure = NULL;
};

CDataStructureWorld::~CDataStructureWorld()
{
    //소멸자에서 메모리 해제를 안해줘도 됨
    //왜냐? Update함수에서 메모리 해제해주기때문에 소멸자에서 해제해버리면 런타임 오류 발생

    //if (DataStructure) delete DataStructure;
}

bool CDataStructureWorld::Update(void)
{
    while (1) 
    {
        system("cls");

        cout << "★ 자료구조세상에 오신걸 환영한다 ★" << endl;
        cout << "1. 재귀함수" << endl;
        cout << "2. 배열리스트" << endl;
        cout << "3. 연결리스트" << endl;
        cout << "4. 스택(연결리스트기반)" << endl;
        cout << "5. 큐(배열기반)" << endl;
        cout << "8. 정렬" << endl;
        cout << "0. 프로그램 종료" << endl;

        if (DataStructure) delete DataStructure;

        uint8_t cmd;

        cin >> cmd;

        switch (cmd)
        {
        case '1':
            system("cls");
            cout << "재귀함수를 선택하였습니다." << endl;
            DataStructure = new CRecursion;
            DataStructure->Update();
            
            break;
        case '2':
            system("cls");
            cout << "배열 리스트" << endl;
            DataStructure = new CArrayListPart;
            DataStructure->Update();
            break;
        case '3':
            system("cls");
            cout << "연결 리스트" << endl;
            DataStructure = new CDBLinkedListPart;
            DataStructure->Update();
            break;
        case '4':
            system("cls");
            cout << "스택(연결리스트기반)" << endl;
            DataStructure = new CStackPart;
            DataStructure->Update();
            break;
        case '5':
            system("cls");
            cout << "큐(배열기반)" << endl;
            DataStructure = new CCircularQueuePart;
            DataStructure->Update();
            break;
        case '8':
            system("cls");
            cout << "정렬" << endl;
            DataStructure = new CSortPart;
            DataStructure->Update();
            break;
        case '0':
            system("cls");
            cout << "다음에 또봐요~~~" << endl;
            return true;
        }
    }

    return 0;
}

int main()
{
    CDataStructureWorld DS;

    DS.Update();

    return 0;
};