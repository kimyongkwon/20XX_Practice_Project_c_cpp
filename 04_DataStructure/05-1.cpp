#include <stdio.h>
#include <stdlib.h>
#include <string>

//#define NOT_DUMMY_NODE

#ifdef NOT_DUMMY_NODE

typedef struct employee
{
    char * name;
    int num;
    int dutyNum;//당직 넘버
}Emp;

struct Node
{
    Emp data;
    struct Node * next;
};

typedef struct CircularList
{
    Node * cur;
    Node * before;
    Node * tail;
    int numOfData;
}list;

void ListInit (list &list)
{
    list.before = NULL;
    list.cur = NULL;
    list.numOfData = 0;
    list.tail = NULL;
}

void Insert(list &list, Emp &emp)
{
    (list.numOfData)++;

    Node * newNode = (Node*)malloc(sizeof(Node));
    newNode->data = emp;
    newNode->data.dutyNum = list.numOfData;

    if ( list.tail == NULL)
    {
        list.tail = newNode;
        newNode->next = list.tail;
    }
    else 
    {
        //틀린답(븅신...)
        /*list.tail->next = newNode;
        newNode->next = list.tail;
        list.tail = list.tail->next;*/

        newNode->next = list.tail->next;
        list.tail->next = newNode;
        list.tail = list.tail->next;
    }
}

void FrontInsert(list &list, Emp &emp)
{
    (list.numOfData)++;

    Node * newNode = (Node*)malloc(sizeof(Node));
    newNode->data = emp;
    newNode->data.dutyNum = list.numOfData;

    newNode->next = list.tail->next->next;
    list.tail->next->next = newNode;
}

void Count(list &list)
{
    printf("%d \n", list.numOfData);
}

int LFirst(list * plist, Emp * data)
{
    //데이터가 하나도 저장되지않았으면 false 리턴
    if (plist->tail == NULL)
        return false;

    plist->before = plist->tail;
    //cur포인터는 더미노드 다음 노드를 가리킨다. 
    plist->cur = plist->tail->next;

    *data = plist->cur->data;

    return true;
}

int LNext(list * plist, Emp * data)
{
    //데이터가 하나도 저장되지않았으면 false 리턴
    if (plist->tail->next == NULL) return false;

    plist->before = plist->cur;
    //cur포인터를 다음칸으로 옮긴다.
    plist->cur = plist->cur->next;

    *data = plist->cur->data;
    return true;
}

Emp * CheckDuty(list &list, Emp &emp, int num)
{
    Emp * tmpEmp = (Emp*)malloc(sizeof(Emp));
    int i;

    LFirst(&list, tmpEmp);

    if (strcmp(tmpEmp->name, emp.name) != 0)
    {
        for ( i = 0 ; i < 4-1; i++)
        {
            LNext(&list, tmpEmp);

            if (strcmp(tmpEmp->name, emp.name) == 0)
                break;
        }
        if (i >= 4 - 1)
            return NULL;
    }

    for (i = 0; i < num; i++)
        LNext(&list, tmpEmp);

    return tmpEmp;
}

int main()
{
    list list;
    Emp * tmp;
    ListInit(list);

    Emp emp1 = { "k", 00 };
    Emp emp2 = { "h", 01 };
    Emp emp3 = { "j", 10 };
    Emp emp4 = { "m", 11 };

    Insert(list, emp1);
    Insert(list, emp2);
    Insert(list, emp3);
    Insert(list, emp4);


    printf("현재 emp의 수 : "); Count(list);

    tmp = CheckDuty(list, emp1, 4);
    printf("Emp Name: %s", tmp->name);
}


#else

typedef struct employee
{
    char * name;
    int num;
    int dutyNum;//당직 넘버
}Emp;

struct Node
{
    Emp data;
    struct Node * next;
};

typedef struct CircularList
{
    Node * cur;
    Node * before;
    Node * tail;
    int numOfData;
}list;

void ListInit(list &list)
{
    list.before = NULL;
    list.cur = NULL;
    list.numOfData = 0;
    list.tail = (Node*)malloc(sizeof(Node));
    list.tail->next = NULL;
}

void Insert(list &list, Emp &emp)
{
    (list.numOfData)++;

    Node * newNode = (Node*)malloc(sizeof(Node));
    newNode->data = emp;
    newNode->next = NULL;
    newNode->data.dutyNum = list.numOfData;

    if ( list.tail->next== NULL)
    {
        list.tail->next = newNode;
        list.tail->next->next = newNode;
    }
    else
    {
        newNode->next = list.tail->next->next;
        list.tail->next->next = newNode;
        list.tail->next = list.tail->next->next;
    }
}

void FrontInsert(list &list, Emp &emp)
{
    (list.numOfData)++;

    Node * newNode = (Node*)malloc(sizeof(Node));
    newNode->data = emp;
    newNode->data.dutyNum = list.numOfData;

    newNode->next = list.tail->next->next;
    list.tail->next->next = newNode;
}

void Count(list &list)
{
    printf("%d \n", list.numOfData);
}

int LFirst(list * plist, Emp * data)
{
    //데이터가 하나도 저장되지않았으면 false 리턴
    if (plist->tail == NULL)
        return false;

    plist->before = plist->tail;
    //cur포인터는 더미노드 다음 노드를 가리킨다. 
    plist->cur = plist->tail->next;

    *data = plist->cur->data;

    return true;
}

int LNext(list * plist, Emp * data)
{
    //데이터가 하나도 저장되지않았으면 false 리턴
    if (plist->tail->next == NULL) return false;

    plist->before = plist->cur;
    //cur포인터를 다음칸으로 옮긴다.
    plist->cur = plist->cur->next;

    *data = plist->cur->data;
    return true;
}

Emp * CheckDuty(list &list, Emp &emp, int num)
{
    Emp * tmpEmp = (Emp*)malloc(sizeof(Emp));
    int i;

    LFirst(&list, tmpEmp);

    if (strcmp(tmpEmp->name, emp.name) != 0)
    {
        for (i = 0; i < 4 - 1; i++)
        {
            LNext(&list, tmpEmp);

            if (strcmp(tmpEmp->name, emp.name) == 0)
                break;
        }
        if (i >= 4 - 1)
            return NULL;
    }

    for (i = 0; i < num; i++)
        LNext(&list, tmpEmp);

    return tmpEmp;
}

int main()
{
    list list;
    Emp * tmp;
    ListInit(list);

    Emp emp1 = { "k", 00 };
    Emp emp2 = { "h", 01 };
    Emp emp3 = { "j", 10 };
    Emp emp4 = { "m", 11 };

    Insert(list, emp1);
    Insert(list, emp2);
    Insert(list, emp3);
    Insert(list, emp4);

    printf("현재 emp의 수 : "); Count(list);

    tmp = CheckDuty(list, emp1, 2);
    printf("Emp Name: %s", tmp->name);
}

#endif