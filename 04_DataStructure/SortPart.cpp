#include "SortPart.h"

CSort::CSort()
{
    srand((unsigned)time(NULL));

    for (int i = 0; i < MAX_SORT_DATA; i++)
        m_arr[i] = rand() % MAX_SORT_DATA + 1;
}

CSort::~CSort()
{
}

void CSort::Init(void)
{

}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//버블 정렬
CBubbleSort::CBubbleSort() : CSort()
{

}

CBubbleSort::~CBubbleSort()
{
}

void CBubbleSort::Sort(void)
{
    auto start = chrono::high_resolution_clock::now();
    for (int j = 0; j < MAX_SORT_DATA; j++)
    {
        // 배열의 i번째까지 돌면서 가장 큰 수를 맨 뒤로 보낸다.
        // ((-1을 한 이유)) 예를 들어 5개의 수를 정렬한다고 하면 4번의 비교연산을 하기 때문이다.
        // ((j를 빼는 이유)) 한 번의 루프에서 가장 큰수를 오른쪽으로 보내면 그 자리에 있는 데이터는
        // 이제 더이상 정렬대상이 아니기 때문에 그 배열 자리를 빼고 정렬을 수행해야한다.
        for (int i = 0; i < MAX_SORT_DATA - 1 - j; i++)
        {
            // 루프를 돌면서 인접한 두 개의 값을 비교한다.
            // 큰 수가 뒤로 가게 한다.
            if ( m_arr[i] > m_arr[i + 1])
            {
                int tmp = m_arr[i];
                m_arr[i] = m_arr[i + 1];
                m_arr[i + 1] = tmp;
            }
        }
    }
    auto end = chrono::high_resolution_clock::now();

    auto msec = chrono::duration_cast<chrono::duration<double>>(end - start);
    cout << MAX_SORT_DATA << "개의 데이터를 정렬하는데 걸린시간은 " << msec.count() << "초 입니다" << endl;
}



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//선택 정렬 
CSelectionSort::CSelectionSort()
{
}

CSelectionSort::~CSelectionSort()
{
}

void CSelectionSort::Sort(void)
{
    //for (int i = 0; i < MAX_SORT_DATA; i++)
    //    cout << m_arr[i] << " ";
    //cout << endl;

    ////((step1)) 배열에서 가장 작은 값(우선순위가 높은)의 인덱스를 저장한다.
    //int maxidx = 0;
    //for ( int i = 0; i < MAX_SORT_DATA - 1; i++)
    //{
    //    if (m_arr[maxidx] > m_arr[i+1])
    //    {
    //        maxidx = i + 1;
    //    }
    //}

    ////((step2)) 가장 작은 값(우선순위가 높은)의 값을 배열의 첫번째 요소와 스왑한다.
    //int maxidx = 0;
    //for (int i = 0; i < MAX_SORT_DATA - 1; i++)
    //{
    //    if (m_arr[maxidx] > m_arr[i + 1])
    //    {
    //        maxidx = i + 1;
    //    }
    //}
    ////
    //int tmp = m_arr[maxidx];
    //m_arr[maxidx] = m_arr[0];
    //m_arr[0] = tmp;

    //((step3)) 배열의 첫번째요소가 가장 작은값으로 갱신되었으므로 
    //두번째 작은 값을 찾아서 배열의 두번째 요소에 갱신한다. ... 이방법을 통해 배열 끝까지 루프 돈다.
    auto start = chrono::high_resolution_clock::now();
    for (int j = 0; j < MAX_SORT_DATA; j++)
    {
        int maxidx = j;
        for (int i = j; i < MAX_SORT_DATA - 1; i++)
        {
            if ( m_arr[maxidx] > m_arr[i + 1])
            {
                maxidx = i + 1;
            }
        }
        int tmp = m_arr[maxidx];
        m_arr[maxidx] = m_arr[j];
        m_arr[j] = tmp;
    }
    auto end = chrono::high_resolution_clock::now();

    auto msec = chrono::duration_cast<chrono::duration<double>>(end - start);
    cout << MAX_SORT_DATA << "개의 데이터를 정렬하는데 걸린시간은 " << msec.count() << "초 입니다" << endl;

    //for (int i = 0; i < MAX_SORT_DATA; i++)
    //    cout << m_arr[i] << " ";
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//삽입 정렬 
CInsertionSort::CInsertionSort() : CSort()
{
}

CInsertionSort::~CInsertionSort()
{
}

void CInsertionSort::Sort(void)
{
    /*for (int i = 0; i < MAX_SORT_DATA; i++)
        cout << m_arr[i] << " ";
    cout << endl;*/

    auto start = chrono::high_resolution_clock::now();
    //((step1)) 비교하면서 자리를 찾아야할 배열의 원소를 저장한다.
    for ( int i = 1; i < MAX_SORT_DATA; i++ )
    {
        int tmp = m_arr[i];
        int j;
        //((step2)) i자리 전(= i-1)부터 앞으로 루프를 돌면서 tmp와 대소 비교를 한다.
        for (j = i-1; j >= 0; j--)
        {
            // 만약 tmp보다 i의 자리의 값이 크다면 i자리 값을 뒤로 땡긴다
            if ( tmp < m_arr[j] )
            {
                m_arr[j + 1] = m_arr[j];
            }
            // 만약 tmp보다 i의 자리의 값이 크지 않다면(tmp와 값이 같거나 크다는 말이다)
            // 그 땡기지 않아도 된다. 그렇기 떄문에 break문을 빠져나온다.
            // Q : 그 앞에 원소들이랑도 비교해야하는거 아니냐? 지금 루프도는 구간은 이미 정렬이 다 되어있으므로
            // 그 앞의 원소들은 비교하지 않아도 된다.
            else
                break;
        }
        //((step3))
        //i자리는 뒤로 땡겨야하는지 말아야하는지 비교하는 자리였다. 그러므로 i자리에 tmp를 대입하면 안됀다.
        //i + 1에 tmp를 대입해야한다.
        m_arr[j + 1] = tmp;
    }
    auto end = chrono::high_resolution_clock::now();

    auto msec = chrono::duration_cast<chrono::duration<double>>(end - start);
    cout << MAX_SORT_DATA << "개의 데이터를 정렬하는데 걸린시간은 " << msec.count() << "초 입니다" << endl;
     
    /*for (int k = 0; k < MAX_SORT_DATA; k++)
        cout << m_arr[k] << " ";*/
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//힙 정렬 
CHeapSort::CHeapSort()
{
    m_nNum = 0;
}

CHeapSort::~CHeapSort()
{
}

int GetParentIDX(int childIDX)
{
    return childIDX / 2;
}

int GetLChildIDX(int parentIDX)
{
    return parentIDX * 2;
}

int GetRChildIDX(int parentIDX)
{
    return (parentIDX * 2) + 1;
}

int CHeapSort::GetHIPriChildIDX(int parentIDX)
{
    //((기억해야할것))
    // - 힙은 완전이진트리다.
    // - 마지막 노드의 인덱스는 힙이 가지고 있는 데이터의 갯수와 일치한다.
   
    //case1
    //왼쪽 자식노드의 인덱스가 지금 전체데이터의 갯수보다 크다는 의미는 자식노드가 없다는 것이다.
    //왜냐하면 전체데이터의 갯수는 힙의 최대 인덱스와 같기 떄문이다. 
    if ( GetLChildIDX(parentIDX) > m_nNum )
        return 0;

    //case2
    //왼쪽 자식노드의 인덱스가 지금 전체 데이터의 갯수와 같다는 의미는 현재 노드는 왼쪽 자식노드만 가지고 있다는 것이다.
    else if ( GetLChildIDX(parentIDX) == m_nNum )
        //그러므로 왼쪽 자식노드의 인덱스를 반환한다.
        return GetLChildIDX(parentIDX);

    //case3
    //case 1,2도 아니면 현재 노드는 자식노드를 둘다 가지고 있다는 의미다.
    else
    {
        //오른쪽 자식 노드의 값이 왼쪽 자식노드의 값보다 더 크면 왼쪽 노드의 인덱스를 반환한다.
        if ( m_arrHeap[GetLChildIDX(parentIDX)] < m_arrHeap[GetRChildIDX(parentIDX)] )
            return GetLChildIDX(parentIDX);
        //그렇지 않으면 오른쪽 노드의 인덱스를 반환한다.
        else
            return GetRChildIDX(parentIDX);       
    }
}

void CHeapSort::InsertForSort(int data)
{
    //최소 힙으로 만든다. 가장 작은 값이 최상위 노드에 있다.

    //1을 더하는 이유는 배열에서 1번째 인덱스부터 값을 정렬하기 때문이다.
    int idx = m_nNum + 1;

    while( idx != 1 )
    {
        //만약 현재 값보다 부모 노드의 값이 더 크다면 바꿔야한다.
        if ( m_arrHeap[GetParentIDX(idx)] > data )
        {
            //부모노드의 값을 idx인덱스에 값을 복사한다.
            m_arrHeap[idx] = m_arrHeap[GetParentIDX(idx)];
            //현재 인덱스를 부모 인덱스로 올린다.
            idx = GetParentIDX(idx);
        }
        //만약 현재 값보다 부모 노드의 값이 같거나 작다면 break로 while문을 빠져나간다.
        else
            break;
        
    }

    //현재 값이 있어야할 자리에 data를 복사한다.
    m_arrHeap[idx] = data;
    m_nNum += 1;
}

int CHeapSort::DeleteForSort(void)
{
    //힙에서 가장 최상위에 있는 노드(가장 값이 높은 수)를 복사한다.
    int tmpData = m_arrHeap[1];

    //힙(배열)에서 가장 우선순위가 낮은 데이터를 가져온다. 
    int lastData = m_arrHeap[m_nNum];

    //이제부터 우선순위가 가장 데이터를 힙의 알맞은 자리에 넣어주는 작업을 시작한다.
    //맨 위의 노드부터 탐색해야하기 때문에 parent는 1로 시작한다.
    int parentIDX = 1;
    int childIDX;

    //child가 0이 반환될때까지 while문을 순회한다.
    //chile가 0이라는 뜻은 현재 부모노드의 자식노드가 하나도 없다는 의미다.
    while ( childIDX = GetHIPriChildIDX(parentIDX))
    {
        //마지막 노드 값이 자식노드의 값보다 더 작으면 while문을 빠져나간다.
        //마지막 노드의 자리가 정해졌기떄문이다.
        if ( lastData < m_arrHeap[childIDX] )
            break;

        //그렇지 않으면 마지막 노드 값이 자식노드의 값보다 크다는 의미므로
        //부모노드의 값을 자식노드에 복사한다.
        m_arrHeap[parentIDX] = m_arrHeap[childIDX];
        parentIDX = childIDX;
    }

    //현재 노드 자리(parentIDX)에 마지막 노드를 삽입한다.
    m_arrHeap[parentIDX] = lastData;
    m_nNum -= 1;
    return tmpData;
}

void CHeapSort::Sort(void)
{
    /*for ( int k = 0; k < MAX_SORT_DATA; k++ )
        cout << m_arr[k] << " ";*/

    auto start = chrono::high_resolution_clock::now();

    //m_arr를 정렬해서 m_arrHeap에 넣는다.
    for ( int i = 0; i < MAX_SORT_DATA; i++ )
        InsertForSort(m_arr[i]);

    //m_arrHeap에 있는 노드들을 부모노드부터 하나씩 제거해가면서(가장 우선순위가 높기때문에)
    //m_arr배열에 차례대로 복사한다.
    for ( int i = 1; i < MAX_SORT_DATA + 1; i++ )
        m_arr[i - 1] = DeleteForSort();

    auto end = chrono::high_resolution_clock::now();

    auto msec = chrono::duration_cast<chrono::duration<double>>(end - start);
    cout << MAX_SORT_DATA << "개의 데이터를 정렬하는데 걸린시간은 " << msec.count() << "초 입니다" << endl;

    /*for ( int j = 0; j < MAX_SORT_DATA; j++ )
        cout << m_arr[j] << " ";*/
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//병합 정렬
CMergeSort::CMergeSort()
{
    m_nLeft = 0;
    m_nRight = MAX_SORT_DATA - 1;
}

CMergeSort::~CMergeSort()
{
}

void CMergeSort::Sort(void)
{
    auto start = chrono::high_resolution_clock::now();
    Divide(m_nLeft, m_nRight);
    auto end = chrono::high_resolution_clock::now();
    auto msec = chrono::duration_cast<chrono::duration<double>>(end - start);
    cout << MAX_SORT_DATA << "개의 데이터를 정렬하는데 걸린시간은 " << msec.count() << "초 입니다" << endl;

    //cout << "끝" << endl;

    /*for ( int i = 0; i < MAX_SORT_DATA; i++)
        cout << m_arr[i] << " ";*/
}

void CMergeSort::Divide(int left, int right)
{
    //cout << "left : " << left << " right: " << right << endl;

    if(left < right)
    {
        int mid = (left + right) / 2;
        Divide(left, mid);
        Divide(mid+1, right);

        Combine(left, mid, right);
    }
}

void CMergeSort::Combine(int left, int mid, int right)
{
    int LAreaIdx = left;                // 왼쪽 영역 인덱스
    int RAreaIdx = mid + 1;             // 오른쪽 영역 인덱스
    int i;

    //SortTmp(임시메모리)에 정렬된 데이터들을 저장한다.
    int * SortTmp = (int*)malloc(sizeof(int)*(right + 1));
    int SIdx = left;

    //양쪽영역을 계속 비교해나가면서 우선순위가 높은 값을 SortTmp에 저장
    while ( LAreaIdx <= mid && RAreaIdx <= right)
    {
        if ( m_arr[LAreaIdx] <= m_arr[RAreaIdx] )
            SortTmp[SIdx] = m_arr[LAreaIdx++];
        else
            SortTmp[SIdx] = m_arr[RAreaIdx++];

        SIdx++;
    }

    //왼쪽영역 인덱스가 왼쪽영역을 넘어서면
    if ( LAreaIdx > mid )
    {
        //오른쪽영역에 남은 데이터들을 차례대로 SortTmp에 저장
        for ( i = RAreaIdx; i <= right; i++, SIdx++ )
            SortTmp[SIdx] = m_arr[i];
    }
    //오른쪽영역 인덱스가 오른쪽영역을 넘어서면
    else 
    {
        //왼쪽영역에 남은 데이터들을 차례대로 SortTmp에 저장
        for ( i = LAreaIdx; i <= mid; i++, SIdx++ )
            SortTmp[SIdx] = m_arr[i];
    }

    //정렬된 배열을 원래 배열에 복사
    for ( i = left; i <= right; i++ )
        m_arr[i] = SortTmp[i];
    
    free(SortTmp);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//퀵 정렬 
CQuickSort::CQuickSort()
{
    m_nLeft = 0;
    m_nRight = MAX_SORT_DATA - 1;
}

CQuickSort::~CQuickSort()
{
}

void CQuickSort::Sort(void)
{
    auto start = chrono::high_resolution_clock::now();
    QuickSort(m_nLeft, m_nRight);
    auto end = chrono::high_resolution_clock::now();
    auto msec = chrono::duration_cast<chrono::duration<double>>(end - start);
    cout << MAX_SORT_DATA << "개의 데이터를 정렬하는데 걸린시간은 " << msec.count() << "초 입니다" << endl;
}

void CQuickSort::QuickSort(int left, int right)
{
    if (left <= right)
    {
        int pivot = Partition(left, right);
        QuickSort(left, pivot - 1);
        QuickSort(pivot + 1, right);
    }
}

int CQuickSort::Partition(int left, int right)
{
    int low = left + 1;
    int high = right;
    int pivot = left;

    //로우가 하이보다 작거나 같을때까지 루프가 계속된다.
    //로우가 하이를 넘어서야 while문을 빠져나온다.
    while ( low <= high )
    {
        //피봇보다 큰 값이 나올때까지 루프를 돌면서 low를 오른쪽으로 이동
        while( (m_arr[pivot] >= m_arr[low]) && low <= right )
            low++;
        //피봇보다 작은 값이 나올떄까지 루프를 돌면서 right를 왼쪽으로 이동
        while( (m_arr[pivot] < m_arr[high]) && high >= (left+1) )
            high--;

        //로우와 하이의 자리를 바꾼다.
        if (low <= high)
            Swap(low, high);
    }

    //레프트와 하이를 바꾸고
    Swap(left, high);
    //하이를 리턴한다. 함수를 벗어나면 하이는 피벗으로 사용된다.
    return high;
}

void CQuickSort::Swap(int low, int high)
{
    int tmp = m_arr[low];
    m_arr[low] = m_arr[high];
    m_arr[high] = tmp;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//소트 파트 
CSortPart::CSortPart()
{
}

CSortPart::~CSortPart()
{
}

void const CSortPart::ShowCommand()
{
    cout << "어떤 정렬을 실행하시겠습니까?" << endl;
    cout << "1. 버블정렬" << endl;
    cout << "2. 선택정렬" << endl;
    cout << "3. 삽입정렬" << endl;
    cout << "4. 힙정렬" << endl;
    cout << "5. 병합정렬" << endl;
    cout << "6. 퀵정렬" << endl;
    cout << "0. 뒤로가기" << endl;
}

bool CSortPart::Update(void)
{
    while (1)
    {
        ShowCommand();
        int cmd;
        cin >> cmd;
        switch (cmd)
        {
        case 1:
        {
            system("cls");
            m_pSort = new CBubbleSort();
            m_pSort->Sort();
            break;
        }
        case 2:
        {
            system("cls");
            m_pSort = new CSelectionSort();
            m_pSort->Sort();
            break;
        }
        case 3:
        {
            system("cls");
            m_pSort = new CInsertionSort();
            m_pSort->Sort();
            break;
        }
        case 4:
        {
            system("cls");
            m_pSort = new CHeapSort();
            m_pSort->Sort();
            break;
        }
        case 5:
        {
            system("cls");
            m_pSort = new CMergeSort();
            m_pSort->Sort();
            break;
        }
        case 6:
        {
            system("cls");
            m_pSort = new CQuickSort();
            m_pSort->Sort();
            break;
        }
        case 0:
        {
            return 0;
        }
        default:
            break;
        }
        if ( m_pSort ) delete m_pSort;
    }
    return 0;
}

