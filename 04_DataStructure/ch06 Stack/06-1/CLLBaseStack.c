#include <stdio.h>
#include <stdlib.h>
#include "CLinkedList.h"
#include "CLLBaseStack.h"

void StackInit(Stack * pstack)
{
    pstack->list = (List*)malloc(sizeof(List));
    ListInit(pstack->list);
}

void SPush(Stack * pstack, Data data)
{
    LInsertFront(pstack->list, data);
}

int SIsEmpty(Stack * pstack)
{
    if (LCount(pstack->list) == 0)
        return TRUE;
    return FALSE;
}

Data SPop(Stack * pstack)
{
    Data data;
    
    //계속 LFirst를 호출해도 되는 이유는
    //LFirst함수가 호출될때마다 cur포인터는 tail의 next노드를 가리키고
    //LRemove에서 cur포인터를 삭제해주기때문에
    LFirst(pstack->list, &data);
    LRemove(pstack->list);
    return data;
}