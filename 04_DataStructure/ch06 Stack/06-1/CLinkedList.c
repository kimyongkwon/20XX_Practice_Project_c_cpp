#include <stdio.h>
#include <stdlib.h>
#include "CLinkedList.h"

//void ListInit(List * plist)
//{
//	plist->tail = NULL;
//	plist->cur = NULL;
//	plist->before = NULL;
//	plist->numOfData = 0;
//}
//
//void LInsertFront(List * plist, Data data)
//{
//	Node * newNode = (Node*)malloc(sizeof(Node));
//	newNode->data = data;
//
//	if(plist->tail == NULL) 
//	{
//		plist->tail = newNode;
//		newNode->next = newNode;
//	}
//	else
//	{
//		newNode->next = plist->tail->next;
//		plist->tail->next = newNode;
//	}
//
//	(plist->numOfData)++;
//}
//
//void LInsert(List * plist, Data data)
//{
//	Node * newNode = (Node*)malloc(sizeof(Node));
//	newNode->data = data;
//
//	if(plist->tail == NULL) 
//	{
//		plist->tail = newNode;
//		newNode->next = newNode;
//	}
//	else 
//	{
//		newNode->next = plist->tail->next;
//		plist->tail->next = newNode;
//		plist->tail = newNode;
//	}
//
//	(plist->numOfData)++;
//}
//
//int LFirst(List * plist, Data * pdata)
//{
//	if(plist->tail == NULL)    // 저장된 노드가 없다면
//		return FALSE;
//
//	plist->before = plist->tail;
//	plist->cur = plist->tail->next;
//
//	*pdata = plist->cur->data;
//	return TRUE;
//}
//
//int LNext(List * plist, Data * pdata)
//{
//	if(plist->tail == NULL)    // 저장된 노드가 없다면
//		return FALSE;
//
//	plist->before = plist->cur;
//	plist->cur = plist->cur->next;
//
//	*pdata = plist->cur->data;
//	return TRUE;
//}
//
//Data LRemove(List * plist)
//{
//	Node * rpos = plist->cur;
//	Data rdata = rpos->data;
//
//	if(rpos == plist->tail)    // 삭제 대상을 tail이 가리킨다면
//	{
//		if(plist->tail == plist->tail->next)    // 그리고 마지막 남은 노드라면
//			plist->tail = NULL;
//		else
//			plist->tail = plist->before;
//	}
//
//	plist->before->next = plist->cur->next;
//	plist->cur = plist->before;
//
//	free(rpos);
//	(plist->numOfData)--;
//	return rdata;
//}
//
//int LCount(List * plist)
//{
//	return plist->numOfData;
//}

void ListInit(List * plist)
{
    plist->tail = NULL;
    plist->cur = NULL;
    plist->before = NULL;
    plist->numOfData = 0;
}

//꼬리에 삽입
void LInsert(List * plist, Data data)
{
    Node * newNode = (Node*)malloc(sizeof(Node));
    newNode->data = data;

    if (plist->tail == NULL)
    {
        plist->tail = newNode;
        newNode->next = newNode;
    }
    else
    {
        //새로운 노드가 리스트 제일 앞에있는 노드를 연결한다.
        newNode->next = plist->tail->next;
        //현재 꼬리의 뒤에 새노드를 연결한다.
        plist->tail->next = newNode;
        //tail을 맨뒤에 있는(현재만들어진) 노드를 가리키게 한다.
        plist->tail = plist->tail->next;
    }

    (plist->numOfData)++;
}

//맨앞에 삽입
void LInsertFront(List * plist, Data data)
{
    Node * newNode = (Node*)malloc(sizeof(Node));
    newNode->data = data;

    if (plist->tail == NULL)
    {
        plist->tail = newNode;
        newNode->next = newNode;
    }
    else
    {
        //새로운 노드가 리스트 제일 앞에있는 노드를 연결한다.
        newNode->next = plist->tail->next;
        //현재 꼬리의 뒤에 새노드를 연결한다.
        plist->tail->next = newNode;
    }

    (plist->numOfData)++;
}

int LCount(List * plist)
{
    return plist->numOfData;
}

int LFirst(List * plist, Data * data)
{
    //데이터가 하나도 저장되지않았으면 false 리턴
    if (plist->tail == NULL)
        return FALSE;

    plist->before = plist->tail;
    //cur포인터는 더미노드 다음 노드를 가리킨다. 
    plist->cur = plist->tail->next;
    *data = plist->cur->data;
    return TRUE;
}

int LNext(List * plist, Data * data)
{
    plist->before = plist->cur;
    //cur포인터를 다음칸으로 옮긴다.
    plist->cur = plist->cur->next;
    *data = plist->cur->data;

    return TRUE;
}

Data LRemove(List * plist)
{
    ////방법1
    //LData tmp = plist->cur->data;
    //plist->before->next = plist->cur->next;
    //free(plist->cur);
    //plist->numOfData -= 1;
    //plist->cur = plist->before;
    //return tmp;

    //방법2
    Node * rpos = plist->cur;
    Data rdata = rpos->data;
    plist->before->next = plist->cur->next;
    plist->cur = plist->before;
    free(rpos);
    (plist->numOfData)--;
    return rdata;
}