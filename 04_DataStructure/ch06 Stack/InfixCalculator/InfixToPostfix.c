#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "../ListBase/ListBaseStack.h"

int GetOpPrec(char op)
{
	switch(op)
	{
	case '*':
	case '/':
		return 5;
	case '+':
	case '-':
		return 3;
	case '(':
		return 1;
	}

	return -1;   // 등록되지 않은 연산자
}

int WhoPrecOp(char op1, char op2)
{
	int op1Prec = GetOpPrec(op1);
	int op2Prec = GetOpPrec(op2);

	if(op1Prec > op2Prec)
		return 1;
	else if(op1Prec < op2Prec)
		return -1;
	else
		return 0;
}

//void ConvToRPNExp(char exp[])
//{
//	Stack stack;
//	int expLen = strlen(exp);
//	char * convExp = (char*)malloc(expLen+1);
//
//	int i, idx=0;
//	char tok, popOp;
//	
//	memset(convExp, 0, sizeof(char)*expLen+1);
//	StackInit(&stack);
//
//	for(i=0; i<expLen; i++)
//	{
//		tok = exp[i];
//		if(isdigit(tok))
//		{
//			convExp[idx++] = tok;
//		}
//		else
//		{
//			switch(tok)
//			{
//			case '(':
//				SPush(&stack, tok);
//				break;
//
//			case ')':
//				while(1)
//				{
//					popOp = SPop(&stack);
//					if(popOp == '(')
//						break;
//					convExp[idx++] = popOp;
//				}
//				break;
//
//			case '+': case '-': 
//			case '*': case '/':
//				while(!SIsEmpty(&stack) && 
//						WhoPrecOp(SPeek(&stack), tok) >= 0)
//					convExp[idx++] = SPop(&stack);
//
//				SPush(&stack, tok);
//				break;
//			}
//		}
//	}
//
//	while(!SIsEmpty(&stack))
//		convExp[idx++] = SPop(&stack);
//
//	strcpy(exp, convExp);
//	free(convExp);
//}

void ConvToRPNExp(char exp[])
{
    //연산자를 담을 쟁반은 stack
    Stack stack;
    
    //후기표기법으로 변환된 수식을 저장할 문자열 
    int len = strlen(exp);
    char * tmpExp = (char*)malloc(len+1);
    int tmpExpIdx = 0;

    memset(tmpExp, 0, sizeof(char)*len + 1);
    StackInit(&stack);

    for ( int i = 0; i < len; i++)
    {
        //isdigit은 숫자인지 판별하는 함수이다. 
        //여기서 숫자는 아스키코드 영역에서 숫자인지 판별한다는 것이다.
        if (isdigit(exp[i]))
        {
            //해당 숫자를 tmpExp에 쓴다.
            tmpExp[tmpExpIdx++] = exp[i];
        }
        //숫자가 아니면(피연산자가 아니므로 연산자일것이다)
        else
        {
            switch (exp[i])
            {
                //'('연산자가 나오면 스택(쟁반)에 새롭게 push한다. 
                //여기서 새롭다는 의미는 스택(쟁반)은 기존의 스택을 사용하지만 전에 
                //'('연산자가 push돼고 ')'연산자가 나오기전까지 사이에 push된 연산자들을
                //다른 연산자들과 구분하기 떄문에 새롭다는 표현을 사용하였다.
            case '(':
                SPush(&stack, exp[i]);
                break;
            case ')':
                //')'연산자가 나오면 스택의 '('연산자가 나올때까지 모든 연산자들을 pop한다.
                while(1)
                {
                    char popOp = SPop(&stack);
                    if (popOp == '(')
                        break;
                    tmpExp[tmpExpIdx++] = popOp;
                }
                break;
            case '+': case '-':
            case '*': case '/':
                //WhoPrecOp는 스택(쟁반)에 있었던 연산자(op1)를 하나 꺼내서 현재 연산자(op2)와 우선순위 비교하는 함수
                //return값이 1일 경우에는 스택에 있었던 연산자(op1)가 현재 연산자(op2)보다 우선순위가 높기때문에 
                //op1 연산자를 pop한다음에 tmpExp에 저장하고, op2 연산자를 스택에 push한다.
                //return값이 0일 경우에는 op1연산자와 op2연산자의 우선순위가 같다(이때는 op1연산자가 우선순위가 높다고 판단해야한다.)
                //위와 같이 op1 연산자를 pop한다음에 tmpExp에 저장하고, op2 연산자를 스택에 push한다.
                while (!SIsEmpty(&stack) && 
                    WhoPrecOp(SPeek(&stack), exp[i]) >= 0)
                    tmpExp[tmpExpIdx++] = SPop(&stack);
                //while문에 들어가지 않으면 스택에서 pop연산을 하지않고 바로 연산자를 push한다.
                //이 경우는 스택이 비어있는 경우다 (!SIsEmpty(&stack))
                SPush(&stack, exp[i]);
                break;
            }
        }
    }

    //exp문자열을 끝까지 다 조사하였으면
    //스택에 남아있는 연산자들을 모두 pop해서 tmpExp(새로운 문자열)에 저장한다. 
    while (!SIsEmpty(&stack))
        tmpExp[tmpExpIdx++] = SPop(&stack);

    strcpy(exp, tmpExp);
    free(tmpExp);
}