#include <stdio.h>
#include <stdlib.h>
#include "BinaryTree3.h"
#include "BinarySearchTree2.h"

void BSTMakeAndInit(BTreeNode ** pRoot)
{
	*pRoot = NULL;
}

BSTData BSTGetNodeData(BTreeNode * bst)
{
	return GetData(bst);
}

void BSTInsert(BTreeNode ** pRoot, BSTData data)
{
	BTreeNode * pNode = NULL;    // parent node
	BTreeNode * cNode = *pRoot;    // current node
	BTreeNode * nNode = NULL;    // new node

	// 새로운 노드가 추가될 위치를 찾는다.
	while(cNode != NULL)
	{
		if(data == GetData(cNode))
			return;    // 키의 중복을 허용하지 않음

		pNode = cNode;

		if(GetData(cNode) > data)
			cNode = GetLeftSubTree(cNode);
		else
			cNode = GetRightSubTree(cNode);
	}
	
	// pNode의 서브 노드에 추가할 새 노드의 생성
	nNode = MakeBTreeNode();    // 새 노드의 생성
	SetData(nNode, data);    // 새 노드에 데이터 저장

	// pNode의 서브 노드에 새 노드를 추가
	if(pNode != NULL)    // 새 노드가 루트 노드가 아니라면,
	{
		if(data < GetData(pNode))
			MakeLeftSubTree(pNode, nNode);
		else
			MakeRightSubTree(pNode, nNode);
	}
	else    // 새 노드가 루트 노드라면,
	{
		*pRoot = nNode;
	}
}

BTreeNode * BSTSearch(BTreeNode * bst, BSTData target)
{
	BTreeNode * cNode = bst;    // current node
	BSTData cd;    // current data

	while(cNode != NULL)
	{
		cd = GetData(cNode);

		if(target == cd)
			return cNode;
		else if(target < cd)
			cNode = GetLeftSubTree(cNode);
		else
			cNode = GetRightSubTree(cNode);
	}

	return NULL;
}

//BTreeNode * BSTRemove(BTreeNode ** pRoot, BSTData target)
//{
//	// 삭제 대상이 루트 노드인 경우를 별도로 고려해야 한다.
//
//	BTreeNode * pVRoot = MakeBTreeNode();    // 가상 루트 노드;
//
//	BTreeNode * pNode = pVRoot;    // parent node
//	BTreeNode * cNode = *pRoot;    // current node
//	BTreeNode * dNode;    // delete node
//
//	// 루트 노드를 pVRoot가 가리키는 노드의 오른쪽 서브 노드가 되게 한다.
//	ChangeRightSubTree(pVRoot, *pRoot);
//	
//	// 삭제 대상을 저장한 노드 탐색
//	while(cNode != NULL && GetData(cNode) != target)
//	{
//		pNode = cNode;
//
//		if(target < GetData(cNode))
//			cNode = GetLeftSubTree(cNode);
//		else
//			cNode = GetRightSubTree(cNode);
//	}
//
//	if(cNode == NULL)    // 삭제 대상이 존재하지 않는다면,
//		return NULL;
//
//	dNode = cNode;    // 삭제 대상을 dNode가 가리키게 한다.
//
//	// 첫 번째 경우: 삭제할 노드가 단말 노드인 경우
//	if(GetLeftSubTree(dNode) == NULL && GetRightSubTree(dNode) == NULL)
//	{
//		if(GetLeftSubTree(pNode) == dNode)
//			RemoveLeftSubTree(pNode);
//		else
//			RemoveRightSubTree(pNode);
//	}
//
//	// 두 번째 경우: 하나의 자식 노드를 갖는 경우
//	else if(GetLeftSubTree(dNode) == NULL || GetRightSubTree(dNode) == NULL)
//	{
//		BTreeNode * dcNode;    // delete node의 자식 노드
//
//		if(GetLeftSubTree(dNode) != NULL)
//			dcNode = GetLeftSubTree(dNode);
//		else
//			dcNode = GetRightSubTree(dNode);
//
//		if(GetLeftSubTree(pNode) == dNode)
//			ChangeLeftSubTree(pNode, dcNode);
//		else
//			ChangeRightSubTree(pNode, dcNode);
//	}
//
//	// 세 번째 경우: 두 개의 자식 노드를 모두 갖는 경우
//	else
//	{
//		BTreeNode * mNode = GetRightSubTree(dNode);    // mininum node
//		BTreeNode * mpNode = dNode;    // mininum node의 부모 노드
//		int delData;
//
//		// 삭제할 노드를 대체할 노드를 찾는다.
//		while(GetLeftSubTree(mNode) != NULL)
//		{
//			mpNode = mNode;
//			mNode = GetLeftSubTree(mNode);
//		}
//		
//		// 대체할 노드에 저장된 값을 삭제할 노드에 대입한다.
//		delData = GetData(dNode);    // 대입 전 데이터 백업
//		SetData(dNode, GetData(mNode));
//
//		// 대체할 노드의 부모 노드와 자식 노드를 연결한다.
//		if(GetLeftSubTree(mpNode) == mNode)
//			ChangeLeftSubTree(mpNode, GetRightSubTree(mNode));
//		else
//			ChangeRightSubTree(mpNode, GetRightSubTree(mNode));
//
//		dNode = mNode;
//		SetData(dNode, delData);    // 백업 데이터 복원
//	}
//
//	// 삭제된 노드가 루트 노드인 경우에 대한 처리
//	if(GetRightSubTree(pVRoot) != *pRoot)
//		*pRoot = GetRightSubTree(pVRoot);
//
//	free(pVRoot);
//	return dNode;
//}

BTreeNode * BSTRemove(BTreeNode ** pRoot, BSTData target)
{
    BTreeNode * pVRoot = MakeBTreeNode();   //가상 루트 노드를 만든다.
    BTreeNode * pNode = pVRoot;             //가상루트노드를 pNode가 가리킨다.
    BTreeNode * cNode = *pRoot;             //실제 루트노드를 cNode가 가리킨다.
    BTreeNode * dNode;                      //삭제할 노드


    //루트 노드를 pVRoot가 가리키는 노드의 오른쪽 자식 노드가 되게 한다.
    ChangeRightSubTree(pVRoot, *pRoot);

    //삭제 노드를 탐색한다.
    //현재노드가 NULL이 아니고 삭제데이터가 아니면 루프를 순회한다
    //이렇게 한 이유는, cNode가 NULL이면 트리에 내가 탐색하고자하는 값이 없다는 의미고
    //삭제데이터를 찾았으면 루프를 더이상 순회할 필요가 없기떄문에 while문을 빠져나간다.
    while ( cNode != NULL && GetData(cNode) != target )
    {
        //cNode가 옮겨지기전에 pNode에 저장한다. 
        //pNode가 왜 필요하지? cNode의 주소를 가지고 있기떄문에
        pNode = cNode;
        if ( target < GetData(cNode) )
            cNode = GetLeftSubTree(cNode);
        else
            cNode = GetRightSubTree(cNode);
    }

    if ( cNode == NULL )        //cNode가 NULL이면 트리에 삭제데이터가 존재하지 않는다.
        return NULL;

    dNode = cNode;              //현재 삭제해야할 노드를 dNode가 가리키게 한다

    //((상황1)) 삭제 노드가 단말노드일 경우
    //현재 노드의 왼쪽, 오른쪽노드가 다 NULL이다.
    if ( GetLeftSubTree(dNode) == NULL && GetRightSubTree(dNode) == NULL )
    {
        //pNode(cNode의 부모노드)의 왼쪽 노드값이 dNode가 가리키는 노드값과 같다면
        if ( GetLeftSubTree(pNode) == dNode )
            RemoveLeftSubTree(pNode);           //pNode의 왼쪽노드 삭제
        //pNode(cNode의 부모노드)의 오른쪽 노드값이 dNode가 가리키는 노드값과 같다면
        else
            RemoveRightSubTree(pNode);          //pNode의 오른쪽노드 삭제
    }

    //((상황2)) 삭제 노드가 하나의 자식노드를 갖는 경우
    else if ( GetLeftSubTree(dNode) == NULL || GetRightSubTree(dNode) == NULL )
    {
        BTreeNode * dcNode;    // delete node의 자식 노드

        //만약 dNode의 왼쪽에 자식노드가 존재한다면
        if ( GetLeftSubTree(dNode) != NULL )
            dcNode = GetLeftSubTree(dNode);
        //만약 dNode의 오른쪽에 자식노드가 존재한다면
        else
            dcNode = GetRightSubTree(dNode);

        //dNode의 자식노드와 부모노드를 서로 연결시켜준다.
        //삭제대상이 부모노드의 왼쪽자식 노드면
        if ( GetLeftSubTree(pNode) == dNode )
            ChangeLeftSubTree(pNode, dcNode);           // 왼쪽에 연결시킨다.
        //삭제대상이 부모노드의 오른쪽자식 노드면
        else
            ChangeRightSubTree(pNode, dcNode);          // 오른쪽에 연결
    }

    //(상황3)) 두개의 자식노드를 모두 갖는 경우
    else
    {
        BTreeNode * mNode = GetRightSubTree(dNode);         //삭제 노드의 오른쪽 노드(오른쪽 서브트리)를 가리킨다.
        BTreeNode * mpNode = dNode;                         //오른쪽노드의 부모노드
        int delData;

        //삭제 대상의 대체노드를 찾는다.
        //오른쪽 서브트리에서 가장 작은 값을 찾는건 트리에서 왼쪽으로 순회를 돌면 된다.
        while (GetLeftSubTree(mNode) != NULL )
        {
            mpNode = mNode;
            mNode = GetLeftSubTree(mNode);
        }

        delData = GetData(dNode);
        SetData(dNode, GetData(mNode));

        //대체 노드의 부모 노드와 자식 노드를 연결한다.
        if ( GetLeftSubTree(mpNode) == mNode )
            ChangeLeftSubTree(mpNode, GetRightSubTree(mNode));
        else
            ChangeRightSubTree(mpNode, GetRightSubTree(mNode));

        //함수바깥에서 대체노드를 free시켜주기위해서 (사실 dNode는 실제 삭제데이터는 아니고 mNode(대체노드)에
        //삭제데이터를 복사한 노드다.
        dNode = mNode;
        SetData(dNode, delData);
    }

    //삭제된 노드가 루트노드인 경우에 대한 추가적인 처리
    if ( GetRightSubTree(pVRoot) != *pRoot )
        *pRoot = GetRightSubTree(pVRoot);

    free(pVRoot);
    return dNode;
}

void ShowIntData(int data)
{
	printf("%d ", data);
}

void BSTShowAll(BTreeNode * bst)
{
	InorderTraverse(bst, ShowIntData);
}