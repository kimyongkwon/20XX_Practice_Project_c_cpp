#include "Recursion.h"

//      ** CRecursion **        //
CRecursion::CRecursion(void)
{
    m_nData = 0;

    for (int i = 0; i < ARRAY_NUM; i++)
        m_arrData[i] = 0;
}

CRecursion::~CRecursion(void)
{
}

void CRecursion::Common(void)
{
    InitData();

    SortData();
}

void CRecursion::InitData(void)
{
    srand((unsigned int)time(NULL));

    auto start = chrono::high_resolution_clock::now();
    for (int i = 0; i < ARRAY_NUM; i++)
        m_arrData[i] = rand() % ARRAY_NUM;
    auto end = chrono::high_resolution_clock::now();

    auto msec = chrono::duration_cast<chrono::duration<double>>(end - start);
    cout << ARRAY_NUM << "개의 데이터를 초기화하는데 걸린시간은 " << msec.count() << "초 입니다" << endl;
}

void CRecursion::SortData(void)
{
    auto start = chrono::high_resolution_clock::now();
    sort(&m_arrData[0], &m_arrData[ARRAY_NUM]);
    auto end = chrono::high_resolution_clock::now();

    auto msec = chrono::duration_cast<chrono::duration<double>>(end - start);
    cout << ARRAY_NUM << "개의 데이터를 정렬하는데 걸린시간은 " << msec.count() << "초 입니다" << endl;
}

bool CRecursion::Update(void)
{
    ////CRecursion의 상위 클래스인 CDataStructure에서 하위 자료구조에 대한 공통된 작업을 진행한다.
    ////공통된 작업은 데이터를 초기화하고 정렬한다.
    //CDataStructure::Common();

    //Common함수에서 공통된 작업을 진행한다.
    //공통된 작업은 데이터를 초기화하고 정렬한다.
    this->Common();

    while (1)
    {
        cout << "재귀함수에서 무엇을 하실건가요?" << endl;
        cout << "1. 검색" << endl;
        cout << "0. 뒤로가기" << endl;
        int cmd;
        cin >> cmd;
        switch (cmd)
        {
        case 1:
            system("cls");
            SearchData();
            break;
        case 2:
            break;
        case 0:
            return 0;
        }
    }
    return 0;
}

void CRecursion::SearchData()
{
    cout << "어떤수를 찾으시겠습니까?(양수만)" << endl;
    uint32_t target;
    cin >> target;

    assert (target <= ARRAY_NUM && "배열크기 안에서의 순자를 입력해줘!!!");

    int first = 0;
    int last = ARRAY_NUM - 1;

    auto start = chrono::high_resolution_clock::now();

    bool idx = Recursion(first, last, target);

    auto end = chrono::high_resolution_clock::now();

    if (idx)
    {
        cout << target << "을 찾았습니다" << endl;
        auto msec = chrono::duration_cast<chrono::duration<double>>(end - start);
        cout << "찾는데 걸린시간은 " << msec.count() << "초 입니다" << endl;
    }

    else cout << target << "을 못찾았습니다" << endl;
}

bool CRecursion::Recursion(int first, int last, int target)
{
    if (first > last)
        return false;

    int mid = (first + last) / 2;

    if (m_arrData[mid] == target)
        return true;
    else if (m_arrData[mid] > target)
        return Recursion(first, mid - 1, target);
    else if (m_arrData[mid] < target)
        return Recursion(mid + 1, last, target);

    return false;
}
