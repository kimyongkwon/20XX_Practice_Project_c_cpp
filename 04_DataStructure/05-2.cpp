#include <stdio.h>
#include <stdlib.h>

#define TRUE	1
#define FALSE	0

typedef int Data;

typedef struct _node
{
    Data data;
    struct _node * next;
    struct _node * prev;
} Node;

typedef struct _dbLinkedList
{
    Node * head;
    Node * tail;
    Node * cur;
    int numOfData;
} DBLinkedList;

typedef DBLinkedList List;

void ListInit(List * plist)
{
    plist->head = (Node*)malloc(sizeof(Node));
    plist->head->prev = NULL;
    plist->head->next = plist->tail;
    plist->tail = (Node*)malloc(sizeof(Node));
    plist->tail->next = NULL;
    plist->tail->prev = plist->head;
    plist->numOfData = 0;
}

void LInsert(List * plist, Data data)
{
    Node * newNode = (Node*)malloc(sizeof(Node));
    newNode->data = data;

    newNode->next = plist->tail;
    newNode->prev = plist->tail->prev;
    plist->tail->prev->next = newNode;
    plist->tail->prev = newNode;
    
    (plist->numOfData)++;
}

int LFirst(List * plist, Data * pdata)
{
    if (plist->tail->prev == plist->head)
        return FALSE;

    plist->cur = plist->tail->prev;
    *pdata = plist->cur->data;

    return TRUE;
}

int LNext(List * plist, Data * pdata)
{
    if (plist->cur->next == plist->tail)
        return FALSE;

    plist->cur = plist->cur->next;
    *pdata = plist->cur->data;

    return TRUE;
}

int LPrevious(List * plist, Data * pdata)
{
    if (plist->cur->prev == plist->head)
        return FALSE;

    plist->cur = plist->cur->prev;
    *pdata = plist->cur->data;

    return TRUE;
}

Data LRemove(List * plist)
{
    Node * rpos = plist->cur;
    Data data = rpos->data;

    rpos->next->prev = rpos->prev;  //plist->cur->next->prev = plist->cur->prev;
    rpos->prev->next = rpos->next;  //plist->cur->prev->next = plist->cur->next;

    plist->cur = plist->cur->next;

    free(rpos);

    (plist->numOfData)--;
    return data;
}

int LCount(List * plist)
{
    return plist->numOfData;
}

int main(void)
{
    // 양방향 연결 리스트의 생성 및 초기화  ///////
    List list;
    int data;
    ListInit(&list);

    // 8개의 데이터 저장  ///////
    LInsert(&list, 1);  LInsert(&list, 2);
    LInsert(&list, 3);  LInsert(&list, 4);
    LInsert(&list, 5);  LInsert(&list, 6);
    LInsert(&list, 7);  LInsert(&list, 8);

    // 저장된 데이터의 조회  ///////
    if (LFirst(&list, &data))
    {
        printf("%d ", data);

        while (LPrevious(&list, &data))
            printf("%d ", data);

        while (LNext(&list, &data))
            printf("%d ", data);

        printf("\n\n");
    }

    int num = LCount(&list);

    LFirst(&list, &data);
    if (data%2 == 0)
        LRemove(&list);

    for (int i = 0; i < num - 1; i++)
    {
        LPrevious(&list, &data);
        if (data % 2 == 0)
            LRemove(&list);
    }
    
    // 저장된 데이터의 조회  ///////
    if (LFirst(&list, &data))
    {
        printf("%d ", data);

        while (LPrevious(&list, &data))
            printf("%d ", data);

        while (LNext(&list, &data))
            printf("%d ", data);

        printf("\n\n");
    }

    return 0;
}