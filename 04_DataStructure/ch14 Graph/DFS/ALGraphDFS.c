#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ALGraphDFS.h"
#include "DLinkedList.h"
#include "ArrayBaseStack.h"

int WhoIsPrecede(int data1, int data2);

// 그래프의 초기화
void GraphInit(ALGraph * pg, int nv)
{
	int i;	

	pg->adjList = (List*)malloc(sizeof(List)*nv);
	pg->numV = nv;
	pg->numE = 0;     // 초기의 간선 수는 0개

	for(i=0; i<nv; i++)
	{
		ListInit(&(pg->adjList[i]));
		SetSortRule(&(pg->adjList[i]), WhoIsPrecede); 
	}

	// 탐색 정보 등록을 위한 공간 할당 및 초기화
	pg->visitInfo= (int *)malloc(sizeof(int) * pg->numV);
	memset(pg->visitInfo, 0, sizeof(int) * pg->numV);
}

// 그래프의 리소스 해제
void GraphDestroy(ALGraph * pg)
{
	if(pg->adjList != NULL)
		free(pg->adjList);

	if(pg->visitInfo != NULL)
		free(pg->visitInfo);
}

// 간선의 추가
void AddEdge(ALGraph * pg, int fromV, int toV)
{
	LInsert(&(pg->adjList[fromV]), toV);
	LInsert(&(pg->adjList[toV]), fromV);
	pg->numE += 1;
}

// 유틸리티 함수: 간선의 정보 출력
void ShowGraphEdgeInfo(ALGraph * pg)
{
	int i;
	int vx;

	for(i=0; i<pg->numV; i++)
	{
		printf("%c와 연결된 정점: ", i + 65);
		
		if(LFirst(&(pg->adjList[i]), &vx))
		{
			printf("%c ", vx + 65);
			
			while(LNext(&(pg->adjList[i]), &vx))
				printf("%c ", vx + 65);
		}
		printf("\n");
	}
}

int WhoIsPrecede(int data1, int data2)
{
	if(data1 < data2)
		return 0;
	else
		return 1;
}


int VisitVertex(ALGraph * pg, int visitV)
{
	//이 정점에 방문하지 않았으면
	if(pg->visitInfo[visitV] == 0)
	{
		pg->visitInfo[visitV] = 1;		// 방문한다.
		printf("%c ", visitV + 65);     // 방문 정점 출력
		return TRUE;
	}
	
	return FALSE;	// 이미 방문했음
}

// Depth First Search: 정점의 정보 출력
void DFShowGraphVertex(ALGraph * pg, int startV)
{
	Stack stack;
	int visitV = startV;			
	int nextV;

	// DFS를 위한 스택의 초기화
	StackInit(&stack);

	VisitVertex(pg, visitV);    // 시작 정점 방문
	SPush(&stack, visitV);		// 시작 정점을 떠나면서 그 정점을 스택에 넣는다.

	// 1. 현재 정점의 인접 정점(인접 정점들 중에서도 당근 가장 우선순위)을 얻는다.
	while(LFirst(&(pg->adjList[visitV]), &nextV) == TRUE)
	{
		int visitFlag = FALSE;

		// 2-1. TRUE는 얻은 인접 정점을 방문한적이 없다는 것이다.
		if(VisitVertex(pg, nextV) == TRUE)
		{
			SPush(&stack, visitV);		// 현재 정점을 stack에 push하고
			visitV = nextV;				// 그리고 인접 정점을 현재 정점으로 세팅
			visitFlag = TRUE;
		}
		
		// 2-2. FALSE는 얻은 인접 정점을 방문한 적이 있다. 
		else
		{
			// 2-2-1. 얻은 인접 정점 말고 다른 인접 정점들을 차례대로 확인한다.
			while(LNext(&(pg->adjList[visitV]), &nextV) == TRUE)
			{
				// 2-2-2. 다른 인접 정점을 방문하지 않았다면
				if(VisitVertex(pg, nextV) == TRUE)
				{
					SPush(&stack, visitV);		// 현재 정점을 stack에 push하고
					visitV = nextV;				// 그리고 인접 정점을 현재 정점으로 세팅
					visitFlag = TRUE;
					break;
				}

				// 위의 if문을 돌지않으면 visitFlag는 FALSE가 된다.
				// 그 의미는 인접하는 정점들을 다 순회헀는데 이미 다 방문한 정점이라는 의미
			}
		}
		
		// 이 의미는 인접하는 정점들을 다 순회헀는데 이미 다 방문한 정점이라는 의미다.
		if(visitFlag == FALSE)
		{
			if(SIsEmpty(&stack) == TRUE)    // 스택이 비면 DFS종료
				break;
			else
				visitV = SPop(&stack);		// pop시켜서 전의 정점으로 visitV로 세팅
		}
	}

	// 탐색 정보 초기화
	memset(pg->visitInfo, 0, sizeof(int) * pg->numV);
}