#pragma once
#include "stdafx.h"
#include "DataStructure.h"

#define MAX_SORT_DATA 1000000

class CSort
{
protected:
    int             m_arr[MAX_SORT_DATA];
public:
    CSort();
    ~CSort();
    void Init(void);
    virtual void Sort(void) {};
};

class CBubbleSort : public CSort
{
    
public:
    CBubbleSort();
    ~CBubbleSort();
    virtual void Sort(void) override;
};

class CSelectionSort : public CSort
{
public:
    CSelectionSort();
    ~CSelectionSort();
    virtual void Sort(void) override;
};

class CInsertionSort : public CSort
{
public:
    CInsertionSort();
    ~CInsertionSort();
    virtual void Sort(void) override;
};

class CHeapSort : public CSort
{
    int             m_nNum;
    int             m_arrHeap[MAX_SORT_DATA + 1];
public:
    CHeapSort();
    ~CHeapSort();
    virtual void Sort(void) override;
    int GetHIPriChildIDX(int);
    void InsertForSort(int);
    int DeleteForSort(void);
};

class CMergeSort : public CSort
{
    int32_t         m_nLeft;
    int32_t         m_nRight;
public:
    CMergeSort();
    ~CMergeSort();
    virtual void Sort(void) override;
    void Divide(int left, int right);                                   //분할단계
    void Combine(int left, int mid, int right);                         //병합단계
};

class CQuickSort : public CSort
{
    int32_t         m_nLeft;
    int32_t         m_nRight;
    int32_t         m_nPivot;
public:
    CQuickSort();
    ~CQuickSort();
    virtual void Sort(void) override;

    void QuickSort(int left, int right);
    int Partition(int left, int right);
    void Swap(int low, int high);
};

class CSortPart : public CDataStructure
{
    CSort *         m_pSort;
public:
    CSortPart();
    ~CSortPart();

    void const ShowCommand(void);
    virtual bool Update(void);
};

