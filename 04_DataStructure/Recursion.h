#pragma once

#include "stdafx.h"
#include "DataStructure.h"

class CRecursion : public CDataStructure
{
protected:
    uint32_t m_arrData[ARRAY_NUM];
    uint32_t m_nData;
public:
    CRecursion();
    ~CRecursion();

    virtual void Common(void);
    virtual void InitData(void);
    virtual void SortData(void);

    virtual bool Update(void) override;

    void SearchData(void);
    bool Recursion(int first, int last, int target);
};

