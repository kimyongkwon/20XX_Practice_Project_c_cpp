﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARRAY
{
    class Array
    {
        static void Main(string[] args)
        {
            // 초기화방법1
            //int[] scores = new int[5];
            //scores[0] = 88;
            //scores[1] = 75;
            //scores[2] = 85;
            //scores[3] = 90;
            //scores[4] = 95;

            // 초기화방법2
            //int[] scores = new int[5] { 88, 75, 85, 90, 95 };

            // 초기화방법3
            //int[] scores = new int[] { 88, 75, 85, 90, 95 };

            // 초기화방법4
            int[] scores = { 88, 75, 85, 90, 95 };

            Console.WriteLine(scores[0]);
            Console.WriteLine(scores[1]);
            Console.WriteLine(scores[2]);
            Console.WriteLine(scores[3]);
            Console.WriteLine(scores[4]);

            foreach (int elem in scores)
                Console.WriteLine(elem);
        }
    }
}
