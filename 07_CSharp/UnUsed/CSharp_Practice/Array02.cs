﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// 배열은 System.Array 클래스의 형식에서 파생되었다.
//
//

namespace ARRAY2
{
    class Array
    {
        static void Main(string[] args)
        {
            int[] arr = new int[] { 10, 100, 1000 };

            // 정렬
            System.Array.Sort(arr);
            
            foreach (int elem in arr)
                Console.WriteLine(elem);

            // 이진탐색했을때 배열안에 데이터값이 있으면 인덱스를 반환하고
            // 없으면 음수 반환
            Console.WriteLine(System.Array.BinarySearch(arr, 100));

            // 배열의 모든 요소를 초기화
            // 숫자는 0으로, 논리는 false, 참조는 null
            System.Array.Clear(arr, 0, arr.Length);

            foreach (int elem in arr)
                Console.WriteLine(elem);
        }
    }
}
