﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// 인덱서는 인덱스를 이용해 객체 내의 데이터를 접근하게 해주는 프로퍼티다.

namespace INDEXER
{
    class Indexer
    {
        private int[] array;
        public Indexer()
        {
            array = new int[3];
        }

        // 인덱서
        public int this[int index]
        {
            get
            {
                return array[index];
            }
            set
            {
                if (index >= array.Length)
                {
                    Array.Resize<int>(ref array, index + 1);
                    Console.WriteLine("Array Resized: {0}", array.Length);
                }

                array[index] = value;
            }
        }

        public int Length
        {
            get
            {
                return array.Length;
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Indexer list = new Indexer();
            for (int i = 0; i < 5; i++)
                list[i] = i; // 배열을 다루듯 사용

            for (int i = 0; i < list.Length; i++)
                Console.WriteLine(list[i]);   // 데이터를 얻어올 때도 인덱스를 사용
        }
    }
}
