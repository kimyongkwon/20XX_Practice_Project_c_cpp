﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07_CSharp
{
    class Base
    {
        public int Data1;
        public int Data2;

        public Base()
        {
            this.Data1 = 10;
            this.Data2 = 20;
        }
        public void print()
        {
            Console.WriteLine("Base클래스호출");
        }
    }

    class Derived : Base
    {
        public int Data3;

        public Derived() : base()
        {
            this.Data3 = 30;
        }

        public void print()
        {
            base.print();
            Console.WriteLine("Derived클래스호출");
        }
    }

    class Inheritance
    {
        static void Main(string[] args)
        {
            Base b = new Base();
            b.print();

            Derived d = new Derived();
            d.print();
        }
    }
}
