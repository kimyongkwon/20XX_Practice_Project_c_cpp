﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07_CSharp.Used
{
    class Mammal
    {
        public void MammalMethod()
        {
            Console.WriteLine("Nurse");
        }
    }

    class Dog : Mammal
    {
        public void DogMethod()
        {
            Console.WriteLine("Dog");
        }
    }

    class Cat : Mammal
    {
        public void CatMethod()
        {
            Console.WriteLine("Cat");
        }
    }

    class TypeConversion
    {
        static void Main(string[] args)
        {
            Mammal mammal = new Dog();
            Dog dog;

            // 자식클래스의 인스턴스는 부모클래스의 인스턴스로 사용할 수 있다.
            if (mammal is Dog)
            {
                dog = (Dog)mammal; // mammal 객체가 Dog 형식임을 확인했으므로 안전하게 형 변환이 가능
                dog.MammalMethod();
                dog.DogMethod();
            }
        }
    }
}
