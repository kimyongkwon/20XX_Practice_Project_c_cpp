﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07_CSharp.Used
{
    partial class Myclass
    {
        public void Method1() { }
        public void Method2() { }
    }

    partial class Myclass
    {
        public void Method3() { }
        public void Method4() { }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Myclass obj = new Myclass();
            obj.Method1();
            obj.Method2();
            obj.Method3();
            obj.Method4();
        }
    }
}
