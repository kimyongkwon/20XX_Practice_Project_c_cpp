﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07_CSharp.UnUsed.CSharp_Practice
{
    // 추상 클래스
    // 추상 클래스의 인스턴스는 생성 할 수 없다.
    abstract class A
    {
        // 추상 메서드
        // 추상 메서드는 추상 클래스에서 구현할 수 없다.
        // 자식클래스에서 구현해야한다.
        public abstract void FunA();

        // 추상 메서드가 아니다
        // 추상 클래스 내에서 정의 가능
        public void FunB()
        {
            Console.WriteLine("안녕 난 FunB야");
        }
    }

    class Derived : A
    {

        public override void FunA()
        {
            Console.WriteLine("안녕 난 FunA야");
        }
    }

    class AbstractClass
    {
        static void Main(string[] args)
        {
            // 추상 클래스의 인스턴스는 생성 할 수 없다.
            //A a = new A();

            Derived derived = new Derived();
            derived.FunA();
            derived.FunB();
        }
    }
}
