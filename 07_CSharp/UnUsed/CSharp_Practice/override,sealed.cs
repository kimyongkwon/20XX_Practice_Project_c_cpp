﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//// 메서드를 오버라이딩하려면 오버라이딩을 할 메서드가 virtual 키워드로 설정되어 있어야 합니다.
//// 그리고 재정의하는 것을 알리기 위해 override를 메서드 앞에 붙여야 합니다

//namespace _07_CSharp
//{
//    //class ArmorSuite
//    //{
//    //    public virtual void Init()
//    //    {
//    //        Console.WriteLine("ArmorSuite");
//    //    }
//    //}

//    //class IronMan : ArmorSuite
//    //{
//    //    public override void Init()
//    //    {
//    //        Console.WriteLine("IronMan");
//    //    }
//    //}

//    //class WarMachine : ArmorSuite
//    //{
//    //    public override void Init()
//    //    {
//    //        Console.WriteLine("WarMachine");
//    //    }
//    //}

//    //class override_sealed
//    //{
//    //    static void Main(string[] args)
//    //    {
//    //        ArmorSuite[] armorSuite = { new ArmorSuite(), new IronMan(), new WarMachine() };
//    //        armorSuite[0].Init();
//    //        armorSuite[1].Init();
//    //        armorSuite[2].Init();
//    //    }
//    //}


//    class ArmorSuite
//    {
//        public void Init()
//        {
//            Console.WriteLine("ArmorSuite");
//        }
//    }

//    // 메서드 숨기기는 부모 클래스에서 구현된 메서드를 감추고
//    // 자식 클래스에서 구현된 메서드만 보여주는 것을 말한다.
//    // new 키워드를 자식클래스의 메서드 앞에 붙이면 메서드를 숨길 수 있다.
//    class IronMan : ArmorSuite
//    {
//        public new void Init()
//        {
//            Console.WriteLine("IronMan");
//        }
//    }

//    class override_sealed
//    {
//        static void Main(string[] args)
//        {
//            IronMan ironman = new IronMan();
//            ironman.Init();


//        }
//    }

//}


using System;

namespace MethodHiding
{
    class Base
    {
        public void MyMethod()
        {
            Console.WriteLine("Base.MyMethod()");
        }
    }

    class Derived : Base
    {
        public new void MyMethod()
        {
            Console.WriteLine("Derived.MyMethod()");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Base baseObj = new Base();
            baseObj.MyMethod();

            Derived derived = new Derived();
            derived.MyMethod();

            Base baseOrDerived = new Derived();
            baseOrDerived.MyMethod();
        }
    }
}