﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// 인터페이스는 인터페이스를 상속받는 클래스가 지켜야 할 내용을 정의할 때 사용한다.
// 클래스를 선언하는 것과 비슷하고, 메서드, 이벤트, 인덱스, 프로퍼티만을 가질 수 있다.
// 인터페이스는 접근제한 한정자를 사용할 수 없고 모든 멤버는 public으로 선언된다.
// 인스턴스는 인스턴스도 만들 수가 없다.
// 인터페이스의 사용은 인터페이스를 상속받는 클래스에서 구현되며 인스턴스를 만드는 것이 가능하다.
// 자식 클래스는 인터페이스에 있는 모든 멤버를 구현해야하며 public으로 구현해야한다.
// 
//namespace _07_CSharp.Used
//{

//    interface ILogger
//    {
//        void WriteLog(String log);
//    }

//    class ConsoleLogger : ILogger
//    {
//        public void WriteLog(string message)
//        {
//            Console.WriteLine("{0} {1}", DateTime.Now.ToLocalTime(), message);
//        }
//    }

//    class Interface
//    {
//        static void Main(string[] args)
//        {

//            ILogger logger = new ConsoleLogger();
//            logger.WriteLog("Hello, World");
//        }
//    }
//}

//---------------------------------------------------------------------------------------------

//namespace _07_CSharp.Used
//{
//    // * 인터페이스는 메소드, 이벤트, 인덱서, 프로퍼티 만을 가질 수 있다.
//    interface ILogger
//    {
//        // * 자동으로 접근제한 지정자가 public으로 지정된다.
//        void func();
//    }

//    class C : ILogger
//    {
//        // * Interface를 상속받은 클래스에서 Interface 메서드를 꼭 정의해줘야한다.
//        // 또한 public으로 구현해야한다.
//        public void func()
//        {
//            Console.WriteLine("안녕 나는 func야");
//        }
//    }

//    class Test
//    {
//        private ILogger logger;
//        // * interface는 자식클래스 객체를 참조형식으로 담을 수 있다.
//        // 즉, C객체는 ILogger의 객체로 취급 가능 하다.
//        public Test(ILogger logger)
//        {
//            this.logger = logger;
//        }
//        public void start()
//        {
//            logger.func();
//        }
//    }

//    class Program
//    {
//        static void Main(string[] args)
//        {
//            Test t = new Test(new C());
//            t.start();
//        }
//    }
//}

//---------------------------------------------------------------------------------------------

// 다중상속의 문제를 interface로 해결할 수 있다?
// 또한 유지보수성이 높아진다?

//namespace _07_CSharp.Used
//{
//    interface ILogger
//    {
//        void func();
//    }

//    // Interface는 Interface를 상속 할 수 있다.
//    interface IRightLogger : ILogger
//    {
//        void Rfunc();
//    }

//    // Interface는 Interface를 상속 할 수 있다.
//    interface ILeftLogger : ILogger
//    {
//        void Lfunc();
//    }

//    // c#에서는 단일상속만 지원하지만 interface를 사용하면 다중상속이 가능하다.
//    class C : IRightLogger, ILeftLogger
//    {
//        public void func()
//        {
//            Console.WriteLine("안녕 나는 ILogger func야");
//        }

//        public void Rfunc()
//        {
//            Console.WriteLine("안녕 나는 IRightLogger func야");
//        }

//        public void Lfunc()
//        {
//            Console.WriteLine("안녕 나는 ILeftLogger func야");
//        }
//    }

//    class Test
//    {
//        private C logger;
//        public Test(C logger)
//        {
//            this.logger = logger;
//        }
//        public void start()
//        {
//            logger.func();
//            logger.Lfunc();
//            logger.Rfunc();
//        }
//    }

//    class Program
//    {
//        static void Main(string[] args)
//        {
//            Test t = new Test(new C());
//            t.start();
//        }
//    }
//}


//---------------------------------------------------------------------------------------------


// 다음은 c++ 다중상속코드다
// v.Run()은 어느 클래스의 Run을 호출해야할지 모른다. 이것이 다중상속의 문제

//#include <iostream>
//using namespace std;

//class Ridable
//{
//    public:
//	void Run()
//    {
//        cout << "달린다" << endl;
//    }
//};

//class Car : public Ridable
//{
//public:
//    void Run()
//    {
//        cout << "차가 ";
//        Ridable::Run();
//    }
//};

//class Plane : public Ridable
//{
//public:
//	void Run()
//    {
//        cout << "비행기가 ";
//        Ridable::Run();
//    }
//};

//class Vehicle : public Car, public Plane
//{
//    public: 
//};

//int main()
//{
//    Vehicle v;
//    v.Run();
//}

// 그래서 c#에서는 클래스간의 다중상속은 지원하지 않고 있다.
// 하지만 interface는 다중상속을 지원한다.
// 또한 위의 코드같은 문제도 발생하지 않는다. 
// 왜냐하면 interface는 내용이 아닌 외형을 물려주기때문에 죽음의 다이아몬드 문제가 발생하지 않는다.

namespace _07_CSharp.Used
{
    interface Ridable
    {
        void Run();
    }

    interface Car : Ridable
    {
        void CarRun();
    }

    interface Plane : Ridable
    {
        void PlaneRun();
    }

    class Vehicle : Car, Plane
    {
        public void Run() { Console.WriteLine("달린다"); }
        public void CarRun() { Console.Write("차가 "); Run(); }
        public void PlaneRun() { Console.Write("비행기가 "); Run(); }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Vehicle v = new Vehicle();
            v.CarRun();
            v.PlaneRun();
        }
    }
}
