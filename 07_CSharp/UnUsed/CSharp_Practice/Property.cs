﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Property란?
// 클래스를 구현하다보면 public필드를 사용하면 더 편하다는것을 안다
// 하지만 public을 사용하면 은닉성이 깨질위험이 있다.
// 프로퍼티란 은닉성과 편의성을 둘 다 해결할 수 있는 기능이다.

// 필드에 접근하는 기존 방식
//namespace _07_CSharp.Used
//{
//    class MyClass
//    {
//        private int myField;

//        public int GetMyField() { return myField; }
//        public void SetMyField(int newValue) { myField = newValue;  }
//    }

//    class Property
//    {
//        static void Main(string[] args)
//        {
//            MyClass obj = new MyClass();
//            obj.SetMyField(3);
//            Console.WriteLine(obj.GetMyField());
//        }
//    }
//}


// 프로퍼티를 사용한 방식
//namespace _07_CSharp.Used
//{
//    class MyClass
//    {
//        private int myField;

//        public int Fun
//        {
//            set
//            {
//                myField = value;
//            }
//            get
//            {
//                return myField;
//            }
//        }
//    }

//    class Property
//    {
//        static void Main(string[] args)
//        {
//            MyClass obj = new MyClass();
//            obj.Fun = 3;                    // Property(Fun)를 통해 데이터를 저장하고
//            Console.WriteLine(obj.Fun);     // 읽어올 수 도 있다.
//        }
//    }
//}


namespace Property
{
    class BirthdayInfo
    {
        private string name;
        private DateTime birthday;

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public DateTime Birthday
        {
            get
            {
                return birthday;
            }
            set
            {
                birthday = value;
            }
        }

        public int Age
        {
            get
            {
                return new DateTime(DateTime.Now.Subtract(birthday).Ticks).Year;
            }
        }
    }


    class Program
    {
        static void Main(string[] args)
        {
            BirthdayInfo birth = new BirthdayInfo();
            birth.Name = "보영";
            birth.Birthday = new DateTime(1990, 2, 12);

            Console.WriteLine("Name: {0}", birth.Name);
            Console.WriteLine("Birthday: {0}", birth.Birthday.ToShortDateString());
            Console.WriteLine("Age: {0}", birth.Age);
        }
    }
}