﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07_CSharp
{
    class My
    {
        public int MyField1;
        public int MyField2;

        public My DeepCopy()
        {
            My newCopy = new My();
            newCopy.MyField1 = this.MyField1;
            newCopy.MyField2 = this.MyField2;

            return newCopy;
        }
    }

    class DeepCopy
    {
        static void Main(string[] args)
        {
            My source = new My();
            source.MyField1 = 10;
            source.MyField2 = 20;

            My dest;
            //dest = source;                  // 얕은 복사
            dest = source.DeepCopy();         // 깊은 복사
            dest.MyField2 = 30;

            Console.WriteLine(source.MyField1);
            Console.WriteLine(source.MyField2);
            Console.WriteLine(dest.MyField1);
            Console.WriteLine(dest.MyField2);
        }
    }
}
