﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// c#에는 이름이 없는 형식이 존재한다.
// 이를 무명형식 anonymousType이라 하는데
// 무명형식은 임시변수가 필요할 때 아주 유용하다.
// 무명형식은 반드시 선언과 함께 new키워드로 인스턴스를 생성해주어야하며, 
// 생성된 인스턴스는 읽기전용이다.

namespace AnonymousType
{
    class Program
    {
        static void Main(string[] args)
        {
            //var a = new { Name = "보영", Age = 22 };
            //Console.WriteLine("Name: {0}, Age: {1}", a.Name, a.Age);

            //var b = new { Subject = "수학", Scores = new int[] { 90, 80, 88, 95 } };
            //Console.Write("Subject: {0}, Scores: ", b.Subject);

            //foreach (var score in b.Scores)
            //    Console.Write("{0} ", score);

            //Console.WriteLine();

            var anony = new { Name = "김용권", SerialNum = 12345 };
            Console.WriteLine("Name : {0} Num : {1}" , anony.Name, anony.SerialNum);
        }
    }
}
