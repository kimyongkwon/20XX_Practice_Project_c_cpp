﻿//18.7.17
//수도요금

using System;

namespace _07_CSharp
{
    class Class1
    {
        static void Main(string[] args)
        {
            string String = Console.ReadLine();
            int A = Convert.ToInt32(String);
            
            String = Console.ReadLine();
            int B = Convert.ToInt32(String);

            String = Console.ReadLine();
            int C = Convert.ToInt32(String);

            String = Console.ReadLine();
            int D = Convert.ToInt32(String);

            String = Console.ReadLine();
            int P = Convert.ToInt32(String);

            int sum1 = A * P;
            int sum2 = (C < P) ? B + ((P - C) * D) : B;

            if (sum1 > sum2)
                Console.WriteLine(sum2);
            else
                Console.WriteLine(sum1);
        }
    }
}
