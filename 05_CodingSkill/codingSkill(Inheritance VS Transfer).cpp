
//상속버전 (Inheritance)
#include <iostream>
using namespace std;

class Robot
{
public:
	void Update(void)
	{
		move();
	}
	virtual void move(void) = 0;
};

class CleanerRobot : public Robot
{
public:
	virtual void move(void) override
	{
		cout << "CleanerRobot이 움직인다" << endl;
	}
};

class CombatRobot : public Robot
{
public:
	virtual void move(void) override
	{
		cout << "CombatRobot이 움직인다" << endl;
	}
};

int main()
{
	Robot * cleanerRobot = new CleanerRobot();

	cleanerRobot->Update();

	Robot * combatRobot = new CombatRobot();

	combatRobot->Update();
}



////이양버전 (Transfer)
//#include <iostream>
//using namespace std;
//
//class RobotBehavior
//{
//public:
//	virtual void move() = 0;
//};
//
//class Robot
//{
//private:
//	RobotBehavior * behavior_;
//public:
//	//생성자와 소멸자.
//	Robot(RobotBehavior * behavior) : behavior_(behavior) {};
//	~Robot() { delete behavior_; }
//
//	//업데이트
//	void Update(){behavior_->move();}
//	void changeBehavior(RobotBehavior * behavior) { behavior_ = behavior; }
//};
//
//class CleanerBehavior : public RobotBehavior
//{
//public:
//	virtual void move(void) override
//	{
//		cout << "CleanerRobot이 움직인다" << endl;
//	}
//};
//
//class CombatBehavior : public RobotBehavior
//{
//public:
//	virtual void move(void) override
//	{
//		cout << "CombatRobot이 움직인다" << endl;
//	}
//};
//
//class SuperRobot
//{
//private:
//	RobotBehavior * behavior_;
//public:
//	//생성자와 소멸자.
//	SuperRobot(RobotBehavior * behavior) : behavior_(behavior) {};
//	~SuperRobot() { delete behavior_; }
//
//	void Update() { behavior_->move(); }
//};
//
//int main()
//{
//	Robot cleanerRobot(new CleanerBehavior());
//	cleanerRobot.Update();
//
//	//청소로봇을 전투로봇으로 변경
//	cleanerRobot.changeBehavior(new CombatBehavior());
//	cleanerRobot.Update();
//
//	//슈퍼로봇에 전투행동 재사용
//	SuperRobot superRobot(new CombatBehavior());
//	superRobot.Update();
//
//	Robot combatRobot(new CombatBehavior());
//	combatRobot.Update();
//}