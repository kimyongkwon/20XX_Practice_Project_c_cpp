//#include <iostream>
//using namespace std;
//
//class CPlayer
//{
//	int data;
//public:
//	CPlayer(int _data) : data(_data) {};
//	CPlayer() {};
//	virtual void move(void) {
//		cout << "플레이어가 음직인다" << endl;
//	}
//
//	virtual void draw(void) {
//		cout << "플레이어를 렌더링" << endl;
//	}
//};
//
//int main()
//{
//	CPlayer * player = new CPlayer();
//
//	if (player != nullptr) 
//		player->move();
//
//	if (player != nullptr)
//		player->draw();
//}




#include <iostream>
using namespace std;

class CPlayer
{
	int data;
public:
	CPlayer(int _data) : data(_data) {};
	CPlayer() {};
	virtual void move(void) {
		cout << "플레이어가 음직인다" << endl;
	}

	virtual void draw(void) {
		cout << "플레이어를 렌더링" << endl;
	}
};

class CNullPlayer : public CPlayer
{

public:
	CNullPlayer() {};

	virtual void move(void) {};
	virtual void draw(void) {};
};

int main()
{
	CPlayer * player = new CNullPlayer();

	player->move();
	player->draw();
}