//#include <iostream>
//using namespace std;
//
//struct Vector2
//{
//	float x;
//	float y;
//
//	Vector2 operator+=(Vector2 pos)
//	{
//		this->x += pos.x;
//		this->y += pos.y;
//		return Vector2{ this->x, this->y };
//	};
//};
//
//class CPlayer
//{
//	Vector2 m_vt2PlayerPos;
//	Vector2 m_vt2PlayerVelocity;
//public:
//	CPlayer(Vector2 Pos) : m_vt2PlayerPos(Pos)
//	{
//		m_vt2PlayerVelocity = { 1.0f, 1.0f };
//	};
//	Vector2 getPosition(void)
//	{
//		return m_vt2PlayerPos;
//	}
//	Vector2 getVelocity(void)
//	{
//		return m_vt2PlayerVelocity;
//	}
//	void setPosition(Vector2 pos)
//	{
//		m_vt2PlayerPos = pos;
//	}
//};
//
//int main()
//{
//	CPlayer player( Vector2 { 100.0f, 100.0f } );
//
//	//플레이어 이동처리
//	Vector2 position = player.getPosition();
//	position += player.getVelocity();
//	player.setPosition(position);
//}

//#include <iostream>
//using namespace std;
//
//struct Vector2
//{
//	float x;
//	float y;
//
//	Vector2 operator+=(Vector2 pos)
//	{
//		this->x += pos.x;
//		this->y += pos.y;
//		return Vector2{ this->x, this->y };
//	};
//};
//
//class CPlayer
//{
//	Vector2 m_vt2PlayerPos;
//	Vector2 m_vt2PlayerVelocity;
//public:
//	CPlayer(Vector2 Pos) : m_vt2PlayerPos(Pos)
//	{
//		m_vt2PlayerVelocity = { 1.0f, 1.0f };
//	};
//	void move(void)
//	{
//		m_vt2PlayerPos += m_vt2PlayerVelocity;
//	};
//};
//
//int main()
//{
//	CPlayer player(Vector2{ 100.0f, 100.0f });
//
//	//플레이어 이동처리
//	player.move();
//}


#include <iostream>
using namespace std;

class Renderer
{
public:
	void draw()
	{
		cout << " 그리다" << endl;
	}
};

class CDrawable
{
public:
	//가상 소멸자
	virtual ~CDrawable() {};

	//렌더링
	virtual void draw(Renderer& renderer) = 0;
};

class CPlayerDraw : public CDrawable
{
public:
	virtual void draw(Renderer& renderer)
	{
		cout << "Player를";
		renderer.draw();
	}
};

class CEnemyDraw : public CDrawable
{
public:
	virtual void draw(Renderer& renderer)
	{
		cout << "Enemy를";
		renderer.draw();
	}
};

int main()
{
	CDrawable * p[2];
	p[0] = new CPlayerDraw;
	p[1] = new CEnemyDraw;

	Renderer renderer;

	for (int i = 0; i < 2; ++i)
		p[i]->draw(renderer);
	
};