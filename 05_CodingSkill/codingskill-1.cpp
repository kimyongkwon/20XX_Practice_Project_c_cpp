//#include <iostream>
//#include <assert.h>
//
//int function(int a)
//{
//	assert((0 <= a && a <= 2) && "값 다시 확인해라");
//	
//	if (a == 0) return 10;
//	if (a == 1) return 20;
//	if (a == 2) return 30;
//}
//
//int main()
//{
//	int cmd;
//
//	while (1) {
//		std::cin >> cmd;
//		std::cout << "반환 : " << function(cmd) << std::endl;
//	}
//}


//#include <iostream>
//#include <assert.h>
//#include <string>
//
//int function(int a)
//{
//	static const int var[] = { 10 , 20 , 30};
//
//	assert((0 <= a && a <= 2) && "값 다시 확인해라");
//
//	return var[a];
//}
//
//int main()
//{
//	int cmd;
//
//	while (1) {
//		std::cin >> cmd;
//		std::cout << "반환 : " << function(cmd) << std::endl;
//	}
//}


//#include <iostream>
//#include <assert.h>
//#include <string>
//
//int function(int a)
//{
//	static const int var[] = { 10 , 20 , 30 };
//
//	for (int i = 0; i < 3; ++i) {
//		if (var[i] == a)
//			return i;
//	}
//	assert(!"부정확한 숫자");
//}
//
//int main()
//{
//	int cmd;
//
//	while (1) {
//		std::cin >> cmd;
//		std::cout << "반환 : " << function(cmd) << std::endl;
//	}
//}


//#include <iostream>
//#include <assert.h>
//#include <string>
//
//int function(int a)
//{
//	static const int var[] = { 10 , 20 , 30 };
//
//	assert(std::find(&var[0], &var[3], a) != &var[3]);
//
//	return std::find(&var[0], &var[3], a) - &var[0];
//}
//
//int main()
//{
//	int cmd;
//
//	while (1) {
//		std::cin >> cmd;
//		std::cout << "반환 : " << function(cmd) << std::endl;
//	}
//}


#include <iostream>
#include <assert.h>
#include <unordered_map>

int function(int num)
{
	static const std::unordered_map<int, int> table = { {10, 0}, {15, 1}, {30, 2} };

	assert((table.find(num) != table.end()) && "부정확한 숫자");

	return table.at(num);
}

int main()
{
	int cmd;

	while (1) {
		std::cin >> cmd;
		std::cout << "반환 : " << function(cmd) << std::endl;
	}
}