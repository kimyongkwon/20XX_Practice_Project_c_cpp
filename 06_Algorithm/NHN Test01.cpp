// 18.9.29

#include <iostream>
#include <vector>
#include <algorithm>
#include <deque>
#include <queue>
#include <stack>
using namespace std;

int main()
{
	int c = 0, p = 0, n = 0;

	cin >> c >> p;

	deque<int> dq;
	stack<int> up_s;
	queue<int> bottom_q;

	stack<int> tmp;
	dq.resize(c, -1);

	for (int i = c; i > 0; --i)
		dq[i-1] = i;

	//cout << endl;

	int Cards;
	while (p--)
	{
		cin >> n;

		Cards = c;
		while (Cards > 2 * n) {
			for (int i = 0; i < n; ++i)
			{
				// 뒤에것을 큐에 저장
				bottom_q.push(dq.back());
				dq.pop_back();

				// 앞에 것을 스택에 저장
				up_s.push(dq.front());
				dq.pop_front();
			}
			
			for (int i = 0; i < n; ++i) {
				tmp.push(bottom_q.front());
				bottom_q.pop();
			}

			for (int i = 0; i < n; ++i) {
				tmp.push(up_s.top());
				up_s.pop();
			}

			Cards -= (n * 2);
		}

		while (!tmp.empty()) {
			dq.push_back(tmp.top());
			tmp.pop();
		}
	}

	for (int i = 0; i < 5; ++i) {
		cout << dq.front() << endl;
		dq.pop_front();
	}
}