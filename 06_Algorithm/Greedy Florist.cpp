// 18.9.18
// Greedy Algorithm
// Greedy Florist

#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
using namespace std;

// Complete the getMinimumCost function below.
int getMinimumCost(int k, vector<int> c) {
	
	// 오름차순 정렬
	sort(c.begin(), c.end());

	long long ans = 0;
	int cnt = 0;
	int num = c.size();
	auto it = c.end();

	// 반복자가 벡터의 처음을 가리킬때까지 순회한다.
	while(it != c.begin()){
		cnt += 1;							// cnt변수로 꽃의 가격을 증가시킨다
		for (int i = 0; i < k; ++i) {		// k명의 사람들이 비싼 꽃들부터 동시에 사들인다.
			it--;
			ans += ((*it) * cnt);
		}
		num = num - k;						// 이미 구매하고 현재 남아있는 꽃들의 갯수를 계산한다.
		
		if (num < k)						// 현재 남아있는 꽃의 갯수가 기존의 k보다 작다면 k를 남아있는 꽃의 갯수만큼 변경한다.
			k = num;
	}
	return ans;
}

int main()
{
	int n, k = 0;
	cin >> n >> k;

	vector<int> v;
	v.resize(n, -1);

	for (int i = 0; i < v.size(); ++i) {
		cin >> v[i];
	}

	cout << getMinimumCost(k, v) << endl;
}