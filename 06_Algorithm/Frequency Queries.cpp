// 18.9.10
// Dictionaries and Hashmaps
// Frequency Queries

#include <iostream>
#include <unordered_map>
using namespace std;

int main()
{
	int num;
	unordered_map<long long, long long> hash;
	unordered_map<long long, long long> hash2;
	scanf("%d", &num);

	// hash에는 n이라는 수가 몇개 들어 있는지 저장한다.
	// hash2에는 같은 갯수를 가지고 있는 hash[n]을 저장한다.
	// ex) 만약 1이 5개, 2가 5개, 3이 4개라면 hash2[5] = 2, hash2[4] = 1;
	// 사실 hash2를 사용하지 않고 hash만 사용하더라도 문제는 풀 수 있다.
	// 하지만 type3에서 hash를 모두 순회하면서 해당 갯수가 있는지 찾으려면
	// 시간이 너무 오래걸린다 (시간초과)
	// 그래서 같은 갯수를 가진 n이 몇개 있는지 관리하는 hash2를 사용하였다.
	int type;
	long long n;
	for (size_t i = 0; i < num; i++)
	{
		bool check = false;
		scanf("%d %lld", &type, &n);
		if (type == 1) {
			hash[n] += 1;
			hash2[hash[n]] += 1;
			hash2[hash[n] - 1] -= 1;		// n의 갯수가 증가했으므로 원래 hash2에서 관리하던 전의 n의 갯수는 또한 변하였으므로 -1해야한다. .
			if (hash2[hash[n] - 1] < 0)
				hash2[hash[n] - 1] = 0;
		}
		else if (type == 2) {
			hash[n] -= 1;
			hash2[hash[n]] += 1;
			hash2[hash[n] + 1] -= 1;

			if (hash2[hash[n] + 1] < 0)
				hash2[hash[n] + 1] = 0;

			if (hash[n] < 0)
				hash[n] = 0;

		}
		else if (type == 3) {
			if (hash2[n] > 0 )				// (hash2[n] > 0)의 의미는 n개를 가진 수가 0보다 큰지 확인한다.
				check = true;
			if (check ) 
				cout << "1" << endl;
			else 
				cout << "0" << endl;
		}
	}
}