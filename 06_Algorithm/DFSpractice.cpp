//18.5.7

//#include <iostream>
//#include <vector>
//#include <algorithm>
//using namespace std;
//
//class Graph {
//public:
//	int N; // 정점의 개수
//	vector<vector<int>> adj; // 인접 리스트
//	vector<bool> visited; // 방문 여부를 저장하는 배열
//
//						  // 생성자
//	Graph() : N(0) {}
//	Graph(int n) : N(n) {
//		adj.resize(N);
//		visited.resize(N);
//	}
//
//	// 간선 추가 함수
//	void addEdge(int u, int v) {
//		adj[u].push_back(v);
//		adj[v].push_back(u);
//	}
//
//	// 모든 리스트의 인접한 정점 번호 정렬
//	void sortList() {
//		for (int i = 0; i<N; i++)
//			sort(adj[i].begin(), adj[i].end());
//	}
//
//	// 깊이 우선 탐색
//	/*void dfs() {
//		fill(visited.begin(), visited.end(), false);
//		dfs(0);
//	}*/
//
//	int dfsAll() {
//		int components = 0;
//		fill(visited.begin(), visited.end(), false);
//		for (int i = 0; i < N; i++) {
//			if (!visited[i]) {
//				cout << "-- new DFS begins --" << endl;
//				int nodes = dfs(i);
//				components++;
//				cout << "size: " << nodes << endl << endl;
//			}
//		}
//		return components;
//	}
//
//private:
//	// curr부터 방문한 정점 개수를 반환
//	int dfs(int curr) {
//		int nodes = 1;
//		visited[curr] = true;
//		cout << "node " << curr << " visited" << endl;
//		for (int next : adj[curr])
//			if (!visited[next]) nodes += dfs(next);
//		return nodes;
//	}
//};
//
//int main() 
//{
//	Graph G(10);
//	G.addEdge(0, 1);
//	G.addEdge(1, 3);
//	G.addEdge(2, 3);
//	G.addEdge(4, 6);
//	G.addEdge(5, 7);
//	G.addEdge(5, 8);
//	G.addEdge(7, 8);
//	G.sortList();
//	int components = G.dfsAll();
//	cout << "The number of component is " << components << endl;
//}


#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>
using namespace std;

vector<vector<int>> v;
vector<int> visisted;

stack<int> Stack;

void GraphInit(int n)
{
	v.resize(n);
	visisted.resize(n, false);
}

void AddEdge(int v1, int v2)
{
	v[v1].push_back(v2);
	v[v2].push_back(v1);
};

void SortList(vector<vector<int>>& v)
{
	for (int i = 0; i < v.size(); i++)
		sort(v[i].begin(), v[i].end());
}

void DFS(vector<vector<int>>& v, int start)
{
	int cur = start;
	visisted[cur] = true;
	cout << start << endl;

	while (1)
	{
		int idx = 0;

		//현재 정점(v[cur])에 인접한 정점들을 순회한다.
		for (auto adj : v[cur])
		{
			//인접한 정점이 한번도 방문되지 않았으면 
			if (visisted[adj] == false)
			{
				visisted[adj] = true;	// 방문하고 
				cout << adj << endl;
				Stack.push(cur);		// 현재 정점의 인덱스(cur)를 스택에 push
				cur = adj;				// 현재 정점을 방문한 정점으로 갱신한다.
				break;					// for문 탈출
			}
			idx += 1;								
		}

		//현재 정점의 인접정점을 다 순회해봤는데, 이미 전에 다 방문했다는 의미다.
		if (idx == v[cur].size())
		{
			if (Stack.empty())
				break;
			else
			{
				cur = Stack.top();
				Stack.pop();
			}
		}
	}
}

int main()
{
	GraphInit(9);

	AddEdge(0, 1);
	AddEdge(1, 3);
	AddEdge(1, 5);
	AddEdge(3, 4);
	AddEdge(5, 4);
	AddEdge(0, 2);
	AddEdge(2, 6);
	AddEdge(2, 8);
	AddEdge(6, 8);
	AddEdge(6, 7);

	SortList(v);

	DFS(v, 0);
}