//18.3.28

//내가 푼 문제
//#include <stdio.h>
//
//int main()
//{
//    char str[1000000] = { 0, };
//
//    //단어의 갯수는 26개
//    int a[26] = { 0, };
//
//    scanf("%s", str);
//
//    //대소문자를 구분하지 않는다.
//    for ( int i = 0; i < 1000000; i++ )
//    {
//        //만약 str[i]가 97보다 크거나 같다는 의미는 소문자라는 뜻이다.
//        if ( str[i] >= 97)
//            a[str[i] - 'a']++;
//        //str[i]가 97보다 작거나 65보다 크다는 의미는 대문자라는 뜻이다.
//        else if ( str[i] >= 65 )
//            a[str[i] - 'A']++;          
//    }
//
//    //가장 많이 사용된 알파벳이 여러개 존재한다는 의미의 bool값
//    bool exception = false;
//    int max = 0;
//    int idx = 0;
//    for ( int j = 0; j < 26; j++ )
//    {
//        //가장 많은 사용된 알파벳 갯수보다 현재 배열의 수가 크다면
//        //max를 갱신하고
//        //그 인덱스를 idx에 저장한다.
//        if ( max < a[j])
//        {
//            //max보다 더 많이 사용된 알파벳이 등장했기때문에 exception을 거짓으로 바꾼다.
//            exception = false;
//            max = a[j];     
//            idx = j;
//        }
//        //가장 많이 사용된 알파벳 갯수가 현재 배열의 수와 같다면 가장 많이 사용된
//        //알파벳이 여러개 존재한다는 의미이므로 exception을 참으로 바꾼다.
//        //배열을 다 순회할 때까지 exception이 참이면 배열 안에서 
//        //가장 많이 사용된 알파벳이 여러개 존재한다는 의미다.
//        //(max != 0을 안하면 문자열을 한개만 입력하면 ?가 나온다)
//        else if ( max != 0 && (max == a[j]) )
//            exception = true;
//    }
//
//    if ( exception ) printf("?");
//    else printf("%c", idx + 65);
//}



//다른 사람이 푼 방법
//
#include <stdio.h>

int main()
{
    char str[1000000] = { 0, };
    char ans;

    //단어의 갯수는 26개
    int a[26] = { 0, };
    int max = 0;

    scanf("%s", str);

    int num;
    for ( int i = 0; str[i] != NULL; i++ )
    {
        if ( str[i] >= 'a' && str[i] <= 'z' )
            num = str[i] - 'a';
        else if ( str[i] >= 'A' && str[i] <= 'Z' )
            num = str[i] - 'A';
        
        //문자와 숫자를 매칭
        a[num]++;

        //이제까지 가장 많이 사용한 알파벳 갯수(max)보다 
        //더 많이 사용한 알파벳 나오면 max를 갱신하고
        //ans에 대문자로 저장
        if ( max < a[num] )
        {
            max = a[num];
            ans = num + 'A';
        }
        //가장 많이 사용한 알파벳과 동일한 횟수만큼 사용된 알파벳이 등장한다면
        //ans를 ?로 저장
        else if ( max == a[num] )
            ans = '?';
    }

    printf("%c", ans);
}