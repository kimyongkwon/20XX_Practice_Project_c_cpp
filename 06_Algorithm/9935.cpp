//18.5.16
//문자열 폭발

//내 멘탈도 폭발하는 문제...

//#include <cstdio>
//#include <iostream>
//#include <string>
//#include <algorithm>
//using namespace std;
//
//int main()
//{
//	ios::sync_with_stdio(false);
//
//	string str;
//	cin >> str;
//
//	string bomb;
//	cin >> bomb;
//
////----- 이렇게 하면 매우 느림(시간초과)
//	//for (int i = 0; i < str.size(); i++){
//	//	if (str[i] == bomb[0]){
//	//		int cnt = 1;
//	//		for (int j = 1; j < bomb.size(); j++) {
//	//			if (!(str[i+j] == bomb[j]))
//	//				break;
//	//			cnt++;
//	//		}
//	//		if (cnt == bomb.size())
//	//			str.erase(i, cnt);
//	//	}
//	//}
//
////------ 이렇게 해도 느리다, 위에껏보단 낫다.(시간초과)
//	int i = 0;
//	while( str[i] != '\0') {
//		if (str[i] == bomb[0]) {
//			int cnt = 1;
//			for (int j = 1; j < bomb.size(); j++) {
//				if (!(str[i + j] == bomb[j]))
//					break;
//				cnt++;
//			}
//			if (cnt == bomb.size()) {
//				str.erase(i, cnt);
//				i -= bomb.size();
//				if (i < 0)
//					i = -1;
//			}
//		}
//		i++;
//	}
//	
//	str.empty() ? cout << "FRULA" << endl : cout << str << endl;
//}

//#include <cstdio>
//#include <iostream>
//#include <string>
//#include <algorithm>
//using namespace std;

//int main()
//{
//	ios::sync_with_stdio(false);
//
//	string str;
//	cin >> str;
//
//	string bomb;
//	cin >> bomb;
//
//	int i = 0;
//	
//	while (str[i] != '\0') {							// 문자열에서 널값이 나올때까지 순회한다.
//		if (str[i] == bomb[0]) {						
//			int cnt = 1;
//			for (int j = 1; j < bomb.size(); j++) {		// 폭발 문자열과 같은 문자열이 있는지 확인한다. 
//				if (!(str[i + j] == bomb[j]))			
//					break;
//				cnt++;
//			}
//			if (cnt == bomb.size()) {					// 폭발 문자열과 같은 문자열이 있으면
//				str.erase(i, cnt);						// Algorithm함수인 erase로 삭제후
//				i -= bomb.size();						// 폭발문자열길이만큼 i를 앞으로 땡긴다.
//				if (i < 0)
//					i = -1;
//			}
//		}
//		i++;
//	}
//
//	str.empty() ? cout << "FRULA" << endl : cout << str << endl;
//}

#include <cstdio>
#include <string>
#include <iostream>
#include <algorithm>
using namespace std;

int main()
{
	ios::sync_with_stdio(false);

	string str;
	cin >> str;

	char Stack[1000001] = { 0, };

	string bomb;
	cin >> bomb;

	int i = 0;
	int bomb_len = bomb.length();
	int cnt;
	int pos = 0;
	while (str[i] != '\0') {
		cnt = 0;
		if (bomb[bomb_len - 1] == str[i]) {
			for (int k = 1; k < bomb_len; k++) {
				printf("%d %d\n", pos - 1 - k, bomb_len - 2 - k);
				if (!(Stack[pos - k] == bomb[bomb_len - 1 - k]))
					break;
				cnt++;
			}
			if (cnt == bomb_len - 1)
				while (cnt) {
					Stack[--pos] = '\0';
					cnt--;
				}

			else {
				if (cnt == 0) Stack[pos++] = str[i];
				else {
					while (cnt) {
						Stack[pos++] = str[i];
						cnt--;
					}
				}
			}
		}
		else
			Stack[pos++] = str[i];
		i++;
	}

	if (Stack[0] == '\0')
		cout << "FRULA";
	else
	{
		for (int i = 0; Stack[i] != '\0'; i++)
			cout << Stack[i];
	}
	cout << endl;
}
