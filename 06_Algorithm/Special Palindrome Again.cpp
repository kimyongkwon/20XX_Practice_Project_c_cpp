// 18.9.15
// String Manipulation
// Special Palindrome Again

#include <iostream>
#include <string>
#include <map>
#include <unordered_map>
#include <vector>
#include <algorithm>
using namespace std;

int CountSpecialPalindrome(int n, string str)
{
	int result = 0;

	vector<int> v;
	v.resize(n, 0);

	int i = 0;

	while (i < n) {
		int sameCharCount = 1;

		int j = i + 1;

		int cnt = 1;
		while ((str[i] == str[i + cnt]) && (i + cnt < n)) {
			sameCharCount++, j++;
			cnt += 1;
		}

		result += (sameCharCount * (sameCharCount + 1) / 2);

		for ( int k = 0; k < cnt; ++k)
			v[i + k] = sameCharCount;

		i = j;
	}

	for (int j = 1; j < n-1; j++)
	{
		if (j > 0 && j < (n - 1) && (str[j - 1] == str[j + 1] && str[j] != str[j - 1]))
			result += min(v[j - 1],v[j + 1]);
	}

	return result;
}

// driver program to test above fun
int main()
{
	int num;
	cin >> num;

	string str;
	cin >> str;

	cout << CountSpecialPalindrome(num, str) << endl;
	return 0;
}


//long substrCount(int n, string s)
//{
//	long iPalCount = n;
//	long iSpecialCnt = 0, iCurrCount = 0, iPrevCount = 0, iPrevPrevCount = 0;
//
//	for (int i = 1; i < n; i++)
//	{
//		char cPrev = s[i - 1]; char cCurr = s[i];
//		if (cPrev == cCurr)
//		{
//			iCurrCount++; 
//			iPalCount += iCurrCount;
//			if (iSpecialCnt > 0)
//			{
//				iSpecialCnt--; iPalCount++;
//			}
//		}
//		else {
//			iCurrCount = 0;
//			// 현재 인덱스에 있는 것이 두개 전의 인덱스와 같다면
//			if (i > 1 && (s[i - 2] == cCurr))
//			{
//				iSpecialCnt = iPrevPrevCount; 
//				iPalCount++;
//			}
//			else
//			{
//				iSpecialCnt = 0;
//			}
//		}
//		if (i > 1)
//		{
//			iPrevPrevCount = iPrevCount;
//		}
//		iPrevCount = iCurrCount;
//	}
//	return iPalCount;
//}


//int main()
//{
//	int num, cnt = 0;
//	cin >> num;
//
//	string s;
//	cin >> s;
//
//	int Lidx = 0;
//	bool check;
//	int mid = 0;
//	char tmp = 0;
//	for (int n = 0; n < s.size(); n++) 
//	{
//		cnt += 1;
//		for (int i = 0; i < s.size()-n; ++i)
//		{
//			check = true;
//			// 짝수 일떄
//			if (i % 2 == 1) {
//				mid = i / 2 + 1;
//				if (mid > 0) {
//					tmp = s[n];
//					for (int j = 0; j < mid; j++) {
//						if (s[n + j] != s[(i+n) - j] || s[n+j] != tmp) {
//							check = false;
//							break;
//						}
//					}
//					if (check) cnt += 1;
//				}
//			}
//			// 홀수 일때
//			else {
//				mid = i / 2;
//				if (mid > 0) {
//					for (int k = 0; k < mid; k++) {
//						if (s[n + k] != s[(i + n) - k] || s[n+k] != tmp) {
//							check = false;
//							break;
//						}
//					}
//					if (check) cnt += 1;
//				}
//			}
//
//		}
//	}
//
//	cout << cnt << endl;
//}

//int main()
//{
//	int num;
//	cin >> num;
//
//	string s;
//	cin >> s;
//
//	cout << CountSpecialPalindrome(num, s) << endl;
//	//vector<int> v;
//	//v.resize(26, 0);
//
//	//string tmp;
//	//string left_str;
//	//string right_str;
//	//int cnt = 0;
//
//	//// 길이가 1일경우에는 팰린드롬이기 때문에 cnt증가후 continue;
//	//if (s.size() == 1) {
//	//	cnt++;
//	//}
//
//	//for (int i = 0; i < s.size(); ++i) {
//	//	for (int j = 1; j <= s.size(); ++j) {
//	//		if (i + j <= s.size()) {
//	//			map<int, int> m;
//
//	//			tmp = s.substr(i, j);
//
//	//			// 길이가 1일경우에는 팰린드롬이기 때문에 cnt증가후 continue;
//	//			if (tmp.size() == 1) {
//	//				cnt++;
//	//				continue;
//	//			}
//	//			 
//	//			char search;
//	//			int cnt2 = 0;
//	//			if (tmp.size() % 2 == 0) {
//	//				search = tmp[0];
//	//				for (int n = 0; n < tmp.size(); ++n)
//	//				{
//	//					if (search == tmp[n])
//	//						cnt2 += 1;
//	//					else
//	//						break;
//	//				}
//	//				if (cnt2 == tmp.size()) {
//	//					cnt++;
//	//				}
//	//			}
//
//	//			else {
//	//				int middle = tmp.size() / 2;
//	//				for (int k = 0; k < tmp.size(); k++) {
//	//					if (k != middle) {
//	//						left_str = tmp.substr(0, middle);
//	//						right_str = tmp.substr(middle + 1, middle);
//	//						if (left_str == right_str) {
//	//							cnt += 1;
//	//							break;
//	//						}
//	//					}
//	//				}
//	//			}
//	//		}
//	//	}
//	//}
//
//	//cout << cnt << endl;
//}