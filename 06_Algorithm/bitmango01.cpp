#include <string>
#include <vector>
#include <iostream>
using namespace std;

void solution(int day, int k) {
    
    vector<int> answer;

    //step1 
    //각 월의 1일이 무슨 요일인지 구한다.
    //그리고 그 요일을 vector에 push_back

    //1월
    answer.push_back(day);

    //2월
    day = (day + 31) % 7;
    answer.push_back(day);

    //3월
    day = (day + 28) % 7;
    answer.push_back(day);

    //4월
    day = (day + 31) % 7;
    answer.push_back(day);

    //5월
    day = (day + 30) % 7;
    answer.push_back(day);

    //6월
    day = (day + 31) % 7;
    answer.push_back(day);

    //7월
    day = (day + 30) % 7;
    answer.push_back(day);

    //8월
    day = (day + 31) % 7;
    answer.push_back(day);

    //9월
    day = (day + 31) % 7;
    answer.push_back(day);

    //10월
    day = (day + 30) % 7;
    answer.push_back(day);

    //11월
    day = (day + 31) % 7;
    answer.push_back(day);

    //12월
    day = (day + 30) % 7;
    answer.push_back(day);

    //step2 
    //1일부터 납부날인 k까지의 차(distance)를 구한다.
    int distance = (k - 1) % 7;
    for (int i = 0; i < answer.size(); i++)
    {
        //step3 
        //각 월의 1일인 요일부터 차를 더하면 무슨 요일인지 알수 있다. 
        answer[i] += distance;
        answer[i] %= 7;
        if (answer[i] == 5 || answer[i] == 6)
            answer[i] = 1;
        else
            answer[i] = 0;
    }

    for ( auto elem : answer)
    {
        cout << elem << endl;
    }
   
    //return answer;
}

int main()
{
    solution(1, 20);
}


