
// 01

#include <iostream>
#include <queue>
#include <vector>
#include <string>
using namespace std;

int main() {
	queue<int> q;
	vector<int> v;
	
	string str;
	getline(cin, str);

	vector<int> tmp;
	for (int i = 0; i < str.size(); ++i)
	{
		if ( str[i] != ' ')
			tmp.push_back(str[i] - '0');
	}
	int n = 0;

	for (int i = 0; i < tmp.size(); ++i){
		if (q.size() < 3)
			q.push(tmp[i]);
		else {
			if (q.front() != tmp[i]) {
				v.push_back(q.front());
			}
			q.pop();
			q.push(tmp[i]);
		}
	}

	for (int i = 0; i < v.size(); ++i)
		cout << v[i] << endl;

	if (v.size() == 0)
		cout << 0 << endl;

	return 0;
}


// 02

//#include <iostream>
//#include <vector>
//#include <algorithm>
//#include <queue>
//using namespace std;
//
//vector<vector<int>> v;
//queue<pair<int,int>> q;
//
//vector<int> vArr;
//
//int n;
//
//int BFS(int y, int x)
//{
//	int cnt = 1;
//	v[y][x] = 0;
//	q.push({ y, x });
//
//	pair<int, int> tmp = { 0,0 };
//	while (!q.empty()) {
//		tmp = q.front();
//		q.pop();
//		// 坷弗率
//		if (v[tmp.first][tmp.second + 1] == 1) {
//			v[tmp.first][tmp.second + 1] = 0;
//			q.push({ tmp.first,tmp.second + 1 });
//			cnt += 1;
//		}
//		// 哭率
//		if (v[tmp.first][tmp.second - 1] == 1) {
//			v[tmp.first][tmp.second - 1] = 0;
//			q.push({ tmp.first,tmp.second - 1 });
//			cnt += 1;
//		}
//		// 拉率
//		if (v[tmp.first - 1][tmp.second] == 1) {
//			v[tmp.first - 1][tmp.second] = 0;
//			q.push({ tmp.first - 1,tmp.second });
//			cnt += 1;
//		}
//		// 酒阀率
//		if (v[tmp.first + 1][tmp.second] == 1) {
//			v[tmp.first + 1][tmp.second] = 0;
//			q.push({ tmp.first + 1, tmp.second});
//			cnt += 1;
//		}
//	}
//
//	return cnt;
//}
//
//int main()
//{
//	cin >> n;
//	v.resize(n + 2);
//	for (int i = 0; i < n+2; ++i) v[i].resize(n + 2, -1);
//
//	for (int i = 1; i <= n; ++i) {
//		for (int j = 1; j <= n; ++j) {
//			cin >> v[i][j];
//		}
//	}
//
//	for (int i = 1; i <= n; ++i) {
//		for (int j = 1; j <= n; ++j) {
//			if (v[i][j] == 1) {
//				int n = BFS(i, j);
//				vArr.push_back(n);
//			}
//		}
//	}
//
//	if (vArr.empty())
//		cout << 0;
//	else {
//		sort(vArr.begin(), vArr.end());
//		cout << vArr.size() << endl;
//		for (auto elem : vArr)
//			cout << elem << " ";
//	}
//}
