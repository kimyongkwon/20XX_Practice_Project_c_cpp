////18.4.5
//피보나치수

//다음과 같은 방법으로는 시간도 줄이고 동시에 0과 1이 출력된 횟수를 못구하겠음 ㅠㅠ

//#include <cstdio>
//#include <vector>
//
//using namespace std;
//
//int zero;
//int one;
//vector<int> v;
//
//int fibonacci(int n)
//{
//    if ( n == 0 )
//    {
//        zero++;
//        return 0;
//    }
//    if ( n == 1 ) 
//    {
//        one++;
//        return 1;
//    }
//    //if ( v[n] != -1 ) return v[n];
//    v[n] = fibonacci(n - 1) + fibonacci(n - 2);
//    return v[n];
//}
//
//int main()
//{
//    int T;
//    scanf("%d", &T);
//
//    int n;
//    for (int i = 0; i < T; i++)
//    {
//        scanf("%d", &n);
//        //v.resize(n + 1, -1);
//        fibonacci(n);
//        printf("%d %d\n", zero, one);
//    }
//}


//#include <cstdio>
//#include <vector>
//using namespace std;
//
//int main()
//{
//    int T;
//    scanf("%d", &T);
//
//    int n = 0;
//    for ( int i = 0; i < T; i++ )
//    {
//        scanf("%d", &n);
//
//        //n은 최대 40까지 입력받을 수 있기때문에
//        //first는 0의 출력횟수, second는 1의 출력횟수를 나타낸다.
//        vector<pair<int, int>> p(41);
//
//        p[0].first = 1; p[0].second = 0;    //n이 0일때는 0이 1번, 1이 0번 출력된다
//        p[1].first = 0; p[1].second = 1;    //n이 1일때는 0이 0번, 1이 1번 출력된다
//
//        //n이 0과 1일 때는 위에서 구했으므로 n은 2부터 시작한다.
//        for ( int j = 2; j <= n; j++ )
//        {
//            p[j].first = p[j - 1].first + p[j - 2].first;
//            p[j].second = p[j - 1].second + p[j - 2].second;;
//        }
//
//        printf("%d %d\n", p[n].first , p[n].second);
//    }
//}


//18.5.28
//다시 풀어보기

//바텀업
//#include <cstdio>
//#include <vector>
//using namespace std;
//
//vector<int> dp;
//vector<pair<int, int>> p;
//
//int main()
//{
//	int t, n;
//	scanf("%d", &t);
//	dp.resize(42, -1);
//	p.resize(42);
//
//	dp[0] = 0;
//	dp[1] = 1;
//
//	p[0] = { 1,0 };
//	p[1] = { 0,1 };
//
//	while (t--)
//	{	
//		scanf("%d", &n);
//		
//		for (int i = 2; i <= n; i++) {
//			dp[i] = dp[i - 1] + dp[i - 2];
//			p[i].first = p[i - 1].first + p[i - 2].first;
//			p[i].second = p[i - 1].second + p[i - 2].second;
//		}
//
//		printf("%d %d\n", p[n].first, p[n].second);
//	}
//}

//탑다운
#include <cstdio>
#include <vector>

using namespace std;

vector<int> v;
vector<pair<int, int>> p;

int fibonacci(int n)
{
	if (n == 0) return 0;
	if (n == 1) return 1;

	if ( v[n] != -1 ) return v[n];

	v[n] = fibonacci(n - 1) + fibonacci(n - 2);
	p[n] = { p[n - 1].first + p[n - 2].first, p[n - 1].second + p[n - 2].second };

	return v[n];
}

int main()
{
	int T,n;
	scanf("%d", &T);
	v.resize(42, -1);
	p.resize(42);

	p[0] = { 1,0 };
	p[1] = { 0,1 };

	for (int i = 0; i < T; i++)
	{
		scanf("%d", &n);
		fibonacci(n);
		printf("%d %d\n", p[n].first, p[n].second);
	}
}