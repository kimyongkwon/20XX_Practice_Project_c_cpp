//18.6.6
//토마토

#include <cstdio>
#include <vector>
#include <queue>
using namespace std;

typedef vector<int> width;
vector<width> v;
queue<pair<int, int>> q;

int n = 0, m = 0;

int BFS(void)
{
	int x = 0, y = 0;
	int cnt = -1;

	while (!q.empty()) {
		cnt += 1;
		int qsize = q.size();
		for (int i = 0; i < qsize; i++) {
			x = q.front().first;
			y = q.front().second;
			q.pop();

			if (x + 1 <= m && v[y][x + 1] == 0) {
				v[y][x + 1] = 1;
				q.push({ x + 1, y });
			}
			if (x - 1 > 0 && v[y][x - 1] == 0) {
				v[y][x - 1] = 1;
				q.push({ x - 1,y });
			}
			if (y + 1 <= n && v[y + 1][x] == 0) {
				v[y + 1][x] = 1;
				q.push({ x,y + 1 });
			}
			if (y - 1 > 0 && v[y - 1][x] == 0) {
				v[y - 1][x] = 1;
				q.push({ x,y - 1 });
			}
		}
	}

	//큐가 다 비었음에도 상자안에 익지않은 토마토가 있다면
	//그 토마토는 익을 수 없다는 말이기때문에 -1을 리턴한다.
	for (int i = 1; i <= n; i++) {
		for (int j = 1; j <= m; j++) {
			if (v[i][j] == 0)
				return -1;
		}
	}

	return cnt;
}

int main()
{
	scanf("%d %d", &m, &n);			// m은 가로, n 세로

	v.resize(n + 1);
	for (int i = 0; i < v.size(); i++) {
		v[i].resize(m + 1, 100);
	}


	//토마토의 상태를 입력받는다. 
	for (int i = 1; i <= n; i++) {
		for (int j = 1; j <= m; j++) {
			scanf("%d", &v[i][j]);
			if (v[i][j] == 1) {			//해당 토마토가 익었으면 큐에 push한다.
				q.push({ j,i });		//{x,y}
			}
		}
	}

	printf("%d", BFS());
}