// 18.9.4
// sorting

#include <cstdio>

int main()
{
	int arr[600] = { 0, };

	int num = 0;
	scanf("%d", &num);

	for (int i = 0; i < num; i++){
		scanf("%d", &arr[i]);
	}

	int cnt = 0;
	for (int i = 0; i < num; i++) {
		for (int j = i; j < num; j++) {
			if (arr[i] > arr[j]) {
				int tmp = arr[i];
				arr[i] = arr[j];
				arr[j] = tmp;
				cnt += 1;
			}
		}
	}

	printf("Array is sorted in %d swaps.\n", cnt);
	printf("First Element: %d\n", arr[0]);
	printf("Last Element: %d", arr[num-1]);
}