//18.3.20

////내가 짠 코드
//#include <stdio.h>
//
//bool HanNumber(int n)
//{
//    int thousand = 0;
//    int hundred = 0;
//    int ten = 0;
//    int one = 0;
//
//    //1000 자리 숫자를 thousand에 저장한다.
//    //그리고 % 연산자로 1000자리 숫자를 제외한다.
//    if ( n / 1000 != 0 )
//    {
//        thousand = n / 1000;
//        n %= 1000;
//    }
//    //100 자리 숫자를 hundred에 저장한다.
//    //그리고 % 연산자로 100자리 숫자를 제외한다.
//    if ( n / 100 != 0 )
//    {
//        hundred = n / 100;
//        n %= 100;
//    }
//    //10 자리 숫자를 ten에 저장한다.
//    //그리고 % 연산자로 10자리 숫자를 제외한다.
//    if ( n / 10 != 0 )
//    {
//        ten = n / 10;
//        n %= 100;
//    }
//    //1 자리숫자를 % 연산자로 바로 one에 저장한다.
//    if ( n % 10 != 0 )
//        one = n % 10;
//
//    //여기까지 하면 그럼 각각의 자리 숫자가 변수들에 저장된다.
//
//    //1000,100,10 자리 수가 모두 0이면 한자리 숫자라는 의미! 한자리 숫자는 모두 한수로 취급하기때문에 true
//    if ( thousand == 0 && hundred == 0 && ten == 0 )
//        return true;
//    //1000,100 자리 수가 모두 0이면 두자리 숫자라는 의미! 두자리 숫자 또한 모두 한수로 취급하기때문에 true
//    else if ( thousand == 0 && hundred == 0 )
//        return true;
//    //1000 자리수만 0이면 세자리 숫자라는 의미 ! 세자리 숫자는 공차를 구해서 비교한 후 true를 반환한다.
//    else if ( thousand == 0 )
//    {
//        if ( (ten - hundred) == (one - ten) )
//            return true;
//    }
//
//    return false;
//}
//
//int main()
//{
//    int n;
//
//    scanf("%d", &n);
//
//    int cnt = 0;
//    for ( int i = 1; i <= n; i++)
//    {
//        if ( HanNumber(i) )
//            cnt++;
//    }
//
//    printf("%d", cnt);
//}

//좀 더 간단한 다른 방법
#include <stdio.h>

bool HanNumber(int n)
{
    //100보다 작은 수는 한수이기 때문에 true 리턴
    if ( n < 100 )
        return true;
    //1000은 한수가 아니기 때문에 false리턴
    else if ( n == 1000 )
        return false;
    //위의 두가지 경우가 아니면 세자리 수다.
    //공차 구해서 true 리턴
    else
    {
        int hundred = n / 100;
        n %= 100;
        int ten = n / 10;
        int one = n % 10;

        if ( (ten - hundred) == (one - ten) )
            return true;
    }

    return false;
}

int main()
{
    int n;

    scanf("%d", &n);

    int cnt = 0;
    for ( int i = 1; i <= n; i++ )
    {
        if ( HanNumber(i) )
            cnt++;
    }

    printf("%d", cnt);
}