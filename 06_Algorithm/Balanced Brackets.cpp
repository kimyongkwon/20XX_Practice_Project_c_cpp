// 18.9.12
// Stacks and Queues
// Balanced Brackets

#include <iostream>
#include <string>
#include <stack>
using namespace std;

// Complete the isBalanced function below.
string isBalanced(string s) {
	stack<char> Stack;

	// 문자열의 길이만큼 순회하다가
	for (int i = 0; i < s.size(); i++) {
		// '(' '[' '{' 가 나오면 스택에 push
		if (s[i] == '(' || s[i] == '[' || s[i] == '{')
			Stack.push(s[i]);
		
		// 그렇지 않고
		else {
			// 만약 스택이 비어있다면 문자열s에 아무것도 없는 것이므로 NO출력
			if (Stack.empty())
				return "NO";

			// ')'이 나오면 스택에서 top해서'('가 나오면 짝이 맞는 것이다.
			if (s[i] == ')') {
				if (Stack.top() == '(') {
					Stack.pop();
				}
				// '('가 안나오면 짝이 안맞는것이므로 NO출력
				else
					return "NO";
			}
			if (s[i] == ']') {
				if (Stack.top() == '[') {
					Stack.pop();
				}
				else
					return "NO";
			}
			if (s[i] == '}') {
				if (Stack.top() == '{') {
					Stack.pop();
				}
				else
					return "NO";
			}
		}
	}

	// s 길이만큼 순회를 다 했는데 stack이 비지않았다는 것은
	// '(' or '[' or '{' 들이 남아있다는 것이기 때문에 짝이 안맞는다는 의미다.
	// ex) ({}()[]
	// 그러므로 NO 출력
	if (!Stack.empty())
		return "NO";

	// 그것도 아니면 YES
	return "YES";
}

int main()
{

	int t;
	cin >> t;
	cin.ignore(numeric_limits<streamsize>::max(), '\n');

	for (int t_itr = 0; t_itr < t; t_itr++) {
		string s;
		getline(cin, s);

		string result = isBalanced(s);

		cout << result << "\n";
	}

	return 0;
}