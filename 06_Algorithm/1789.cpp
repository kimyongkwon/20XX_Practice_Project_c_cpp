//18.6.14
//수들의 합

#include <cstdio>

int main()
{
	long long s;
	scanf("%lld", &s);

	long long n = 0, sum = 0;

	//1부터 더해 나간다.
	//더해나가다가 sum이 s를 넘는 순간
	//절대로 s를 넘을때까지 더했던 자연수의 갯수는 s의 최대값이 될 수 없다는 의미다.
	//그러므로 그때까지 더했던 자연수의 갯수 -1을 하면 s를 구성하는 N의 최대값이 나온다.
	while (1)
	{
		n++;
		sum += n;				
		if (sum > s) {
			n--;
			break;
		}
	}

	printf("%lld", n);
}