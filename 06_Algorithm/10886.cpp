//18.6.1
//0 = not cute / 1 = cute

#include <cstdio>

int main()
{
	int n;
	scanf("%d", &n);

	int isCute = 0;
	int isntCute = 0;

	int ans = 0;
	for ( int i = 0; i < n; i++) {
		scanf("%d", &ans);
		if (ans) isCute++;
		else isntCute++;
	}

	if (isCute > (n / 2)) printf("Junhee is cute!");
	else printf("Junhee is not cute!");
}

