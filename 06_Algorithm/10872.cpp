//18.4.10

#include <cstdio>

auto factorial(int n) -> int
{
    if ( n == 0 )
        return 1;
    else if ( n == 1 )
        return 1;
    else
        return n * factorial(n - 1);
}

int main()
{
    int n, k;
    scanf("%d",&n);

    printf("%d", factorial(n));
}