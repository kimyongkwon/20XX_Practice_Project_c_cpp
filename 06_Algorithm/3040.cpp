//18.6.19
//백설 공주와 일곱 난쟁이

#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

vector<int> v;

int main()
{
	v.resize(9, -1);

	for (int i = 0; i < v.size(); i++)
		scanf("%d", &v[i]);

	sort(v.begin(), v.end());

	//9개 중에 7개를 합해서 100이 도출되는지 구하는문제이기때문에
	//9개 중에서 2개를 정해놓고 나머지 7개 for문 돌려서 풀기
	for (int i = 0; i < 8; i++) {
		for (int j = i; j < 9; j++) {
			int sum = 0;
			for (int k = 0; k < 9; k++) {
				if (k != i && k != j)
					sum += v[k];
			}
			if (sum == 100) {
				for (int c = 0; c < 9; c++) {
					if (c != i && c != j)
						printf("%d\n", v[c]);
				}
				return 0;
			}
		}
	}
}