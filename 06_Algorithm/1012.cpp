//18.5.7
//유기농 배추

//재귀함수 버전

//#include <iostream>
//#include <cstdio>
//#include <vector>
//#include <algorithm>
//using namespace std;
//
//bool map[55][55] = { false, };
//bool visited[55][55] = { false, };
//int component = 0;
//
//bool dfs(int i, int j)
//{
//	if (map[i][j] && !visited[i][j])			//배추밭의 i.j지점이 배추가 심어져있고 한번도 방문하지 않았다면 참
//	{
//		visited[i][j] = true;					//해당 지점을 방문한것으로 갱신
//		if ( i+1 < 55) dfs(i + 1, j);			//그리고 상하좌우 4방향으로 깊이우선 탐색을 진행한다.
//		if ( j+1 < 55) dfs(i, j + 1);
//		if ( i-1 >= 0 ) dfs(i - 1, j);
//		if ( j-1 >= 0 ) dfs(i, j - 1);			
//		return true;							//상하좌우 4방향으로 진행하다가 true를 만나더라도 component는 증가하지않는다.
//	}
//	return false;
//}
//
//int main()
//{
//	int t = 0;
//	
//	scanf("%d", &t);
//	for (int i = 0; i < t; i++)
//	{
//		int width = 0, height = 0, n = 0;
//		scanf("%d %d %d", &width, &height, &n);			//배추밭의 가로,세로 넓이와 배추갯수를 입력
//		for (int j = 0; j < n; j++)
//		{
//			int x = 0, y = 0;
//			scanf("%d %d", &x, &y);						//배추 n개의 좌표를 입력
//			map[y][x] = true;							//map이 true면 배추밭에서 배추가 심겨져있다는 의미 
//		}
//
//		component = 0;									//component는 배추흰지렁이 마리수
//		for (int i = 0; i < height; i++)
//		{
//			for (int j = 0; j < width; j++)
//			{
//				if (dfs(i, j))							//dfs가 참이면 (배추밭의 i,j지점이 배추가 심어져있고 한번도 방문하지 않았다면 참)
//					component += 1;						//배추흰지렁이 마릿수를 한마리 추가				
//			}
//		}
//		printf("%d \n", component);
//
//		//다음 testcase를 돌려야하므로 배추밭과 방문여부를 모두 초기화
//		for (int i = 0; i < height; i++)
//		{
//			for (int j = 0; j < width; j++)
//			{
//				map[i][j] = false;
//				visited[i][j] = false;
//			}
//		}
//	}
//}


//스택 버전
//#include<iostream>
//#include<stack>
//#include<vector>
//#include<memory.h>
//using namespace std;
//
//stack<pair<int, int>> S;
//vector<vector<bool>> V;
//
//void DFS(int x, int y)
//{
//	if (x > 0 && V[x - 1][y] == true)				// 해당지점의 왼쪽에 배추가 심겨져있으면 
//	{
//		S.push(pair<int, int>(x - 1, y));			// 그 지점의 좌표를 스택에 저장
//		V[x - 1][y] = false;						// 그리고 false로 변경
//	}
//
//	if (x + 1 < 50 && V[x + 1][y] == true)			// 해당지점의 오른쪽에 배추가 심겨져있으면 
//	{
//		S.push(pair<int, int>(x + 1, y));
//		V[x + 1][y] = false;
//	}
//
//	if (y > 0 && V[x][y - 1] == true)				// 해당지점의 아랫쪽에 배추가 심겨져있으면 
//	{
//		S.push(pair<int, int>(x, y - 1));
//		V[x][y - 1] = false;
//	}
//
//	if (y + 1 < 50 && V[x][y + 1] == true)			// 해당지점의 윗쪽에 배추가 심겨져있으면
//	{
//		S.push(pair<int, int>(x, y + 1));
//		V[x][y + 1] = false;
//	}
//}
//
//int main() {
//
//	int T, x, y, K;
//	int cnt;
//
//	cin >> T;
//
//	while (T--) 
//	{
//		V.clear();
//		cin >> x >> y >> K;
//
//		V.assign(x + 1, vector<bool>(y + 1, false));		// vector<bool>(N+1, false)를 N+1개 할당한다.
//
//		cnt = 0;
//
//		while (K--) 
//		{
//			int x, y;
//			cin >> x >> y;			
//			V[x][y] = true;									// 배추밭 해당 지점에 배추를 심는다.
//		}
//
//		for (int i = 0; i < x; i++) 
//		{
//			for (int j = 0; j < y; j++) 
//			{			
//				if (V[i][j] == true)						// 배추밭을 순회하다가 배추가 발견되면
//				{					
//					cnt++;									// 흰지렁이를 살포!
//					DFS(i, j);								// 흰지렁이를 살포한 지점부터 DFS
//					V[i][j] = false;						// 해당 지점 false
//					while (!S.empty())						// 빈 스택이 아니면
//					{
//						pair<int, int> temp = S.top();		// 차례대로 그 스택을 pop한다.
//						S.pop();							
//						DFS(temp.first, temp.second);		// pop한 정보를 다시 DFS
//															
//						//stack이 빌 때까지 pop하게 된다.
//					}
//				}
//			}		
//		}
//		cout << cnt << endl;
//	}
//	return 0;
//}


//18.5.29
//다시 풀어보기
#include <cstdio>
#include <vector>
#include <queue>
using namespace std;

typedef vector<bool> height;
vector<height> v;
queue<pair<int,int>> q;

int cnt;
int m, n;

//DFS, BFS 모두 배추가 심겨져있는지 배추밭을 검사할떄 
//꼭 범위가 밖으로 넘어가는지 확인해야한다.
//false가 지렁이가 방문했다는 표시다.

//재귀함수로 dfs를 풀었음
void DFS(int x, int y)
{
	v[x][y] = false;

	if (x + 1 < m && v[x + 1][y] == true)
		DFS(x + 1, y);
	if (y + 1 < n && v[x][y + 1] == true)
		DFS(x, y + 1);
	if (x - 1 >= 0 && v[x - 1][y] == true)
		DFS(x - 1, y);
	if (y - 1 >= 0 && v[x][y - 1] == true)
		DFS(x, y - 1);
}

void BFS(int x, int y)
{
	int curX = x, curY = y;
	v[curX][curY] = false;
	q.push({ curX, curY });

	while (!q.empty()) {
		curX = q.front().first;
		curY = q.front().second;
		q.pop();
		if (curX + 1 < m && v[curX + 1][curY] == true) {
			v[curX + 1][curY] = false;
			q.push({ curX + 1, curY });
		}
		if (curY + 1 < n && v[curX][curY + 1] == true) {
			v[curX][curY + 1] = false;
			q.push({ curX, curY + 1});
		}
		if (curX - 1 >= 0 && v[curX - 1][curY] == true) {
			v[curX - 1][curY] = false;
			q.push({ curX - 1, curY });
		}
		if (curY - 1 >= 0 && v[curX][curY - 1] == true) {
			v[curX][curY - 1] = false;
			q.push({ curX, curY -1});
		}
	}
}

void show(void)
{
	for (int i = 0; i < v.size(); i++) {
		for (int j = 0; j < v[i].size(); j++) {
			if (v[i][j] == true){
				cnt += 1;				//이때 지렁이를 +1하고 DFS나 BFS를 수행한다.
				//DFS(i, j);
				BFS(i, j);
			}
		}
	}
}

int main()
{
	int t;
	scanf("%d", &t);

	while (t--){
		scanf("%d %d", &m, &n);					//배추밭 가로 세로 길이 입력

		v.resize(m);							//가로 
		for (int i = 0; i < v.size(); i++)
			v[i].resize(n, false);				//세로
		

		int k = 0;
		int x = 0, y = 0;
		scanf("%d", &k);
		while (k--){
			scanf("%d %d", &x, &y);
			v[x][y] = true;						//x,y좌표에 배추 심기
		}
		
		show();

		printf("%d\n", cnt);
		cnt = 0;
	}
}

