//18.4.9

#include <cstdio>

int main()
{
    int T;
    scanf("%d", &T);

    int n1, n2;
    for (int i = 0; i < T; i++)
    {
        scanf("%d %d", &n1, &n2);
        printf("%d\n", n1 + n2);
    }
}