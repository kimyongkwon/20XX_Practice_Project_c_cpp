//18.4.10

//��������
#include <cstdio>

const int MAX = 1000;

int arr[MAX];

int main()
{
    int n;
    scanf("%d", &n);

    for ( int i = 0; i < n; i++ )
        scanf("%d", &arr[i]);

    for ( int j = 0; j < n; j++ )
    {
        int idx = j;
        for ( int i = j + 1; i < n; i++ )
        {
            if ( arr[i] < arr[idx] )
            {
                idx = i;
            }
        }
        int tmp = arr[idx];
        arr[idx] = arr[j];
        arr[j] = tmp;
    }

    for ( int i = 0; i < n; i++ )
        printf("%d\n", arr[i]);
}