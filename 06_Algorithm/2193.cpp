//18.5.29
//이친수

////바텀업
//#include <cstdio>
//#include <vector>
//using namespace std;
//
//vector<long long> dp;
//
//int main()
//{
//	int n;
//	scanf("%d", &n);
//	dp.resize(95, -1);
//	dp[1] = 1;
//	dp[2] = 1;
//	for (int i = 3; i <= n; i++)
//		dp[i] = dp[i - 1] + dp[i - 2];
//
//	printf("%lld", dp[n]);
//}


//탑 다운
#include <cstdio>
#include <vector>
using namespace std;

vector<long long> dp;
auto f(int n) -> long long
{
	if (n == 2) return 1;
	if (n == 1) return 1;

	if (dp[n] != -1) return dp[n];

	dp[n] = f(n - 1) + f(n - 2);
	return dp[n];
}

int main()
{
	int n;
	scanf("%d", &n);

	dp.resize(n+1, -1);

	printf("%lld", f(n));
}