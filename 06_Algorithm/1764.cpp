//18.5.5
//듣보잡

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
using namespace std;

int main()
{
	vector<string> v;

	int n, m;
	cin >> n >> m;

	//n사이즈만큼 벡터 사이즈 잡기
	v.resize(n);

	//n개만큼 듣도못한 사람 삽입
	for (int i = 0; i < n; i++)
		cin >> v[i];

	//정렬한다 ( 왜? 바이너리 서치해야하기때문에 )
	sort(v.begin(), v.end());

	string tmp;
	vector<string> result;
	//m개만큼 보도못한 사람을 삽입해야한다.
	//이떄 바로 삽입하는게 아니라 듣도못한사람과 비슷한 보도못한 사람을 찾아서 삽입한다
	//바이너리서치로 찾은다음에 result vector에 삽입.
	while (m--)
	{
		cin >> tmp;
		if (binary_search(v.begin(), v.end(), tmp))
			result.push_back(tmp);
	}

	//다시 sort
	sort(result.begin(), result.end());

	cout << result.size() << endl;
	for (auto elem : result)
		cout << elem << endl;
}