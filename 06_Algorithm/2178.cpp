//18.6.1
//미로 탐색

#include <cstdio>
#include <vector>
#include <string>
#include <iostream>
#include <queue>
using namespace std;

typedef vector<char> width;		//가로	
vector<width> v;				//세로
queue<pair<int, int>> q;			//미로 좌표를 보관할 큐

int n, m;
int level = 0;

//int BFS(int x, int y)
//{
//	level++;
//	int curX = x, curY = y;
//	v[curY][curX] = '0';
//	q.push({ curX, curY });
//
//	while (!q.empty()) {
//		int qsize = q.size();
//		for (int i = 0; i < qsize; i++) {
//			curX = q.front().first;
//			curY = q.front().second;
//			q.pop();
//
//			if (curX == m - 1 && curY == n - 1)
//				return level;
//
//			if (curX + 1 < m && v[curY][curX + 1] == '1') {
//				v[curY][curX + 1] = '0';
//				q.push({ curX + 1, curY });
//			}
//			if (curY + 1 < n && v[curY + 1][curX] == '1') {
//				v[curY + 1][curX] = '0';
//				q.push({ curX, curY + 1 });
//			}
//			if (curX - 1 >= 0 && v[curY][curX - 1] == '1') {
//				v[curY][curX - 1] = '0';
//				q.push({ curX - 1, curY });
//			}
//			if (curY - 1 >= 0 && v[curY - 1][curX] == '1') {
//				v[curY - 1][curX] = '0';
//				q.push({ curX,curY - 1 });
//			}
//			
//		}
//		level++;
//	}
//
//	return level;
//
////이건 내가 푼거(오답)
//	//while (!q.empty()) {
//	//	curX = q.front().first;
//	//	curY = q.front().second;
//	//	q.pop();
//	//	if (curX == m - 1 && curY == n - 1)
//	//		break;
//	//	if (curX + 1 < m && v[curY][curX + 1] == '1') {
//	//		v[curY][curX + 1] = '0';
//	//		cnt += 1;
//	//		q.push({ curX + 1, curY });
//	//	}
//	//	if (curY + 1 < n && v[curY + 1][curX] == '1') {
//	//		v[curY + 1][curX] = '0';
//	//		cnt += 1;
//	//		q.push({ curX, curY + 1 });
//	//	}
//	//	if (curX - 1 >= 0 && v[curY][curX - 1] == '1') {
//	//		v[curY][curX - 1] = '0';
//	//		cnt += 1;
//	//		q.push({ curX - 1, curY });
//	//	}
//	//	if (curY - 1 >= 0 && v[curY - 1][curX] == '1') {
//	//		v[curY - 1][curX] = '0';
//	//		cnt += 1;
//	//		q.push({ curX,curY - 1 });
//	//	}
//	//}
//}

int BFS(int x, int y)
{
	int cnt = 0;
	int curX = x, curY = y;
	v[curY][curX] = '0';
	q.push({ curX, curY });
	
	while (!q.empty()) {
		cnt += 1;
		int qsize = q.size();
		for (int i = 0; i < qsize; i++) {
			curX = q.front().first;
			curY = q.front().second;
			q.pop();

			if (curX == m - 1 && curY == n - 1)
				return cnt;

			if (curX + 1 < m && v[curY][curX + 1] == '1') {
				v[curY][curX + 1] = '0';
				q.push({ curX + 1, curY });
			}
			if (curY + 1 < n && v[curY + 1][curX] == '1') {
				v[curY + 1][curX] = '0';
				q.push({ curX, curY + 1 });
			}
			if (curX - 1 >= 0 && v[curY][curX - 1] == '1') {
				v[curY][curX - 1] = '0';
				q.push({ curX - 1, curY });
			}
			if (curY - 1 >= 0 && v[curY - 1][curX] == '1') {
				v[curY - 1][curX] = '0';
				q.push({ curX, curY - 1 });
			}
		}
	}

	return cnt;
}

int main()
{
	scanf("%d %d", &n, &m);		//n이 세로, m이 가로

	//미로 크기 할당
	v.resize(n);
	for (int i = 0; i < v.size(); i++)
		v[i].resize(m, -1);

	//입력받을 때 한줄을 문자열로 입력받는다
	string str;
	for (int i = 0; i < v.size(); i++) {
		cin >> str;
		for (int j = 0; j < str.size(); j++)
			v[i][j] = str[j];			//문자열의 요소들을 v에 대입한다.
	}

	printf("%d", BFS(0, 0));
}