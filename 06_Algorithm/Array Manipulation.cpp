// 18.9.11
// Arrays
// Array Manipulation

#include <iostream>
#include <vector>
using namespace std;

int main()
{
	int n, queries;
	scanf("%d %d", &n, &queries);
	vector<long long> v;

	v.resize(n, 0);

	// 배열의 요소마다 매번 k값을 더해주면 시간초과가 발생한다.
	// 배열의 요소는 변하는 기울기량을 저장해줘야한다.
	int start, end;
	long long k = 0;
	for (size_t i = 0; i < queries; i++)
	{
		scanf("%d %d %lld", &start, &end, &k);
		
		// start-1부터 k만큼 증가되므로 시작점인 start-1에 k를더해준다.
		v[start-1] += k;
		// 그리고 start-1부터 end-1까지 기울기가 같으므로 
		// end부터는 기울기가 변한다. end에 k를 감소시킨다 
		v[end] -= k;
	}

	long long sum = 0;
	long long max = 0;
	// 이제는 v를 순회하면서 기울기를 더한다.
	// 더하면서 나오는 가장 큰 수를 구한다.
	for (size_t i = 0; i < v.size(); i++)
	{
		sum += v[i];
		if (sum > max)
			max = sum;
	}

	cout << max << endl;
}