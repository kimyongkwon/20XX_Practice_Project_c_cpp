//18.3.10

#include <stdio.h>

int main()
{
    int n;
    float arr[1000];
    int m = 0;
    float sum = 0;

    scanf("%d", &n);

    for (int i = 0; i < n; i++)
    {
        scanf("%f", &arr[i]);
        if ( m < arr[i] )
            m = arr[i];
    }

    for (int i = 0; i < n; i++)
    {
        sum += arr[i] / m * 100;
    }

    printf("%f", (sum / n));
}