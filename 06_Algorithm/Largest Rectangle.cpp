// 18.9.12
// Stacks and Queues
// Largest Rectangles

#include <vector>
#include <iostream>
#include <stack>

using namespace std;

// Complete the largestRectangle function below.
long largestRectangle(vector<int> h) {
	stack<int> s;

	int max_area = 0;
	int tp;
	int area_with_top;
	int i = 0;
	for (i = 0; i < h.size();)
	{
		if (s.empty() || h[s.top()] <= h[i]) {
			s.push(i);
			i++;
		}

		else
		{
			tp = s.top();
			s.pop();

			int tmp = 0;
			if (s.empty())
				tmp = i;
			else 
				tmp = i - s.top() - 1;

			area_with_top = h[tp] * tmp;

			if (max_area < area_with_top)
				max_area = area_with_top;
		}
	}

	while (s.empty() == false)
	{
		tp = s.top();
		s.pop();

		int tmp = 0;
		if (s.empty())
			tmp = i;
		else 
			tmp = i - s.top() - 1;

		area_with_top = h[tp] * tmp;

		if (max_area < area_with_top)
			max_area = area_with_top;
	}

	return max_area;
}

int main()
{
	int n;
	cin >> n;

	vector<int> h(n);

	int tmp;
	for (int i = 0; i < n; i++) {
		cin >> tmp;
		h[i] = tmp;
	}

	long result = largestRectangle(h);

	cout << result << "\n";

	return 0;
}

