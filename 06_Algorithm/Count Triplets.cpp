// 18.9.10
// Dictionaries and Hashmaps
// Count Triplets
// 이것도 개어려움.. 이해못했다 ㅠㅠ

#include <iostream>
#include <unordered_map>
#include <map>
#include <algorithm>
using namespace std;

int arr[100005];
int num;

//int CountTriplets(int mul, unordered_map<int, int>& hash)
//{
//	int cnt = 0;
//	for (size_t i = 0; i < num; i++)
//	{
//		int tmp = arr[i];
//		if (hash[tmp*mul] > 0 && hash[tmp*mul*mul] > 0) {
//			cnt += hash[tmp*mul] * hash[tmp*mul*mul];
//			if (mul == 1)
//				cnt -= i;
//		}
//	}
//	return cnt;
//}
//
//int main()
//{
//	int mul;
//	scanf("%d %d", &num, &mul);
//	unordered_map<int, int> hash;
//	
//	for (size_t i = 0; i < num; i++)
//	{
//		int tmp;
//		scanf("%d", &tmp);
//		arr[i] = tmp;
//		hash[tmp] += 1;
//	}
//
//	cout << CountTriplets(mul, hash) << endl;
//}

//-----------------------------------------------------

long long CountTriplets(int mul, int* arr)
{
	unordered_map<long long, long long> umap1, umap2;

	int tmp = 0;
	// umap1에 arr요소들과 갯수를 저장한다.
	for (size_t i = 0; i < num; i++)
	{
		tmp = arr[i];
		if (umap1[tmp] > 0)
			umap1[tmp] += 1;
		else
			umap1[tmp] = 1;
	}

	long long cnt = 0;
	// 그리고 umap1에 있는 요소들을 다시 꺼내서
	for (size_t i = 0; i < num; i++)
	{
		tmp = arr[i];
		umap1[tmp] -= 1;

		// 그 요소가 mul의 배수인지 확인한다.
		// 만약 배수면
		if (tmp % mul == 0)
		{
			// mul로 나누고 곱한값이 
			if (umap2[tmp / mul] && umap1[tmp  * mul] )
				cnt += (umap2[tmp / mul]) * (umap1[tmp * mul]);
		}

		// umap2에 저장.
		if (umap2[tmp] > 0)
			umap2[tmp] += 1;
		else
			umap2[tmp] = 1;
	}
	return cnt;

	/*long cnt = 0;
	for (int i = 0; i < num; ++i) {
		if (hash2.find(arr[i]) != hash2.end()) {
			cnt += hash2[arr[i]];
		}
		if (hash1.find(arr[i]) != hash1.end()) {
			hash2[arr[i] * mul] += hash1[arr[i]];
		}
		hash1[arr[i] * mul]++;
	}*/
}

int main()
{
	int mul;
	scanf("%d %d", &num, &mul);

	for (size_t i = 0; i < num; i++)
	{
		int tmp;
		scanf("%d", &tmp);
		arr[i] = tmp;
	}

	cout << CountTriplets(mul, arr) << endl;
}

