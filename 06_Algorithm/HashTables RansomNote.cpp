// 18.9.4
// dictionaries and hashmaps

// note에 있는 모든 단어들이 magazine에 1번씩 있으면 Yes
// note에 있는 모든 단어들이 magazine에 없으면 No
// (만약 note에 같은 단어가 두개 있었으면 magazine에도 두번 있어야한다)

#include <cstdio>
#include <iostream>
#include <unordered_map>
#include <vector>
#include <string>
using namespace std;

void checkMagazine(vector<string>& magazine, vector<string>& note) 
{
	unordered_map<string, int> hash;

	// magazine에 있는 단어들을 하나씩 꺼내서
	// hash라는 unordered_map에 넣는다.
	for (string &s : magazine) {
		hash[s]++;
	}
	// 그리고 note에 있는 단어들을 하나씩 꺼내서
	for (string &s : note) {
		// hash에 그 단어가 있는지 확인한다. 그리고 -1해준다.
		// 단어가 있는지는 int 값이 1이면 있다는 의미다.
		if (hash[s] > 0) {
			hash[s]--;
		}
		// note에 있는 단어들중 하나라도 magazine에 없으면
		// "No" 출력
		else {
			cout << "No" << endl;
			return;
		}
	}
	cout << "Yes" << endl;
}

int main()
{
	vector<string> magazine;
	vector<string> note;

	int m, n;
	scanf("%d %d", &m, &n);

	magazine.resize(m, "");
	note.resize(n, "");

	for (int i = 0; i < m; i++)
		std::cin >> magazine[i];

	for (int j = 0; j < n; j++)
		std::cin >> note[j];

	checkMagazine(magazine, note);
}
