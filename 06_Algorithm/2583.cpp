//18.5.15
//영역 구하기

//#include <iostream>
//#include <stack>
//#include <vector>
//#include <algorithm>
//using namespace std;
//
//stack<pair<int, int>> S;
//vector<vector<bool>> Map;
//vector<int> Score;
//
//int width, height;
//
//void DFS(int y, int x)
//{
//	if (x > 0 && Map[y][x - 1] == false)				
//	{
//		S.push(pair<int, int>(y, x - 1));		
//		Map[y][x - 1] = true;
//	}
//
//	if (x + 1 < width && Map[y][x + 1] == false)
//	{
//		S.push(pair<int, int>(y, x + 1));
//		Map[y][x + 1] = true;
//	}
//
//	if (y > 0 && Map[y - 1][x] == false)				
//	{
//		S.push(pair<int, int>(y - 1, x));
//		Map[y - 1][x] = true;
//	}
//
//	if (y + 1 < height && Map[y + 1][x] == false)
//	{
//		S.push(pair<int, int>(y + 1, x));
//		Map[y + 1][x] = true;
//	}
//}
//
//int main()
//{
//	int t, x1, y1, x2, y2;
//	
//	scanf("%d %d %d", &height, &width, &t);
//
//	int cnt = 0;
//
//	Map.clear();
//	Map.assign(height, vector<bool>(width, false));
//
//	while (t--){
//		scanf("%d %d %d %d", &x1, &y1, &x2, &y2);
//		for (int j = y1; j < y2; j++){
//			for (int i = x1; i < x2; i++){
//				Map[j][i] = true;
//			}
//		}
//	}
//	
//	for (int j = 0; j < height; j++){
//		for (int i = 0; i < width; i++){
//			if (Map[j][i] == false)
//			{
//				cnt++;
//				DFS(j, i);
//				Map[j][i] = true;
//				Score.push_back(1);
//				while (!S.empty()){
//					pair<int, int> tmp = S.top();
//					S.pop();
//					Score[cnt-1]++;
//					DFS(tmp.first, tmp.second);
//				}
//			}
//		}
//	}
//	printf("%d \n", cnt);	
//
//	sort(Score.begin(), Score.end());
//	
//	for (auto elem : Score)
//		cout << elem << " ";
//}


//18.5.30
//다시 풀어보기

#include <cstdio>
#include <vector>
#include <algorithm>
#include <queue>
using namespace std;

typedef vector<bool> height;
vector<height> v;
queue<pair<int,int>> q;

int m, n, k;

vector<int> range;
int range_n = 0;

void DFS(int x, int y)
{
	v[x][y] = true;
	range[range_n] += 1;

	if (x + 1 < n && v[x + 1][y] == false) {
		v[x+1][y] = true;
		DFS(x + 1, y);
	}
	if (y + 1 < m && v[x][y + 1] == false) {
		v[x][y+1] = true;
		DFS(x, y + 1);
	}
	if (x - 1 >= 0 && v[x - 1][y] == false) {
		v[x-1][y] = true;
		DFS(x - 1, y);
	}
	if (y - 1 >= 0 && v[x][y - 1] == false) {
		v[x][y-1] = true;
		DFS(x, y - 1);
	}
}

void BFS(int x, int y)
{
	int curX = x, curY = y;
	v[curX][curY] = true;
	q.push({ curX,curY });
	range[range_n] += 1;

	while (!q.empty()) {
		curX = q.front().first;
		curY = q.front().second;
		q.pop();
		if (curX + 1 < n && v[curX + 1][curY] == false) {
			v[curX+1][curY] = true;
			q.push({ curX+1,curY });
			range[range_n] += 1;
		}
		if (curY + 1 < m && v[curX][curY + 1] == false) {
			v[curX][curY+1] = true;
			q.push({ curX,curY+1 });
			range[range_n] += 1;
		}
		if (curX - 1 >= 0 && v[curX - 1][curY] == false) {
			v[curX-1][curY] = true;
			q.push({ curX-1,curY });
			range[range_n] += 1;
		}
		if (curY - 1 >= 0 && v[curX][curY - 1] == false) {
			v[curX][curY-1] = true;
			q.push({ curX,curY-1 });
			range[range_n] += 1;
		}
	}
}

void show(void)
{
	//i = 가로 = x 
	//j = 세로 = y
	for (int i = 0; i < v.size(); i++) {
		for (int j = 0; j < v[i].size(); j++) {
			if (v[i][j] == false) {
				range.push_back(0);
				BFS(i, j);
				//DFS(i,j);
				range_n += 1;
			}
		}
	}
}

int main()
{
	scanf("%d %d %d", &m, &n, &k);					// m : 세로, n : 가로

	v.resize(n);									// 눈금종이 가로 세팅
	for (int i = 0; i < v.size(); i++)		
		v[i].resize(m,false);						// 눈금종이 세로 세팅

	int x1 = 0, y1 = 0, x2 = 0, y2 = 0;
	while (k--) {
		scanf("%d %d %d %d", &x1, &y1, &x2, &y2);

		for (int i = x1; i < x2; i++) {
			for (int j = y1; j < y2; j++) {
				v[i][j] = true;
			}
		}
	}

	show();

	printf("%d\n", range.size());
	sort(range.begin(), range.end());
	for (auto elem : range)
		printf("%d ", elem);
	printf("\n");
}