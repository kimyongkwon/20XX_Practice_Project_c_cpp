// 18.9.28
// Graphs
// DFS Connected Cell in a Grid

#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

vector<vector<int>> grid;
vector<int> v;

int n, m;
int cnt = 0;

void DFS(int y, int x)
{
	grid[y][x] = 0;
	cnt += 1;
	// 오른쪽
	if (x < m - 1 && grid[y][x + 1] == 1)
		DFS(y, x + 1);
	// ↘
	if (x < m - 1 && y < n - 1 && grid[y + 1][x + 1] == 1)
		DFS(y+1, x + 1);
	// 왼쪽
	if (x > 0 && grid[y][x - 1] == 1) 
		DFS(y, x - 1);
	// ↙
	if (x > 0 && y < n - 1 && grid[y+1][x - 1] == 1)
		DFS(y+1, x - 1);
	// 아래
	if (y < n - 1 && grid[y + 1][x] == 1) 
		DFS(y + 1, x);
	// ↖
	if (y > 0 && x > 0 && grid[y - 1][x -1] == 1) 
		DFS(y - 1, x-1);
	// 위
	if (y > 0 && grid[y - 1][x] == 1)
		DFS(y - 1, x);
	// ↗
	if (y > 0 && x < m - 1 && grid[y - 1][x + 1] == 1) 
		DFS(y - 1, x + 1);
}

// Complete the maxRegion function below.
int maxRegion(vector<vector<int>> grid) 
{

	for (int i = 0; i < grid.size(); ++i)
	{
		for (int j = 0; j < grid[i].size(); ++j)
		{
			if (grid[i][j] == 1) {
				cnt = 0;
				DFS(i, j);
				v.push_back(cnt);
			}
		}
	}

	return *max_element(v.begin(), v.end());;
}

int main()
{
	cin >> n >> m;

	grid.resize(n);
	for ( int i = 0; i < n; ++i)
	{
		grid[i].resize(m, -1);
	}
	
	for ( int i = 0; i < n; ++i )
	{
		for (int j = 0; j < m; ++j)
		{
			cin >> grid[i][j];
		}
	}

	cout << maxRegion(grid) << endl;

}