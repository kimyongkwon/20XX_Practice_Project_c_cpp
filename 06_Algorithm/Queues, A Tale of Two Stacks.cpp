// 18.9.12
// Stacks and Queues
// Queues: A Tale of Two Stacks

#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <stack>
#include <queue>
using namespace std;

class MyQueue {

public:
	// oldest가 LIFO역할을 한다.
	stack<int> stack_newest_on_top, stack_oldest_on_top;
	void push(int x) {
		// newest에 push
		stack_newest_on_top.push(x);

		if (stack_oldest_on_top.empty()) {
			stack_oldest_on_top = stack<int>();

			for (int i = 0, nSize = stack_newest_on_top.size(); i < nSize; ++i) {
				stack_oldest_on_top.push(stack_newest_on_top.top());
				stack_newest_on_top.pop();
			}
		}
	}

	void pop() {
		// oldest에 pop
		stack_oldest_on_top.pop();

		// pop을 할 때는 oldest에 거꾸로 넣어야한다.
		if (stack_oldest_on_top.empty()) {
			stack_oldest_on_top = stack<int>();

			// 그러므로 newest크기만큼 순회 돌면서
			// 하나씩 꺼내서 oldest에 push해준다.
			for (int i = 0, nSize = stack_newest_on_top.size(); i < nSize; ++i) {
				stack_oldest_on_top.push(stack_newest_on_top.top());
				// 그리고 한개씩 push할때마다 newest에서 pop해준다.
				stack_newest_on_top.pop();
			}
		}
	}

	int front() {
		return stack_oldest_on_top.top();
	}
};

int main() {
	MyQueue q1;
	int q, type, x;
	cin >> q;

	for (int i = 0; i < q; i++) {
		cin >> type;
		if (type == 1) {
			cin >> x;
			q1.push(x);
		}
		else if (type == 2) {
			q1.pop();
		}
		else cout << q1.front() << endl;
	}
	/* Enter your code here. Read input from STDIN. Print output to STDOUT */
	return 0;
}