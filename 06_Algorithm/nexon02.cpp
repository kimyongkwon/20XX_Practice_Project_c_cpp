#include <iostream>
#include <vector>
using namespace std;

void calculate(vector <int> arr)
{
    int chocolate;
    for ( int days : arr)
    {
        chocolate = 0;
        while (days)
        {
            if (days % 2 == 1){
                chocolate += days;
            }
            days--;
        }
        cout << chocolate << endl;
    }
}

int main()
{
    int arr_size = 0;
    cin >> arr_size;
    cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

    vector<int> arr;
    for ( int i = 0; i < arr_size; i++)
    {
        int arr_item;
        cin >> arr_item;
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
        arr.push_back(arr_item);
    }

    calculate(arr);
    
    return 0;
 
}