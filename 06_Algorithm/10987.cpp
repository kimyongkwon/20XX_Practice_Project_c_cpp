//18.5.5
//모음의 개수

#include <iostream>
#include <string>
using namespace std;

int main()
{
	string str;

	cin >> str;

	int cnt = 0;
	for (int i = 0; i < str.size(); i++)
	{
		if (str[i] == 'i' || str[i] == 'a'
			|| str[i] == 'e' || str[i] == 'o' || str[i] == 'u')
			cnt++;
	}

	printf("%d", cnt);
}