// 18.9.27
// Dynamic Programming
// Max Array Sum

//#include <iostream>
//#include <vector>
//#include <algorithm>
//using namespace std;
//
//vector<int> dp;
//
//// Complete the maxSubsetSum function below.
//int maxSubsetSum(vector<int> arr) {
//
//	dp[0] = max(0, arr[0]);
//
//	if (arr.size() == 1)
//		return dp[0];
//	for (int i = 1; i < arr.size(); i++)
//	{
//		dp[i] = max(dp[i - 2], max(dp[i - 1], dp[i - 2] + arr[i]));
//	}
//	int n = arr.size();
//	return max(dp[n - 1], dp[n - 2]);
//}
//
//int main()
//{
//	int num;
//	cin >> num;
//	
//	vector<int> arr;
//	arr.resize(num, 0);
//
//	for (int i = 0; i < num; ++i)
//		cin >> arr[i];
//
//	dp.resize(100005, 0);
//
//	cout << maxSubsetSum(arr) << endl;
//}

#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

vector<int> dp;

// Complete the maxSubsetSum function below.
int maxSubsetSum(vector<int> arr) 
{
	dp.resize(arr.size() + 2, 0);

	dp[0] = 0;
	dp[1] = 0;

	int Max = 0;
	for (int i = 2; i < dp.size(); ++i)
		dp[i] = max(dp[i - 1], arr[i - 2] + dp[i - 2]);

	return dp[dp.size()-1];
}

int main()
{
	int n;
	cin >> n;

	vector<int> v;
	v.resize(n, 0);
	
	for (int i = 0; i < v.size(); ++i)
		cin >> v[i];

	cout << maxSubsetSum(v) << endl;
}