//18.3.30

#include <stdio.h>

int main()
{
    int n = 0;
    
    scanf("%d", &n);

    //입력값(n)이 1일 경우에는 1을 바로 출력하고 프로그램을 끝낸다.
    if ( n == 1 )
    {
        printf("1");
        return 0;
    }

    //min과 max까지의 벌집방은 같은 분류의 벌집이다.
    int min = 2;
    int max = 7;

    //cnt는 n까지 몇 개의 방을 지나가는지 표현하는 변수다.
    //n이 1일때는 위에서 예외처리해줬으므로 2부터 시작한다.
    int cnt = 2;

    while (1)
    {
        //n이 min과 max사이에 존재한다면 루프 빠져나와서 프로그램 종료
        if ( min <= n && n <= max )
            break;
        //만약 min과 max에 존재하지않는다면 현재 벌집라인에 존재하지 않는다는 의미이므로
        //다음 벌집라인으로 min과 max를 갱신하다.
        //그리고 cnt도 당근 증가시킨다.
        else
        {
            min = max + 1;      //min은 max+1
            max += 6 * cnt;     //벌집라인의 갯수는 6개씩 추가로 증가한다.
            cnt++;
        }
    }

    printf("%d", cnt);
}
   
