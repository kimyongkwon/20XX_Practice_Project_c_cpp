//18.4.10
//이항 계수 2

//파스칼 삼각형을 이용해서 풀면 된다.
//nCr = n-1Cr-1 + n-1Cr
//개인적으로 어려움, 물론 다른 블로그를 참고했다.
//

//참고로 mod연산자에서 주의해야할 식이 있다.
//(A+B) % MOD = (A%MOD + B%MOD) % MOD 이다.
//흔히 하는 실수 중에서
//(A+B) % MOD가 (A%MOD + B%MOD) 와 같다고 생각하는 것이다. 하지만 틀리다.
//예를 들어 9%10 + 9%10와 (9+9)%10이 같냐?

//바텀업 방식
//#include <stdio.h>
//#define MOD 10007
//#define NMAX 1001
//
//typedef long long int ll;
//
//ll dp[NMAX][NMAX];
//
//auto makePascalTriangle(int n, int r) -> void
//{
//    dp[0][0] = 1;
//    for ( int i = 1; i <= n; i++ )
//    {
//        dp[i][0] = 1;           //삼각형의 가장 왼쪽 변에 있는 수들은(nC0) 모두 값이 1이다. (파스칼삼각형의 성질)
//        for ( int j = 1; j <= i; j++ ){
//            dp[i][j] = (dp[i - 1][j] + dp[i - 1][j - 1]) % MOD;         //삼각형의 왼쪽변부터 오른쪽변까지 수들을 차례대로 파스칼삼각형의 성질(nCr = n-1Cr-1 + n-1Cr)로 구한다.
//        }
//    }
//}
//
//int main() 
//{
//    int n, k;
//    scanf("%d %d", &n, &k);
//    makePascalTriangle(n, k);
//    printf("%lld", dp[n][k]);
//    
//    //위의 MOD를 없애고 밑과 같이 코드를 짜면 당근 안됨
//    //왜냐? MOD연산을 하기도 전에 dp[n][k]에서 이미 오버플로우가 나기때문이다.
//    //그렇기때문에 위의 함수에서 수를 하나하나 구할떄마다 미리미리 MOD연산을 해주는 것이다.
//    //printf("%lld", dp[n][k] % MOD);
//}


//탑다운 방식
#include <stdio.h>
#define MOD 10007
#define NMAX 1001

typedef long long int ll;

ll dp[NMAX][NMAX];

ll makePascalTriangle(ll n, ll r)
{
    //삼각형의 가장 왼쪽 변에 있는 수들은(nC0) 모두 값이 1이다. (파스칼삼각형의 성질)
    //또한 삼각형의 가장 오른쪽 변에 있는 수들도(n=r) 모두 값이 1이다. (파스칼삼각형의 성질)
    if ( n == r || r == 0 ) return 1;
    if ( r == 1 ) return n;         //r이 1인 수는 모두 값이 n이다. (파스칼삼각형의 성질)
    
    ll temp1, temp2;

    //n-1Cr-1을 구하는 과정
    if ( dp[n - 1][r - 1] != 0 ) temp1 = dp[n - 1][r - 1];
    else
    {
        dp[n - 1][r - 1] = makePascalTriangle(n - 1, r - 1);
        temp1 = dp[n - 1][r - 1];
    }

    //n-1Cr을 구하는 과정
    if ( dp[n - 1][r] != 0 ) temp2 = dp[n - 1][r];
    else
    {
        dp[n - 1][r] = makePascalTriangle(n - 1, r);
        temp2 = dp[n - 1][r];
    }

    return (temp1 + temp2) % MOD;
}

int main() 
{
    int n, k;

    scanf("%d %d", &n, &k);
    printf("%lld", makePascalTriangle(n, k));
}












