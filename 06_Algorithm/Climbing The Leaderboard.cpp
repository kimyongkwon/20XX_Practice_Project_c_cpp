#include <iostream>
#include <vector>
using namespace std;

vector<int> func(vector<int> scores, vector<int> alice)
{
	vector<int> v;
	v.resize(alice.size(), -1);

	for (int i = 0; i < alice.size(); i++) {
		int cnt = 1;
		int tmp = alice[i];
		int prev = 0;
		for (int j = 0; j < scores.size(); j++) {

			if (prev == scores[j])
				continue;

			//제일 앞에 올때
			if (scores[j] < tmp) {
				v[i] = cnt;
				break;
			}

			if (scores[j] > tmp) {
				prev = scores[j];
				cnt += 1;
			}
			else
				break;
				
		}
		v[i] = cnt;
	}

	return v;
}

int main()
{
	int scores_n;
	cin >> scores_n;
	vector<int> scores;
	scores.resize(scores_n, -1);
	for (size_t i = 0; i < scores.size(); i++)
		cin >> scores[i];

	int alice_n;
	cin >> alice_n;
	vector<int> alice;
	alice.resize(alice_n, -1);
	for (size_t i = 0; i < alice.size(); i++)
		cin >> alice[i];

	vector<int> v;
	v = func(scores, alice);

	for (int i = 0 ; i < v.size(); i++)
		cout << v[i] << endl;
}