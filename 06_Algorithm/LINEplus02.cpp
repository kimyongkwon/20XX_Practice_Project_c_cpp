#include <cstdio>
#include <vector>
#include <utility>
#include <string>
#include <algorithm>
using namespace std;

typedef struct Tmp_Shop
{
    char name[BUFSIZ];
    double shop_x, shop_y;
    int coupon_count;
    int priority;
    int distance;
}SHOP;

/*
* Time complexity: TODO
* Space complexity: TODO
*/

bool comp(const SHOP& a, const SHOP& b)
{
    if ( a.distance < b.distance )
    {
        if ( a.distance == b.distance)
        {
            if ( a.coupon_count < b.coupon_count)
                return true;
        }
        return true;
    }
    
    return false;
}

int Distance(double user_x, double user_y, double shop_x, double shop_y)
{
    int dis = sqrt(pow(abs(user_x - shop_x), 2) + pow(abs(user_y - shop_y), 2));
    dis -= dis % 100;
    return dis;
}

int main(int argc, const char *argv[]) 
{
    int N;
    double x, y;
    scanf("%lf %lf %d\n", &x, &y, &N);

    SHOP * tmp = new SHOP[N];

    int shop;
    for ( int i = 0; i < N; ++i )
    {
        char name[BUFSIZ];
        double shop_x, shop_y;
        int coupon_count;
        scanf("%lf %lf %s %d\n", &shop_x, &shop_y, name, &coupon_count);

        strcpy(tmp[i].name, name);
        tmp[i].shop_x = shop_x;
        tmp[i].shop_y = shop_y;
        tmp[i].priority = 0;
        tmp[i].coupon_count = coupon_count;
        tmp[i].distance = Distance(x, y, shop_x, shop_y);
    }

    sort(tmp, tmp + N, comp);

    for ( int j = 0; j < N; j++)
    {
        printf("%g %g %s %d \n", tmp[j].shop_x, tmp[j].shop_y, tmp[j].name, tmp[j].coupon_count);
    }

    return 0;
}