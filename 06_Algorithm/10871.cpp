//18.3.10

//풀이1
//하지만 메모리가 너무 많이 소비되는 문제점이 있다.
//#include <stdio.h>
//
//int main()
//{
//    int n, x;
//    int arr[10000];
//
//    scanf("%d %d", &n, &x);
//    
//    for ( int i = 0 ; i < n; i++){
//        scanf("%d ", &arr[i]);
//    }
//
//
//    for ( int i = 0 ; i < n; i++){
//        if ( arr[i] < x )
//            printf("%d ", arr[i]);
//    }
//}

//풀이2
//#include <stdio.h>
//#include <stdlib.h>
//int main()
//{
//    int n, x;
//
//    scanf("%d %d", &n, &x);
//
//    int * a = (int*)malloc(sizeof(int) * n);
//
//    for ( int i = 0; i < n; i++ )
//    {
//        scanf("%d", &a[i]);
//    }
//
//    for ( int i = 0; i < n; i++ )
//    {
//        if ( a[i] < x )
//            printf("%d ", a[i]);
//    }
//}

#include <stdio.h>
int main()
{
    int n, x;
    int tmp;

    scanf("%d %d", &n, &x);

    for ( int i = 0; i < n; i++ )
    {
        scanf("%d", &tmp);
        if ( tmp < x )
            printf("%d ", tmp);
    }
}