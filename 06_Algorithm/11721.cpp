#include <stdio.h>

int main()
{
    char str[100] = { 0,};

    scanf("%s", str);

    for ( int i = 0; i < 100; ++i )
    {
        if ( str[i] == '\0' ) break;
        printf("%c", str[i]);
        if ( i % 10 == 9 ) printf("\n");

    }
}

// 다른 답
//for문안에 중단문을 넣어주니 더 깔끔

//#include <stdio.h>
//
//int main()
//{
//    char str[100] = { 0, };
//
//    scanf("%s", str);
//
//    for ( int i = 0; str[i] != NULL; ++i )
//    {
//        printf("%c", str[i]);
//        if ( i % 10 == 9 ) printf("\n");
//    }
//}


//다른답2
//cstring헤더를 선언하고 strlen함수를 사용

//#include <stdio.h>
//#include <cstring>
//
//int main()
//{
//    char str[100] = { 0, };
//
//    scanf("%s", str);
//
//    for ( int i = 0; i < strlen(str); ++i )
//    {
//        printf("%c", str[i]);
//        if ( i % 10 == 9 ) printf("\n");
//    }
//}


