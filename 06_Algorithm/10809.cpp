//18.3.22

//#include <stdio.h>
//
//int main()
//{
//    char str[100] = {0,};
//    scanf("%s", str);
//
//    char arr[] = { 'a', 'b', 'c', 'd', 'e',
//        'f', 'g', 'h', 'i', 'j', 'k', 'l',
//        'm', 'n', 'o', 'p', 'q', 'r', 's',
//        't', 'u', 'v', 'w', 'x', 'y', 'z' };
//
//    int arr2[26];
//
//    for ( int k = 0; k < 26; k++ )
//        arr2[k] = -1;
//
//    for ( int i = 0; str[i] != '\0'; i++)
//    {
//        for ( int j = 0; j < 26; j++)
//        {
//            if ( str[i] == arr[j] )
//                if ( arr2[j] == -1 )
//                    arr2[j] = i;
//        }
//    }
//
//    for ( auto elem : arr2 )
//        printf("%d ", elem);
//}

#include <stdio.h>

int main()
{
    char str[100] = { 0, };
    scanf("%s", str);

    //알파벳 26개를 나타내는 배열
    int arr[26];

    //알파벳 배열을 다 -1로 초기화
    for ( int k = 0; k < 26; k++ )
        arr[k] = -1;

    //문자열을 널값까지 확인하면서
    for ( int i = 0; str[i] != '\0'; i++ )
    {
        //해당 알파벳이 있는지 for문으로 확인한다
        for ( int j = 0; j < 26; j++ )
        {
            if ( str[i] - 'a' == j )            // 해당 알파벳에서 'a'을 빼면 알파벳순서가 나온다.(ex, a는 0, b는 1, c는 2) 이것이 알파벳배열의 인덱스와 같으면 해당 알파벳이 존재하는것이다.
                if ( arr[j] == -1 )             // 같은 문자가 여러번 나오면 처음 문자의 자리만 적용되도록
                    arr[j] = i;                 // arr에 해당 알파벳이 존재하는 문자열 자리를 저장
        }
    }

    for ( auto elem : arr )
        printf("%d ", elem);
}