//18.5.15
//DFS와 BFS

//#include <cstdio>
//#include <vector>
//#include <stack>
//#include <queue>
//#include <algorithm>
//using namespace std;
//
//vector<vector<int>> Vertex;			//정점을 표현하는 벡터(하나의 요소는 인접한 정점들의 요소다)
//vector<bool> Visited;				//정점들의 방문여부를 관리하는 벡터
//stack<int> Stack;
//queue<int> Q;
//
//void GraphInit(int v_num)
//{
//	Vertex.resize(v_num + 1);				// 정점은 1번부터 N까지이다.(0번 정점은 없다)
//	Visited.resize(v_num + 1);
//}
//
//void AddEdge(int v1, int v2)
//{
//	Vertex[v1].push_back(v2);
//	Vertex[v2].push_back(v1);
//}
//
//void SortList(void)
//{
//	for (int i = 0; i < Vertex.size(); i++)
//		sort(Vertex[i].begin(), Vertex[i].end());
//}
//
//void DFS(int v)
//{
////---------- 로직1(재귀로 풀었음)
//	//int cur = v;
//	//Visited[cur] = true;
//	//printf("%d ", cur);
//
//	//for (auto elem : Vertex[cur]) {
//	//	if (!Visited[elem])
//	//		DFS(elem);
//	//}
//
//
////----------- 로직2(스택을 사용해서)
//	int cur = v;
//	Visited[cur] = true;
//	printf("%d ", cur);
//	Stack.push(cur);
//
//	while (!Stack.empty()){
//		cur = Stack.top();
//		Stack.pop();
//		for (auto elem : Vertex[cur]) {					// 현재 정점(cur)의 인접정점들중에서
//
//			if (Visited[elem] == false) {				// 방문하지 않았던 정점이라면
//				Visited[elem] = true;					// 방문했다고 갱신하고
//				Stack.push(cur);						// 현재 정점과
//				Stack.push(elem);						// 인접 정점을 스택에 push
//				printf("%d ", elem);
//				break;									// 그리고 빠져나온다~
//			}
//														// 만약 인접정점이 방문했던 정점이라면 위의 if문을 들어가지 않을것이고
//														// 한번도 들어가지 않으면 자동으로 for문을 빠져나와 스택을 확인 후 
//														// 부모노드를 pop하거나 while문을 종료할것이다.
//		}
//	}
//
////----------- 불량로직
//	//int cur = v;
//	//Visited[cur] = true;
//	//printf("%d ", cur);
//
//	//while (1){
//	//	int idx = 0;
//
//	//	for (auto elem : Vertex[cur]){
//	//		if (Visited[elem] == false){	//인접한 정점이 한번도 방문되지 않았으면
//	//			Visited[elem] = true;		// 방문하고
//	//			Stack.push(cur);			// 현재 정점의 인덱스(cur)를 스택에 push
//	//			cur = elem;					// 현재 정점을 방문한 정점으로 갱신한다.
//	//			printf("%d ", cur);			// for문 탈출
//	//			break;
//	//		}
//	//		idx += 1;						//만약 Vertex의 인접정점이 이미 방문했었다면
//	//										//idx가 +1 할것이다.
//	//	}
//
//	//	//현재 정점의 인접정점을 다 순회해봤는데, 이미 전에 다 방문했다는 의미다.
//	//	if (idx == Vertex[cur].size()){
//	//		if (Stack.empty())				//만약 스택이 비었다면 모든 정점들을 다 순회했다는 의미
//	//			break;
//	//		else {
//	//			cur = Stack.top();			//스택이 비지않았다면 Stack의 요소를 top해서 현재 정점으로 갱신한다.
//	//			Stack.pop();			
//	//		}
//	//	}
//	//}
//}
//
//void BFS(int v)
//{
////---------- 로직1
//	//int cur = v;
//	//Visited[cur] = true;
//	//printf("%d ", cur);
//
//	//while (1){
//	//	for (auto elem : Vertex[cur]){
//	//		if (Visited[elem] == false){
//	//			Visited[elem] = true;
//	//			Q.push(elem);
//	//			printf("%d ", elem);
//	//		}
//	//	}
//
//	//	if (Q.empty())
//	//		break;
//	//	else {
//	//		cur = Q.front();
//	//		Q.pop();
//	//	}
//	//}
//
////----------- 로직2
//	int cur = v;
//	Q.push(cur);
//	Visited[cur] = true;
//	printf("%d ", cur);
//
//	while (!Q.empty()) {
//		cur = Q.front();
//		Q.pop();
//		for (auto elem : Vertex[cur]) {
//			if (Visited[elem] == false) {
//				Visited[elem] = true;
//				Q.push(elem);
//				printf("%d ", elem);
//			}
//		}
//	}
//}
//
//int main()
//{
//	int Vertexs, Edges, start;
//
//	scanf("%d %d %d", &Vertexs, &Edges, &start);
//
//	GraphInit(Vertexs);
//
//	int v1, v2;
//	while (Edges--) {
//		v1 = 0, v2 = 0;
//		scanf("%d %d", &v1, &v2);
//		AddEdge(v1, v2);
//	}
//
//	SortList();
//	DFS(start);
//	printf("\n");
//
//	for (int i = 0; i < Visited.size(); i++)
//		Visited[i] = false;
//
//	BFS(start);
//	printf("\n");
//	return 0;
//}



////18.5.29
////다시 풀어보기
//
//#include <cstdio>
//#include <vector>
//#include <algorithm>
//#include <queue>
//using namespace std;
//
//typedef vector<int> edges;
//vector<pair<edges,bool>> G;
//
//queue<int> q;
//
//void addEdge(int v1, int v2)
//{
//	G[v1].first.push_back(v2);
//	G[v2].first.push_back(v1);
//}
//
//void SortEdge(void)
//{
//	//auto에 &가 붙고 안붙고의 차이를 이해하자
//
//	//for (auto& elem : G)
//	//	sort(elem.first.begin(), elem.first.end());
//
//	for (int i = 0; i < G.size(); i++)
//		sort(G[i].first.begin(), G[i].first.end());
//}
//
//void visitInit(void)
//{
//	//for (auto& elem : G)
//	//	elem.second = false;
//
//	for (int i = 0; i < G.size(); i++)
//		G[i].second = false;
//}
//
//void DFS(int v)
//{
//	//재귀함수 사용
//	int cur = v;
//	G[cur].second = true;
//	printf("%d ", cur);
//
//	for (auto elem : G[cur].first) {
//		if (!G[elem].second) {
//			DFS(elem);
//		}
//	}
//
//	//stack사용
//	//int cur = v;
//	//if (G[cur].visited == false) {
//	//	G[cur].visited = true;
//	//	S.push(cur);
//	//	printf("%d ", cur);
//	//}
//
//	//while (!S.empty()) {
//	//	cur = S.top();
//	//	S.pop();
//	//	for (auto elem : G[cur].edge) {
//	//		if (G[elem].visited == false) {
//	//			G[elem].visited = true;
//	//			S.push(cur);
//	//			S.push(elem); 
//	//			printf("%d ", elem);
//	//			cur = elem;
//	//			break;
//	//		}
//	//	}
//	//}
//}
//
//void BFS(int v)
//{
//	int cur = v;
//	G[cur].second = true;
//	q.push(cur);
//	printf("%d ", cur);
//
//	while (!q.empty()){
//		cur = q.front();
//		q.pop();
//		for (auto elem : G[cur].first){
//			if (G[elem].second == false) {
//				G[elem].second = true;
//				q.push(elem);
//				printf("%d ", elem);
//			}
//		}
//	}
//}
//
//int main()
//{
//	int n, m, v;
//	scanf("%d %d %d", &n, &m, &v);
//
//	G.resize(n+1);
//	
//	int v1 = 0, v2 = 0;
//	for (int i = 0; i < m; i++)
//	{
//		scanf("%d %d", &v1, &v2);
//		addEdge(v1, v2);
//	}
//
//	SortEdge();
//
//	DFS(v);
//
//	printf("\n");
//
//	visitInit();
//
//	BFS(v);
//	
//	printf("\n");
//}


// 18.9.28
// 또 다시 풀어보기 ㅠ 걍 무식하게 푼다.

#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>
using namespace std;

vector<vector<int>> v;
vector<bool> visit;

queue<int> q;

void DFS(int start)
{
	cout << start << " ";
	visit[start] = true;

	for (auto elem : v[start]) {
		if (visit[elem] == false)
			DFS(elem);
	}
}

void BFS(int start)
{
	cout << start << " ";
	visit[start] = true;
	q.push(start);

	while (!q.empty())
	{
		int n = q.front();
		q.pop();
		for (auto elem : v[n]) {
			if (visit[elem] == false) {
				cout << elem << " ";
				visit[elem] = true;
				q.push(elem);
			}
		}
	}
}

int main()
{
	// n은 정점의 갯수, m은 간선의 갯수
	int n, m;	
	int start;

	cin >> n >> m >> start;

	v.resize(n+1);
	visit.resize(n + 1, false);

	int v1, v2;
	for (int i = 0; i < m; ++i)
	{
		cin >> v1 >> v2;
		v[v1].push_back(v2);
		v[v2].push_back(v1);
	}

	for (int i = 0; i < v.size(); ++i) 
		sort(v[i].begin(), v[i].end());

	DFS(start);

	fill(visit.begin(), visit.end(),false);

	cout << endl;
	BFS(start);
}