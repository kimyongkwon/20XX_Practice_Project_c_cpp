//// 18.9.13
//// String Manipulation
//// Sherlock and the Valid String

#include <iostream>
#include <string>
#include <map>
#include <vector>
using namespace std;

int main()
{
	string s;
	cin >> s;

	vector<int> v;
	v.resize(26, 0);
	for (size_t i = 0; i < s.size(); i++)
	{
		v[s[i] - 'a'] += 1;
	}
	
	map<int, int> m;

	// m의 첫번째는 어떤 단어가 문자열에서 등장하는 횟수, 두번째는 횟수가 같은 단어들의 갯수
	for (auto elem : v)
	{
		if (elem != 0)
			m[elem] += 1;
	}

	bool ans = false;
	// m의 size가 1이라는 말은 각각의 알파벳들이 모두 같은 갯수라는 의미
	if (m.size() == 1)
		ans = true;
	// size가 3이상일 경우에는 "YES"가 나올수 가 없다. 그러므로 ans는 무조건 false
	else if (m.size() == 2){
		auto it = m.begin();
		pair<int, int> min = { it->first, it->second }; it++;
		pair<int, int> max = { it->first, it->second };

		// min의 pair값이 {1,1}이면 단어가 1번 등장하는 것이 1개라는 것이다.
		// 이뜻은 만약 이것이 성립된다면 이것만 제외시키면 문자열에서 모든 단어들이 같은 횟수 등장한다는 뜻이다.
		if (min.first == 1 && min.second == 1) 
			ans = true;
		// 만약 max의 first가 min의 first보다 1크고 second가 1이라면 이것만 제외시키면 문자열에서 모든 단어들이 같은 횟수 등장한다는 뜻이다.
		// (꼭 max의 first가 min의 first보다 1 커야하고 max의 second가 1이여야하는 이유는 다음과 같은 예를 보면 이해할 수 있다)
		// ex) {2, 1}  와  {2, 2}   중에 어떤것이 문자 하나만 제외하면 "YES"가 가능할까?
		//     {3, 2}	   {3, 1}
		else if ((max.first - 1 == min.first) && max.second == 1) 
			ans = true;
	}

	if (ans)
		cout << "YES" << endl;
	else
		cout << "NO" << endl;
}

//#include <iostream>
//#include <string>
//#include <map>
//#include <vector>
//using namespace std;
//
//int main()
//{
//	string str;
//	cin >> str;
//
//	map<char, int> m;
//	vector<int> v;
//	v.resize(100005, 0);
//
//	for (size_t i = 0; i < str.size(); ++i){
//		m[str[i]] += 1;
//	}
//
//	for (auto elem : m) {
//		v[elem.second] += 1;
//	}
//
//	bool check = false;
//	bool check2 = false;
//
//	for (auto it = v.begin() + 1; it != v.end(); ++it) {
//		if (*it == 1) {
//			if (check == true) {
//				check = false;
//				break;
//			}
//			
//			if((*(it - 1) > 1) || (*(it + 1) > 1))
//				check = true;
//			else {
//				if (it == v.begin() +1) {
//					check2 = true;
//					continue;
//				}
//				check = false;
//			}
//
//		}
//		if (*it > 1) {
//			if (check2 == true) {
//				check = true;
//				continue;
//			}
//			else if (check == true)
//				continue;
//			else
//				check = false;
//				
//		}
//	}
//		
//	if (str.size() == 1)
//		check = true;
//
//	if (check)
//		cout << "YES" << endl;
//	else
//		cout << "NO" << endl;
//}