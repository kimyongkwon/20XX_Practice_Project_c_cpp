//18.5.4
//팰린드롬인지 확인하기

#include <string>
#include <iostream>
using namespace std;

int main()
{
	string str;
	cin >> str;

	int last = str.length();
	for (int i = 0; i < last; i++)
	{ 
		//문자열에서 가운데를 기준으로 대응되는 양쪽 문자가 같지않으면
		//팰린드롬이 아니다. 0을 출력 후 프로그램 종료
		if (!(str[i] == str[last - i - 1]))
		{
			cout << 0;
			return 0;
		}
	}
	
	//만약 for문을 무사히 넘긴다면
	//팰린드롬 문자열이라는 뜻
	cout << 1;
}