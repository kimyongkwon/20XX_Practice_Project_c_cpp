#include <cstdio>
#include <vector>
using namespace std;

vector<int> rotLeft(vector<int> src, int d) {
	vector<int> dest;
	dest.resize(src.size(), -1);

	int shift = d;

	for (int i = 0; i < src.size(); i++) {
		int idx = i - shift;
		//shift만큼 옮기되
		//인덱스를 벗어나면(음수가 되면) 벡터 사이즈만큼 더해준다.
		if (idx < 0) {
			idx += src.size();
			dest[idx] = src[i];
		}
		//인덱스를 안넘어가면 그대로 삽입
		else 
			dest[idx] = src[i];
	}

	return dest;
}

int main()
{
	vector<int> a;
	int n,d ; 
	scanf("%d %d", &n, &d);
	a.resize(n, -1);
	for (int i = 0; i < a.size(); i++)
		scanf("%d", &a[i]);

	a = rotLeft(a, d);

	for (auto elem : a)
		printf("%d ", elem);
}