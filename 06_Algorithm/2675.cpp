//18.3.28

#include <stdio.h>

int main()
{
    int testnum;
    scanf("%d", &testnum);

    while (testnum)
    {
        int n;
        char str[20];
        scanf("%d %s", &n, str);

        int k = 0;
        for ( int i = 0; str[i] != '\0'; i++)
        {
            for ( int j = 0; j < n; j++)
            {
                printf("%c", str[i]);
            }
        }
        printf("\n");

        testnum--;
    }
}