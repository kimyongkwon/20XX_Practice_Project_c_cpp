// 18.9.18
// Greedy Algorithms
// Luck Balance

#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

// Complete the luckBalance function below.
int luckBalance(int k, vector<vector<int>> contests) {
	vector<int> tmp;
	vector<int> tmp2;
	for (int i = 0; i < contests.size(); ++i) {
		if (contests[i][1] == 1) {
			tmp.push_back(contests[i][0]);
		}
		if (contests[i][1] == 0) {
			tmp2.push_back(contests[i][0]);
		}
	}

	sort(tmp.begin(), tmp.end(), greater<int>());

	int cnt = 0;
	for (int i = 0; i < tmp.size(); ++i) {
		if (i < k)
			cnt += tmp[i];
		else
			cnt -= tmp[i];
	}

	for (int i = 0; i < tmp2.size(); ++i) {
		cnt += tmp2[i];
	}

	return cnt;
}

int main()
{

	int n, k = 0;

	cin >> n >> k;

	vector<vector<int>> contests(n);

	for (int i = 0; i < n; i++) {
		contests[i].resize(2);

		for (int j = 0; j < 2; j++) {
			cin >> contests[i][j];
		}
	}

	int result = luckBalance(k, contests);

	cout << result << "\n";

	return 0;
}