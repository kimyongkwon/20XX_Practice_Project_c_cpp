//18.5.4
//5와 6의 차이

#include <string>
#include <iostream>
using namespace std;

int main()
{
	string n1, n2;
	cin >> n1 >> n2;

	for (int i = 0; i < n1.length(); i++)
	{
		if (n1[i] == '6')
			n1[i] = '5';
	}

	for (int i = 0; i < n2.length(); i++)
	{
		if (n2[i] == '6')
			n2[i] = '5';
	}
	cout << stoi(n1) + stoi(n2);			//string을 int로 타입변환

	for (int i = 0; i < n1.length(); i++)
	{
		if (n1[i] == '5')
			n1[i] = '6';
	}

	for (int i = 0; i < n2.length(); i++)
	{
		if (n2[i] == '5')
			n2[i] = '6';
	}

	cout << " ";
	cout << stoi(n1) + stoi(n2);
}