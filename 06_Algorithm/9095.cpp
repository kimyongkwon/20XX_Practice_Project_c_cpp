//18.4.13
//1,2,3 더하기

//1을 만드는 경우의 수 = 1 (1가지)
//2를 만드는 경우의 수 = 1+1, 2 (2가지)
//3을 만드는 경우의 수 = 1+1+1, 2+1, 1+2, 3 (4가지)
//4를 만드는 경우의 수 = 1을 만드는 경우의 수에 3더하기 + 2를 만드는 경우의 수에 2더하기 + 3을 만드는 경우의 수에 1더하기
//                     = 1+3, 1+1+2, 2+2, 1+1+1+1, 2+1+1, 1+2+1, 3+1 (7가지)

//dp[n] = dp[n-1] + dp[n-2] + dp[n-3]

#include <cstdio>

int dp[11];

int func(int n)
{
    if ( dp[n] != 0 ) return dp[n];
    dp[n] = func(n-3) + func(n - 2) + func(n - 1);
    return dp[n];    
}

int main()
{
    int t;
    scanf("%d", &t);

    dp[0] = 1;
    dp[1] = 1;
    dp[2] = 2;

    while ( t-- )
    {
        int n;
        scanf("%d", &n);

        printf("%d\n", func(n));
    }
}