//18.4.12
//스택 수열

#include <cstdio>

int arr[100005];
char OperArr[100005];
int OperIdx;

typedef struct Node {
    int val;
    Node * next = nullptr;
}Node;

class CStack {
    Node * m_pHead = nullptr;
    int m_nDatas;
public:
    CStack();
    ~CStack();
    void Push(int val);
    int Pop(void);
    int Size(void) const;
    int Top(void) const;
    int Empty(void) const;
};

CStack::CStack()
{
    m_nDatas = 0;
}

CStack::~CStack()
{
    if ( m_pHead ) delete m_pHead;
}

void CStack::Push(int val)
{
    if ( m_pHead == nullptr )
    {
        m_pHead = new Node;
        m_pHead->next = nullptr;
        m_pHead->val = val;
    }
    else
    {
        Node * newNode = new Node;
        newNode->val = val;
        newNode->next = m_pHead;

        m_pHead = newNode;
    }

    OperArr[OperIdx++] = '+';
    m_nDatas++;
}

int CStack::Pop(void)
{
    if ( !Empty() )
    {
        Node * tmp = m_pHead;       //top노드를 임시적으로 가르키는 tmp
        int data = tmp->val;        //top노드의 값을 data변수에 임시저장

        m_pHead = tmp->next;        //head포인터를 top 다음 노드로 옴긴다.
        delete tmp;                 //top제거(이제부터 top노드는 다음 노드가 된다)

        m_nDatas--;

        OperArr[OperIdx++] = '-';

        return data;
    }
    return -1;
}

int CStack::Size(void) const
{
    return m_nDatas;
}

int CStack::Top(void) const
{
    if ( m_nDatas )
        return m_pHead->val;
    return -1;
}

int CStack::Empty(void) const
{
    if ( m_nDatas )
        return 0;
    return 1;
}

int main()
{
    CStack stack;

    int n;
    scanf("%d", &n);

    int arridx = 1;
    for ( int i = 1; i <= n; i++ )
    {
        scanf("%d", &arr[i]);
        stack.Push(i);

        while ( stack.Top() == arr[arridx] )
        {
            stack.Pop();
            arridx += 1;
            if ( stack.Top() != arr[arridx] )
                break;
        }
    }

    OperArr[OperIdx] = '\0';

    if ( !stack.Empty() )
        printf("NO");
    else
        for ( int i = 0; OperArr[i] != '\0'; i++ )
            printf("%c\n", OperArr[i]);
}