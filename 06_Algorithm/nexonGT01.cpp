//#include <cstdio>
//#include <vector>
//#include <iostream>
//#include <algorithm>
//#include <string>
//using namespace std;
//
//int main()
//{
//	string str;
//
//	cin >> str;
//
//	int max = str.size();
//
//	cout << str[max-1];
//}

#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <string>
using namespace std;

int programmerStrings(string s)
{
	bool set = false;
	
	vector<pair<char, bool>> v;
	v.resize(10);
	v[0] = { 'p',false };
	v[1] = { 'r',false };
	v[2] = { 'o',false };
	v[3] = { 'g',false };
	v[4] = { 'r',false };
	v[5] = { 'a',false };
	v[6] = { 'm',false };
	v[7] = { 'm',false };
	v[8] = { 'e',false };
	v[9] = { 'r',false };

	int tmp = 0;
	for (int i = 0; i < s.size(); i++)
	{
		if (set == false && (s[i] == 'p' || s[i] == 'r' || s[i] == 'o' ||
			s[i] == 'g' || s[i] == 'a' || s[i] == 'm' || s[i] == 'e'))
		{
			set = true;
			break;
		}

		s.erase(i, 1);
		i -= 1;
	}

	if (set) {
		for (int i = 0; i < s.size(); i++){
			if ((s[i] == 'p' || s[i] == 'r' || s[i] == 'o' ||
				s[i] == 'g' || s[i] == 'a' || s[i] == 'm' || s[i] == 'e')){
				for (int j = 0; j < v.size(); j++) {
					if (v[j].first == s[i] && v[j].second == false) {
						v[j].second = true;
						break;
					}
				}
			}

			s.erase(i,1);
			i -= 1;

			int cnt = 0;
			for (int k = 0; k < v.size(); k++) {
				if (v[k].second == true)
					cnt += 1;
			}
			
			if (cnt == 10)
				break;
		}
	}

	//---------------------------------------------

	//�ʱ�ȭ
	set = false;
	for (int i = 0; i < v.size(); i++)
		v[i].second = false;

	for (int i = s.size() - 1; i >= 0; i--)
	{
		if (set == false && (s[i] == 'p' || s[i] == 'r' || s[i] == 'o' ||
			s[i] == 'g' || s[i] == 'a' || s[i] == 'm' || s[i] == 'e'))
		{
			set = true;
			break;
		}
		s.erase(i, 1);
	}

	if (set) {
		for (int i = s.size() -1 ; i >= 0; i--) {
			if ((s[i] == 'p' || s[i] == 'r' || s[i] == 'o' ||
				s[i] == 'g' || s[i] == 'a' || s[i] == 'm' || s[i] == 'e')) {
				for (int j = 0; j < v.size(); j++) {
					if (v[j].first == s[i] && v[j].second == false) {
						v[j].second = true;
						break;
					}
				}
			}

			s.erase(i, 1);

			int cnt = 0;
			for (int k = 0; k < v.size(); k++) {
				if (v[k].second == true)
					cnt += 1;
			}

			if (cnt == 10)
				break;
		}
	}

	return s.size();
}

int main()
{
	//ofstream fout(getenv("OUTPUT_PATH"));

	string s;
	getline(cin, s);

	int res = programmerStrings(s);

	cout << res << "\n";

	//fout.close();

	return 0;
}