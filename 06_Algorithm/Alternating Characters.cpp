// 18.9.13
// String Manipulation
// Alternating Characters

#include <iostream>
#include <vector>
#include <string>
using namespace std;

int main()
{
	int num;
	scanf("%d", &num);

	vector<string> v;
	v.resize(num);

	for (int i = 0; i < num; ++i) {
		cin >> v[i];
	}

	char tmp;
	int cnt;
	for (string str : v) {
		tmp = str[0];
		cnt = -1;
		for (int i = 0; i < str.size(); ++i) {
			if (tmp == str[i])
				cnt++;
			else
				tmp = str[i];
		}
		cout << cnt << endl;
	}
}