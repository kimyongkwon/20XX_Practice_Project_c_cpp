//18.4.5

#include <cstdio>

int main()
{
    int M;
    int N;

    scanf("%d", &M);
    scanf("%d", &N);

    int n, k;
    int sum = 0;
    int min = 0;
    bool lock = true;
    for (M; M <= N; M++)
    {
        n = M;
        k = 0;
        while ( n )
        {
            if ( M % n == 0 )
                k++;
            n--;
        }

        if ( k == 2 )
        {
            if ( lock )
            {
                min = M;
                lock = false;
            }
            sum += M;
        }
    }

    if ( sum == 0 )
        printf("-1");
    else
    {
        printf("%d\n", sum);
        printf("%d", min);
    }
}