//18.3.12

////내가 푼 방법
//#include <stdio.h>
//#include <string>
//
//int main()
//{
//    char str[3] = {0,};
//
//    //처음 숫자를 문자열로 입력받는다.
//    scanf("%s", str);
//
//    //tmp에 str 문자열을 복사한다.
//    char tmp[3];
//    for ( int i = 0; i < 3; i++ )
//        tmp[i] = str[i];
//
//    int num1;
//    int num2;
//
//    //사이클 카운트 변수 cnt
//    int cnt = 1;
//
//    while ( 1 )
//    {
//        //만약 입력한 수가 10미만이었다면 str[1]은 널문자였을것이다
//        if ( str[1] == '\0' )
//        {
//            str[1] = str[0];
//            str[0] = '0';           //숫자앞에 0을 추가한다 (str[0]에 문자 '0'을 삽입한다.)           
//            str[2] = '\0';
//        }
//
//        //문자열로 입력받았기때문에 각자리의 숫자(문자)를 숫자로 바꾼 후 더한다.
//        num1 = str[0] - '0';
//        num2 = str[1] - '0';
//        int num3;
//        num3 = num1 + num2;
//
//        //더한 숫자(num3)가 한자리 수가 아니면 1의 자리숫자만 다음에 사용되야하므로
//        //10의 자리숫자(k)를 구해서 num3을 나눈 나머지 값을 num3에 다시 대입한다.
//        int k;
//        for ( int i = 1; i <= 9; i++ )
//        {
//            k = i * 10;
//            if ( num3 / k == 1 )
//            {
//                num3 = num3 % k;
//                break;
//            }
//        }
//
//        //숫자를 문자로 변환해서 문자열에 다시 대입한다.
//        str[0] = num2 + '0';
//        str[1] = num3 + '0';
//        str[2] = '\0';
//
//        //만약 '05'나 '08'같은 문자열이 나오면 두자리수의 문자열로 보기때문에
//        //5나 8같은 한자리수 문자열로 바꿔준다.
//        //이렇게 해야지만 사이클 돌면서 나온 결과값과 원래 초기값의 문자열을 비교 할 수 있다.
//        //위에서 0을 추가하는 과정은 단순히 더하는 연산을 하기 위함이다.
//        if ( str[0] == '0' && str[1] != '\0' )
//        {
//            str[0] = str[1];
//            str[1] = '\0';
//        }
//
//        //사이클도는 문자열과 초기 문자열을 비교한다.
//        if ( str[0] == tmp[0] && str[1] == tmp[1] && str[2] == tmp[2] )
//            break;
//        cnt++;
//    }
//
//    printf("%d", cnt);
//}


// 다른사람이 푼 방법
#include <stdio.h>

int main()
{
    int n, tmp;
    
    scanf("%d", &n);
    tmp = n;
    
    int cnt = 0;
    while (1)
    {
        int news = tmp / 10 + tmp % 10;
        tmp = tmp % 10 * 10 + news % 10;
        cnt++;
        if ( tmp == n )
            break;
    }
    printf("%d", cnt);
}