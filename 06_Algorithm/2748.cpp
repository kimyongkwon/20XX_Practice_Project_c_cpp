//18.4.9

////2747문제에서에서 자료형만 바꿔주면 됨

//바텀업방식

//#include <cstdio>
//#include <vector>
//using namespace std;
//
//int main()
//{
//    int n;
//
//    scanf("%d", &n);
//
//    vector<long long> dp(n + 1, 0);
//
//    dp[1] = 1;
//
//    for ( int i = 2; i <= n; i++ )
//        dp[i] = dp[i - 1] + dp[i - 2];
//
//    printf("%lld", dp[n]);
//}


//탑다운방식

#include <cstdio>

long long dp[90];

long long fibonacci(int n)
{
    if ( n == 0 )
        return 0;
    else if ( n == 1 )
        return 1;
    if ( dp[n] != 0 ) return dp[n];
    dp[n] = fibonacci(n - 1) + fibonacci(n - 2);
    return dp[n];
}

int main()
{
    int n;

    scanf("%d", &n);

    printf("%lld", fibonacci(n));
}