//-------------------------------------------------------------------------------------------
// 피보나치 수열(재귀함수 + 메모이제이션) - 탑다운
//-------------------------------------------------------------------------------------------

//#include <stdio.h>
//#include <string.h>
//#include <vector>
//using namespace std;
//
//vector<long long> v;
//
//long long fibonacci(long long n)
//{
//	if (n == 1)
//		return 1;
//	if (n == 0)
//		return 0;
//
//	if (v[n] != -1) return v[n];
//	v[n] = fibonacci(n - 1) + fibonacci(n - 2);
//	return v[n];
//}
//
//int main()
//{
//	v.resize(100, -1);
//
//	printf("%lld", fibonacci(90));
//}

//-------------------------------------------------------------------------------------------
// 피보나치 수열(반복문) - 바텁업
//-------------------------------------------------------------------------------------------

#include <stdio.h>
#include <string.h>
#include <vector>
using namespace std;

vector<long long> v;

int main()
{
	int n = 100;
	v.resize(n + 1, 0);

	v[1] = 1;
	for (int i = 2; i <= n; ++i)
		v[i] = v[i - 1] + v[i - 2];
	
	printf("%lld", v[n]);
}