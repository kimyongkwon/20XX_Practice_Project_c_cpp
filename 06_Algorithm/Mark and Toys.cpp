// 18.9.10
// Sorting
// Mark and Toys

#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

vector<int> v;

int main()
{
	int num, money;
	scanf("%d %d", &num, &money);

	v.resize(num, 0);
	for (int i = 0; i < num; i++)
	{
		scanf("%d", &v[i]);
	}

	sort(v.begin(), v.end());

	int cnt = 0;
	for (int i = 0; i < num; i++) {
		money -= v[i];
		if (money < 0)
			break;
		cnt += 1;
	}

	std::cout << cnt << std::endl;
}