//18.5.31
//세로읽기

#include <iostream>
#include <string>
using namespace std;

int main()
{
	string str[5];

	for (int i = 0; i < 5; i++) {
		cin >> str[i];
		int empty = 15 - str[i].size();
		while (empty--){
			str[i].append(" ");
		}
	}

	for (int i = 0; i < 15; i++) {
		for (int j = 0; j < 5; j++) {			//j는 세로
			if (str[j][i] != ' ' )
				cout << str[j][i];
		}
	}
}