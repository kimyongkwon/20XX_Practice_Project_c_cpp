//18.4.6

//맞은 정답

#include <cstdio>

const int MAX = 10001;

int main()
{
    //bool 배열 선언
    bool arr[MAX] = { false };

    //1은 항상 소수이므로 true
    arr[1] = true;

    //에라토스테네스의 체를 사용

    //i는 2부터 MAX까지의 배수를 체크하기 위한 변수다. 
    for ( int i = 2; i <= MAX; i++ )
    {
        //j가 1일때는 제외하기하기위해 2부터 사용한다. 
        //그리고 배열의 인덱스는 10000까지이므로 i * j < MAX
        for ( int j = 2; i * j < MAX; j++ )
        {
            //arr배열의 값이 false면 true로 바꿔준다.
            //이러면 중복체크 하지 않게 된다.
            //다음 if문은 없어도 상관없다.
            arr[i * j] = true;
        }
    }

    int t;
    scanf("%d", &t);

    while ( t-- )
    {
        int n;
        scanf("%d", &n);

        int n1, n2;
        //i는 2부터 n의 절반까지만 순회한다.
        //왜? 어차피 문제 자체가 "출력하는 소수는 작은것부터 먼저 출력한다"
        //이므로 n이 10일때 (3,7) or (5,5) or (7,3) 세개가 나올텐데 굳이 n/2를 넘어선
        //(7,3)까지 탐색해서 시간낭비를 할 필요가 없기때문이다.
        for ( int i = 2; i <= (n / 2); i++ )
        {
            //i가 소수고
            if ( arr[i] == false )
            {
                //그리고 n-i가 소수인지만 확인한 후 
                //맞으면 n1,n2에 저장한다.
                if ( arr[n - i] == false )
                {
                    n1 = i;
                    n2 = n - i;
                }
            }
        }
        printf("%d %d\n", n1, n2);
    }
}


// 다음은 시간초과로 틀린 답이다.

//#include <cstdio>
//
//const int MAX = 10001;
//
//int main()
//{
//    //bool 배열 선언
//    bool arr[MAX] = { false };
//
//    //1은 항상 소수이므로 true
//    arr[1] = true;
//
//    //i는 2부터 MAX까지의 배수를 체크하기 위한 변수다. 
//    for ( int i = 2; i <= MAX; i++ )
//    {
//        //j가 1일때는 제외하기하기위해 2부터 사용한다. 
//        //그리고 배열의 인덱스는 10000까지이므로 i * j < MAX
//        for ( int j = 2; i * j < MAX; j++ )
//        {
//            //arr배열의 값이 false면 true로 바꿔준다.
//            //이러면 중복체크 하지 않게 된다.
//            //다음 if문은 없어도 상관없다.
//            arr[i * j] = true;
//        }
//    }
//
//    int t;
//    scanf("%d", &t);
//    while ( t-- )
//    {
//        int n;
//        scanf("%d", &n);
//
//        int i, j;
//        int n1, n2;
//
//        //밑에와 같이 이중으로 반복문 돌리면 시간초과 뜬다.
//        for ( i = 2; i <= (n / 2); i++ )
//        {
//            for ( j = n; j >= (n / 2); j-- )
//            {
//                if ( arr[i] == false && arr[j] == false )
//                {
//                    if ( i + j == n )
//                    {
//                        n1 = i;
//                        n2 = j;
//                    }
//                }
//            }
//        }
//        printf("%d %d\n", n1, n2);
//    }
//}
