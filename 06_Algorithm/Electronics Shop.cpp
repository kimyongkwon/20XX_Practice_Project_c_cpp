//18.6.15

#include <cstdio>

int main()
{
	int budget, n, m;
	scanf("%d %d %d", &budget, &n, &m);

	int keyboards[1000];
	int usbs[1000];
	for (int i = 0; i < n; i++) 
		scanf("%d", &keyboards[i]);

	for (int i = 0; i < m; i++) 
		scanf("%d", &usbs[i]);

	int sum = 0;
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			if (sum < keyboards[i] + usbs[j] && budget >= keyboards[i] + usbs[j])
				sum = keyboards[i] + usbs[j];
		}
	}

	if (sum == 0)
		printf("-1");
	else printf("%d", sum);
}