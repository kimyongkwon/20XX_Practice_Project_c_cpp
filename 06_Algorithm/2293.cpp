//18.4.11
//동전1

//이해가 안감 ㅠㅠ

#include <cstdio>

//int는 2147483647까지
int dp[10005];
int arr[105];

int main()
{
    int n, k;
    
    scanf("%d %d", &n, &k);

    for ( int i = 1; i <= n; i++ )
        scanf("%d", &arr[i]);

    dp[0] = 1;
   
    for ( int i = 1; i <= n; i++ )
    {
        for ( int j = 0; j <= k; j++ )
        {        
            if ( j - arr[i] >= 0 )
            {
                dp[j] += dp[j - arr[i]];
            }
        }
    }

    printf("%d", dp[k]);
}