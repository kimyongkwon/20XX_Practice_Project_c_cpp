// 18.9.21
// Search
// Hash Tables: Ice Cream Parlor

#include <iostream>
#include <unordered_map>
#include <map>
using namespace std;

// Complete the whatFlavors function below.
void whatFlavors(vector<int> cost, int money) {
	map<int, int> unmap;
	map<int, int> tmpunmap;
	for (int i = 0; i < cost.size(); ++i) {
		if ( unmap[cost[i]] == 0 )
			unmap[cost[i]] = i + 1;
		else 
			tmpunmap[cost[i]] = i + 1;
	}

	for (int i = 0; i < cost.size(); ++i) {
		if (unmap[cost[i]] != 0 && unmap[money - cost[i]] != 0) {
			if (cost[i] == money - cost[i]) {
				if (tmpunmap[cost[i]] == 0)
					continue;
				cout << unmap[cost[i]] << " " << tmpunmap[cost[i]] << endl;
			}
			else
				cout << unmap[cost[i]] << " " << unmap[money - cost[i]] << endl;
			break;
		}
	}

	//for (int i = 1; i < money; ++i) {
	//	if (unmap[i] != 0 &&
	//		unmap[money - i] != 0)
	//	{
	//		if (i == money - i)
	//			cout << unmap[i] << " " << tmpunmap[i] << endl;
	//		else
	//			cout << unmap[i] << " " << unmap[money - i] << endl;
	//		break;
	//	}
	//}

}

int main()
{
	int n = 0;
	cin >> n;

	int money, costs = 0;
	vector<int> v;

	for (int i = 0; i < n; ++i) {
		cin >> money >> costs;
		
		v.resize(costs, 0);

		for (int i = 0; i < costs; ++i)
			cin >> v[i];

		whatFlavors(v, money);
	}
}