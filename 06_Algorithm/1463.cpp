////4.10
////내가 푼게 아님 ㅠㅠ
////https://blog.naver.com/hwasub1115/221159987336
////
//
//#include <cstdio>
//#include <algorithm>
//
//using namespace std;
//
////dp[i]의 의미는 i가 1이 되기위해 필요한 최소 연산횟수를 담고 있는 배열이다.
//int dp[1000001];
//
//int main()
//{
//    int n;
//    scanf("%d", &n);
//    
//    //1이 1이 되기위해서는 0번의 연산횟수가 필요하다.
//    dp[1] = 0;
//    for ( int i = 2; i <= n; i++ )
//    {
//        //i를 1로 만드는 방법은 점화식으로 다음 총 3가지다.
//        //1) dp[i] = dp[i - 1] + 1;
//        //2) dp[i] = dp[i / 2] + 1;
//        //3) dp[i] = dp[i / 3] + 1;
//        //이 3가지 중에서 가장 최소의 값을 찾아야한다.
//
//        //case1
//        //i가 1이되기위한 최소연산횟수는 
//        //i-1에서 +1의 연산의 횟수와 같다.
//        //그 횟수를 dp[i]에 저장한다.
//        dp[i] = dp[i - 1] + 1;
//        
//        //case2
//        //i가 만약 2로 나눌수 있다면
//        //위에서 계산한 dp[i]와 dp[i/2] + 1을 비교한다 (만약 dp[i/2]+1이 더 작으면 그걸 로 dp[i]를 갱신)
//        if ( i % 2 == 0 )
//            dp[i] = min(dp[i], dp[i / 2] + 1);
//
//        //case3
//        //i가 만약 3로 나눌수 있다면
//        //위에서 계산한 dp[i]와 dp[i/3] + 1을 비교한다 (만약 dp[i/3]+1이 더 작으면 그걸 로 dp[i]를 갱신)
//        if ( i % 3 == 0 )
//            dp[i] = min(dp[i], dp[i / 3] + 1);
//    }
//
//    printf("%d\n", dp[n]);
//}


//18.5.28
//다시 풀어보기

//탑다운 방식
//#include <cstdio>
//#include <vector>
//#include <algorithm>
//using namespace std;
//
//vector<int> dp;
//
//int f(int n)
//{
//	if (n == 1) return dp[1] = 0;
//	if (n == 2) return dp[2] = 1;
//	if (n == 3) return dp[3] = 1;
//
//	//메모이제이션
//	if (dp[n] != -1) return dp[n];
//
//	//n을 1로 만드는 방법은 
//	//총 3가지
//	//dp[i] = dp[i - 1] + 1;
//	//dp[i] = dp[i / 2] + 1;
//	//dp[i] = dp[i / 3] + 1;
//	//이 3가지중에 최솟값을 찾아야한다.
//
//	dp[n] = f(n-1) + 1;									// 첫번째 방법
//	if (n % 2 == 0) dp[n] = min(dp[n], f(n / 2) + 1);	// 두번째 방법(첫번째 방법의 최소값과 비교한다.)
//	if (n % 3 == 0) dp[n] = min(dp[n], f(n / 3) + 1);	// 세번째 방법(첫번쨰 OR 두번쨰 방법의 최소값과 비교한다.)
//
//	return dp[n];
//}
//
//int main()
//{
//	int n;
//	scanf("%d", &n);
//
//	dp.resize(1000005, -1);
//
//	f(n);
//	printf("%d", dp[n]);
//}


//바텀업
#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

vector<int> dp;

int f(int n)
{
	for (int i = 1; i <= n; i++)
	{
		dp[i] = dp[i - 1] + 1;
		if (i % 2 == 0) dp[i] = min(dp[i], dp[i / 2] + 1);
		if (i % 3 == 0) dp[i] = min(dp[i], dp[i / 3] + 1);
	}

	return dp[n];
}

int main()
{
	int n;
	scanf("%d", &n);

	dp.resize(1000005, -1);

	dp[1] = 0;
	dp[2] = 1;
	dp[3] = 1;

	printf("%d", f(n));
}