//18.5.29
//쉬운계단수

//내가 안풀었음
//http://sexycoder.tistory.com/87

#include <cstdio>
using namespace std;

// dp[i][j]의 의미는 i자릿수의 마지막 수가 j인 숫자들의 개수를 말한다.
// 그러므로 dp는 j자릿수에서 1의 자리가 i로 끝나는 수들의 갯수를 보관한다.
int dp[100 + 1][100 + 1];

const int MAX = 1000000000;

int main(void)
{
	int n;
	scanf("%d", &n);


	// 처음 값인 1의 자릿수에서 1~9로 끝나는 숫자들은 다 1개이다.
	for (int i = 1; i <= 9; i++)
		dp[1][i] = 1;

	// j가 9일 경우 j+1이 10이 되서 의미가 없어지고 인덱스를 벗어난다.
	// j가 0일 경우 j-1이 -1이 되서 의미가 없어지고 인덱스를 벗어난다.
	// 따라서 따로 처리를 해준다.

	// ((자리수(i)에서 1의 자리가 j로 끝나는 수는 i-1자리수에서 1의 자리가 j-1 , j+1로 끝나는 수의 합과 같다))
	// 하지만 예외가 있다.
	// 처음 값은 계산했으므로 2자릿수부터 시작한다.
	for (int i = 2; i <= n; i++) {
		for (int j = 0; j <= 9; j++) {
			if (j == 0)												// 1의 자리가 0(j)으로 끝나는 수는 j-1을 해버리면 -1이 되므로 j+1과 j+1의 수를 더하지 않고 dp[i][j+1] 갯수만 가져온다
				dp[i][j] = dp[i - 1][j + 1];
			else if (j + 1 == 10)									// 1의 자리가 9(j)으로 끝나는 수는 j+1을 해버리면 10이 되므로 j+1과 j+1의 수를 더하지 않고 dp[i][j-1] 갯수만 가져온다
				dp[i][j] = dp[i - 1][j - 1];
			else													// 위의 경우가 아니면 i-1자리수에서 1의 자리가 j-1 , j+1로 끝나는 수의 합을 i자리에서 1의 자리가 j로 끝나는 수로 한다.
				dp[i][j] = (dp[i - 1][j - 1] + dp[i - 1][j + 1]);
			dp[i][j] %= MAX;
		}
	}

	// 총 계단수의 개수
	int sum = 0;

	// 다 계산하고 모듈러 연산을 해줄 경우 값의 크기를 감당할 수 없으므로
	// 모듈러 연산의 성질을 이용해서 계산할 때마다 해준다.
	// (a+b) % c = (a%c+b%c)%c			(현재 우항을 적용해줬음)
	for (int i = 0; i <= 9; i++) {
		sum += dp[n][i];
		sum %= MAX;
	}

	printf("%d", sum);

	return 0;
}