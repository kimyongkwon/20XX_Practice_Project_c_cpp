//18.3.17
//2741 , 2742
//처음에 c++로 cout과 endl로 짰더니 시간초과가 떳음
//알고보니 c의 printf, \n보다 월등히 느려서 그런거였음
//c로 다시 짜니 성공

/*
c++자체가 c와의 호환성을 위해 버퍼의 sync를 맞추는 작업 등을 수행하며 출력하기 때문에
cin / cout이 scanf / printf 에 비해 현저히 느리다고 하고, 특히 endl까지 붙이는 경우 output buffer을 flush 하기 때문에
더 많이 느려진다고 한다.
결론 : 출력이 많은 경우 cout 대신 printf를 사용해야 함 !
*/


//#include <iostream>
//using namespace std;
//
//int main()
//{
//    int n;
//    cin >> n;
//
//    if (!(n <= 100000 && n > 0))
//        return 0;
//
//    int i = 0;
//    for ( n; n >= 0; n-- )
//        cout << i+1 << endl;
//}

//#include <stdio.h>
//
//int main()
//{
//    int n;
//    scanf("%d", &n);
//
//    for ( int i = n; i >= 1 ; i--)
//    {
//        printf("%d \n",i);
//    }
//}

//8393
//#include <iostream>
//using namespace std;
//
//int main()
//{
//    int n;
//
//    cin >> n;
//
//    int sum = 0;
//
//    for ( n; n > 0; n--)
//    {
//        int i;
//        cin >> i;
//        sum += i;
//    }
//
//    cout << sum;
//}


//ab를 입력하면 당근 뻑나야하는거 아닌가 str[i+2]에서 뻑나야하는거 아닌가??
//
//#include <stdio.h>
//
//int main()
//{
//    char str[3];
//
//    scanf("%s", str);
//
//    int cnt = 0;
//
//    for ( int i = 0; str[i] != '\0'; i++)
//    {
//        if ( str[i] == 'a' )
//            cnt++;
//        else if ( str[i] == 'b' && str[i + 2] == '=' && str[i + 1] == 'z' )
//        {
//            i += 2;
//            cnt++;
//        }
//    }
//
//    printf("%d", cnt);
//}
