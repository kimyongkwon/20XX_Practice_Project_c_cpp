// 18.9.19
// Greedy Algorithm
// Max Min

#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int maxMin(int k, vector<int> arr)
{
	sort(arr.begin(), arr.end());

	int min = 100000000, tmp;
	for (int i = 0; i <= arr.size() - k; ++i) {
		tmp = arr[i + k - 1] - arr[i];
		if (min > tmp)
			min = tmp;
	}

	return min;
}


int main()
{
	int n, k = 0;
	vector<int> v;

	cin >> n >> k;

	v.resize(n, 0);

	for (int i = 0; i < v.size(); ++i)
		cin >> v[i];

	cout << maxMin(k, v) << endl;
}