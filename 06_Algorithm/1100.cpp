//18.5.2
//하얀 칸

#include <cstdio>

int main()
{
	//행을 9로 한 이유는 마지막에는 행의 마지막에는 널이 들어가기 때문에.
	char board[8][9];

	for (int i = 0; i < 8; i++)
		for (int j = 0; j < 9; j++)
			scanf("%c", &board[i][j]);

	int cnt = 0;
	for (int i = 0; i < 8; i++)
	{ 
		for (int j = 0; j < 8; j++)
		{ 
			//1,3,5,7번째줄
			if (i % 2)
			{
				if ((j % 2) && board[i][j] == 'F')
					cnt++;
			}
			//0,2,4,6,8번째줄
			else
			{
				if ((j % 2 == 0) && board[i][j] == 'F')
					cnt++;
			}
		}
	}

	printf("%d", cnt);
}