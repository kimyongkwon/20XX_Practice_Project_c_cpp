//18.6.13
//포도주 시식
//내가 안풀었음 다음에 다시 도전해보장
//https://uss425.blog.me/221272831044

#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

vector<int> dp;
vector<int> grape;

int main()
{
	dp.resize(10005, 0);
	grape.resize(10005, 0);
	
	int n; scanf("%d", &n);

	for ( int i = 1; i <= n; i++ )
		scanf("%d", &grape[i]);
	
	dp[0] = 0;
	dp[1] = grape[1];
	dp[2] = grape[2] + dp[1];

	for (int i = 3; i <= n; i++)
		dp[i] = max( max((dp[i-1]), (dp[i-2] + grape[i])), (dp[i - 3] + grape[i - 1] + grape[i]) );

	printf("%d", dp[n]);
}