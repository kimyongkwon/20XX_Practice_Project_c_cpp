//18.5.3
//방 번호

#include <cstdio>
#include <algorithm>
using namespace std;

int main()
{
	char str[1000000];

	//set 한개는 0~9까지 있다.
	int set[10] = { 0, };

	scanf("%s", str);

	//문자열끝까지 순회하면서 문자에 해당되는 set배열 숫자를 +1
	for (int i = 0; str[i] != '\0'; i++)
		set[str[i] - '0']++;

	int num = 0;
	//set배열을 순회하면서 가장 카운팅이 많은 수를 num에 저장한다.
	//이 때 6과 9는 제외한다.
	for (int i = 0; i < 10; i++){
		if (i != 9 && i != 6) num = max(num, set[i]);
	}

	//6과 9는 같은므로 둘을 합쳐서 반으로 나눈 값으로 카운팅한다.
	//이 때 둘을 합친값이 홀수면 반올림해야하므로 +1을 한 뒤 반으로 나눈다.
	//(나눠서 반올림 한 값을 구하려면 제수에 +1을 한다?) 
	printf("%d", max(num, (set[6] + set[9] + 1) / 2));
}