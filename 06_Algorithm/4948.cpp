//18.4.6

#include <cstdio>

const int MAX = 246913;

int main()
{
    //bool 배열 선언
    bool arr[MAX] = { false };

    //1은 항상 소수이므로 true
    arr[1] = true;

    //i는 2부터 MAX까지의 배수를 체크하기 위한 변수다. 
    for ( int i = 2; i <= MAX; i++ )
    {
        //j가 1일때는 제외하기하기위해 2부터 사용한다. 
        for ( int j = 2; i * j <= MAX; j++ )
        {
            //arr배열의 값이 false면 true로 바꿔준다.
            //이러면 중복체크 하지 않게 된다.
            //다음 if문은 없어도 상관없다.
            if ( arr[i * j] != true )
                arr[i * j] = true;
        }
    }

    while(1)
    {
        int n;
        int cnt = 0;
        scanf("%d", &n);
    
        if ( n == 0 )
            break;

        //n보다 크고 n*2보다 작거나 같은 자연수중에서 소수를 카운팅한다.
        for ( int i = n + 1; i <= 2*n; i++ )
        {
            if ( arr[i] == false )
                cnt++;
        }
        printf("%d\n", cnt);
    }
}