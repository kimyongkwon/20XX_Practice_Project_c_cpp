//18.6.7
//싱기한 네자리 숫자

#include <cstdio>
using namespace std;

int main()
{
	int tmp = 0;
	for (int i = 1000; i <= 9999; i++)
	{
		int sum1 = 0, sum2 = 0, sum3 = 0;

		tmp = i;
		for (int j = 0; j < 4; j++) {
			sum1 += tmp % 10;
			tmp /= 10;
		}

		tmp = i;
		for (int j = 0; j < 4; j++) {
			sum2 += tmp % 12;
			tmp /= 12;
		}

		tmp = i;
		for (int j = 0; j < 4; j++) {
			sum3 += tmp % 16;
			tmp /= 16;
		}

		if (sum1 == sum2 && sum2 == sum3)
			printf("%d\n", i);
	}
}

