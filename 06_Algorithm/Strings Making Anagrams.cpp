// 18.9.13
// String Manipulation
// String: Making Anagrams

#include <iostream>
#include <string>
#include <map>
#include <algorithm>
using namespace std;

// 두 문자열에서 존재하는 가장 긴 애너그램을 찾아야한다.
// 이 문제를 해결하기 위해서는
// 두 문자열을 구성하는 문자들을 하나씩 순회하면서
// 해당 문자가 문자열마다 몇개씩 존재하는지 각각 파악하고, 
// 두 문자열의 알파벳 갯수의 차이를 구한다.
// 여기서 어떤 문자의 차이(절대값)가 0이라면 그 문자는 두 문자열에 동시에 존재하는 문자이고 
// 또한 두개의 문자열에 존재하는 애너그램이라는 의미이다. 
// 만약 차이가 1이상이라면 그것은 애너그램을 형성하는데 필요없는 즉, 삭제해야할 문자의 갯수이다. 
// 다음은 그 예이다.
// ex) A문자열 : abdEFG
//     B문자열 : HIJdba
// A문자열에는 a=1, b=1, d=1, E=1, F=1, G=1
// B문자열에는 a=1, b=1, d=1, H=1, I=1, J=1
// 각 문자들의 차이는 a=0, b=0, d=0, E=1, F=1, G=1, H=1, I=1, J=1 이고
// EFGHIJ 6개를 두 문자열에서 제거하면 A,B문자열은 a,b,d로 이루어지는 애너그램이 완성된다.

int makeAnagram(string a, string b) 
{
	
	map<char, int> mA;
	map<char, int> mB;

	// 문자열 a,b를 순회하면서 각자의 알파벳을 다 저장한다.
	for (char ch : a)
		mA[ch]++;

	for (char ch : b)
		mB[ch]++;

	int nDelete = 0;

	for (char i = 'A'; i < 'Z' + 1; ++i)
		nDelete += abs(mA[i] - mB[i]);

	for (char i = 'a'; i < 'z' + 1; ++i)
		nDelete += abs(mA[i] - mB[i]);

	return nDelete;
}

int main()
{
	string a;
	getline(cin, a);

	string b;
	getline(cin, b);

	int res = makeAnagram(a, b);

	cout << res << "\n";

	return 0;
}
