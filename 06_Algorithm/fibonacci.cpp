//18.4.5

//피보나치수열을 코드로 나타냈다.
//하지만 아래처럼 하면 매개변수가 같은 함수를 여러번 중복호출하게 된다
//불필요한 연산을 했기때문에 시간낭비!

//#include <cstdio>
//#include <vector>
//
//using namespace std;
//    
//vector<int> call;
//
//int fibonacci(int n)
//{
//    call[n]++;
//    if ( n == 0 ) return 0;
//    if ( n == 1 ) return 1;
//    return fibonacci(n - 2) + fibonacci(n - 1);
//}
//
//int main()
//{
//    int N;
//    scanf("%d", &N);
//    call.resize(N + 1);  
//    printf("F(%d): %d\n", N, fibonacci(N));
//    puts("호출횟수");
//    for ( int i = 0; i <= N; i++ )
//       printf("F(%d): %d회 호출\n", i, call[i]);
//}


//아래는 메모이제이션(memoization)을 사용해서 위 코드와 같은 문제를 해결한 방법이다.
//#include <cstdio>
//#include <vector>
//
//using namespace std;
//
//vector<int> dp;
//
//int fibonacci(int n)
//{
//    if ( n == 0 ) return 0;
//    if ( n == 1 ) return 1;
//    //이미 값을 계산적이 있다면 그 값을 바로 리턴
//    if ( dp[n] != -1 ) return dp[n];
//    //아니라면 계산해서 dp리스트에 넣어 보존한다.
//    dp[n] = fibonacci(n - 2) + fibonacci(n - 1);
//    return dp[n];
//}
//
//int main()
//{
//    int N;
//    scanf("%d", &N);
//    dp.resize(N + 1, -1);     //-1로 초기화한다.
//    printf("%d \n", fibonacci(N));
//}


//반복문을 통한방법(바텀업 방식) (재귀함수를 이용한 방법은 탑다운방식이라한다)
#include <cstdio>
#include <vector>
using namespace std;

int main()
{
    int n;
    scanf("%d", &n);
    vector<int> dp(n + 1, 0);
    dp[1] = 1;

    for ( int i = 2; i <= n; i++ )
        dp[i] = dp[i - 1] + dp[i - 2];
    printf("%d\n", dp[n]);
}

