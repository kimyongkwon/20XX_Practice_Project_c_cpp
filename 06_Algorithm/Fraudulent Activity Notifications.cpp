// 18.9.17
// Sorting
// Fraudulent Activity Notifications

#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

vector<string> split_string(string);

// Complete the activityNotifications function below.
int activityNotifications(vector<int> expenditure, int d) {

	//int cnt = 0;

	//for (int i = 0; i < expenditure.size() - d; ++i) {
	//	auto it = expenditure.begin();
	//	sort(it, it + d);

	//	int median = 0;
	//	// 짝수
	//	if (d % 2 == 0) {
	//		median = (expenditure[i + (d / 2 - 1)] + expenditure[i + (d / 2)] / 2);
	//	}
	//	else {
	//		median = expenditure[i + (d / 2)];
	//	}

	//	int dollar = *(it + d);
	//	if (median * 2 <= dollar)
	//		cnt += 1;

	//	expenditure.erase(expenditure.begin());
	//}

	//return cnt;

//----------------------------------------------------

	int count[201] = { 0 };
	int result = 0;
	int mid, mid2;
	int n = expenditure.size();
	int start = 0;
	int end = d - 1;

	// 홀수일경우(여기서 mid는 d개 중에 mid번째에 있는 것이 중앙값이다 라는 의미)
	if (d % 2 != 0) mid = mid2 = d / 2 + 1;
	// 짝수일경우
	else {
		mid = d / 2;
		mid2 = d / 2 + 1;
	}

	// count배열에 해당 수가 몇 번 나왔는지 저장한다.
	// 접근은 d만큼만 한다.
	for (int i = 0; i < d; ++i) 
		count[expenditure[i]]++;

	for (int i = 0; i < n - d; ++i) {
		int acc = 0;
		for (int j = 0; j < 201; ++j) {
			if (count[j] != 0) 
				acc += count[j];
			// acc가 mid보다 크거나 같다는것은 정렬된 값의 갯수를 저장하는 count배열에서 
			// 값의 갯수를 순서대로 더해가다가(이것을 저장하는 곳이 acc) mid번째보다 크거나 같은 순간이
			// 곧 중앙값과 같거나 넘어섰다는 의미다. 사실 이부분이 제일 헷깔린다...
			if (acc >= mid) {
				// d가 홀수일 경우 : 그러면 비로소 j가 중앙값이라는 것이고 d다음에 있는 값과 비교한다. 
				if (d % 2 != 0) {
					if (j * 2 <= expenditure[d + i]) result++;
					break;
				} 
				// d가 짝수일 경우 : 
				else 
				{
					if (acc >= mid2) {
						if (j * 2 <= expenditure[d + i]) result++;
						break;
					}
					else {
						int k = j + 1;
						while (count[k] == 0) ++k;
						if (j + k <= expenditure[d + i]) result++;
						break;
					}
				}
			}
		}
		count[expenditure[start]]--;
		start++;
		count[expenditure[end + 1]]++;
		end++;
	}
	return result;
}

int main()
{
	int n, d;
	cin >> n >> d;

	vector<int> expenditure(n);

	for (int i = 0; i < n; i++) {
		cin >> expenditure[i];
	}

	int result = activityNotifications(expenditure, d);

	cout << result << "\n";

	return 0;
}
