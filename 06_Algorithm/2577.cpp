//18.3.21

#include <stdio.h>

int main()
{
    int a;
    int b;
    int c;

    scanf("%d %d %d", &a, &b, &c);

    int num = a * b * c;

    int arr[10] = { 0, };
    int n = 0;

    while (1)
    {
        //num을 %연산자로 1의 자리부터 구한다.
        n = num % 10;

        //n이 어떤 수인지에 따라 배열에 넣는다.
        arr[n]++;
        
        //num을 10으로 나누었을때 0이라는 의미는 num이 가장 왼쪽의 자리의 있는 숫자이라는 것이다.
        //num이 가장 왼쪽에 있는 수이고 이미 배열에 값이 입력되었으므로 더 이상 while문을 진행할 필요가 없다.
        if ( num / 10 == 0 ) break;

        //num을 10으로 나눈 값을 num에 입력한다. 
        //오른쪽부터 숫자를 하나씩 없앤다.
        num /= 10;
    }

    for ( auto elem : arr )
        printf("%d \n", elem);
}