//18.6.19
//최소공배수

#include <cstdio>

int main()
{
	int n = 0;
	int a, b;
	

	scanf("%d", &n);
	
	while (n--) {
		int r = 1;
		scanf("%d %d", &a, &b);

		int high, low;
		if (a > b) {
			high = a;
			low = b;
		}
		else {
			high = b;
			low = a;
		}

		while (r != 0) {
			r = high % low;
			high = low;
			low = r;
		}

		printf("%d\n", (a*b) / high);
	}
}