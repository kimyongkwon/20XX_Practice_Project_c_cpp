//18.7.24

// solution
// 각각 자기 위치에 있는 숫자와 swap한다.
#include <cstdio>

int arr[100005] = { -1, };

int main()
{
	int n;
	scanf("%d", &n);

	for (int i = 0; i < n; i++)
		scanf("%d", &arr[i]);

	int cnt = 0;
	for (int i = 0; i < n-1; i++)
	{
		if (arr[i] == i + 1)
			continue;
		for (int j = i + 1; j < n; j++)
		{
			if (i + 1 == arr[j])
			{
				int tmp = arr[i];
				arr[i] = arr[j];
				arr[j] = tmp;
				cnt += 1;
				break;
			}
		}
	}

	printf("%d\n", cnt);
}