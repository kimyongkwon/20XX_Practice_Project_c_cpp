// 18.9.21
// Search
// Pairs

#include <iostream>
#include <unordered_map>
#include <map>
#include <vector>
using namespace std;

// Complete the pairs function below.
int pairs(int k, vector<int> arr) {
	map<int, int, greater<int>> m;

	for (int i = 0; i < arr.size(); ++i)
		m[arr[i]] = 1;

	int cnt = 0;
	for (int i = 0; i < arr.size(); ++i) {
		if (m[arr[i] - k] != 0) {
			cnt += 1;
		}
	}
	return cnt;
}

int main()
{
	int n, k;
	cin >> n >> k;

	vector<int> v;
	v.resize(n, 0);

	for (int i = 0; i < v.size(); ++i)
		cin >> v[i];

	cout << pairs(k, v) << endl;
}