#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <string>
using namespace std;

// Complete the countMoves function below.
long countMoves(vector<int> numbers) {
	
	int cnt = 0;

	while (1) {
		int equal = 0;
		int tmp = numbers[0];
		int check = numbers[0];
		int except = 0;

		for (int i = 0; i < numbers.size(); i++) {
			if (numbers[i] >= tmp) {
				tmp = numbers[i];
				except = i;
			}
			if (numbers[i] == check)
				equal += 1;
		}

		if (equal == numbers.size())
			break;

		for (int i = 0; i < numbers.size(); i++) {
			if (except != i)
				numbers[i]++;
		}

		cnt += 1;
	}

	return cnt;
}


int main()
{
	//ofstream fout(getenv("OUTPUT_PATH"));

	int numbers_count;
	cin >> numbers_count;
	cin.ignore(numeric_limits<streamsize>::max(), '\n');

	vector<int> numbers(numbers_count);

	for (int i = 0; i < numbers_count; i++) {
		int numbers_item;
		cin >> numbers_item;
		cin.ignore(numeric_limits<streamsize>::max(), '\n');

		numbers[i] = numbers_item;
	}

	long res = countMoves(numbers);

	cout << res << "\n";

	//fout.close();

	return 0;
}