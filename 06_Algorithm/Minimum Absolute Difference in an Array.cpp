// 18.9.17
// Greedy Algorithms
// Minimum Absolute Difference in an Array

#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <math.h>
using namespace std;


// Complete the minimumAbsoluteDifference function below.
int minimumAbsoluteDifference(vector<int> arr) {

	sort(arr.begin(), arr.end());

	long long min = 1000000;
	long long tmp = 0;
	for (int i = 0; i < arr.size()-1; ++i) {
			tmp = abs(arr[i] - arr[i + 1]);
			if (tmp < min)
				min = tmp;
	}
	return min;
}

int main()
{
	int n = 0;
	cin >> n;

	vector<int> v;
	v.resize(n, 0);

	for (int i = 0; i < v.size(); ++i)
		cin >> v[i];

	int result = minimumAbsoluteDifference(v);

	cout << result << "\n";

	return 0;
}
