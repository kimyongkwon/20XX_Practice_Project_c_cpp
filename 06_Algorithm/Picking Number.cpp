#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

vector<int> v;

int main()
{
	int n;
	scanf("%d", &n);

	v.resize(n, 0);

	for (size_t i = 0; i < v.size(); i++){
		scanf("%d", &v[i]);
	}

	int tmp = 0;
	int cnt1, cnt2;
	int tmpcnt = 0;
	for (size_t i = 0; i < v.size(); i++){
		cnt1 = 0;
		cnt2 = 0;
		tmp = v[i];
		for (size_t j = i; j < v.size(); j++) {
			if (tmp == v[j]) {
				cnt1 += 1;
				cnt2 += 1;
			}
			if (tmp - v[j] == 1) cnt1 += 1;
			if (tmp - v[j] == -1) cnt2 += 1;
		}
		tmpcnt = max(tmpcnt, max(cnt1, cnt2));
	}

	printf("%d", tmpcnt);
}