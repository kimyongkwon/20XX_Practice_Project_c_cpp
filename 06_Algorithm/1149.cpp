//18.4.9
//RGB거리
//다른사람이 푼 정답을 참고하였음

//#include <cstdio>
//#include <algorithm>
//
//using namespace std;
//
//int rgb[1001][3];
//
////최소값들을 저장하기위한 배열을 rgb배열과 같은 크기로 만든다.
//int dp[1001][3];
//
//int n;
//
//int RGB(void)
//{
//    //dp 칸은 0으로 초기화한다.
//    dp[0][0] = 0; dp[0][1] = 0; dp[0][2] = 0;
//    for ( int i = 1; i <= n; i++)
//    {   
//        //전 칸에 두개의 값을 비교해서 작은값을 현재 rgb값과 더해서 같은자리의 dp배열에 저장한다.
//        //이렇게 N까지 다 더하면 가장 끝 배열에는 RED부터 시작한 최소값, GREEN부터 시작한 최소값, BLUE부터 시작한 최소값이 저장된다. 
//        dp[i][0] = min(dp[i - 1][1], dp[i - 1][2]) + rgb[i][0];
//        dp[i][1] = min(dp[i - 1][0], dp[i - 1][2]) + rgb[i][1];
//        dp[i][2] = min(dp[i - 1][0], dp[i - 1][1]) + rgb[i][2];
//    }
//
//    return min(min(dp[n][0], dp[n][1]), dp[n][2]);
//}
//
//int main()
//{ 
//    scanf("%d", &n);
//
//    for ( int i = 1; i <= n; i++)
//        scanf("%d %d %d", &rgb[i][0], &rgb[i][1], &rgb[i][2]);
//
//    printf("%d", RGB());
//}


//5.28
//다시 풀어보기

//탑다운
//#include <cstdio>
//#include <vector>
//#include <algorithm>
//using namespace std;
//
//int r[1005];
//int g[1005];
//int b[1005];
//
//int main()
//{
//	int t;
//	scanf("%d", &t);
//
//	g[0], r[0], b[0] = 0;
//
//	for (int i = 1; i <= t; i++)
//	{
//		scanf("%d %d %d", &r[i], &g[i], &b[i]);
//		r[i] = min(g[i-1] + r[i], b[i-1] + r[i]);
//		g[i] = min(r[i-1] + g[i], b[i-1] + g[i]);
//		b[i] = min(r[i-1] + b[i], g[i-1] + b[i]);
//	}
//
//	printf("%d", min(min(r[t], g[t]), b[t]));
//}


//바텀업
#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

int r[1005];
int g[1005];
int b[1005];

int dp(int n)
{
	if (n == 0) return min(min(r[n],g[n]),b[n]);

	r[n - 1] = min(g[n] + r[n - 1], b[n] + r[n - 1]);
	g[n - 1] = min(r[n] + g[n - 1], b[n] + g[n - 1]);
	b[n - 1] = min(r[n] + b[n - 1], g[n] + b[n - 1]);
	
	return dp(n-1);
}

int main()
{
	int t;
	scanf("%d", &t);

	for (int i = 0; i < t; i++)
		scanf("%d %d %d", &r[i], &g[i], &b[i]);
	printf("%d", dp(t));
}