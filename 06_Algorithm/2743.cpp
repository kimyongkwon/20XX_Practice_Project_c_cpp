//18.5.3
//단어 길이 재기

#include <cstdio>
#include <string>

int main()
{
	char str[100];
	scanf("%s", str);

	int cnt = 0;
	for (int i = 0; str[i] != '\0'; i++)
		cnt++;

	printf("%d", cnt);
}