//18.6.15
//������
//��Ƴ�

#include <cstdio>
#include <algorithm>
using namespace std;

int dp[100005];

int main()
{
	int str[100005];

	int n;
	scanf("%d", &n);

	for (int i = 1; i <= n; i++)
		scanf("%d", &str[i]);

	dp[0] = 0;
	dp[1] = str[1];

	for (int i = 2; i <= n; i++)
		dp[i] = max(max(dp[i - 1] + str[i], dp[i]), dp[i - 1]);

	printf("%d", dp[n]);
}