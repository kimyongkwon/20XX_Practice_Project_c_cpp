#include <stdio.h>

int main()
{
    int num;
    scanf("%d", &num);

    int sum = 0;
    int score = 0;
    for ( int i = 0; i < num; i++ )
    {
        int ans;
        scanf("%d", &ans);

        if ( ans == 1 )
            score += 1;
        else
            score = 0;
        sum += score;
    }
    printf("%d \n", sum);
}


