// 18.9.12
// LInked List
// Inserting a Node Into a Sorted Doubly Linked List

#include <iostream>
using namespace std;

class DoublyLinkedListNode {
public:
	int data;
	DoublyLinkedListNode *next;
	DoublyLinkedListNode *prev;

	DoublyLinkedListNode(int node_data) {
		this->data = node_data;
		this->next = nullptr;
		this->prev = nullptr;
	}
};

class DoublyLinkedList {
public:
	DoublyLinkedListNode *head;
	DoublyLinkedListNode *tail;

	DoublyLinkedList() {
		this->head = nullptr;
		this->tail = nullptr;
	}

	void insert_node(int node_data) {
		DoublyLinkedListNode* node = new DoublyLinkedListNode(node_data);

		if (!this->head) {
			this->head = node;
		}
		else {
			this->tail->next = node;
			node->prev = this->tail;
		}

		this->tail = node;
	}
};

void print_doubly_linked_list(DoublyLinkedListNode* node) {
	while (node) {
		cout << node->data;

		node = node->next;

		if (node) {
			cout << " ";
		}
	}
}

void free_doubly_linked_list(DoublyLinkedListNode* node) {
	while (node) {
		DoublyLinkedListNode* temp = node;
		node = node->next;

		free(temp);
	}
}

DoublyLinkedListNode* sortedInsert(DoublyLinkedListNode* head, int data) {
	
	//DoublyLinkedListNode* cur = head;

	//DoublyLinkedListNode* newNode = new DoublyLinkedListNode(data);
	//newNode->next = nullptr;

	//while (1) {
	//	if ( cur->next != nullptr) {
	//		if (cur->next->data > data && cur->data < data) {
	//			//
	//			cur->next->prev = newNode;
	//			newNode->next = cur->next;
	//			cur->next = newNode;
	//			newNode->prev = cur;
	//			break;
	//		}
	//		else if (cur->data > data) {
	//			head = newNode;
	//			newNode->prev = head;
	//			newNode->next = cur;
	//			cur->next->prev = newNode;
	//			break;
	//		}
	//	}
	//	else if (cur->next == nullptr) {
	//		cur->next = newNode;
	//		newNode->prev = cur;
	//		break;
	//	}
	//	cur = cur->next;
	//}
	//return head;

	DoublyLinkedListNode* newNode = new DoublyLinkedListNode(data);
	DoublyLinkedListNode* cur = head;

	// 새 노드가 리스트 맨 처음에 들어가야할 경우
	if (cur->data > data) { 
		newNode->next = cur;
		cur->prev = newNode;
		return newNode;
	}
	// 새 노드가 리스트 중간이나 마지막에 들어가야할 경우
	else {
		/* Walk list with 2 pointers */
		DoublyLinkedListNode* prev = nullptr;
		
		// cur이 nullptr일 때까지(리스트 마지막에 삽입된다는 뜻) 
		// 또는 cur이 가르키는 데이터보다 삽입 데이터가 작을 때(리스트 중간에 값이 삽입된다는 뜻)까지 while문을 순회한다.
		while (cur != nullptr && cur->data < data) {
			prev = cur;
			cur = cur->next;
		}

		// cur이 nullptr을 가르키고 prev는 리스트의 마지막 유효한 값을 가리키기 때문에
		// newNode는 리스트 마지막에 삽입된다.
		if (cur == nullptr) { 
			prev->next = newNode;
			newNode->prev = prev;
		}
		// cur이 nullptr을 가리키지 않는다면 newNode는 리스트 중간에 삽입된다
		else { 
			prev->next = newNode;
			cur->prev = newNode;
			newNode->prev = prev;
			newNode->next = cur;
		}
		return head;
	}
}

int main()
{

	int t;
	cin >> t;
	cin.ignore(numeric_limits<streamsize>::max(), '\n');

	for (int t_itr = 0; t_itr < t; t_itr++) {
		DoublyLinkedList* llist = new DoublyLinkedList();

		int llist_count;
		cin >> llist_count;
		cin.ignore(numeric_limits<streamsize>::max(), '\n');

		for (int i = 0; i < llist_count; i++) {
			int llist_item;
			cin >> llist_item;
			cin.ignore(numeric_limits<streamsize>::max(), '\n');

			llist->insert_node(llist_item);
		}

		int data;
		cin >> data;
		cin.ignore(numeric_limits<streamsize>::max(), '\n');

		DoublyLinkedListNode* llist1 = sortedInsert(llist->head, data);

		print_doubly_linked_list(llist1);
		cout << "\n";

		free_doubly_linked_list(llist1);
	}


	return 0;
}
