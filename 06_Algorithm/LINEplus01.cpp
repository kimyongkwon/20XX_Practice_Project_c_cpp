//01

#include <cstdio>

using namespace std;

/*
* Time complexity: TODO
* Space complexity: TODO
*/
int main(int argc, const char *argv[]) 
{
    int T;
    scanf("%d\n", &T);

    for ( int i = 0; i < T; ++i )
    {
        char buf[BUFSIZ];
        scanf("%s", buf);

        char tmp = buf[0];
        int cnt = 0;

        for ( int j = 0; buf[j] != '\0'; j++)
        {
            if ( tmp != buf[j] )
            {
                printf("%d%c", cnt, tmp);
                tmp = buf[j];
                cnt = 1;
            }
            else
                cnt++;
        }
        printf("%d%c \n", cnt, tmp);
    }

    return 0;
}
