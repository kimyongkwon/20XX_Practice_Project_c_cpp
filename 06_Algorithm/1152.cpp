//18.3.21

//#include <stdio.h>
//
//int main()
//{
//    char str[1000000] = {};
//
//    fgets(str,1000000,stdin);
//
//    int cnt = 0;
//    for ( int i = 0; i < 1000000; i++ )
//        if ( str[i] == ' ' ) cnt++;
//
//    printf("%d", cnt + 1);
//}


#include <stdio.h>

int main()
{
    char str[1000001] = {};
    bool b = false;

    fgets(str, 1000001, stdin);

    int cnt = 0;
    for ( int i = 0; str[i] != '\n'; i++ )
    {
        //공백이 아니면 문자라는 뜻이다
        //b를 true로 바꿔준다.
        if ( str[i] != ' ' )
            b = true;

        //만약 b가 true고(전에 있던 배열요소가 공백이 아니었다는 것이다.
        //지금 배열요소가 공백이라면 단어이므로 cnt를 하나 증가시켜준다.
        //그리고 false
        if ( (b == true && str[i] == ' '))
        {
            b = false;
            cnt++;
        }

        //printf("%d \n", i);
    }

    // qwe' 'qwe\n\0 같이 ' '이 마지막이 아니라 개행으로 문장이 끝났다면 아래의 코드가 있어야한다.
    if ( b )
        cnt++;

    printf("%d", cnt);
}
