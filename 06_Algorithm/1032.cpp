//18.5.4
//명령 프롬프트

#include <cstdio>

int main()
{
	int n;
	scanf("%d", &n);

	char str[52][52] = { 0, };

	//단어를 n개만큼 입력
	for (int i = 0; i < n; i++)
		scanf("%s", str[i]);

	//첫번째 단어를 임시변수에 저장
	char * tmp = str[0];

	//두번째단어부터 n까지 순회한다.
	for (int i = 1; i < n; i++)
	{
		//tmp와 단어를 하나하나 비교한다.
		for (int j = 0; str[i][j] != '\0'; j++)
		{ 
			if (tmp[j] == str[i][j])		//만약 tmp와 단어가 같으면 tmp에 저장
				tmp[j] = str[i][j];
			else
				tmp[j] = '?';				//다르면 ?을 저장
		}
	}

	printf("%s", tmp);						
}
