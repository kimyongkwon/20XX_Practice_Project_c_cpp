// 18.9.21
// Search
// Triple sum

#include <iostream>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <set>
#include <map>
#include <vector>
using namespace std;

// Complete the triplets function below.
long triplets(vector<int> a, vector<int> b, vector<int> c) {
	sort(a.begin(), a.end());
	sort(b.begin(), b.end());
	sort(c.begin(), c.end());

	a.erase(unique(a.begin(), a.end()), a.end());
	b.erase(unique(b.begin(), b.end()), b.end());
	c.erase(unique(c.begin(), c.end()), c.end());

	int j = 0, k = 0;
	long cnt = 0;
	for (int i = 0; i < b.size(); ++i)
	{
		while (j < a.size() && a[j] <= b[i] )
			j++;
		while (k < c.size() && c[k] <= b[i] )
			k++;
		cnt += j * k;
		//j = 0, k = 0;
	}
	return cnt;
}

int main()
{
	vector <int> a, b, c;

	int n1, n2, n3;

	cin >> n1 >> n2 >> n3;

	a.resize(n1, 0);
	b.resize(n2, 0);
	c.resize(n3, 0);

	for (int i = 0; i < n1; ++i)
		cin >> a[i];

	for (int i = 0; i < n2; ++i)
		cin >> b[i];

	for (int i = 0; i < n3; ++i)
		cin >> c[i];

	cout << triplets(a, b, c) << endl;
}
