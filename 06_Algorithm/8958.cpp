#include <stdio.h>

int main()
{
    int num;
    scanf("%d", &num);

    char str[80] = {};
    for ( int i = 0 ; i < num; i++ )
    {
        scanf("%s", str);

        int sum = 0;
        int score = 0;
        for ( int j = 0; str[j] != '\0'; j++ )
        {
            if ( str[j] == 'O' )
                score += 1;
            else
                score = 0;
            sum += score;
        }

        printf("%d \n", sum);
    }
}