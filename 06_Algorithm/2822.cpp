//18.6.5
//���� ���

#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

int sum = 0;

int main()
{
	vector<pair<int, bool>> scores;
	scores.resize(8, { -1, false });

	for (int i = 0; i < 8; i++)
		scanf("%d", &scores[i].first);

	for (int i = 0; i < 5; i++) {
		int tmp = -1, tmpidx = 0;
		for ( int j = 0; j < scores.size(); j++){
			if (tmp < scores[j].first && scores[j].second == false) {
				tmp = scores[j].first;
				tmpidx = j;
			}
		}
		sum += tmp;
		scores[tmpidx].second = true;
	}

	printf("%d\n", sum);
	for (int i = 0; i < scores.size(); i++) {
		if (scores[i].second == true )
			printf("%d ", i+1);
	}
}