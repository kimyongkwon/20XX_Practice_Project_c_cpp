//18.6.13
//경로 찾기

#include <iostream>
#include <cstdio>
#include <vector>
#include <queue>
using namespace std;

vector<pair<vector<int>, bool>> G;
queue<int> q;
int N;

bool DFS(int curVertex, int targetVertex)
{
	G[curVertex].second = true;						//현재 정점을 방문했으므로 true로 바꿔준다.
	
	for (auto adj : G[curVertex].first) {			//현재 정점에 인접해있는 정점들을 순회한다.
		if (adj == targetVertex)					//만약 인접정점들중에 targetVertex를 찾았다면 true반환
			return true;

		if (G[adj].second == false) {				//인접정점이 내가 찾고자하는 정점이 아니었고 한번도 방문하지 않았다면 DFS로 재귀호출한다.
			if (DFS(adj, targetVertex))				//DFS가 true가 반환됐다는 것은 재귀호출하는 도중에 targetVertex를 찾았다는 의미
				return true;
		}
	}

	//for문을 빠져나왔다는것은 정점의 인접정점들중에 targetVertex를 찾지 못했다는 의미다.
	return false;
}

bool BFS(int curVertex, int targetVertex)
{
	G[curVertex].second = true;
	q.push(curVertex);

	while (!q.empty()) {
		int qsize = q.size();
		for (int i = 0; i < qsize; i++){
			curVertex = q.front();
			q.pop();
			for (int elem : G[curVertex].first) {
				if (elem == targetVertex)
					return true;

				if (G[elem].second == false) {
					G[elem].second = true;
					q.push(elem);
				}
			}
		}
	}

	return false;
}

void init(void)
{
	int qsize = q.size();
	while (qsize--)
		q.pop();

	for (int k = 1; k <= N; k++)
		G[k].second = false;
}
int main()
{
	scanf("%d", &N);
	
	//정점의 갯수를 셋팅(1부터 시작하기떄문에 +1해줬다)
	G.resize(N+1);

	int adj;
	//그래프의 인접행렬 요소를 입력한다. 
	//입력하는 과정에서 어떤 정점(G)에 인접해있는 정점들을 추가시켜준다.
	for (int i = 1; i <= N; i++) {				
		G[i].second = false;				//정점은 한번도 방문하지 않았기떄문에 false로 초기화
		for (int j = 1; j <= N; j++) {
			scanf("%d", &adj);
			if (adj == 1)					//숫자가 1일때만 한 정점의 인접정점으로 추가된다.
				G[i].first.push_back(j);							
		}
	}

	//인접행렬의 요소를 하나하나씩 순회한다.
	for (int i = 1; i <= N; i++) {
		for (int j = 1; j <= N; j++) {
			//if (DFS(i, j))					//DFS로 순회한다.
			if (BFS(i, j))						//BFS로 순회한다.
				printf("1 ");
			else
				printf("0 ");

			init();
			
		}
		printf("\n");
	}
}