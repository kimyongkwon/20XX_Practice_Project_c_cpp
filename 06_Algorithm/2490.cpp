//18.5.15
//������

#include <cstdio>

int main()
{
	int arr[4];

	for ( int i = 0; i < 3; i++){

		for (int j = 0; j < 4; j++)
			arr[j] = 0;

		scanf("%d %d %d %d", &arr[0], &arr[1], &arr[2], &arr[3]);

		int cnt = 0;
		for (auto elem : arr){
			if (elem == 0) {
				cnt++;
			}
		}

		if (cnt == 0) 
			printf("%c", 'E');
		else if (cnt == 1) 
			printf("%c", 'A');
		else if (cnt == 2) 
			printf("%c", 'B');
		else if (cnt == 3) 
			printf("%c", 'C');
		else if (cnt == 4) 
			printf("%c", 'D');

		printf("\n");
	}
}