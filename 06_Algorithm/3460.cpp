//18.5.9
//������

#include <iostream>

int main()
{
	int t;
	scanf("%d", &t);

	int num = 0;
	for (int i = 0; i < t; i++)
	{
		scanf("%d", &num);

		int cnt = 0;
		while (num)
		{
			if (num % 2)
				printf("%d ", cnt);
			num /= 2;
			cnt++;
		}
	}
}
