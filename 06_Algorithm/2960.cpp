//18.6.2
//에라토스테네스의 체

#include <cstdio>
#include <vector>
using namespace std;

vector<bool> v;

int main()
{	
	int n, k;
	scanf("%d %d", &n, &k);

	v.resize(n + 5, false);

	for (int i = 2; i <= n; i++){
		int tmp = i;
		int p = 1;
		while (tmp <= n){
			if (v[tmp] == false) {
				v[tmp] = true;
				k--;
				if (k == 0) {
					printf("%d", tmp);
					return 0;
				}
			}
			tmp = i * p;
			p++;
		}
	}

	return 0;
}