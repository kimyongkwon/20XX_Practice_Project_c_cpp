//18.4.11
//스택

#include <cstdio>
#include <string.h>
using namespace std;

typedef struct Node {
    int val;
    Node * next = nullptr;
}Node;

class CStack {
    Node * m_pHead = nullptr;
    int m_nDatas;
public:
    CStack();
    ~CStack();
    void Push(int val);
    int Pop(void);
    int Size(void) const;
    int Top(void) const;
    int Empty(void) const;
};

CStack::CStack()
{
    m_nDatas = 0;
}

CStack::~CStack()
{
    if ( m_pHead ) delete m_pHead;
}

void CStack::Push(int val)
{
    if ( m_pHead == nullptr )
    {
        m_pHead = new Node;
        m_pHead->next = nullptr;
        m_pHead->val = val;
    }
    else
    {
        Node * newNode = new Node;
        newNode->val = val;
        newNode->next = m_pHead;

        m_pHead = newNode;
    }

    m_nDatas++;
}

int CStack::Pop(void)
{
    if ( !Empty() )
    {
        Node * tmp = m_pHead;       //top노드를 임시적으로 가르키는 tmp
        int data = tmp->val;        //top노드의 값을 data변수에 임시저장

        m_pHead = tmp->next;        //head포인터를 top 다음 노드로 옴긴다.
        delete tmp;                 //top제거(이제부터 top노드는 다음 노드가 된다)

        m_nDatas--;

        return data;
    }
    return -1;
}

int CStack::Size(void) const
{
    return m_nDatas;
}

int CStack::Top(void) const
{
    if ( m_nDatas )
        return m_pHead->val;
    return -1;
}

int CStack::Empty(void) const
{
    if ( m_nDatas )
        return 0;
    return 1;
}

int main()
{
    int n;
    scanf("%d", &n);

    char str[50];

    CStack stack;

    while ( n-- )
    {
        scanf("%s", str);

        if ( !strcmp(str, "push") )
        {
            int num = 0;
            scanf("%d", &num);
            stack.Push(num);
        }

        else if ( !strcmp(str, "pop") )
            printf("%d\n", stack.Pop());

        else if ( !strcmp(str, "top") )
            printf("%d\n", stack.Top());

        else if ( !strcmp(str, "size") )
            printf("%d\n", stack.Size());

        else if ( !strcmp(str, "empty") )
            printf("%d\n", stack.Empty());
    }
}