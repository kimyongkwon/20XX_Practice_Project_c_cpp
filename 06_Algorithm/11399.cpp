//11399
//ATM

#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

int main()
{
	int n;
	scanf("%d", &n);

	vector<int> v;
	v.resize(n, -1);

	for (int i = 0; i < v.size(); i++) 
		scanf("%d", &v[i]);

	sort(v.begin(), v.end());

	int sum = 0;
	for (int i = 0; i < v.size(); i++) {
		for (int j = 0; j <= i; j++) {
			sum += v[j];
		}
	}

	printf("%d", sum);
}