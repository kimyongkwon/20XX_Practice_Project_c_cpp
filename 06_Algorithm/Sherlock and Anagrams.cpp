// 18.9.10
// Dictionaries and Hashmaps
// Sherlock and Anagrams

#include <iostream>
#include <string>
#include <algorithm>
#include <unordered_map>
using namespace std;

int main()
{
	int num;
	string str;
	scanf("%d", &num);

	int pairs;

	for (size_t i = 0; i < num; i++)
	{
		unordered_map<string, int> hash;
		pairs = 0;
		cin >> str;
		for (size_t i = 0; i < str.length(); i++)
		{
			for (size_t j = 1; j < str.length(); j++)
			{
				if (i + j <= str.length()) {
					// substr은 문자열에서 i인덱스부터 j개의 서브스트링을 불러온다.
					string tmp = str.substr(i, j);
					// sort를 하면 현재 문자열이 어떤 문자열의 애너그램인지를 알 수 있다.
					sort(tmp.begin(), tmp.end());
					hash[tmp] += 1;
				}
			}
		}

		for (auto elem : hash) {
			int val = elem.second;
			if (val > 1) {
				for (int i = val - 1; i > 0; --i) {
					pairs += i;
				}
			}
		}
		cout << pairs << endl;
	}
}