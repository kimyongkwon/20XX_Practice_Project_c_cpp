//18.3.10

//10817
//Ǯ��1
//#include <stdio.h>
//
//int main()
//{
//    int n1,n2,n3;
//    int tmp;
//
//    scanf("%d %d %d", &n1, &n2, &n3);
//    
//    if ( (n1 >= n2 && n1 <= n3) || (n1 <= n2 && n1 >= n3) ) printf("%d", n1);
//    else if ( (n2 >= n1 && n2 <= n3) || (n2 <= n1 && n2 >= n3) ) printf("%d", n2);
//    else if ( (n3 >= n1 && n3 <= n2) || (n3 <= n1 && n3 >= n2) ) printf("%d", n3);
//}

//Ǯ��2
#include <stdio.h>
#include <algorithm>

int main()
{
    int n[4];

    scanf("%d %d %d", &n[0], &n[1], &n[2]);

    std::sort(n, n + 3);

    printf("%d", n[1]);
}