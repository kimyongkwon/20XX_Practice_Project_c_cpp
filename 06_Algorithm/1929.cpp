//18.4.6

//'에라토스테네스의 체'를 사용했음

#include <cstdio>

const int MAX = 1000001;

int main()
{
    int m, n;
    scanf("%d %d", &m, &n);

    //bool 배열 선언
    bool arr[MAX] = {false};

    //1은 항상 소수이므로 true
    arr[1] = true;

    //i는 2부터 n까지의 배수를 체크하기 위한 변수다. 
    for ( int i = 2; i <= n; i++ )
    {
        //j가 1일때는 제외하기하기위해 2부터 사용한다. 
        for ( int j = 2; i * j <= n; j++ )
        {
            //arr배열의 값이 false면 true로 바꿔준다.
            //이러면 중복체크 하지 않게 된다.
            //다음 if문은 없어도 상관없다.
            if ( arr[i * j] != true )
                arr[i * j] = true;
        }
    }

    //m부터 n까지 소수를 출력한다. 
    //false값을 가진 것이 소수다.
    for ( int i = m; i <= n; i++ )
    {
        if ( arr[i] == false )
            printf("%d\n", i);
    }
}