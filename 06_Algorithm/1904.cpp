//18.5.29
//01타일

#include <cstdio>
#include <vector>

using namespace std;
const int MOD = 15746;
vector<int> dp;

int main()
{
	int n = 0;
	scanf("%d", &n);

	dp.resize(n + 1, -1);
	
	dp[0] = 1;
	dp[1] = 1;

	// 다 계산하고 모듈러 연산을 해줄 경우 값의 크기를 감당할 수 없으므로
	// 모듈러 연산의 성질을 이용해서 계산할 때마다 해준다.
	// (a+b) % c = (a%c+b%c)%c			(현재 우항을 적용해줬음)

	for (int i = 2; i <= n; i++)
		dp[i] = dp[i - 1] % MOD + dp[i - 2] % MOD;

	printf("%d", dp[n] % MOD);
}