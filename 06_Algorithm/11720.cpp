//18.3.9
//제출본

#include <stdio.h>

int main()
{
    int n;
    char str[100];

    //str에 들어간 숫자는 숫자가 아니라 문자 '숫자' 다.
    scanf("%d %s", &n, str);

    int sum = 0;
    for ( int i = 0; i < n; i++ )
        sum += str[i] - '0';
    //'0'을 빼는 이유는 예를 들어 첫번째에 5를 입력하면
    //문자'5'가 입력되고 '5' - '0'을 하면 숫자 5가 나오기떄문이다.

    printf("%d", sum);
}


//백준알고리즘에서 컴파일 에러. conio를 못읽음

//#include <stdio.h>
//#include <conio.h>
//
//int main()
//{
//    int n;
//    scanf("%d", &n);
//
//    char key;
//    int sum = 0;
//    while(n)
//    {
//        key = getch();
//        printf("%c", key);
//        n--;
//        sum += key - 48;
//    }
//
//    printf("%d", sum);
//}

