//18.3.29

#include <stdio.h>

int main()
{
    int num;
    scanf("%d", &num);

    char str[100] = { 0, };
    
    //그룹 단어가 몇개 있는지 체크하는 변수 cnt
    int cnt = 0;
   
    while (num)
    {
        scanf("%s", str);

        //알파벳의 갯수는 26개다. false는 해당 알파벳이 한번도 안나왔다는 의미
        bool a[26] = { false, };
        bool isGroup = true;        //그룹 단어인지 체크하는 변수, true가 그룹 단어라는 의미다.

        int tmp;
        for ( int i = 0 ; str[i] != NULL; i++)
        {
            //어떤 문자가 false였으면 한번도 안나왔다는 의미다.
            if ( a[str[i] - 'a'] == false )
            {
                //해당 문자가 등장했으므로 true로 변경한다.
                a[str[i] - 'a'] = true;
                //그리고 tmp라는 임시변수에 해당 단어를 저장한다.
                tmp = str[i] - 'a';
            }
            //해당 문자가 이전에 등장했다면?
            else if ( a[str[i] - 'a'] == true )
            {
                //근데 만약 바로 전에 등장했다면? (tmp는 이전의 단어가 들어가 있다)
                //그냥 continue
                if ( tmp == str[i] - 'a' )
                    continue;
                //바로 전이 아니라 예전에 등장한 문자라면 현재 문자열은 그룹 단어이다.
                else 
                    isGroup = false;
            }
        }

        if ( isGroup ) cnt++;

        num--;
    }

    printf("%d", cnt);
}