// 18,9.26
// Search
// Minimum Time Required

#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

long func(vector<long>& machines, long mid)
{
	long items = 0;
	for (int i = 0; i < machines.size(); ++i) {
		items += (mid / machines[i]);
	}
	return items;
}

long bs(vector<long>& machines, long goalitems, long high_day)
{
	long low_day = 1;
	long mid_day = 0;

	while (low_day < high_day)
	{
		mid_day = (low_day + high_day) / 2;
		
		cout << "low => " << low_day << " mid => " << mid_day << " high => " << high_day << endl;

		long items = func(machines, mid_day);

		if (items < goalitems)
			low_day = mid_day + 1;
		else
			high_day = mid_day;
	}

	return low_day;
}

// Complete the minTime function below.
long minTime(vector<long> machines, long goalitems)
{
	//long day = *min_element(machines.begin(), machines.end());
	//int cnt = 0;
	//while (1) {
	//	for (int i = 0; i < machines.size(); ++i) {
	//		if (( day % machines[i]) == 0) {
	//			cnt += 1;
	//			if (cnt == goal)
	//				return day;
	//		}
	//	}
	//	day += 1;
	//}

	//return day;
	
	long max_machine = *max_element(machines.begin(), machines.end());

	return bs(machines, goalitems, goalitems * max_machine);
}

int main()
{
	int n, goal;
	cin >> n >> goal;

	vector<long> v;
	v.resize(n, -1);
	for (int i = 0; i < n; ++i)
		cin >> v[i];

	cout << minTime(v, goal) << endl;
}

// Efficient C++ program to find minimum time 
// required to produce m items. 
//#include <iostream>
//#include <vector>
//#include <algorithm>
//using namespace std;
//
//// Return the number of items can be 
//// produced in temp sec. 
//int findItems(int arr[], int n, int temp)
//{
//	int ans = 0;
//	for (int i = 0; i < n; i++)
//		ans += (temp / arr[i]);
//	return ans;
//}
//
//// Binary search to find minimum time required 
//// to produce M items. 
//int bs(int arr[], int n, int goal, int high)
//{
//	int low = 1;
//
//	// Doing binary search to find minimum 
//	// time. 
//	while (low < high)
//	{
//		// Finding the middle value. 
//		int mid = (low + high) >> 1;
//
//		// Calculate number of items to 
//		// be produce in mid sec. 
//		int itm = findItems(arr, n, mid);
//
//		// If items produce is less than 
//		// required, set low = mid + 1. 
//		if (itm < goal)
//			low = mid + 1;
//
//		//  Else set high = mid. 
//		else
//			high = mid;
//	}
//
//	return high;
//}
//
//// Return the minimum time required to 
//// produce m items with given machine. 
//int minTime(int arr[], int n, int goal)
//{
//	int maxval = INT_MIN;
//
//	// Finding the maximum time in the array. 
//	for (int i = 0; i < n; i++)
//		maxval = max(maxval, arr[i]);
//
//	return bs(arr, n, goal, maxval*goal);
//}
//
//// Driven Program 
//int main()
//{
//	int arr[] = { 1, 2, 3 };
//	int n = sizeof(arr) / sizeof(arr[0]);
//	int goal = 11;
//
//	cout << minTime(arr, n, goal) << endl;
//
//	return 0;
//}