//18.4.9

////업다운 방식
//#include <cstdio>
//
//int dp[45];
//
//int fibonacci(int n)
//{
//    if ( n == 0 )
//        return 0;
//    else if ( n == 1 )
//        return 1;
//    if ( dp[n] != 0 ) return dp[n];
//    dp[n] = fibonacci(n - 1) + fibonacci(n - 2);
//    return dp[n];
//}
//
//int main()
//{
//    int n;
//
//    scanf("%d", &n);
//
//    printf("%d", fibonacci(n));
//}

//바텀업 방식
#include <cstdio>
#include <vector>
using namespace std;

int main()
{
    int n;

    scanf("%d", &n);
    
    vector<int> dp(n + 1, 0);

    
    dp[1] = 1;

    for ( int i = 2; i <= n; i++ )
        dp[i] = dp[i - 1] + dp[i - 2];

    printf("%d", dp[n]);
}