//18.5.30

//#include <iostream>
//#include <vector>
//using namespace std;
//
//vector<string> split_string(string);
//
//// Complete the plusMinus function below.
//void plusMinus(vector<int> arr) {
//
//	int positiveN = 0, negativeN = 0, zeroN = 0;
//	for (auto elem : arr) {
//		if (elem > 0)
//			positiveN += 1;
//		else if (elem < 0)
//			negativeN += 1;
//		else
//			zeroN += 1;
//	}
//
//	cout << (positiveN / (float)arr.size()) << endl;
//	cout << (negativeN / (float)arr.size()) << endl;
//	cout << (zeroN / (float)arr.size()) << endl;
//}
//
//int main()
//{
//	vector<int> arr;
//	arr.resize(6);
//	arr[0] = -4;
//	arr[1] = 3;
//	arr[2] = -9;
//	arr[3] = 0;
//	arr[4] = 4;
//	arr[5] = 1;
//
//	plusMinus(arr);
//}


//18.5.30
//stair case
//#include <iostream>
//#include <vector>
//using namespace std;
//
//// Complete the staircase function below.
//void staircase(int n) 
//{
//	for (int k = 1; k <= n; k++) {
//		for (int i = 0; i < n - k; i++)
//			cout << " ";
//		for (int j = 0; j < k; j++)
//			cout << "#";
//		cout << endl;
//	}
//}
//
//int main()
//{
//	int n;
//	cin >> n;
//	cin.ignore(numeric_limits<streamsize>::max(), '\n');
//
//	staircase(n);
//
//	return 0;
//}

//18.5.30
//mini max sum
//#include <iostream>
//#include <vector>
//#include <cstdio>
//#include <algorithm>
//using namespace std;
//
//vector<string> split_string(string);
//
//// Complete the miniMaxSum function below.
//void miniMaxSum(vector<int> arr) {
//
//	vector<long long> sum;
//	sum.resize(5, 0);
//
//	for (int i = 0; i < arr.size(); i++) {
//		for (int j = 0; j < arr.size(); j++) {
//			if (i != j)
//				sum[i] += arr[j];
//		}
//	}
//
//	int max = sum[0], min = sum[0];
//	for (int elem : sum) {
//		if (max < elem)
//			max = elem;
//		if (min > elem)
//			min = elem;
//	}
//
//	cout << min << " " << max << endl;
//}
//
//int main()
//{
//	
//	vector<int> v;
//	v.resize(5, 0);
//
//	for (int i = 0; i < v.size(); i++)
//		cin >> v[i];
//
//	miniMaxSum(v);
//}

//18.5.30
//Time Conversion
//#include <cstdio>
//#include <vector>
//#include <iostream>
//#include <algorithm>
//#include <string>
//
//using namespace std;
//
//string timeConversion(string s)
//{
//	int max = s.size();
//
//	if (s[max - 2] == 'P') {
//		if (s[0] == '0') {
//			int str = s[1] - '0';
//			str += 12;
//			s.erase(0, 2);
//			string tmp = to_string(str);
//			s.insert(0, tmp);
//		}
//		else if (s[0] == '1' && (s[1] == '0' || s[1] == '1')) {
//			int str1 = s[0] - '0';
//			str1 += 1;
//			int str2 = s[1] - '0';
//			str2 += 2;
//
//			s.erase(0, 2);
//
//			string tmp = to_string(str2);
//			s.insert(0, tmp);
//			tmp = to_string(str1);
//			s.insert(0, tmp);
//		}
//	}
//	else if (s[max - 2] == 'A') {
//		if (s[0] == '1') {
//			s[0] = '0';
//			s[1] = '0';
//		}
//	}
//
//	s.erase(max - 2, 2);
//
//	return s;
//}
//
//int main()
//{
//	string s;
//	getline(cin, s);
//
//	string result = timeConversion(s);
//
//	cout << result << endl;
//
//	return 0;
//}



//18.5.30
//Between Two Sets
//#include <cstdio>
//#include <vector>
//#include <iostream>
//#include <algorithm>
//#include <string>
//using namespace std;
//
//int main()
//{
//	bool arr[101] = { false, };
//
//	int a1 = 2, a2 = 4;
//
//	for (int i = 1; a1 * i <= 100; i++)
//	{
//		int tmp1 = a1 * i;
//		if (arr[tmp1] == false)
//			arr[tmp1] = true;
//	}
//
//
//
//
//	for (int i = 1; a2 * i <= 100; i++)
//	{
//		int tmp2 = a2 * i;
//		if (arr[tmp2] == false)
//			arr[tmp2] = true;
//	}
//
//
//
//	int b1 = 16, b2 = 32, b3 = 96;
//
//	for (int i = 1; i < 101; i++) {
//		if (arr[i] == true) {
//			if (b1%arr[i] != 0 || b2%arr[i] != 0 || b3 % arr[i] != 0)
//				arr[i] = false;
//		}
//	}
//
//	int cnt = 0;
//	for (int i = 1; i < 101; i++) {
//		if (arr[i] == true)
//			cnt += 1;
//	}
//
//	printf("%d", cnt);
//}