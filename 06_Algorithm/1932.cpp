//18.4.9

//내가 풀었음
//1149와 비슷한 방법으로 풀면된다.

#include <cstdio>
#include <algorithm>

using namespace std;

int n;

int arr[501][501];
int dp[501][501];

int func(void)
{
    for (int i = 0; i < n; i++)
    {
        for ( int j = 0; j <= i; j++)
        {
            if ( j == 0 )
                dp[i][j] = dp[i - 1][j] + arr[i][j];
            else if ( j == i )
                dp[i][j] = dp[i - 1][j - 1] + arr[i][j];
            else
                dp[i][j] = max(dp[i - 1][j - 1], dp[i - 1][j]) + arr[i][j];
        }
    }

    int tmp = 0;
    for ( int i = 0; i < n; i++)
    {
        if (dp[n-1][i] > tmp )
        {
            tmp = dp[n-1][i];
        }
    }

    return tmp;
}

int main()
{
    scanf("%d", &n);

    for ( int i = 0; i < n; i++)
    {
        for ( int j = 0; j <= i; j++ )
            scanf("%d", &arr[i][j]);
    }

    printf("%d", func());
}
