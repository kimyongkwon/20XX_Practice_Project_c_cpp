//18.4.3

#include <stdio.h>

int main()
{
    int n;

    scanf("%d", &n);

    //첫번째 분수 예외처리
    if ( n == 1 )
    {
        printf("1/1");
        return 0;
    }

    int a = 1;
    int b = 1;
    
    //분수는 왼쪽 아래 대각선 아니면 오른쪽 위 대각선으로 진행된다.
    //그 대각선의 방향을 구분하기위해 turn이라는 변수를 사용한다.
    bool turn = true;

    while ( n - 1 )
    {
        //turn에 따라서 끄트머리에 갔을 때 a(분자) 또는 b(분모)를 증가시킨다.
        turn ? b++ : a++;
        n--;                        //증가도 하나의 단계이기 때문에 n을 감소
        if ( n == 1 )               
            break;
        
        
        //turn이 참일때는 왼쪽 아래 대각선으로 진행된다.
        if ( turn )
        {
            //왼쪽아래 대각선으로 진행될때는 분모가 1일때까지 반복문진행
            while ( b != 1 )
            {
                a++;
                b--;
                n--;
                if ( n == 1 )
                    break;
            }
            //다음은 오른쪽 위 대각선으로 진행될것이다.
            turn = false;
        }
        //turn이 거짓일때는 오른쪽 위 대각선 진행
        else
        {
            //왼쪽아래 대각선으로 진행될때는 분자가 1일때까지 반복문진행
            while ( a != 1 )
            {
                a--;
                b++;
                n--;
                if ( n == 1 )
                    break;
            }
            turn = true;
        }
    }

    printf("%d/%d", a, b);
}