//18.3.14

#include <stdio.h>
#include <iostream>

int f(int n)
{
    int num = n;
 
    if ( n < 100 && n >= 10 )
        num += n / 10;
    else if ( n < 1000 && n >= 100 )
    {
        num += n / 100;
        n %= 100;
        num += n / 10;
    }
    else if ( n < 10000 && n >= 1000 )
    {
        num += n / 1000;
        n %= 1000;
        num += n / 100;
        n %= 100;
        num += n / 10;
    }
    num += n % 10;
    
    return num;
}

int main()
{
    bool arr[10001] = { false, };

    int n = 1;
    while (n <= 10000)
    {
        int k = f(n);
        if ( k <= 10000)
            arr[k] = true;
        n++;
    }

    for (int i = 1; i <= 10000; i++)
    {
        if ( arr[i] == false )
            printf("%d \n", i);
    }
}
