//18.4.3

#include <stdio.h>

int main()
{
    int N;
    scanf("%d", &N);
    
    int cnt = 0;
    
    for ( int i = 0; i < N; i++)
    {
        int n;
        scanf("%d", &n);

        int c = 0;
        int tmp = n;
        while ( n )
        {
            if ( tmp % n == 0 )
                c++;
            n--;
        }

        if ( c == 2 )
            cnt++;
    }

    printf("%d", cnt);
}