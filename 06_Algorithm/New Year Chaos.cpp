//18.7.20

//#include <iostream>
//#include <cstdio>
//#include <vector>
//using namespace std;
//
//void minimumBribes(vector<int> q)
//{
//	vector<int> tmp;
//	tmp.resize(q.size(), -1);
//	for (int i = 0; i < q.size(); i++)
//		tmp[i] = i + 1;
//
//	int cnt = 0;
//	int idx = tmp.size() - 1;
//	while (idx)
//	{
//		if ((tmp[idx - 1] == q[idx]) && (tmp[idx] == q[idx - 1]))
//		{
//			int n = tmp[idx - 1];
//			tmp[idx - 1] = tmp[idx];
//			tmp[idx] = n;
//			cnt += 1;
//			idx -= 2;
//			if (idx < 0)
//				break;
//		}
//		else if ((tmp[idx - 1] == q[idx]) || (tmp[idx] == q[idx - 1]))
//		{
//			int n = tmp[idx - 1];
//			tmp[idx - 1] = tmp[idx];
//			tmp[idx] = n;
//			cnt += 1;
//			idx--;
//		}
//		else
//			idx--;
//	}
//
//
//	for (int i = 0; i < q.size(); i++)
//	{
//		if (tmp[i] != q[i])
//		{
//			printf("Too chaotic\n");
//			break;
//		}
//		if (i == q.size() - 1)
//			printf("%d\n", cnt);
//	}
//}
//
//int main()
//{
//	int n;
//	scanf("%d", &n);
//
//	while (n--)
//	{
//		int k;
//		scanf("%d", &k);
//		vector<int> v;
//		v.resize(k, -1);
//		for (int i = 0; i < v.size(); i++)
//			scanf("%d", &v[i]);
//		minimumBribes(v);
//	}
//}

//--------------------------------

//
//#include <cstdio>
//#include <vector>
//using namespace std;
//
//int main()
//{
//	int t;
//	scanf("%d", &t);
//
//	while (t--)
//	{
//		int n = 0;
//		int chk = 0;
//		int result = 0;
//		vector<int> v;
//		vector<int> bribe;
//		scanf("%d", &n);
//		v.resize(n, 0);
//		bribe.resize(n+1, 0);
//		for (int k = 0; k < n; k++) {
//			scanf("%d", &v[k]);
//		}
//
//		//---------
//
//		for (int i = 0; i < n - 1; i++) {
//			for (int j = i + 1; j < n; j++) {
//				if (v[i] > v[j])
//					bribe[v[i]]++;
//			}
//			if (bribe[v[i]] > 2)
//				chk = 1;
//			else if (bribe[v[i]] <= 2)
//				result += bribe[v[i]];
//		}
//		if (chk == 0)
//			printf("%d\n", result);
//		else if (chk == 1)
//			printf("Too chaotic\n");
//	}
//}

//--------------------------------

//#include <vector>
//#include <iostream>
//#include <algorithm>
//
//int main() {
//	int t, n; std::cin >> t;
//	while (t-- > 0) {
//		std::cin >> n;
//		std::vector<int> line(n);
//		for (auto& p : line) 
//			std::cin >> p;
//
//		bool chaotic = false;
//		int count = 0;
//		for (int i = line.size() - 1; i >= 0; i--) {
//			if (line[i] - (i + 1) > 2) {
//				std::cout << "Too chaotic" << std::endl;
//				chaotic = true;
//				break;
//			}
//			for (int j = line[i] - 2 > 0 ? line[i] - 2 : 0; j < i; j++) {
//				if (line[j] > line[i]) {
//					count++;
//				}
//			}
//		}
//		if (!chaotic)
//			std::cout << count << std::endl;
//	}
//	return 0;
//}

//----------------------------------------

//#include <cstdio>
//using namespace std;
//
//const int maxN = 2e5 + 5;
//
//int n, a[maxN], ans, T, k;
//int bit[maxN];
//int invalid;
//
//void del() {
//	for (k = 0; k<maxN; k++) {
//		bit[k] = 0;
//	}
//	ans = 0;
//	invalid = 0;
//}
//
//void upd(int x) {
//	for (k = x; k < maxN; k += (k & (-k))) {
//		bit[k]++;
//	}
//}
//
//int get_sum(int x) {
//	int p = 0;
//	for (k = x; k > 0; k -= (k & (-k)))
//		p += bit[k];
//	return p;
//}
//
//int main() {
//	scanf("%d", &T);
//	while (T--) {
//		del();
//		scanf("%d", &n);
//
//		for (int i = 0; i < n; i++) {
//			scanf("%d", &a[i]);
//		}
//
//		for (int i = n - 1; i >= 0; i--) {
//			if (get_sum(a[i]) > 2)
//				invalid++;
//
//			ans += get_sum(a[i]);
//			upd(a[i]);
//		}
//		if (invalid > 0) {
//			printf("Too chaotic\n");
//		}
//		else {
//			printf("%d\n", ans);
//		}
//	}
//	return 0;
//}

#include <cstdio>

int arr[100005] = { -1, };
int sorted[100005];

int main()
{
	int t;
	scanf("%d", &t);
	while (t--)
	{
		int n;
		scanf("%d", &n);
		for (int i = 0; i < n; i++) {
			scanf("%d", &arr[i]);
			sorted[i] = i + 1;
		}

		bool chaotic = false;
		int sum = 0;
		for (int i = n - 1; i >= 0; i--)
		{
			int ans = i;
			int cnt = 0;
			while (sorted[i] != arr[ans])
			{
				cnt += 1;
				ans--;
			}

			if (cnt > 2) {
				chaotic = true;
				break;
			}
			else {
				sum += cnt;
				for (int k = 0; k < cnt; k++)
				{
					int tmp = sorted[i - k];
					sorted[i - k] = sorted[i - k - 1];
					sorted[i - k - 1] = tmp;
				}
			}
		}

		if (chaotic)
			printf("Too chaotic\n");
		else
		{
			printf("%d\n", sum);
			sum = 0;
		}
	}
}