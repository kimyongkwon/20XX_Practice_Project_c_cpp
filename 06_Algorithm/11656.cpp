//18.5.5
//접미사 배열

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
using namespace std;

int main()
{
	string str;
	cin >> str;

	vector<string> v1;
	string tmp;
	for (int i = 0; i < str.length(); i++)
	{
		tmp = str.substr(i, str.size());
		v1.push_back(tmp);
	}

	sort(v1.begin(), v1.end());

	for (auto elem : v1)
		cout << elem << endl;
}