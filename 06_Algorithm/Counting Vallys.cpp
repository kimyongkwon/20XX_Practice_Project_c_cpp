//18.6.15

#include <cstdio>

int main()
{
	int n;
	scanf("%d", &n);

	char* str = new char[n];

	scanf("%s", str);
	//str[n] = '\0';

	int height = 0;
	bool lock = false;
	int vallys = 0;
	for (int i = 0; i < n; i++) {
		if (str[i] == 'U') {
			height += 1;
		}
		if (str[i] == 'D') {
			height -= 1;
		}

		if (height < 0 && lock == false) {
			lock = true;
		}
		if (height == 0 && lock == true) {
			vallys += 1;
			lock = false;
		}
	}

	printf("%d", vallys);
}