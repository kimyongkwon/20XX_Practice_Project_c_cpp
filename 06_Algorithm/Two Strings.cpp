// 18.9.9
// Dictionaries and Hashmaps
// TwoStrings

#include <cstdio>
#include <string>
#include <iostream>
#include <unordered_map>
using namespace std;

int main()
{
	int num;
	scanf("%d", &num);

	std::string s1;
	std::string s2;
	for (size_t i = 0; i < num; i++)
	{
		// 문자열을 두개 입력받는다.
		std::cin >> s1;
		std::cin >> s2;

		// umap을 생성해서
		unordered_map<char, int> hash;
		// 첫번째 문자열의 문자들을 하나씩 
		// umap에 넣고
		for (auto s1_elem : s1)
		{
			hash[s1_elem] = 1;
		}
		bool check = false;
		for (auto s2_elem : s2)
		{
			if (hash[s2_elem] == 1) {
				cout << "YES" << endl;
				check = true;
				break;
			}
		}

		if (!check) printf("NO\n");
	}
}