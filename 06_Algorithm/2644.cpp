//18.6.4
//�̼����

#include <cstdio>
#include <vector>
#include <queue>
using namespace std;

typedef vector<int> adjs;
vector<pair<adjs,bool>> v;
queue<int> q;
int cnt = 0;

void makeEdge(int x, int y)
{
	v[x].first.push_back(y);
	v[y].first.push_back(x);
}

int BFS(int a, int b)
{
	int cur = a;
	v[cur].second = true;
	q.push(cur);

	while (!q.empty()) {
		cnt += 1;
		int qsize = q.size();
		for (int k = 0; k < qsize; k++) {
			cur = q.front();
			q.pop();
			for (int adjidx : v[cur].first ){
				if (adjidx == b)
					return cnt;
				if (v[adjidx].second == false) {
					v[adjidx].second = true;
					q.push(adjidx);
				}
			}
		}	
	}

	return -1;
}

int main()
{
	int n;
	scanf("%d", &n);
	v.resize(n + 1);

	for (int i = 0; i < v.size(); i++)
		v[i].second = false;

	int a, b;
	scanf("%d %d", &a, &b);

	int m;
	scanf("%d", &m);

	while (m--){
		int x, y;
		scanf("%d %d", &x, &y);

		makeEdge(x, y);
	}

	printf("%d", BFS(a, b));
}