//18.3.10

//내가 짠 코드
#include <stdio.h>

int main()
{
    int c;
    
    int stunum;
    int score[1000] = {0,};

    float arr[1000] = {0,};

    scanf("%d", &c);

    int j = 0;

    for ( int i = 0; i < c; i++)
    {
        scanf("%d", &stunum);
        int sum = 0;
        for ( int k = 0; k < stunum; k++)
        {
            scanf("%d", &score[k]);
            sum += score[k];
        }

        int cnt = 0;
        for ( int k = 0 ; k < stunum; k++ )
        {
            if ( sum / stunum < score[k] ) 
                cnt++;
            //다음과 같이 바꿀수 있다. 이렇게 바꾸는 이유는 실수연산을 줄이기 위해 이항한다.
            //if ( sum < score[k] * stunum )
        }
       
        arr[j++] = (float)cnt / stunum * 100;
    }

    for ( int i = 0 ; i < c; i++)
    {
        printf("%.3f%% \n", arr[i]);
    }
}