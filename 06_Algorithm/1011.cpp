//18.4.4

// 이건 시간초과 뜸 ㅠㅠ

//#include <stdio.h>
//#include <math.h>
//
//int main()
//{
//    int n;
//
//    scanf("%d", &n);
//
//    int x, y;
//    while ( n )
//    {
//        scanf("%d %d", &x, &y);
//
//        x += 1;
//        y -= 1;
//
//        //공간이동장치 작동 횟수
//        int cnt = 1;
//
//        int gap = y - x;
//        
//        int a = 0;
//        int b = 2;
//        int c = 0;
//        while ( gap )
//        {
//            
//            if ( a == b )
//            {
//                cnt++;
//                a = 0;
//                c += 1;
//                if ( c == 2 )
//                    b += 1;
//            }
//            
//            a++;
//            gap--;
//        }
//
//        printf("%d \n", cnt + 2);
//        n--;
//    }
//}



#include <stdio.h>

int main()
{
    int n;

    scanf("%d", &n);

    int x, y;
    int dist;
    while ( n-- )
    {
        scanf("%d %d", &x, &y);

        dist = y - x;

        int cnt = 0;
        long long i,min, max, powN = 0;
        for ( i = 1; ; i++ )
        {
            powN = i * i;
            min = powN - i + 1;
            max = powN + 1 + i - 1;

            if ( min <= dist && dist <= max )
            {
                if ( min <= dist && dist <= powN )
                    cnt = (i<<1) - 1;
                else 
                    cnt = (i<<1);
                break;
            }
        }
        printf("%d \n", cnt);
    }
}