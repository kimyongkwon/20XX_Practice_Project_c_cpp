//18.3.22

//내가 푼 거
#include <stdio.h>

int main()
{
    int a[8];

    for ( int i = 0; i < 8; i++ )
        scanf("%d", &a[i]);

    for ( int i = 0; i < 8; i++ )
    {
        //오름차순대로 수가 맞지 않으면 for문 탈출
        if ( a[i] != i + 1 )
            break;

        //탈출하지 않고 i가 7까지 왔다는 의미는 오름차순으로 수가 다 맞다는 의미다.
        if ( i == 7 )
        {
            printf("ascending");
            return 0;
        }
    }

    for ( int i = 0, k = 8; i < 8; i++, k-- )
    {
        //내림차순으로 수가 맞지 않으면 for문 탈출
        if ( a[i] != k )
            break;

        //탈출하지 않고 i가 7까지 왔다는 의미는 내림차순으로 수가 다 맞다는 의미다.
        if ( i == 7 )
        {
            printf("descending");
            return 0;
        }
    }

    //위의 두가지의 경우가 아닐경우
    printf("mixed");
}

//다른사람이 푼 문제
//#include <stdio.h>
//
//int main()
//{
//    int n;
//    int ans = 0;
//
//    //값을 입력할때마다 그 값을 자리수마다 더해나간다.
//    for ( int i = 0; i < 8; i++ )
//    {
//        scanf("%d", &n);
//        ans = ans * 10 + n;
//    }
//
//    if ( ans == 12345678 )
//        printf("ascending");
//    else if ( ans == 87654321 )
//        printf("descending");
//    else
//        printf("mixed");
//}