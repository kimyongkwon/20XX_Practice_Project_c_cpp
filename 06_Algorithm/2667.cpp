//18.6.7
//단지번호붙이기

#include <cstdio>
#include <vector>
#include <queue>
#include <string>
#include <iostream>
#include <algorithm>
using namespace std;

typedef vector<char> width;
vector<width> v;
queue<pair<char, char>> q;
vector<int> tmp;

int n = 0;
int cnt = 0;

int BFS(int n1, int n2)
{
	cnt = 0;
	int x = n1;
	int y = n2;
	v[y][x] = '0';
	q.push({ x,y });
	

	while (!q.empty()) {
		int qsize = q.size();
		for (int i = 0; i < qsize; i++) {
			cnt += 1;
			x = q.front().first;
			y = q.front().second;
			q.pop();

			if (x + 1 < n && v[y][x + 1] == '1') {
				v[y][x + 1] = '0';
				q.push({ x + 1, y });
			}
			if (x - 1 >= 0 && v[y][x - 1] == '1') {
				v[y][x - 1] = '0';
				q.push({ x - 1,y });
			}
			if (y + 1 < n && v[y + 1][x] == '1') {
				v[y + 1][x] = '0';
				q.push({ x,y + 1 });
			}
			if (y - 1 >= 0 && v[y - 1][x] == '1') {
				v[y - 1][x] = '0';
				q.push({ x,y - 1 });
			}
		}
	}

	return cnt;
}

int DFS(int n1, int n2)
{
	int x = n1;
	int y = n2;

	if (x + 1 < n && v[y][x + 1] == '1') {
		cnt += 1;
		v[y][x + 1] = '0';
		DFS(x + 1, y);
	}
	if (x - 1 >= 0 && v[y][x - 1] == '1') {
		cnt += 1;
		v[y][x - 1] = '0';
		DFS(x - 1, y);
	}
	if (y + 1 < n && v[y + 1][x] == '1') {
		cnt += 1;
		v[y + 1][x] = '0';
		DFS(x, y + 1);
	}
	if (y - 1 >= 0 && v[y - 1][x] == '1') {
		cnt += 1;
		v[y - 1][x] = '0';
		DFS(x, y - 1);
	}

	return cnt;
}

int main()
{
	scanf("%d", &n);

	v.resize(n);
	for (int i = 0; i < v.size(); i++)
		v[i].resize(n, -1);

	string str;
	for (int i = 0; i < n; i++) {
		cin >> str;
		for (int j = 0; j < str.size(); j++) {
			v[i][j] = str[j];
		}
	}

	int idx = 0;
	for (int i = 0; i < v.size(); i++) {
		for (int j = 0; j < v[i].size(); j++) {
			if (v[i][j] == '1') {
				
				//BFS
				tmp.push_back(0);
				tmp[idx++] = BFS(j, i);			// x, y

				//DFS
				//tmp.push_back(0);
				//cnt = 1;
				//v[i][j] = '0';
				//tmp[idx++] = DFS(j, i);
			}
		}
	}

	printf("%d\n", idx);

	sort(tmp.begin(), tmp.end());
	for (auto elem : tmp)
		printf("%d\n", elem);
}