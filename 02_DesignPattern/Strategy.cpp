//#include <iostream>
//#include <string>
//using namespace std;
//
//typedef enum AI_Level{
//	AI_EASY,
//	AI_NORMAL,
//	AI_HARD
//}AI_Level;
//
//class ComPlayer {
//	AI_Level level_;		//AI의 레벨
//public:
//	// 생성자로 AI의 사고 레벨을 받음
//	ComPlayer(AI_Level level) : level_(level) {}
//
//	// 사고
//	void think() {
//		switch (level_) {
//		case AI_EASY: thinkEasy(); break;
//		case AI_NORMAL: thinkNormal(); break;
//		case AI_HARD: thinkHard(); break;
//		}
//	}
//
//	void thinkEasy() {cout << "thinkEasy" << endl;}
//	void thinkNormal() {cout << "thinkNormal" << endl;}
//	void thinkHard(){cout << "thinkHard" << endl;}
//};
//
//int main()
//{
//	ComPlayer com(AI_EASY);
//	com.think();
//}



#include <iostream>
#include <string>
using namespace std;

//AI 추상 인터페이스
class AI {
public:
	virtual ~AI() {}
	virtual void think() = 0;
};

class EasyAI : public AI {
public:
	virtual void think() override {
		cout << "thinkEasy" << endl;
	}
};

class NormalAI : public AI {
public:
	virtual void think() override {
		cout << "thinkNormal" << endl;
	}
};

class HardAI : public AI {
public:
	virtual void think() override {
		cout << "thinkHard" << endl;
	}
};

class ComPlayer {
	AI* ai_;
public:
	//생성자로 추상화된 AI를 받음
	ComPlayer(AI* ai) : ai_(ai) {}
	//소멸자
	~ComPlayer() {
		delete ai_;
	}
	//사고
	void think() {
		ai_->think();
	}
};

int main()
{
	HardAI * ai = new HardAI;
	ComPlayer com(ai);
	com.think();
}