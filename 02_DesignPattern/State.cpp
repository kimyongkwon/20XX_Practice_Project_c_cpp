#include <iostream>
using namespace std;

typedef enum ACTOR_STATE {
	STATE_WAIT,
	STATE_WALK,
	STATE_ATTACK,
	STATE_DAMAGE
}ActorState;

class Actor 
{
	ActorState state_;
public:
	Actor(ActorState _ActorState) : state_(_ActorState) {};
	//상태 업데이트
	void update() {
		switch (state_)
		{
		case STATE_WAIT: wait(); break; 
		case STATE_WALK: walk(); break;
		case STATE_ATTACK: attack(); break;
		case STATE_DAMAGE: damage(); break;
		}
	}

	void wait() { cout << "대기상태" << endl; }
	void walk() { cout << "이동상태" << endl; }
	void attack() { cout << "공격상태" << endl; }
	void damage() { cout << "데미지상태" << endl; }
};

int main()
{
	Actor actor(STATE_WAIT);

	actor.update();
}


//#include <iostream>
//using namespace std;
//
//class ActorState
//{
//public:
//	virtual ~ActorState() {}
//	// 업데이트
//	virtual void update() = 0;
//};
//
//// 대기상태
//class WaitState : public ActorState 
//{
//	Actor * owner_;
//public:
//	// 생성자
//	WaitState(Actor* owner) : owner_(owner) {
//	}
//	// 소멸자
//	~WaitState() { }
//	virtual void update() override
//	{
//		cout << "대기상태" << endl;
//	}
//};
//
//// 걷기상태
//class WalkState : public ActorState
//{
//	Actor * owner_;
//public:
//	// 생성자
//	WalkState(Actor* owner) : owner_(owner) {
//	}
//	// 소멸자
//	~WalkState() { }
//	virtual void update() override
//	{
//		cout << "걷기상태" << endl;
//	}
//};
//
//// 공격상태
//class AttackState : public ActorState
//{
//	Actor * owner_;
//public:
//	// 생성자
//	AttackState(Actor* owner) : owner_(owner) {
//	}
//	// 소멸자
//	~AttackState() { }
//	virtual void update() override
//	{
//		cout << "공격상태" << endl;
//	}
//};
//
//
//class Actor
//{
//	ActorState * state_;
//public:
//	Actor() : state_(nullptr)
//	{
//		changeState(new WaitState(this));
//	};
//	~Actor() { delete state_; };
//
//	//상태 업데이트
//	void update() {
//		state_->update();
//	}
//
//	void changeState(ActorState* state)
//	{
//		delete state_;
//		state_ = state;
//	}
//};
//
//int main()
//{
//	Actor actor;
//
//	actor.update();
//}
//
