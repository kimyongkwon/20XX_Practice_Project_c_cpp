#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <string>
using namespace std;

int main()
{
	//문자열을 위한 빈 벡터를 만든다.
	vector<string> sentence;

	//재할당을 막기 위해 다섯개의 요소를 저장할 수 있는 메모리 공간을 확보한다.
	sentence.reserve(5);

	//몇몇 요소를 덧붙인다.
	sentence.push_back("Hello,");
	sentence.insert(sentence.end(), { "How", "are", "you", "?" });

	//요소를 출력한다(공백으로 분리)
	copy(sentence.cbegin(), sentence.cend(),
		ostream_iterator<string>(cout, " "));

	cout << endl;

	//"기술적 정보" 출력
	cout << "max_size() : " << sentence.max_size() << endl;
	cout << "size() : " << sentence.size() << endl;
	cout << "capacity() : " << sentence.capacity() << endl;
	cout << endl;

	//두번째와 네번째 요소 교환
	swap(sentence[1], sentence[3]);

	//요소 "?"앞에 "always"라는 요소 삽입
	sentence.insert(find(sentence.begin(), sentence.end(), "?"), "always");

	//마지막 요소로 "!" 삽입
	sentence.back() = "!";

	//요소를 출력한다(공백으로 분리)
	copy(sentence.cbegin(), sentence.cend(),
		ostream_iterator<string>(cout, " "));
	cout << endl;

	//"기술적 정보" 다시 출력
	cout << "size() : " << sentence.size() << endl;
	cout << "capacity() : " << sentence.capacity() << endl;

	//마지막 두 요소 삭제
	sentence.pop_back();
	sentence.pop_back();

	//용량 줄임
	sentence.shrink_to_fit();

	//"기술적 정보" 다시 출력
	cout << "size() : " << sentence.size() << endl;
	cout << "capacity() : " << sentence.capacity() << endl;

}

