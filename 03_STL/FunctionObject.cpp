//#include <iostream>
//#include <string>
//#include <set>
//using namespace std;
//
//class Person 
//{
//    string first;
//    string last;
//public:
//    Person(string _first, string _last)
//    {
//        first = _first;
//        last = _last;
//    }
//    string firstname() const;
//    string lastname() const;
//};
//
//class PersonSortCriterion
//{
//public:
//    bool operator() (const Person& p1, const Person& p2) const {
//        // 한 Person이 다른 Person보다 작으려면
//        // - 성이 더 앞서거나
//        // - 성은 같고 이름은 더 앞선다
//        return p1.lastname() < p2.lastname() || 
//              (p1.lastname() == p2.lastname() &&
//               p1.firstname() < p2.firstname());
//    }
//};
//
////함수 조건자를 위한 클래스
//int main()
//{
//    //특별한 정렬 기준을 갖는 집합을 만든다.
//    set<Person, PersonSortCriterion> coll;
//   
//    coll.insert(Person("b", "c"));
//    coll.insert(Person("a", "b"));
//    coll.insert(Person("a", "a"));
//   
//    for ( auto elem : coll)   {
//        cout << " " << elem.firstname() << elem.lastname() << " " << endl;
//    }
//}
//
//string Person::firstname() const
//{
//    return first;
//}
//
//string Person::lastname() const
//{
//    return last;
//}

#include <iostream>
#include <list>
#include <algorithm>
#include <iterator>

using namespace std;

class IntSequence
{
private:
    int value;
public:
    
    IntSequence(int initvalue) : value (initvalue){}
    int operator() () {return ++value;}

    int print() const {
        return value;
    }
};

inline void PRINT_ELEMENTS(const list<int> &_list) 
{
   /* for (int i = 0; i < _list.size(); ++i)
        cout << i << endl;*/
        
    for (int elem : _list)
        cout << elem << endl;
}


int main()
{
    list <int> coll;

    //1에서 부터 9까지의 값을 삽입한다
    generate_n(back_inserter(coll), 9, IntSequence(1));
    PRINT_ELEMENTS(coll);
}

