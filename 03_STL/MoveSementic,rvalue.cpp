#include <iostream>
#include <vector>
#include <utility>
using namespace std;

//#if defined (_MSC_VER)
//#endif

class Data
{
    int id;
    char * name;
    uint32_t len;
public:
    Data(char * _name)
    {
        cout << "생성자" << endl;
        uint32_t len = strlen(_name) + 1;
        name = new char[len];
        strcpy(name, _name);
        id = cnt++;
        cout << "id -> " << id << " name -> " << name << " 주소값 -> " << (void*)name << endl;
    };

    //복사생성자
    Data(const Data& _data)
    {
        cout << "복사생성자" << endl;
        len = strlen(_data.name) + 1;
        name = new char[len];
        strcpy(name, _data.name);
        id = cnt++;
        cout << "id -> " << id << " name -> " << name << " 주소값 -> " << (void*)name << endl;

    }

    //이동생성자
    Data(Data &&_data)
    {
        cout << "이동생성자" << endl;
        id = _data.id;
        name = _data.name;
        _data.name = nullptr;
        cout << "id -> " << id << " name -> " << name << " 주소값 -> " << (void*)name << endl;
    }

    ~Data() {
        cout << "소멸자" << endl;
        cout << "id -> " << id-- << " 주소값 -> " << (void*)name << endl;
        delete[] name;
    };

    static int cnt;
};

int Data::cnt = 1;

int main()
{
    Data data1("김용권");
    cout << "----------------------------------------------------------------------------" << endl;

    Data data2("김한샘");
    cout << "----------------------------------------------------------------------------" << endl;

    vector<Data> v;
    v.push_back(std::move(data1));
    cout << "----------------------------------------------------------------------------" << endl;

    v.push_back(std::move(data2));
    cout << "----------------------------------------------------------------------------" << endl;
}


//#include <iostream>
//#include <vector>
//
//using namespace std;
//
//class Test {
//public:
//    char *data;
//    char name;
//    int id;
//public:
//    //생성자
//    Test(char _name)
//    {
//        //데이터 생성
//        data = new char[1000000];
//        id = cnt++;
//        name = _name;
//
//        cout << "id : " << id << " 생성자 호출 - 메모리의 주소 : " << (void*)data << endl;
//        cout << "name : " << name << endl;
//    }
//
//    //복사생성자 (Deep Copy)
//    Test(const Test &other)
//    {
//        data = new char[1000000];
//        memcpy(data, other.data, 1000000);
//        id = cnt++;
//        name = other.name;
//
//        cout << "id : " << id << " 복사생성자 호출 - 메모리의 주소 : " << (void*)data << endl;
//        cout << "name : " << name << endl;
//    }
//
//    //할당연산자
//    Test& operator =(const Test &other)
//    {
//        if (this == &other)
//            return *this;
//        delete[]data; // 기존 메모리 소멸
//
//        data = new char[1000000];
//        memcpy(data, other.data, 1000000);
//        name = other.name;
//
//        cout << "id : " << id << " 할당연산자 - 메모리의 주소 : " << (void*)data << endl;
//        cout << "name : " << name << endl;
//
//        return *this;
//    }
//
//    //소멸자
//    ~Test()
//    {
//        cout << "id : " << id << " 소멸자 호출 - 메모리의 주소 : " << (void*)data << endl;
//        cout << "name : " << name << endl;
//        delete[]data;
//    }
//    static int cnt;
//};
//
//int Test::cnt = 1;
//
//int main()
//{
//    //Test 객체 a생성
//    Test a{ 'a' };
//    cout << "---------------------------------------------------------" << endl;
//
//    //Test 객체 b생성
//    Test b{ 'b' };
//    cout << "---------------------------------------------------------" << endl;
//
//    //Test객체를 저장할 vector클래스 생성
//    std::vector<Test> vec;
//    //vec에 a 복사
//    vec.push_back(a);
//    cout << "---------------------------------------------------------" << endl;
//
//    //vec에 b 복사
//    vec.push_back(b);
//    cout << "---------------------------------------------------------" << endl;
//}
