#include <iostream>
#include <set>
#include <algorithm>
#include <iterator>
#include <functional>

using namespace std;

int main()
{
	// 모음의 데이터형
	// - 중복값 없음
	// - 요소는 정수형 값
	// - 내림차순 정렬
	set <int, greater <int>> coll1;

	//다양한 멤버 함수를 사용해 요소를 임의의 순서대로 삽입한다.
	coll1.insert({ 4,3,5,1,6,2 });
	coll1.insert(5);

	for (int elem : coll1)
		cout << elem << ' ';
	cout << endl;

	//또 한번 4를 삽입하고 반환값을 처리한다.
	auto status = coll1.insert(4);

	if (status.second) {
		cout << "4 inserted as element "
			<< distance(coll1.begin(), status.first) + 1 << endl;
	}
	else {
		cout << "4 already exists" << endl;
	}

	//오름차순으로 정렬하는 다른 집합에 요소 할당
	set <int> coll2(coll1.cbegin(), coll1.cend());

	//스트림 반복자를 사용해 복사본의 모든 요소 출력
	copy(coll2.cbegin(), coll2.cend(), ostream_iterator<int> (cout, " "));
	cout << endl;

	//값이 3인 요소까지의 모든 요소 삭제
	coll2.erase(coll2.begin(), coll2.find(3));

	//값이 5인 모든 요소 삭제
	int num;
	num = coll2.erase(5);
	cout << num << " element(s) removed" << endl;

	//모든 요소 출력
	copy(coll2.cbegin(), coll2.cend(),
		ostream_iterator<int>(cout, " "));
	cout << endl;
}