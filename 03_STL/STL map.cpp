//#include <iostream>
//#include <map>
//using namespace std;
//
//int main()
//{
//	std::map<std::string, float> coll; // 빈모음
//
//	coll["otto"] = 7.7;
//}


//#include <iostream>
//#include <map>
//#include <string>
//#include <iostream>
//#include <algorithm>
//using namespace std;
//
//int main()
//{
//	map<string, double> coll{ {"tim", 9.9 },
//							  {"struppi", 11.77}
//							};
//
//	// 각 요소의 값을 제곱한다.
//	for_each(coll.begin(), coll.end(), [](pair<const string, double>& elem) {
//		elem.second *= elem.second;
//	});
//
//	// 각 요소를 출력한다
//	for_each(coll.begin(), coll.end(), [] (const map<string, double>::value_type& elem) {
//		cout << elem.first << ": " << elem.second << endl;
//	});
//}

#include <map>
#include <string>
#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
	// 맵/연관 배열을 만든다.
	// - 키는 문자열
	// - 값은 부동 소수점이다.
	typedef map<string, float> StringFloatMap;
	StringFloatMap stocks;	// 빈 컨테이너를 만든다.
	//요소 몇 개를 삽입한다.
	stocks["BASF"] = 369.50;
	stocks["VW"] = 413.50;
	stocks["Daimler"] = 819.50;
	stocks["BMW"] = 834.00;
	stocks["Siemens"] = 842.20;

	// 모든 요소 출력
	StringFloatMap::iterator pos;
	cout << left;	// 오른쪽으로 정렬된 값.
	for (pos = stocks.begin(); pos != stocks.end(); ++pos) {
		cout << "stock : " << setw(12) << pos->first
			<< "price: " << pos->second << endl;
	}
	cout << endl;

	// 모든 값을 두 배로 만든다.
	for (pos = stocks.begin(); pos != stocks.end(); ++pos) {
		pos->second *= 2;
	}

	// 모든 요소 출력
	for (pos = stocks.begin(); pos != stocks.end(); ++pos) {
		cout << "stock : " << setw(12) << pos->first
			<< "price: " << pos->second << endl;
	}
	cout << endl;

	// 키값을 "VW"에서 "Volkswagen"으로 바꾼다.
	// - 그러려면 요소를 바꿔야만 한다.
	stocks["Volkswagen"] = stocks["VW"];
	stocks.erase("VW");

	// 모든 요소 출력
	for (pos = stocks.begin(); pos != stocks.end(); ++pos) {
		cout << "stock : " << setw(12) << pos->first
			<< "price: " << pos->second << endl;
	}
}