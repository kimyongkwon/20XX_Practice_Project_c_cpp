#include <vector>
#include <iostream>
#include <string>

int main()
{
	//for (auto elem : a)
	//	cout << elem << " ";
	//cout << endl;

	//vector<int> b = a;
	//for (auto elem : b)
	//	cout << elem << " ";
	//cout << endl;

	//for (auto elem : a)
	//	cout << elem << " ";
	//cout << endl;

	//vector<int> c(2, 5);
	//c = b;

	//vector<int> d;
	//d.resize(10);

	//for (auto elem : d)
	//	cout << elem << " ";
	//cout << endl;

	//vector <int> v1;
	//v1.reserve(5);

	//vector<int> v2(5);

	/*vector<int> v1(5);
	v1[0] = 0;
	v1[1] = 1;
	v1[3] = 2;*/

	std::string s;
	int a1 = 1;
	int a2 = 2;
	int a3 = 3;
	s.insert(0, std::to_string(a1));
	s.insert(1, std::to_string(a2));
	s.insert(2, std::to_string(a3));

	std::cout << s << std::endl;

	std::vector<int> v1(5);

	std::vector<int> v2 = v1;

	for (int i = 0; i < v2.size(); i++)
		std::cout << v2.at(i) << " ";
	std::cout << std::endl;

	std::vector<int> v3;
	v3.reserve(100);

	v2 = v3;
	for (int i = 0; i < v2.size(); i++)
		std::cout << v2[i] << " ";
	std::cout << std::endl;

}