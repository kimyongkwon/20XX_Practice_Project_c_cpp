#include <iostream>
#include <map>

using namespace std;

typedef enum MODEL {
	car1, tree1, fence, bus,
}MODEL;

class A
{
	int a;
public:
	A(int _a) : a(_a) {};
};

int main()
{
	map<int, A> map1;

	A a1(1);
	A a2(2);
	A a3(3);
	A a4(4);

	map1.insert({ car1, a1 });
	map1.insert({ tree1, a2 });
	map1.insert({ fence, a3 });
	map1.insert({ bus, a4 });



	for (auto elem : map1)
		cout << elem.first << endl;

}