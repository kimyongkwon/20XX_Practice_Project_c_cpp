#pragma once

#include <string>

//-----------------------------------------------------------------------------------------
//SpreadsheetCell은 추상클래스이므로 인스턴스화 될 수 없다.
class SpreadsheetCell
{
public:
	SpreadsheetCell() {};
	virtual ~SpreadsheetCell() {};
	virtual void set(const std::string& inString) = 0;
	virtual std::string getString() const = 0;
};

