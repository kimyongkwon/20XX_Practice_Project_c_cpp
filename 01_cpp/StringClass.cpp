#include <iostream>
#include <string>

using namespace std;

class String
{
	int len;
	char * str;
public:
	String() {};

	String(char * s);

	String(String& s);

	~String();

	String operator+(const String& s);
	
	String& operator=(const String& s);

	String& operator += (String& s);

	bool operator == (String& s)
	{
		return strcmp(str, s.str) ? true : false;
	};

	friend ostream& operator<< (ostream& os, String& s);
	friend istream& operator>> (istream& os, String& s);
};

String::String(char * s)
{
	cout << "기본생성자" << endl;
	len = strlen(s) + 1;
	str = new char[len];
	strcpy(str, s);
};

String::String(String& s)
{
	cout << "복사생성자" << endl;
	len = s.len;
	str = new char[len];
	strcpy(str, s.str);
};

String::~String()
{
	cout << "소멸자" << endl;
	delete[]str;
};

String String::operator+(const String& s)
{
	cout << "+ 연산자" << endl;

	char * tstr = new char[len+s.len-1];

	strcpy(tstr, str);
	strcat(tstr, s.str);

	String temp(tstr);

	delete[]tstr;

	return temp;
};

String& String::operator=(const String& s)
{
	cout << "대입 연산자" << endl;
	delete[]str;

	len = s.len;
	str = new char[len];
	strcpy(str, s.str);
	                                              
	return *this;
};

String& String::operator+= (String& s)
{
	len = len + s.len - 1;
	char * tstr = new char[len];
	strcpy(tstr, str);
	delete[]str;

	strcat(tstr, s.str);
	str = tstr;
	return *this;

	/*	String tmp(*this + s);
	*this = tmp;*/
}

ostream& operator<< (ostream& os, String& s)
{
	cout << s.str;
	return os;
}

istream& operator>> (istream& is, String& s)
{
	char str[100];
	is >> str;

	s = String(str);

	return is;
}

int main()
{
	String str1 = "Good";
	String str2 = "morning";	
	String str3 = str1 + str2;

	cout << str1 << endl;
	cout << str2 << endl;
	cout << str3 << endl;

	str1 += str2;
	if (str1 == str3) {
		cout << "equal" << endl;
	}

	String str4;
	cout << "문자열 입력: ";
	cin >> str4;
	cout << "입력한 문자열: " << str4 << endl;

	return 0;
}



//#include <iostream>
//using namespace std;
//
//class A
//{
//	int data;
//public :
//	A(int _data) : data(_data) {};
//	
//	A& operator+ (const A& b)
//	{
//		data += b.data;
//		return *this;
//	}
//	void show(void)
//	{
//		cout << data << endl;
//	}
//};
//
//int main()
//{
//	A a = 10;
//	A b = 10;
//	A c = a + b;
//	c.show();
//}