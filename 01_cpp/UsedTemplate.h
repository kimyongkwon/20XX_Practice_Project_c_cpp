#pragma once

class GamePiece
{
public:
	virtual std::unique_ptr<GamePiece> clone() const = 0;
};

class ChessPiece : public GamePiece
{
public:
	virtual std::unique_ptr<GamePiece> clone() const override;
};

//원본 템플릿
template <typename T>
class Grid
{
public:
	explicit Grid(size_t inWidth = kDefaultWidth,
		size_t inHeight = kDefaultHeight);
	virtual ~Grid();

	//말을 특정 위치에 둔다. 이때부터 GameBaard가 그 말을 소유하게 된다.
	//특정 위치의 말을 없애고 싶으면 inPiece의 값으로 nullptr을 넘긴다
	void setElementAt(size_t x, size_t y, const T& inPiece);
	T& getElementAt(size_t x, size_t y);
	const T& getElementAt(size_t x, size_t y) const;

	size_t getHeight() const { return mHeight; }
	size_t getWidth() const { return mWidth; }
	static const size_t kDefaultWidth = 10;
	static const size_t kDefaultHeight = 10;

private:
	void initializeCellsContainer();
	std::vector<std::vector<T>> mCells;
	size_t mWidth, mHeight;
};

//특수화한 템플릿
template <>
class Grid<const char*>
{
public:
	explicit Grid(size_t inWidth = kDefaultWidth,
		size_t inHeight = kDefaultHeight);
	virtual ~Grid();

	//말을 특정 위치에 둔다. 이때부터 GameBaard가 그 말을 소유하게 된다.
	//특정 위치의 말을 없애고 싶으면 inPiece의 값으로 nullptr을 넘긴다
	void setElementAt(size_t x, size_t y, const char* inPiece);
	const char* getElementAt(size_t x, size_t y) const;

	size_t getHeight() const { return mHeight; }
	size_t getWidth() const { return mWidth; }
	static const size_t kDefaultWidth = 10;
	static const size_t kDefaultHeight = 10;

private:
	void initializeCellsContainer();
	std::vector<std::vector<const char*>> mCells;
	size_t mWidth, mHeight;
};


