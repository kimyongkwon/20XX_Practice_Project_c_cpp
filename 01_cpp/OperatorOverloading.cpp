//멤버함수로 오버로딩
#include <iostream>
using namespace std;

class A
{
	int data;
public:
	A() : data(5) {};
	A(int a) : data(a) {};
	void print(void)
	{
		cout << data << endl;
	}
	A& operator+(const A& a)
	{
		data += a.data;
		return *this;
	}
};

int main()
{
	A a;
	A b;
	A c;
	c = a + b;
	c.print();
}




////전역함수로 오버로딩
//#include <iostream>
//using namespace std;
//
//class A
//{
//	int data;
//public:
//	A() : data(5) {};
//	A(int a) : data(a) {};
//	void print(void)
//	{
//		cout << data << endl;
//	}
//
//	friend A& operator+(const A& a, const A& b);
//};
//
//A& operator+(const A& a, const A& b)
//{
//	A tmp(a.data + b.data);
//	return tmp;
//}
//
//int main()
//{
//	A a;
//	A b;
//	A c;
//	c = a + b;
//	c.print();
//}




////전위증가 연산자 오버로딩
//#include <iostream>
//using namespace std;
//
//class A
//{
//	int data;
//public:
//	A() : data(5) {};
//	A(int a) : data(a) {};
//	void print(void)
//	{
//		cout << data << endl;
//	}
//
//	// 멤버함수 버전
//	/*A operator++()
//	{
//		data += 1;
//		return *this;
//	}*/
//
//	// 전역함수 버전
//	friend A& operator++(A &a);
//};
//
//A& operator++ (A &a)
//{
//	a.data += 1;
//	return a;
//}
//
//int main()
//{
//	A a;
//	++a;
//	a.print();
//}




////후위증가 연산자 오버로딩
//#include <iostream>
//using namespace std;
//
//class A
//{
//	int data;
//public:
//	A() : data(5) {};
//	A(int a) : data(a) {};
//	void print(void)
//	{
//		cout << data << endl;
//	}
//
//	// 멤버함수 버전
//	A operator++(int)
//	{
//		data += 1;
//		return *this;
//	}
//
//	// 전역함수 버전
//	//const friend A& operator++(A &a, int);
//};
//
////const A& operator++ (A &a, int)
////{
////	A tmp(a);
////	a.data += 1;
////	return tmp;
////}
//
//int main()
//{
//	A a;
//	a++;
//	a.print();
//}




////<< >> 연산자 오버로딩
//#include <iostream>
//using namespace std;
//
//class A
//{
//	int data;
//public:
//	A() : data(5) {};
//	A(int a) : data(a) {};
//	void print(void)
//	{
//		cout << data << endl;
//	}
//
//	friend ostream& operator<< (ostream&, A& a);
//};
//
//ostream& operator << (ostream& ost, A& a)
//{
//	cout << a.data;
//	return ost;
//}
//
//int main()
//{
//	A a;
//	cout << a << " " << a;
//}


//#include <iostream>
//using namespace std;
//
//class Point
//{
//	int a;
//	int b;
//public:
//	Point(int _a, int _b) : a(_a) , b(_b) {};
//	
//	friend ostream& operator<< (ostream& ostream, Point& p);
//};
//
//ostream& operator<< (ostream& ostream, Point& p)
//{
//	cout << "[" << p.a << "," << p.b << "]" << endl;
//	return ostream;
//}
//
//int main()
//{
//	Point p(1, 2);
//	Point p2(2, 3);
//	cout << p << p2;
//}

//#include <iostream>
//using namespace std;
//
//const int SIZE = 3;
//
//class Arr
//{
//	int arr[SIZE];
//	int idx;
//public:
//	Arr() : idx(0) {};
//	void AddElem(int _data)
//	{
//		if (idx >= SIZE)
//		{
//			cout << "배열의 범위는 3까지입니다" << endl;
//			return;
//		}
//		arr[idx++] = _data;
//	}
//	void ShowAllData(void)
//	{
//		for (int elem : arr)
//			cout << elem << endl;
//	}
//	void SetElem(int _idx, int _data)
//	{
//		if (_idx >= SIZE)
//		{
//			cout << "없는 요소입니당" << endl;
//		}
//		arr[_idx] = _data;
//	}
//	int GetElem(int _idx)
//	{
//		return arr[_idx];
//	}
//};
//
//int main()
//{
//	Arr arr;
//	arr.AddElem(1);
//	arr.AddElem(2);
//	arr.AddElem(3);
//	arr.ShowAllData();
//
//	arr.SetElem(0, 10);
//	arr.SetElem(1, 20);
//	arr.SetElem(2, 30);
//
//	//
//	cout << arr.GetElem(0) << endl;
//	cout << arr.GetElem(1) << endl;
//	cout << arr.GetElem(2) << endl;
//}



//#include <iostream>
//using namespace std;
//
//const int SIZE = 5;
//
//class Point
//{
//	int x;
//	int y;
//public:
//	Point(int data1, int data2) : x(data1), y(data2) {};
//	Point() {};
//
//	friend ostream& operator<< (ostream& os, Point& elem);
//};
//
//ostream& operator<< (ostream& os, Point& elem)
//{
//	cout << "[" << elem.x << "," << elem.y << "]";
//	return os;
//}
//
//class Arr
//{
//	int idx;
//	Point arr[SIZE];
//public:
//	Arr() 
//	{
//		idx = 0;
//	};
//	~Arr() {};
//
//	void AddElem(const Point& point)
//	{
//		arr[idx++] = point;
//	}
//
//	void ShowAllData(void)
//	{
//		for (int i = 0; i < idx; i++)
//		{
//			cout << "arr[" << i << "] = " << arr[i] << endl;
//		}
//	}
//
//	Point operator[] (int idx)
//	{
//		return arr[idx];
//	}
//};
//
//int main()
//{
//	Arr arr;
//	arr.AddElem(Point(1,1));
//	arr.AddElem(Point(2,2));
//	arr.AddElem(Point(3,3));
//	arr.ShowAllData();
//
//	arr[0] = Point(10, 10);
//	arr[1] = Point(20, 20);
//	arr[2] = Point(30, 30);
//
//	cout << arr[0] << endl;
//	cout << arr[1] << endl;
//	cout << arr[2] << endl;
//}

