//18.5.25
//타입 캐스팅 4가지

//1.const_cast
//const 속성을 없애주는 캐스팅
//사실 const_cast를 안써주는 상황을 만드는게 가장 좋다.
//만약 꼭 써야하는 상황이 생긴다면 함수 안에서 객체(변수)가 
//수정하지 않는다는 것을 확인하고 써라


//2.static_cast
// - 일반적인 타입 캐스팅에서 사용된다.
// - 상위클래스에서 하위클래스로 캐스팅할때 사용된다.(다운캐스팅)
//class Base
//{
//public:
//	Base() {};
//	virtual ~Base() {}
//};
//
//class Derived : public Base
//{
//public:
//	Derived() {}
//	virtual ~Derived() {};
//};
//
//int main()
//{
//	//포인터에서 사용
//	Base * b;
//	Derived * d = new Derived();
//	b = d;
//	d = static_cast<Derived*>(b);
//
//	//참조형에서도 사용가능
//	Base base;
//	Derived derived;
//	Base& br = derived;
//	Derived& br = static_cast<Derived&>(br);
//}
// 주의 해야할 점은 static_cast는 런타임에서도 유효한지는 검사하지 않는다.
// 다음과 같은 코드는 컴파일도 되고 실행도 되지만 포인터 d의 이용은 객체의 범위를
// 벗어난 메모리 접근과 같은 예상할 수 없는 심각한 문제를 일으킨다.
//Base * b = new Base();
//Derived * d = static_cast<Derived*>(b);


//3.reinterpret_cast
//타입 간 변환을 강제적으로 한다.
//클래스 계층 등과 전혀 관계없이 다른 타입으로 변환한다.
//하지만 타입검사를 전혀 하지 않기때문에 주의해서 사용해야됨.
//class X {};
//class Y {};
//
//int main()
//{
//	X x;
//	Y y;
//	X * xp = &x;
//	Y * yp = &y;
//	//관계없는 클래스 포인터로 변환하기 위해 reinterpret_cast 필요
//	//static_cast는 작동하지 않음
//	xp = reinterpret_cast<X*>(yp);
//
//	//포인터를 void포인터로 변환할 때는 캐스팅이 필요없음
//	void * p = xp;
//
//	//관계없는 포인터 타입으로 변환하기 위해 reinterpret_cast 필요
//	xp = reinterpret_cast<X*>(p);
//
//	X& xr = x;
//	Y& yr = reinterpret_cast<Y&>(x);
//}


//4. dynamic_cast
//클래스 계층에 대한 런타임 타입 정보 검사를 수행한다.
//런타임 타입 정보는 객체의 vtable에 저장된다.
//그렇기 때문에 dynamic_cast가 타입 검사를 하기 위해서는 클래스에 
//하나 이상의 virtual 메서드가 있어야 한다.
//캐스팅이 부적합할 경우에는 null포인터를 리턴하거나
//bad_cast 익셉션을 발생시킨다.
//class Base
//{
//public:
//	Base() {};
//	virtual ~Base() {}
//};
//
//class Derived : public Base
//{
//public:
//	Derived() {};
//	virtual ~Derived() {}
//};
//
//int main()
//{
//	Base * b;
//	Derived * d = new Derived();
//	b = d;
//	d = dynamic_cast<Derived*>(b);
//}


