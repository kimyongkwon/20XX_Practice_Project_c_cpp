// 18.8.17

//#include <stdio.h>
//
//// template function
//template<typename T>
//T GetMax(T a, T b)
//{
//	printf("%s\n", __FUNCDNAME__);
//	return a > b ? a : b;
//}
//
//void main()
//{
//	int i = 2;
//	int j = 3;
//	GetMax<int> (i, j);	// (컴파일러가 유추함)implict call
//	GetMax<float> (2.1f, 3.1f); // (명시적 호출)explict call
//}

//-------------------------------------------

//#include <stdio.h>
//#include <string.h>
//
//template<typename T, int SIZE = 100>
//class KStack
//{
//public:
//	KStack() : sp(0) {}
//	void Push(T d)
//	{
//		data[sp] = d;
//		sp += 1;
//	}
//	bool Pop(T& d);
//	bool IsEmpty() const
//	{
//		return sp == 0;
//	}
//private:
//	int		sp;
//	T		data[SIZE];
//};
//
//template<typename T, int SIZE>
//bool KStack<T,SIZE>::Pop(T& d)
//{
//	if (sp == 0)
//		return false;
//
//	sp -= 1;
//	d = data[sp];
//	return true;
//}
////
//// template specialization(템플릿 특화)
//// 기존의 template인자에서 특화되는 부분을 빼서
//template <int SIZE>
//// 클래스 이름 다음에 명시적으로 적어주고, 
//// 바뀌지않는 인자는 template인자랑 클래스인자에 둘다 적어준다..
//class KStack<char*, SIZE>
//{
//public:
//	KStack() : sp(0) {}
//	void Push(char* d)
//	{
//		strcpy(data[sp], d);
//		sp += 1;
//	}
//	bool Pop(char*& d);
//	bool IsEmpty() const
//	{
//		return sp == 0;
//	}
//private:
//	int		sp;
//	char	data[SIZE][20];
//};
//
//template <int SIZE>
//bool KStack<char*, SIZE>::Pop(char*& d)
//{
//	if (sp == 0)
//		return false;
//
//	sp -= 1;
//	d = data[sp];
//	return true;
//}
//
//void main()
//{
//	KStack<int, 10> s;
//	KStack<float, 100> s2;
//
//	s.Push(3);
//	s.Push(5);
//	int i = 0;
//	s.Pop(i);
//	printf("%d\n", i);
//
//	KStack<char*>	s3;
//	s3.Push("hello");
//	s3.Push("world");
//	char* pText = nullptr;
//	s3.Pop(pText);
//	printf("%s", pText);
//}

//------------------------------------------------

//#include <stdio.h>
//
//template <class T>
//struct PTS
//{
//	enum
//	{
//		IsPointer = 0,
//		IsPointerToDataMember = 0
//	};
//};
//
//template <class T>
//struct PTS<T*>
//{
//	enum
//	{
//		IsPointer = 1,
//		IsPointerToDataMember = 0
//	};
//};
//
//template <class T, class U>
//struct PTS<T U::*>
//{
//	enum
//	{
//		IsPointer = 0,
//		IsPointerToDataMember = 1
//	};
//};
//
//struct S 
//{
//};
//
//int main()
//{
//	S		s;
//	S*		pS;
//	int		S::*ptm;
//
//	printf_s("PTS<S>::IsPointer == %d PTS<S>::IsPointerToDataMember == %d\n"
//		, PTS<S>::IsPointer, PTS<S>::IsPointerToDataMember);
//	printf_s("PTS<S*>::IsPointer == %d PTS<S*>::IsPointerToDataMember == %d\n"
//		, PTS<S*>::IsPointer, PTS<S*>::IsPointerToDataMember);
//	printf_s("PTS<int S::*>::IsPointer == %d "
//		"PTS<int S::*>::IsPointerToDataMember == %d\n"
//		, PTS<int S::*>::IsPointer
//		, PTS<int S::*>::IsPointerToDataMember);
//
//}

//-----------------------------------------------

//#include <stdio.h>
//
//template<typename T>
//void func(T data)
//{
//	printf("func primary template\n");
//}
//
//template<>
//void func(double data)
//{
//	printf("func specialize template\n");
//}
//
//template<typename T>
//void func(T* data)
//{
//	printf("func parital template\n");
//}
//
//int main()
//{
//	int a = 10;
//	double b = 0.1111111111111112222;
//
//	func(a);
//
//	func(&a);
//
//	func(b);
//
//	func(&b);
//}


//------------------------------------------------
//
//#include <stdio.h>
//
//template <class T>
//struct STRUCT
//{
//	enum
//	{
//		IsPointer = 0,
//		IsPointerToDataMember = 0
//	};
//};
//
//
//template <class T>
//struct STRUCT<T*>
//{
//	enum
//	{
//		IsPointer = 1,
//		IsPointerToDataMember = 0
//	};
//};
//
//
//template <class T, class U>
//struct STRUCT<T U::*>
//{
//	enum
//	{
//		IsPointer = 0,
//		IsPointerToDataMember = 1
//	};
//};
//
//struct S 
//{
//	int a = 10;
//};
//
//int main()
//{
//	S		s;
//	S*		ps;
//	int		S::*ptm = &S::a;
//	printf("%d\n", s.*ptm);
//
//	printf("%d %d\n", STRUCT<S>::IsPointer, STRUCT<S>::IsPointerToDataMember);
//	printf("%d %d\n", STRUCT<S*>::IsPointer, STRUCT<S*>::IsPointerToDataMember);
//	printf("%d %d\n", STRUCT<S S::*>::IsPointer, STRUCT<S S::*>::IsPointerToDataMember);
//	printf("%d %d\n", STRUCT<int S::*>::IsPointer, STRUCT<int S::*>::IsPointerToDataMember);
//}

//--------------------------------------

//#include <stdio.h>
//
//template<typename T>
//void func(T data)
//{
//	printf("func primary template\n");
//}
//
////template<>
////void func(double data)
////{
////	printf("func specialize template\n");
////}
////
////template<typename T>
////void func(T* data)
////{
////	printf("func parital template\n");
////}
//
//int main()
//{
//	int a = 10;
//	double b = 0.1111111111111112222;
//
//	//func(a);
//
//	func(&a);
//
//	//func(b);
//
//	func(&b);
//}

//---------------------------------------------------------------

//#include <stdio.h>
//
//// 클래스 템플릿 
//template <typename T, size_t SIZE = 10>
//class TClass
//{
//public:
//	T data;
//
//	TClass(int _data) : data(_data) {};
//	void NormalT(void);
//};
//
//template <typename T, size_t SIZE>
//void TClass<T, SIZE>::NormalT(void)
//{
//	for (int i = 0; i < SIZE; i++)
//		printf("나는 평벙한 템플릿이야\n");
//}
//
//// 클래스 템플릿 특수화 
//template <size_t SIZE>
//class TClass<double, SIZE>
//{
//public:
//	double data;
//	TClass(int _data) : data(_data) {};
//	void SpecializeT(void);
//};
//
//template<size_t SIZE>
//void TClass<double, SIZE>::SpecializeT(void)
//{
//	for (int i = 0; i < SIZE; i++)
//		printf("나는 특수한 템플릿이야\n");
//}
//
//// 여기서는 double형에 대해 특수화하였다.
//// 특수화하는 타입을 template인자에서 빼고
//// 그 인자를 class(struct) 옆에 써서 이 class는 double형을 
//// 특수화한다는 것을 컴파일러에게 알려준다.
//
//// 템플릿 특수화는 원래 템플릿을 상속하는 것이 아니다.
//// 그러므로 특수화된 템플릿 클래스 전체를 작성해야한다.
//// 함수나 데이터는 기존 템플릿과 달라도 상관없다.
//int main()
//{
//	TClass<int> t1(100);
//	t1.NormalT();
//
//	TClass<double> t2(100);
//	t2.SpecializeT();
//}

//------------------------------------------------------------------

//#include <stdio.h>
//
//// 함수 템플릿
//template<typename T, typename U>
//void func(T _data1, U _data2)
//{
//	printf("나는 평벙한 템플릿 함수야\n");
//}
//
//// 함수 템플릿 특수화
//template<>
//void func<double>(double _data1, double _data2)
//{
//	printf("나는 특수한 템플릿 함수야\n");
//}
//
//int main()
//{
//	func<int, float> (10, 0.123f);
//
//	func<double> ((double)0.213123123, (double)0.213123123);
//}

//-----------------------------------------------------------------

//#include <stdio.h>
//
//template <int n >
//struct Factorial
//{
//	enum { value = n * Factorial<n-1>::value };
//};
//
//// 템플릿 특화를 이용해서 종료조건 구현
//template <>
//struct Factorial<0>
//{
//	enum { value = 1 };
//};
//
//void main()
//{
//	Factorial<4> a;
//	int i = a.value;
//	printf("%i\n", i);	
//}//main

//-----------------------------------------------------------------

//#include <stdio.h>
//
//template<typename DERIVED>
//struct Base
//{
//	void Interface()
//	{
//		// ...작업
//		(static_cast<DERIVED*>(this))->Implementation();
//	}
//};
//
//struct Derived : public Base<Derived>
//{
//	void Implementation()
//	{
//		printf("Implementation()\n");
//	}
//};
//
//void main()
//{
//	Derived	k;
//	k.Interface();
//}

//-----------------------------------------------------------------

//#include <stdio.h>
//
//class Test
//{
//public:
//	void Print()
//	{
//		printf("Test::Print()\n");
//	}
//};
//
//class Hello 
//{
//public:
//	void Print()
//	{
//		printf("Test::Hello()\n");
//	}
//};
//
//void main()
//{
//	Test* t = new Test();
//	
//	// Hello* p = (Hello*)t;
//	Hello* p = static_cast<Hello*>(t);
//
//	p->Print();
//
//	delete t;
//}


//------------------------------------------------------------------

//#include <stdio.h>
//
//void Process(int i)
//{
//	printf("%d\n", i);
//}
//
//void Process(const char* s)
//{
//	printf("%s\n", s);
//}
//
//void Process(float f)
//{
//	printf("%f\n", f);
//}
//
//// 재귀호출의 무한루프를 막아주기 위한 함수
//void Func() {}
//
//template<typename Arg1, typename... Args> // template argument pack
//void Func(const Arg1& arg1, const Args&... args) // function argument pack
//{
//	Process(arg1);
//
//	Func(args...);
//}
//
//void main()
//{
//	Func(1, "hello", 3.1f);
//}

//------------------------------------------------------------------

#include <iostream>

void myprintf(const char *s)
{
	while (*s)
	{
		if (*s == '%')
		{
			if (*(s + 1) == '%')
			{
				++s;
			}
			else
			{
				throw std::runtime_error("invalid format string: missing arguments");
			}
		}
		std::cout << *s++;
	}
}

template<typename T, typename... Args>
void myprintf(const char * s, T value, Args... args)
{
	while (*s)
	{
		if (*s == '%')
		{
			if (*(s + 1) == '%')
			{
				++s;
			}
			else
			{
				std::cout << value;
				s += 2;
				myprintf(s, args...);
				return;
			}
		}
		std:: cout << *s++;
	}
}

void main()
{
	int i = 2;
	float f = 3.1f;
	myprintf("hello%% %i, %f\n", i, f);
}