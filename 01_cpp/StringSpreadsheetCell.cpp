#pragma

#include "StringSpreadsheetCell.h"

using namespace std;

StringSpreadsheetCell operator+ (const StringSpreadsheetCell& lhs, const StringSpreadsheetCell& rhs)
{
	StringSpreadsheetCell newCell;
	newCell.set(lhs.getString() + rhs.getString());
	return newCell;
}

//형제클래스에서 다른 형제 클래스로 캐스팅
StringSpreadsheetCell::StringSpreadsheetCell(const DoubleSpreadsheetCell& inDoubleCell)
{
	mValue = inDoubleCell.getString();
}

void StringSpreadsheetCell::set(const std::string & inString)
{
	mValue = inString;
}

std::string StringSpreadsheetCell::getString() const
{
	return mValue;
}