//18.5.9
//Polymorphism

#include <iostream>
#include <vector>
#include <memory>
using namespace std;

#include "DoubleSpreadsheetCell.h"
#include "StringSpreadsheetCell.h"

//-----------------------------------------------------------------------------------------
int main()
{
	vector<unique_ptr<SpreadsheetCell>> cellArray;

	cellArray.push_back(make_unique<StringSpreadsheetCell>());
	cellArray.push_back(make_unique<StringSpreadsheetCell>());
	cellArray.push_back(make_unique<DoubleSpreadsheetCell>());

	cellArray[0]->set("hello");
	cellArray[1]->set("10");
	cellArray[2]->set("18");

	cout << "Vector values are [" << cellArray[0]->getString() << "," <<
		cellArray[1]->getString() << "," <<
		cellArray[2]->getString() << "]" <<
		endl;

	unique_ptr<SpreadsheetCell> p = make_unique<StringSpreadsheetCell>();

	//make_unique를 안쓰면 다음과 같이 해야한다.
	//unique_ptr<SpreadsheetCell> p (new StringSpreadsheetCell);

	p->set("hello");

	DoubleSpreadsheetCell myDbl;
	myDbl.set(8.4);
	StringSpreadsheetCell result = myDbl + myDbl;

	return 0;
}


