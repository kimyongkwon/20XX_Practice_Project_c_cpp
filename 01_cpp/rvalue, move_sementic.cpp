//-------------------------------------------------------------
////18.5.17
////우측값 참조(415~416p)
////우측값 참조는 우항이 임시객체나 임시변수일 때 적용되는 개념이다.
////우측값 참조는 임시객체를 깊은복사하지 않고 포인터주소만 얕게 복제하여 오버헤드를 줄일 수 있다.
//
//#include <iostream>
//using namespace std;
//
////좌측값 참조형 파라미터
//void incr(int& value)
//{
//	cout << "lvalue reference" << endl;
//	++value;
//}
//
////우측값 참조형 파라미터
//void incr(int&& value)
//{
//	cout << "rvalue reference" << endl;
//	++value;
//}
//
//int main()
//{
//	int a = 10, b = 20;
//
//	incr(a);				//lvalue
//
//	incr(a += b);			//lvaue
//
//							//임시 변수가 생성된다. 그러므로 우측값 참조형 함수가 호출
//	incr(a + b);			//rvalue
//
//							//상수는 우항이 될 수 없으므로 우측값 참조형 함수가 호출
//	incr(3);				//rvalue
//
//							//만약 일반 변수에서 우측값 참조형 함수를 호출하고 싶으면
//							//move함수를 이용한다.
//	incr(std::move(b));
//}

//-------------------------------------------------------------
////18.5.17
////이동 시멘틱(move_sementic)
//
//#include <iostream>
//#include <vector>
//using namespace std;
//
//class A
//{
//	int a;
//	int b;
//	int * ptr = nullptr;
//public:
//	A() : a(10), b(100) 
//	{
//		ptr = new int(100000);
//		cout << "일반 생성자" << endl;
//	};
//
//	//일반 대입 연산자
//	A& operator=(const A& src)
//	{
//		cout << "일반 대입 연산자" << endl;
//		if (this == &src)
//			return *this;
//
//		a = src.a;
//		b = src.b;
//
//		ptr = new int;
//		*ptr = *(src.ptr);
//
//		return *this;
//	}
//
//	//이동 생성자
//	A(A&& src) noexcept
//	{
//		cout << "이동 생성자" << endl;
//		a = src.a;
//		a = src.b;
//		ptr = src.ptr;
//
//		src.a = 0;
//		src.b = 0;
//		src.ptr = nullptr;
//	}
//
//	//이동 대입 연산자
//	A& operator=(A&& src) noexcept
//	{
//		cout << "이동 대입 연산자" << endl;
//		if (this == &src)
//			return *this;
//
//		freemomory();
//
//		a = src.a;
//		a = src.b;
//		ptr = src.ptr;
//
//		src.a = 0;
//		src.b = 0;
//		src.ptr = nullptr;
//		return *this;
//	}
//
//	void freemomory(void)
//	{
//		delete ptr;
//	}
//};
//
//A CreateObject(void)
//{
//	//7. 일반생성자 호출해서 임시변수(변수 이름이 없기때문에)를 리턴
//	return A();
//}
//int main()
//{
//	vector<A> v;
//
//	// 첫번째 루프
//	// 1. A 객체를 생성한다(일반 생성자 호출)
//	// 2. vector v는 A객체를 담기위해 자신의 크기를 늘린다. 그리고 v에 A객체를 담기위해 이동생성자를 호출한다.
//
//	// 두번째 루프
//	// 3. A 객체를 생성한다(일반생성자 호출)
//	// 4,5. vector v는 A객체를 총 2개 담아야하므로 자신의 크기를 늘린다. 그리고 기존에 있었던 A객체와 새로운 A객체를 새로만들어진 vector v에 담는다. (이동생성자 2번호출)
//	for (int i = 0; i < 2; i++) {
//		cout << "Iteration" << i << endl;
//		v.push_back(A());				
//		cout << endl;
//	}
//
//
//	A s;					//6. 일반생성자 호출
//	s = CreateObject();		//8. s의 이동 대입 연산자 호출
//	A s2;					//9. 일반생성자 호출
//	s2 = s;					//10. 여기서는 일반 대입 연산자 호출 (왜? s가 임시객체가 아니라 이름을 가진 일반객체이기때문에)
//	return 0;
//}

//-------------------------------------------------------------
//18.5.23
//만약 위와 다르게 이동 대입 생성자와 이동 생성자가 존재하지 않으면 어떻게 될까???
//#include <iostream>
//#include <vector>
//using namespace std;
//
//class A
//{
//	int a;
//	int b;
//	int * ptr = nullptr;
//public:
//	A() : a(10), b(100)
//	{
//		ptr = new int(100000);
//		cout << "일반 생성자" << endl;
//	};
//
//	//일반 대입 연산자
//	A& operator=(const A& src)
//	{
//		cout << "일반 대입 연산자" << endl;
//		if (this == &src)
//			return *this;
//
//		a = src.a;
//		b = src.b;
//
//		ptr = new int;
//		*ptr = *(src.ptr);
//
//		return *this;
//	}
//
//	//일반 복사 생성자
//	A(const A& src)
//	{
//		cout << "일반 복사 생성자" << endl;
//
//		a = src.a;
//		b = src.b;
//
//		ptr = new int;
//		*ptr = *(src.ptr);
//
//	}
//
//	void freemomory(void)
//	{
//		delete ptr;
//	}
//};
//
//A CreateObject(void)
//{
//	//7. 일반생성자 호출해서 임시변수(변수 이름이 없기때문에)를 리턴
//	return A();
//}
//int main()
//{
//	vector<A> v;
//
//	// 첫번째 루프
//	// 1. A 객체를 생성한다(일반 생성자 호출)
//	// 2. vector v는 A객체를 담기위해 자신의 크기를 늘린다. 그리고 v에 A객체를 담기위해 복사생성자를 호출한다.
//
//	// 두번째 루프
//	// 3. A 객체를 생성한다(일반생성자 호출)
//	// 4,5. vector v는 A객체를 총 2개 담아야하므로 자신의 크기를 늘린다. 그리고 기존에 있었던 A객체와 새로운 A객체를 새로만들어진 vector v에 담는다. (복사생성자 2번호출)
//	for (int i = 0; i < 2; i++) {
//		cout << "Iteration" << i << endl;
//		v.push_back(A());
//		cout << endl;
//	}
//
//
//	A s;					//6. 일반생성자 호출
//	s = CreateObject();		//8. s의 이동 대입 연산자가 정의되어있지않으므로 일반 대입연산자 호출
//	A s2;					//9. 일반생성자 호출
//	s2 = s;					//10. 여기서는 일반 대입 연산자 호출 (왜? s가 임시객체가 아니라 이름을 가진 일반객체이기때문에)
//	return 0;
//}

//-------------------------------------------------------------
//18.5.23
////이동 생성자를 사용하지않은 swap함수
//#include <iostream>
//using namespace std;
//
//class A
//{
//	int a;
//public:
//	A(int data) : a(data) 
//	{
//	};
//	A(const A& src)
//	{
//		cout << "복사생성자" << endl;
//		a = src.a;
//	}
//	A& operator=(const A& src)
//	{
//		if (this == &src)
//			return *this;
//
//		cout << "일반 대입연산자" << endl;
//		a = src.a;
//	}
//	void print(void) {
//		cout << a << endl;
//	}
//};
//
//void swapCopy(A& a, A& b)
//{
//	A tmp(a);		//복사 생성자 호출
//	a = b;			//일반 대입연산자 호출
//	b = tmp;		//일반 대입연산자 호출
//}
//
//int main()
//{
//	A a(10);
//	A b(20);
//	a.print(); b.print();
//	swapCopy(a, b);
//	a.print(); b.print();
//}

//-------------------------------------------------------------
//18.5.23
//이동 생성자를 사용한 swap함수
//#include <iostream>
//using namespace std;
//
//class A
//{
//	int a;
//public:
//	A(int data) : a(data)
//	{
//	};
//	A() : a(100) {};
//	A(const A&& src)
//	{
//		cout << "이동 생성자" << endl;
//		a = src.a;
//	}
//	A& operator=(const A&& src)
//	{
//		if (this == &src)
//			return *this;
//
//		cout << "이동 대입연산자" << endl;
//		a = src.a;
//	}
//	void print(void) {
//		cout << a << endl;
//	}
//};
//
//void swapCopy(A& a, A& b)
//{
//	A tmp(std::move(a));		//복사 생성자 호출
//	a = std::move(b);			//일반 대입연산자 호출
//	b = std::move(tmp);			//일반 대입연산자 호출
//}
//
//int main()
//{
//	A a(10);
//	A b(20);
//	a.print(); b.print();
//	swapCopy(a, b);
//	a.print(); b.print();
//}

//-------------------------------------------------------------
////18.5.23
//#include <iostream>
//using namespace std;
//
//class A
//{
//	int data;
//public:
//	A() : data(10) 
//	{
//		cout << "생성자" << endl;
//	};
//	A(const A& src)
//	{
//		cout << "복사생성자" << endl;
//
//		data = src.data;
//	};
//	//A& operator=(const A& src)
//	//{
//	//	cout << "대입연산자" << endl;
//	//}
//	A(const A&& src) 
//	{
//		cout << "이동생성자" << endl;
//	};
//	A& operator=(const A&& src)
//	{
//		cout << "대입 이동 연산자" << endl;
//	}
//};
//
//int main()
//{
//	A a;		
//	//A b = static_cast<A&&>(a);		//이때 a메모리는 더이상 사용되지 않을것이다.
//	//A b = std::move(a);
//}

//-------------------------------------------------------------
////18.5.24
//#include <iostream>
//using namespace std;
//
//class A
//{
//	int data;
//public:
//	A() : data(10)
//	{
//		cout << "생성자" << endl;
//	};
//	A(const A& src)
//	{
//		cout << "복사생성자" << endl;
//
//		data = src.data;
//	};
//	A(const A&& src)
//	{
//		cout << "이동생성자" << endl;
//	};
//	A& operator=(const A&& src)
//	{
//		cout << "대입 이동 연산자" << endl;
//	}
//};
//
//int main()
//{
//	A a = A();
//	//A a{ A() };
//}

//18.5.24
//함수 매개변수는 함수안에서 rvalue일까 lvalue일까?
//#include <iostream>
//using namespace std;
//
//void g(int&& src)
//{
//	cout << "g rvalue" << endl;
//}
//
//void g(int& src)
//{
//	cout << "g lvalue" << endl;
//}
//
//void f(int&& src)
//{
//	//함수의 매개변수는 함수안에서 lvalue로 처리된다.
//	g(src);
//	cout << "f rvalue" << endl;
//}
//
//int main()
//{
//	int a = 10;
//	int b = 100;
//	f(a + b);
//}