//18.5.28
//템플릿 사용하기
#include <iostream>
#include <vector>
#include "UsedTemplate.h"

template <typename T>
Grid<T>::Grid(size_t inWidth, size_t inHeight) : mWidth(inWidth), mHeight(inHeight)
{
	initializeCellsContainer();
}

template <typename T>
Grid<T>::~Grid()
{
	//vector 스스로 메모리 해제를 하기 떄문에 특별히 할 일이 없음
}

template <typename T>
void Grid<T>::initializeCellsContainer()
{
	mCells.resize(mWidth);
	for (std::vector<T>& column : mCells)
		column.resize(mHeight);
}

template <typename T>
void Grid<T>::setElementAt(size_t x, size_t y, const T& inElem)
{
	mCells[x][y] = inElem;
}


template <typename T>
T& Grid<T>::getElementAt(size_t x, size_t y)
{
	return mCells[x][y];
}

template <typename T>
const T& Grid<T>::getElementAt(size_t x, size_t y) const
{
	return mCells[x][y];
}


//------------------------------------------------------------------------------------

//특수화된 템플릿 클래스 구현부

Grid<const char*>::Grid(size_t inWidth, size_t inHeight) : mWidth(inWidth), mHeight(inHeight)
{
	initializeCellsContainer();
}

Grid<const char*>::~Grid()
{
	//vector 스스로 메모리 해제를 하기 떄문에 특별히 할 일이 없음
}

void Grid<const char*>::initializeCellsContainer()
{
	mCells.resize(mWidth);
	for (auto& column : mCells)
		column.resize(mHeight);
}

void Grid<const char*>::setElementAt(size_t x, size_t y, const char* inElem)
{
	mCells[x][y] = inElem;
}

const char* Grid<const char*>::getElementAt(size_t x, size_t y) const
{
	return mCells[x][y];
}

//-------------------------------------------------------------------------------------

int main()
{
	Grid<int> myIntGrid;		// int 항목을 저장하는 Grid를 디폴트 파라미터로 받는
								// 생성자를 이용해서 선언

	Grid<double> myDoubleGrid(11, 11);		// double타입의 11x11 Grid선언

	myIntGrid.setElementAt(0, 0, 10);		
	int x = myIntGrid.getElementAt(0, 0);

	Grid<int> grid2(myIntGrid);	// 복제 생성자
	Grid<int> anotherIntGrid;
	anotherIntGrid = grid2;		// 대입 연산자

	//특수화된 템플릿
	Grid<const char*> stringGrid1(2, 2);
	const char* dummy = "dummy";
	stringGrid1.setElementAt(0, 0, "hello");
	stringGrid1.setElementAt(0, 1, dummy);
	stringGrid1.setElementAt(1, 0, dummy);
	stringGrid1.setElementAt(1, 1, "there");

	Grid<const char*> stringGrid2(stringGrid1);
}