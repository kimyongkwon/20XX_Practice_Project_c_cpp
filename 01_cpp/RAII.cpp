// 18.8.16
// RAII란?
// Resource Acquisition Is Initialization

//#include <stdio.h>
//
//// ##이라는 연산자가 있다.
//// 이 연산자는 전처리 명령문에서만 쓰일 수 있다.
//// 이 연산자를 사용하면 문자열을 합칠 수 있다.
//// 즉, 밑에서는 매크로 파라미터로 받은 a,b를 합쳐서 ab가 된다.
//// main문에서는 i,j를 합쳤으므로 ij가 된다.
//
//#define STRING_CAT(a,b)	a ## b
//
//void main()
//{
//	int i = 10;
//	int j = 5;
//	int ij = i + j;
//	
//	printf("%d", STRING_CAT(i, j));
//}

//------------------------------------------------

//// 다음 문장은 DUMMY매크로를 실행하면 
//// _dummy0이라는 변수가 생성되고 0으로 초기화 된다.
//// 하지만 DUMMY라는 문장을 또 사용할 수는 없다.
//// 이유는 이미 _dummy0이라는 변수가 선언됬는데 또다시 재선언 할 수 없기 때문에
//#include <stdio.h>
//
//#define STRING_CAT(a,b)		a ## b
//
//#define DUMMY				int STRING_CAT(_dummy,0) = 0;
//
//void main()
//{
//	DUMMY;
//	int i = 10;
//	int j = 5;
//	int ij = i + j;
//
//	printf("%d", STRING_CAT(i, j));
//}

//------------------------------------------------

//// 위 코드의 문제점을 해결하기위해 
//// predefined된  __LINE__이라는 매크로를 사용한다.
//// __LINE__ 매크로는 호출한 현재 코드줄 넘버를 나타낸다.
//#include <stdio.h>
//#define STRING_CAT(a,b)		a ## b
//#define DUMMY				int STRING_CAT(_dummy,__LINE__) = 0;
//
//void main()
//{
//	DUMMY;			// _dummy7
//	DUMMY;			// _dummy8의 변수가 될것이라 기대한다. 
//
//	// 하지만 그래도 재정의 했다고 컴파일 에러를 띄운다.
//	// 왜? STRING_CAT매크로에서 __LINE__을 매크로로 구분하는 것이 아니라
//	// __LINE__이라는 문자열로 간주하기 떄문이다.
//
//	int i = 10;
//	int j = 5;
//	int ij = i + j;
//
//	printf("%d", STRING_CAT(i, j));
//}

//---------------------------------------------------

// 해결방법은 또 다른 매크로 함수를 한번 더 
// 호출해서 __LINE__이라는 매크로가 확장되게한다.
//#include <stdio.h>
//
//#define _STRING_CAT(a,b)		a ## b
//#define STRING_CAT(a,b)		_STRING_CAT(a, b)
//#define DUMMY					int STRING_CAT(_dummy,__LINE__) = 0;
//
//void main()
//{
//	DUMMY;			// int _dummy9 
//	DUMMY;			// int _dummy10
//
//	int i = 10;
//	int j = 5;
//	int ij = i + j;
//
//	printf("%d", STRING_CAT(i, j));
//}

//--------------------------------------------------

// RAII의 핵심은 생성자에서 자동으로 뭔가를 해주고
// 소멸자에서 자동으로 해준 뭔가를 처리해주는 것이다.

#include <stdio.h>

template<typename F>
struct ScopeExit
{
	ScopeExit( F f_ ) 
		: f(f_) 
	{
	}
	
	~ScopeExit() 
	{ 
		f(); 
	}
	F	f;
};

template<typename F>
ScopeExit<F> MakeScopeExit(F f)
{
	return ScopeExit<F> (f) ;
}

int Test(int value)
{
	auto scope_exit = MakeScopeExit( [=]() { printf("scope exit\n"); });
	printf("hello\n");
	return 0;
}

void main()
{
	Test(2);
}

//------------------------------------------------

//#include <stdio.h>
//
//#define _STRING_CAT(a,b)	a ## b
//#define STRING_CAT(a,b)		_STRING_CAT(a, b)
//#define DUMMY				int STRING_CAT(_dummy,__LINE__) = 0;
//
//template<typename F>
//struct ScopeExit
//{
//	ScopeExit(F f_) : f(f_) {}
//	~ScopeExit() { f(); }
//	F	f;
//};
//
//template<typename F>
//ScopeExit<F> MakeScopeExit(F f)
//{
//	return ScopeExit<F>(f);
//}
//
//#define SCOPE_EXIT(expression) \
//	auto scope_exit = MakeScopeExit( [=]() { expression } );
//	
//int Test(int value)
//{
//	SCOPE_EXIT ( printf("scope exit\n"); );
//
//	printf("hello\n");
//	return 0;
//}
//
//void main()
//{
//	Test(2);
//}

//------------------------------------------------

//#include <stdio.h>
//
//#define _STRING_CAT(a,b)	a ## b
//#define STRING_CAT(a,b)		_STRING_CAT(a, b)
//#define DUMMY				int STRING_CAT(_dummy,__LINE__) = 0;
//
//template<typename F>
//struct ScopeExit
//{
//	ScopeExit(F f_) : f(f_) {}
//	~ScopeExit() { f(); }
//	F	f;
//};
//
//template<typename F>
//ScopeExit<F> MakeScopeExit(F f)
//{
//	return ScopeExit<F>(f);
//}
//
//#define SCOPE_EXIT(expression) \
//	auto STRING_CAT(scope_exit, __LINE__) = MakeScopeExit( [=]() { expression } );
//
//int Test(int value)
//{
//	SCOPE_EXIT(printf("scope exit1\n"); );
//	SCOPE_EXIT(printf("scope exit2\n"); );
//
//	printf("hello\n");
//	return 0;
//}
//
//void main()
//{
//	Test(2);
//}

//-------------------------------------------------

//#include <stdio.h>
//
//#define _STRING_CAT(a,b)	a ## b
//#define STRING_CAT(a,b)		_STRING_CAT(a, b)
//#define DUMMY				int STRING_CAT(_dummy,__LINE__) = 0;
//
//template<typename F>
//struct ScopeExit
//{
//	ScopeExit(F f_) : f(f_) {}
//	~ScopeExit() { f(); }
//	F	f;
//};
//
//template<typename F>
//ScopeExit<F> MakeScopeExit(F f)
//{
//	return ScopeExit<F>(f);
//}
//
//#define SCOPE_EXIT(expression) \
//	auto STRING_CAT(scope_exit, __LINE__) = MakeScopeExit( [=]() { expression } );
//
//int g_value = 1;
//
//int Test(int value)
//{
//	int oldValue = g_value;
//
//	SCOPE_EXIT(printf("scope exit1\n"); g_value = oldValue; );
//
//	printf("hello\n");
//
//	g_value = 99;
//
//	return 0;
//}
//
//void main()
//{
//	Test(2);
//	printf("%d\n", g_value);
//}