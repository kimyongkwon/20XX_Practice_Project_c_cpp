//18.5.25
//템플릿 사용하지 않고..
#include <iostream>
#include <vector>

class GamePiece
{
public:
	virtual std::unique_ptr<GamePiece> clone() const = 0;
};

class ChessPiece : public GamePiece
{
public:
	virtual std::unique_ptr<GamePiece> clone() const override;
};

std::unique_ptr<GamePiece> ChessPiece::clone() const
{
	//이 인스턴스를 복제하기 위해 복제 생성자를 호출한다.
	return std::make_unique<ChessPiece>(*this);
}

class GameBoard
{
public:
	explicit GameBoard(size_t inWidth = kDefaultWidth,
		size_t inHeight = kDefaultHeight);
	GameBoard(const GameBoard& src);		//복사 생성자
	virtual ~GameBoard();
	GameBoard& operator=(const GameBoard& rhs);		//대입 연산자

	//말을 특정 위치에 둔다. 이때부터 GameBaard가 그 말을 소유하게 된다.
	//특정 위치의 말을 없애고 싶으면 inPiece의 값으로 nullptr을 넘긴다
	void setPieceAt(size_t x, size_t y, std::unique_ptr<GamePiece> inPiece);
	std::unique_ptr<GamePiece>& getPieceAt(size_t x, size_t y);
	const std::unique_ptr<GamePiece>& getPieceAt(size_t x, size_t y) const;

	size_t getHeight() const { return mHeight; }
	size_t getWidth() const { return mWidth; }

	static const size_t kDefaultWidth = 10;
	static const size_t kDefaultHeight = 10;

private:
	void copyFrom(const GameBoard& src);
	void initializeCellsContainer();
	std::vector<std::vector<std::unique_ptr<GamePiece>>> mCells;
	size_t mWidth, mHeight;
};

GameBoard::GameBoard(size_t inWidth, size_t inHeight) : mWidth(inWidth), mHeight(inHeight)
{
	initializeCellsContainer();
}

GameBoard::GameBoard(const GameBoard& src)
{
	copyFrom(src);
}

GameBoard::~GameBoard()
{
	//vector 스스로 메모리 해제를 하기 떄문에 특별히 할 일이 없음
}

void GameBoard::initializeCellsContainer()
{
	mCells.resize(mWidth);
	for (auto& column : mCells)
		column.resize(mHeight);
}

void GameBoard::copyFrom(const GameBoard& src)
{
	mWidth = src.mWidth;
	mHeight = src.mHeight;
	initializeCellsContainer();
	for (size_t i = 0; i < mWidth; i++) {
		for (size_t j = 0; j < mHeight; j++) {
			if (src.mCells[i][j])
				mCells[i][j] = src.mCells[i][j]->clone();
			else
				mCells[i][j].reset();
		}
	}
}

GameBoard& GameBoard::operator=(const GameBoard& rhs)
{
	//자기 대입 여부 검사
	if (this == &rhs)
		return *this;

	//인자로 주어진 GameBoard를 복제
	copyFrom(rhs);
	return *this;
}

void GameBoard::setPieceAt(size_t x, size_t y, std::unique_ptr<GamePiece> inPiece)
{
	mCells[x][y] = move(inPiece);
}

std::unique_ptr<GamePiece>& GameBoard::getPieceAt(size_t x, size_t y)
{
	return mCells[x][y];
}

const std::unique_ptr<GamePiece>& GameBoard::getPieceAt(size_t x, size_t y) const
{
	return mCells[x][y];
}

int main()
{
	GameBoard chessBoard(8, 8);
	auto pawn = std::make_unique<ChessPiece>();
	chessBoard.setPieceAt(0, 0, std::move(pawn));
	chessBoard.setPieceAt(0, 1, std::make_unique<ChessPiece>());
	chessBoard.setPieceAt(0, 1, nullptr);
}


