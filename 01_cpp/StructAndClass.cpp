#include <stdio.h>
#include <Windows.h>

struct People
{
	int age;
	char name[10];
};

void PrintPeople(People* a)
{
	printf("%s : %d\n", a->name, a->age);
}

void SetAge(People* a, int newAge)
{
	a->age = newAge;
}

int main()
{
	People a;
	People* b = new People;

	a.age = 47;
	strcpy(a.name, "Hello");
	char * cp = (char*)&a;
	printf("%c\n", cp[5]);

	b->age = 48;
	strcpy(b->name, "World");

	SetAge(&a, a.age + 1);
	PrintPeople(&a);
	PrintPeople(b);
}