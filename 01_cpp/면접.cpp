
//#include <iostream>
//#include <vector>
//#include <cstdio>
//using namespace std;
//
//class Test
//{
//public:
//	int data;
//
//	Test() {
//	}
//	Test(int tmp) : data(tmp) {
//	}
//	~Test() {
//	}
//	Test(const Test& test)
//	{
//		data = test.data;
//		cout << "복사생성" << endl;
//	}
//};

//int main()
//{
//	vector<Test> v;
//
//	//아래의 1,2가 왜 다르게 출력결과는 나타내는지 말해보시오
//
//	//1.
//	//v.resize(3);
//
//	//2.
//	//v.reserve(3);
//
//	Test t(1);
//	v.push_back(t);
//	v.push_back(t);
//	v.push_back(t);
//}


//----------------------------------------------------------------------------------------------------------------------------

//weak_ptr

//#include <memory>
//#include <iostream>
//using namespace std;
//
//class Tenant;
//
//class Room {
//public:
//	Room() {}
//	~Room() { release_tenant(); }
//
//	void set_tenant(const std::shared_ptr<Tenant>& tenant) {
//		this->tenant = tenant;
//	}
//
//	void release_tenant() {
//		this->tenant.reset();
//	}
//
//private:
//	std::shared_ptr<Tenant> tenant;
//};
//
//class Tenant {
//public:
//	void set_room(const std::shared_ptr<Room>& room) {
//		this->room = room;
//	}
//
//	void leave_room() {
//		if (!this->room.expired()) {
//			std::shared_ptr<Room> room = this->room.lock();
//
//			if (room) {
//				room->release_tenant();
//			}
//		}
//	}
//private:
//	std::weak_ptr<Room> room;
//};
//
//int main() {
//	/*
//	* 아래 표시된 숫자들은 해당 객체의 레퍼런스 카운트
//	*/
//
//	std::shared_ptr<Room> room(new Room()); // room: 1
//
//	{
//		std::shared_ptr<Tenant> tenant(new Tenant()); // room:1, tenant: 1
//
//		room->set_tenant(tenant); // room: 1, tenant: 2
//		tenant->set_room(room); // room: 1, tenant: 2
//		cout << room.use_count() << endl;
//		cout << tenant.use_count() << endl;
//	} // room: 1, tenant: 1
//
//	room.reset(); // room: 0, tenant: 0
//}

//#include <iostream>
//#include <memory>
//using namespace std;
//
//class WidgetClass
//{
//public:
//	WidgetClass()
//	{
//		cout << "객체 생성" << endl;
//	}
//
//	~WidgetClass()
//	{
//		cout << "객체 해제" << endl;
//	}
//
//	//1번과 2번의 차이점이 무엇이고 왜 그런 출력결과가 나오는지 생각해봐
//	
//	//1.
//	//shared_ptr<WidgetClass> mspW;
//
//	//2.
//	weak_ptr<WidgetClass> mspW;
//};
//
//int main()
//{
//	shared_ptr<WidgetClass> spW1(new WidgetClass);
//	shared_ptr<WidgetClass> spW2(new WidgetClass);
//
//	//spW1,과 spW2는 각각 서로를 shared_ptr로 가리키고 있어서 순환참조가 발생한다.
//	//두 객체는 메모리가 해제되지 않고, 이로 인해 메모리 릭이 발생한다.
//	spW1->mspW = spW2;
//	spW2->mspW = spW1;
//
//	cout << spW1.use_count() << endl;
//	cout << spW2.use_count() << endl;
//
//	return 0;
//}


//----------------------------------------------------------------------------------------------------------------------------


//#include <iostream>
//using namespace std;
//
//void func(int * arr, int len)
//{
//	int j = len-1;
//	for (int i = 0; i < len/2; i++) {
//		int tmp = arr[i];
//		arr[i] = arr[j - i];
//		arr[j - i] = tmp;
//	}
//
//	for (int i = 0; i < len; i++)
//		cout << arr[i] << " ";
//}
//
//int main()
//{
//	int arr[] = { 1,2,3,4,5};
//	
//	int len = sizeof(arr) / sizeof(int);
//	cout << len << endl;
//
//	func(arr,len);
//}


//----------------------------------------------------------------------------------------------------------------------------
//strlen,strcpy

//#include <string.h>
//#include <cstdio>
//
//unsigned int str_len(const char * string)
//{
//	int cnt = 0;
//	int i;
//	for (i = 0; string[i] != '\0'; i++) {
//	}
//
//	return i;
//};
//
//char * str_cpy(char * dest, const char * src)
//{
//	for (int i = 0; src[i] != '\0'; i++)
//		dest[i] = src[i];
//
//	return dest;
//}
//
//char * str_ncpy(char * dest, const char * src, size_t n)
//{
//	for (int i = 0; i < n; i++)
//		dest[i] = src[i];
//
//	return dest;
//}
//
//char * str_cpy_s(char * dest, rsize_t dest_size, const char *src)
//{
//	int i;
//	for (i = 0; i < dest_size; i++) {
//		dest[i] = src[i];
//		//printf("호출");
//		if (dest[i] == '\0')			//널문자가 나오면 break, 이러지않으면 dest_size까지 읽기때문에(불필요한 영역까지 읽을수있음)
//			break;
//	}
//
//	dest[i] = '\0';
//
//	return dest;
//}
//
//int main()
//{
//	char str[] = "abc";
//	printf("%d\n", strlen(str));		//라이브러리함수
//	printf("%d\n", str_len(str));		//내가 짠 함수 
//
//	char dest1[25] = {};
//	strcpy(dest1, str);					//라이브러리함수
//	printf("%s\n", dest1);
//
//	char dest2[25] = {};
//	str_cpy(dest2, str);				//내가 짠 함수 
//	printf("%s\n", dest2);
//
//	//strcpy의 고질적인 문제는 뭐가 있을까?
//	//문제점 : 목적지의 배열의 크기가 충분히 크기 않을 경우 어떤일이 발생될지 알 수 가 없다(예상치못한 메모리에 접근할 수 있다)
//	//다음과 같은 코드가 그 예이다.
//	/*char dest3[2] = {};
//	str_cpy(dest3, str);				
//	printf("%s\n", dest3);*/
//
//	//해결책1 : strncpy를 쓴다.
//	char dest4[25];
//	strncpy(dest4, str, 4);				//4개라고 한 이유는 null문자를 포함한것이다.
//	printf("strncpy : %s\n", dest4);
//
//	char dest5[25];
//	str_ncpy(dest5, str, 4);			//내가 짠 함수	
//	printf("str_ncpy : %s\n", dest5);
//
//	//해결책2 : strcpy_s를 쓴다
//	char dest6[25];
//	strcpy_s(dest6, 4, str);			
//	printf("strcpy_s : %s\n", dest6);
//
//	char dest7[25];						//내가 짠 함수
//	str_cpy_s(dest7, 1, str);
//	printf("str_cpy_s : %s\n", dest7);
//
//}


#include <stdio.h>

int factorial(int n)
{
	if (n == 0)
		return 1;
	else
		return n * factorial(n - 1);
}

void main()
{
	int i;
	scanf("%d", &i);
	printf("%d! => %d", i, factorial(i));
}