#include <stdio.h>
#include <string.h>

class People
{
private:
	char name[12];

public:
	People()
	{
		printf("constructor\n");
	}
	explicit People(const char * _name)
	{
		strcpy(name, _name);
		printf("constructor(const char* )\n");
	}
	People(const People& people)
	{
		strcpy(name, people.name);
		printf("copy constructor \n");
	}
	~People()
	{
		printf("destructor\n");
	}
	void PrintPeople()
	{
		printf("%s\n", &name);
	}
};

void Test(People p)
{
	p.PrintPeople();
}

People GetPeople()
{
	People t;
	return t;
}

void main()
{
	// 암시적인 생성자 호출을 했기때문에 컴파일 오류
	//People p = "John";			
	//Test("Mike");

	People p("John");			
	People temp = People("KimHanSam");
	Test(temp);	
}
