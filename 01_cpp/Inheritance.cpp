#include <stdio.h>

class KTire
{
public:
	float radius;
};

class KCar
{
public:
	void Move() 
	{
		printf("KCar move");
	};
private:
	// composition
	KTire	tire[4];		
};

class KWing
{
public:
};

class KDriver 
{
};

class KSportCar : public KCar
{
public:
	KSportCar() : wing(nullptr), driver(nullptr) {}
	~KSportCar()
	{
		delete wing;
	}
	void Turbo() {}
	void Move()
	{
		printf("KSport move");
	};
	void SetDriver(KDriver* pDriver)
	{
		driver = pDriver;
	}
	void CreateAccseeories()
	{
		wing = new KWing;
	}
private:
	KWing*		wing;	// aggregation
	KDriver*	driver;	// association
};

void main()
{
	KSportCar* pCar = new KSportCar;
	pCar->Move();
	delete pCar;
}
