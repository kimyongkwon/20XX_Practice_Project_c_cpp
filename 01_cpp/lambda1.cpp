//18.8.17

// 다음은 배열에서 4보다 작은 값의 갯수를 구하는 프로그램이다.
// 원래 count_if라는 함수는 algorithm에 구현되어있지만
// 여기서는 function pointer와 template으로 구현해줬다.

//#include <stdio.h>
//#include <vector>
//
//bool less2(int i)
//{
//	return i < 4;
//}
//
//// first와 last는 포인터형식으로 되야한다.
//// != 연산과 dereference연산이 가능해야하기때문이다.
//template<typename T, typename U>
//int count_if(T first, T last, U predicate_)
//{
//	int ret = 0;
//	while (first != last)
//	{
//		if (predicate_(*first))
//		{
//			ret++;
//		}
//		++first;
//	}
//	return ret;
//}
//
//void main()
//{
//	int v[] { 5, 4, 3, 2, 1 };
//	int count = 0;
//
//	count = count_if( &v[0], &v[_countof(v)], less2);
//	printf("%d\n", count);
//}

////--------------------------------------------------------------------------
//
//#include <stdio.h>
//#include <vector>
//
//bool less2(int i)
//{
//	return i < 4;
//}
//
//struct less3	// function object, functor
//{
//	bool operator() (int left)
//	{
//		return left < 4;
//	}
//};
//
//template<typename T, typename U>
//int count_if(T first, T last, U predicate_)
//{
//	int ret = 0;
//	while (first != last)
//	{
//		if (predicate_(*first))
//		{
//			ret++;
//		}
//		++first;
//	}
//	return ret;
//}
//
//void main()
//{
//	int v[]{ 5, 4, 3, 2, 1 };
//	int count = 0;
//
//	count = count_if(&v[0], &v[_countof(v)], less2);
//	printf("%d\n", count);
//
//	less3 a = less3();
//	count = count_if(&v[0], &v[_countof(v)], a);
//	//위 두 줄을 대신해서 다음 코드도 가능하다
//	//count = count_if(&v[0], &v[_countof(v)], less3());
//
//	printf("%d\n", count);
//}

//------------------------------------------------------------------
//
//#include <stdio.h>
//#include <vector>
//
//bool less2(int i)
//{
//	return i < 4;
//}
//
//struct less3	// function object, functor
//{
//	bool operator() (int left)
//	{
//		return left < 4;
//	}
//};
//
//template<typename T, typename U>
//int count_if(T first, T last, U predicate_)
//{
//	int ret = 0;
//	while (first != last)
//	{
//		if (predicate_(*first))
//		{
//			ret++;
//		}
//		++first;
//	}
//	return ret;
//}
//
//template<typename T>
//class less
//{
//public:
//	typedef const T			argument_type;
//	typedef bool			result_type;
//	result_type operator() (argument_type& left_, argument_type& right_) const
//	{
//		return left_ < right_;
//	}
//};
//
//void main()
//{
//	int v[]{ 5, 4, 3, 2, 1 };
//	int count = 0;
//
//	count = count_if(&v[0], &v[_countof(v)], less2);			// function pointer
//	printf("%d\n", count);
//
//	less3 a = less3();
//	count = count_if(&v[0], &v[_countof(v)], a);				// function object
//	//위 두 줄을 대신해서 다음 코드도 가능하다
//	//count = count_if(&v[0], &v[_countof(v)], less3());
//	printf("%d\n", count);
//
//	count = count_if(&v[0], &v[_countof(v)], less<int>());		
//	printf("%d\n", count);
//}

//------------------------------------------------------------------
//
//#include <stdio.h>
//#include <vector>
//
//template<typename T, typename U>
//int count_if(T first, T last, U predicate_)
//{
//	int ret = 0;
//	while (first != last)
//	{
//		if (predicate_(*first))
//		{
//			ret++;
//		}
//		++first;
//	}
//	return ret;
//}
//
//template<typename T>
//class less
//{
//public:
//	typedef const T			argument_type;
//	typedef bool			result_type;
//	result_type operator() (argument_type& left_, argument_type& right_) const
//	{
//		return left_ < right_;
//	}
//};
//
//void main()
//{
//	int v[]{ 5, 4, 3, 2, 1 };
//	int count = 0;
//
//	count = count_if(&v[0], &v[_countof(v)], less<int>());		
//	printf("%d\n", count);
//}

