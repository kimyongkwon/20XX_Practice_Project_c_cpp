#include <iostream>

using namespace std;

class Super
{
public:
	virtual void func1(void) { cout << "Super 클래스의 func1" << endl; }
	void func2(void) { cout << "Super 클래스의 func2" << endl; }
};

class Sub : public Super
{
public:
	virtual void func1(void) { cout << "Sub 클래스의 func1" << endl; }
	void func3(void) { cout << "Sub 클래스의 func3" << endl; }
};

int main()
{
	Sub * sub = new Sub();
	sub->func1();
	sub->func2();

}