//#include <stdio.h>
//
//class KBase
//{
//public:
//	~KBase()
//	{
//		printf("~KBase()\n");
//	}
//	void Test1()
//	{
//		printf("KBase::Test1()\n");
//	}
//	void Test2()
//	{
//		printf("KBase::Test2()\n");
//	}
//};
//
//class KDerived : public KBase
//{
//public:
//	~KDerived()
//	{
//		printf("~KDerived()\n");
//	}
//	void Test1()
//	{
//		printf("KDerived::Test1()\n");
//	}
//	void Test2()
//	{
//		printf("KDerived::Test2()\n");
//	}
//};
//
//void main()
//{
//	KBase*		pa;
//	KBase*		pb;
//	pa = new KBase();
//	pb = new KDerived();
//
//	pa->Test2();
//	pb->Test2();
//
//	delete pa;
//	delete pb;
//
//}

//virtual table
#include <stdio.h>
#include <memory.h>
using namespace std;

class Base;

typedef void (Base::*FP) ();

class Base
{
	int a, b, c;
public:
	Base()
	{
		a = 1;
		b = 5;
		c = 3;
	}
	void A()
	{
		int*	p = reinterpret_cast<int*> (this);

		printf("%d\n", p[0]);
		printf("%d\n", p[1]);
		printf("%d\n", p[2]);
		printf("%d\n", p[3]);
		printf("%d\n", p[4]);

		void*** po = (void***)this;
		void**	pvtbl = (void**)(*po);
		void*	pfn = (void*)pvtbl[0];
		FP		fp;
		memcpy(&fp, &pfn, 8);

		(this->*fp)();
	}
	virtual void C()
	{
		printf("Base::C\n");
	}
	virtual void B()
	{
		printf("Base::B\n");
	}
};

class Derived : public Base
{
public:
	virtual void C()
	{
		printf("Derived::C\n");
	}
	virtual void D()
	{
		printf("Derived::D\n");
	}
	void E()
	{
		printf("Derived::E\n");
	}
};

void main()
{
	Derived d;
	d.A();
}