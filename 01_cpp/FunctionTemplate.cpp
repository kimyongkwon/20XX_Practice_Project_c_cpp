//18.5.28
//함수 템플릿
//클래스 템플릿이 아니라 일반 함수도 템플릿으로 작성 할 수 있다.

#include <iostream>
using namespace std;

static const size_t NOT_FOUND = (size_t)(-1);

template <typename T>
size_t Find(T& value, T* arr, size_t size)
{
	for (size_t i = 0; i < size; i++) {
		if (arr[i] == value)
			return i;
	}

	return NOT_FOUND;
}

int main()
{
	int x = 3; int intArr[] = { 1,2,3,4 };
	size_t sizeIntArr = sizeof(intArr) / sizeof(int);
	size_t res;
	res = Find(x, intArr, sizeIntArr);					// 타입 연역을 통해 함수 호출
	res = Find<int>(x, intArr, sizeIntArr);				// 명시적인 타입 지정으로 함수 호출
	if (res != NOT_FOUND)
		cout << res << endl;
	else
		cout << "NOT found" << endl;
}