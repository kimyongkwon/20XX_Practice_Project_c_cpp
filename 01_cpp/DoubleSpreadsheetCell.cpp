
#include <limits>
#include <string>
#include <iostream>
#include <sstream>

#include "DoubleSpreadsheetCell.h"

DoubleSpreadsheetCell::DoubleSpreadsheetCell() : mValue(std::numeric_limits<double>::quiet_NaN())
{
}

void DoubleSpreadsheetCell::set(double inDouble)
{
	mValue = inDouble;
}

void DoubleSpreadsheetCell::set(const std::string & inString)
{
	mValue = stringToDouble(inString);
}

std::string DoubleSpreadsheetCell::getString() const
{
	return doubleToString(mValue);
}

std::string DoubleSpreadsheetCell::doubleToString(double inValue)
{
	std::ostringstream ostr;
	ostr << inValue;
	return ostr.str();
}

double DoubleSpreadsheetCell::stringToDouble(const std::string & inValue)
{
	double tmp;
	std::istringstream istr(inValue);
	istr >> tmp;
	if (istr.fail() || !istr.eof())
	{
		return 0;
	}
	return tmp;
}