// 18.9.25

//-------------------------------------------------------------------------------------------
// strcpy
//-------------------------------------------------------------------------------------------

//#include <iostream>
//using namespace std;
//
//char* Strcpy(char* dest, const char* src)
//{
//	int i;
//	for (i = 0; src[i] != '\0'; ++i) {
//		dest[i] = src[i];
//	}
//	dest[i] = '\0';
//
//	return dest;
//}
//
//int main()
//{
//	char dest[100] = { };
//
//	Strcpy(dest, "aaa");
//	cout << dest << endl;
//
//	cout << Strcpy(dest, "bbb") << endl;
//
//	char* dest2 = Strcpy(dest, "ccc");
//	cout << dest2 << endl;
//}

//-------------------------------------------------------------------------------------------
// strncpy
// strncpy는 복사한 문자 뒤에 널문자를 대입해주지 않는다.
//-------------------------------------------------------------------------------------------

//#include <iostream>
//using namespace std;
//
//char* Strncpy(char* dest, const char* src, size_t destlen)
//{
//	int i;
//	for (i = 0; i < destlen; ++i) {
//		dest[i] = src[i];
//	}
//
//	return dest;
//}
//
//int main()
//{
//	// dest의 크기가 src보다 크고 복사하려는 문자 갯수(destlen)이 dest크기보다 작으면
//	// dest에 '\0'가 대인되기 떄문에전혀 문제 없이 동작
//	/*char dest[50];
//	cout << Strncpy(dest, "aaaaaaaa", 15) << endl;*/
//
//	// 하지만 src보다 복사하려는 문자 갯수가 작으면 dest에 '\0'가 대입되지않으므로 쓰레기값 출력
//	char dest[9];
//	cout << Strncpy(dest, "aaaaaaaa", 3) << endl;
//}


//-------------------------------------------------------------------------------------------
// strcpy_s
// 확실하지 않음
//-------------------------------------------------------------------------------------------

//#include <iostream>
//#include <assert.h>
//using namespace std;
//
//char* Strcpy_s(char* dest, size_t destlen, const char* src)
//{
//	if (destlen > strlen(src)) {
//		cout << "dest보다 src크기가 더 크다" << endl;
//		return dest;
//	}
//
//
//	int i;
//	for (i = 0; i < destlen; i++) {
//		dest[i] = src[i];
//	}
//	dest[i] = '\0';
//	return dest;
//}
//
//int main()
//{
//	char dest[6];
//
//	cout << Strcpy_s(dest, 7, "aaaaaa");
//}


//-------------------------------------------------------------------------------------------
// strlen
//-------------------------------------------------------------------------------------------

//#include <stdio.h>
//
//int Strlen(const char* src)
//{
//	int i = 0;
//	while (src[i] != '\0')
//		i++;
//	return i;
//}
//
//int main()
//{
//	char* str = "test";
//	printf("%d", Strlen(str));
//}

//-------------------------------------------------------------------------------------------
// strcmp
//-------------------------------------------------------------------------------------------

//#include <stdio.h>
//#include <string.h>
//
//int Strcmp(const char* src1, const char* src2)
//{
//	int i = 0;
//	while (src1[i] != '\0' || src2[i] != '\0'){
//		if (src1[i] != src2[i])
//			return 1;
//		i++;
//	}
//	return 0;
//}
//
//int main()
//{
//	char* str1 = "abc";
//	char* str2 = "abcd";
//	printf("%d", Strcmp(str1, str2));
//}

//-------------------------------------------------------------------------------------------
// 피보나치 수열(재귀함수)
//-------------------------------------------------------------------------------------------

//#include <stdio.h>
//#include <string.h>
//using namespace std;
//
//int fibonacci(int n)
//{
//	if (n == 1)
//		return 1;
//	if (n == 0)
//		return 0;
//
//	return fibonacci(n - 1) + fibonacci(n - 2);
//}
//
//int main()
//{
//	printf("%d", fibonacci(7));
//}

//-------------------------------------------------------------------------------------------
// 어떤 값 N을 함수의 매개변수로 전달했을때 
// 그 보다 작은 자연수들을 모두 더한값을 출력하는 문제 (재귀함수)
//-------------------------------------------------------------------------------------------

//#include <stdio.h>
//#include <string.h>
//using namespace std;
//
//int fibonacci(int n)
//{
//	if (n == 0)
//		return 0;
//
//	return n-1 + fibonacci(n-1);
//}
//
//int main()
//{
//	printf("%d", fibonacci(5));
//}

//-------------------------------------------------------------------------------------------
// 어떤 문자열에서 가장 많이 사용된 문자와 갯수를 찾는 알고리즘 문제
//-------------------------------------------------------------------------------------------

//#include <stdio.h>
//using namespace std;
//
//void func(char* str)
//{
//	int arr[26] = {};
//
//	for (int i = 0; str[i] != '\0'; ++i)
//	{
//		arr[str[i] - 'a'] += 1;
//	}
//
//	int tmp = 0;
//	int idx = 0;
//	for (int i = 0; i < 26; ++i)
//	{
//		if (arr[i] > tmp) {
//			tmp = arr[i];
//			idx = i;
//		}
//	}
//
//	printf("가장 많이 쓰인 문자는 %c이고 호출된 횟수는 %d이다", 'a' + idx, tmp);
//}
//int main()
//{
//	func("aaaaaaaaaaaaaabbbbbcc");
//}

//-------------------------------------------------------------------------------------------
// 연결리스트
//-------------------------------------------------------------------------------------------

//#include <stdio.h>
//#include <Windows.h>
//
//typedef struct NODE
//{
//	int val;
//	struct NODE* next;
//}Node;
//
//typedef struct LIST
//{
//	Node* head;
//	Node* cur;
//	Node* tail;
//}List;
//
//Node* MakeNode(int val)
//{
//	Node* newNode = (Node*)malloc(sizeof(Node));
//	newNode->val = val;
//	newNode->next = NULL;
//
//	return newNode;
//}
//
//void InsertNode(List* list, Node* node)
//{
//	if (list->head == NULL) {
//		list->head = node;
//		list->cur = node;
//	}
//	else {
//		list->cur->next = node;
//		list->cur = node;
//	}
//}
//
//void PrintNodes(List* list)
//{
//	if (list->head == NULL) {
//		printf("리스트에 값이 없습니다");
//	}
//	else {
//		Node* ptr = list->head;
//		while (ptr != NULL)
//		{
//			printf("%d ", ptr->val);
//			ptr = ptr->next;
//		}
//	}
//}
//
//void DeleteNode(List* list, int val)
//{
//	if (list->head == NULL) {
//		printf("리스트에 값이 없습니다");
//	}
//	else {
//		Node* ptr = list->head;
//		Node* prev = NULL;
//		while (ptr != NULL)
//		{
//			if (ptr->val == val)
//			{
//				if (ptr == list->head)
//				{
//					list->head = ptr->next;
//					free(ptr);
//					ptr = list->head;
//				}
//				else if (ptr->next == NULL) {
//					free(ptr);
//					prev->next = NULL;
//					break;
//				}
//				else {
//					prev->next = ptr->next;
//					free(ptr);
//					ptr = prev->next;
//				}
//			}
//			prev = ptr;
//			ptr = ptr->next;
//		}
//	}
//}
//
//int main()
//{
//	List list;
//	list.head = NULL;
//	list.tail = NULL;
//	
//	Node* node1 = MakeNode(1);
//	Node* node2 = MakeNode(2);
//
//	InsertNode(&list, node1);
//	InsertNode(&list, node2);
//	InsertNode(&list, MakeNode(3));
//	InsertNode(&list, MakeNode(4));
//
//	DeleteNode(&list, 3);
//
//	PrintNodes(&list);
//}


//-------------------------------------------------------------------------------------------
// 문자열에서 연속되는 문자 제거
//-------------------------------------------------------------------------------------------

//#include <stdio.h>
//#include <Windows.h>
//#include <iostream>
//
//char* func(char* str)
//{
//	int len = strlen(str);
//	char* copystr = new char[len + 1];
//
//	char tmp = str[0];
//	copystr[0] = tmp;
//	int idx = 1;
//	for (int i = 1; str[i] != '\0'; ++i) {
//		if (tmp != str[i]) {
//			copystr[idx++] = str[i];
//			tmp = str[i];
//		}
//	}
//	copystr[idx] = '\0';
//
//	return copystr;
//}
//
//int main()
//{
//	printf("%s", func("asd  bbbbscc"));
//}


//-------------------------------------------------------------------------------------------
// 팩토리얼 함수
//-------------------------------------------------------------------------------------------

//#include <stdio.h>
//
//int factorial(int n)
//{
//	if (n == 1)      // n이 1일 때
//		return 1;    // 1을 반환하고 재귀호출을 끝냄
//
//	return n * factorial(n - 1);    // n과 factorial 함수에 n - 1을 넣어서 반환된 값을 곱함
//}
//
//int main()
//{
//	printf("%d", factorial(5));
//
//	return 0;
//}
