// 
#include <iostream>
using namespace std;

class Ridable
{
public:
	void Run()
	{
		cout << "달린다" << endl;
	}
};

class Car : public Ridable
{
public:
	void Run()
	{
		cout << "차가 ";
		Ridable::Run();
	}
};

class Plane : public Ridable
{
public:
	void Run()
	{
		cout << "비행기가 ";
		Ridable::Run();
	}
};

class Vehicle : public Car, public Plane
{
public: 
	
};


int main()
{
	Vehicle v;
	v.Run();
}