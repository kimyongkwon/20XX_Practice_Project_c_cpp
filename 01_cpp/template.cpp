//#include <iostream>
//
//using namespace std;
//
//template <class T>
//T func(T data1, T data2)
//{
//	return data1 + data2;
//}
//
//int main()
//{
//	cout << func(1.232, 2.23) << endl;
//	cout << func(1, 2) << endl;
//	cout << func(5.2, 5.3) << endl;
//}



//#include <iostream>
//
//using namespace std;
//
//template <class T1, class T2>
//T2 func(T1 data1, T2 data2)
//{
//	return data1 + data2;
//}
//
//int main()
//{
//	cout << func(1, 2.23) << endl;
//	cout << func(1.231 , 5) << endl;
//	cout << func(5.2, 5.3) << endl;
//}



////클래스 특수화
//#include <iostream>
//using namespace std;
//
//template<class T>
//int SizeOf(T a)
//{
//	return sizeof(a);
//}
//
//template<> int SizeOf(char * a)
//{
//	return strlen(a);
//}
//                                     
//int main()
//{
//	int i = 10;
//	double e = 7.7;
//	char * str = "Good morning";
//
//	cout << SizeOf(i) << endl;
//	cout << SizeOf(e) << endl;
//	cout << SizeOf(str) << endl;
//}



//#include <iostream>
//using namespace std;
//
//template <typename T>
//class Data
//{
//	T data;
//public:
//	Data(T d);
//	void SetData(T d);
//	T GetData();
//};
//
//template <typename T>
//Data<T>::Data(T d)
//{
//	data = d;
//}
//
//template <typename T>
//void Data<T>::SetData(T d)
//{
//	data = d;
//}
//
//template <typename T>
//T Data<T>::GetData(void)
//{
//	return data;
//}
//
//int main()
//{
//	Data<int> d1(0);
//	d1.SetData(10);
//	Data<int> d2('a');
//
//	cout << d1.GetData() << endl;
//	cout << d2.GetData() << endl;
//}


//#include <iostream>
//
//using namespace std;
//
//class Stack
//{
//private:
//	int topIdx;
//	char * stackPtr;
//public:
//	Stack(int s = 10);
//	~Stack();
//	void Push(const char& pushValue);
//	char Pop();
//};
//
//Stack::Stack(int len)
//{
//	topIdx = -1;
//	stackPtr = new char[len];
//}
//
//Stack::~Stack()
//{
//	delete[] stackPtr;
//}
//
//void Stack::Push(const char& pushValue)
//{
//	stackPtr[++topIdx] = pushValue;
//}
//
//char Stack::Pop()
//{
//	return stackPtr[topIdx--];
//}
//
//int main()
//{
//	Stack stack(10);
//	stack.Push('A');
//	stack.Push('B');
//	stack.Push('C');
//
//	for (int i = 0; i < 3; i++)
//		cout << stack.Pop() << endl;
//}




//#include <iostream>
//
//using namespace std;
//
//template <class T>
//class Stack
//{
//private:
//	int topIdx;
//	T * stackPtr;
//public:
//	Stack(int s = 10);
//	~Stack();
//	void Push(const T& pushValue);
//	T Pop();
//};
//
//template <class T>
//Stack<T>::Stack(int len)
//{
//	topIdx = -1;
//	stackPtr = new T[len];
//}
//
//template <class T>
//Stack<T>::~Stack()
//{
//	delete[] stackPtr;
//}
//
//template <class T>
//void Stack<T>::Push(const T& pushValue)
//{
//	stackPtr[++topIdx] = pushValue;
//}
//
//template <class T>
//T Stack<T>::Pop()
//{
//	return stackPtr[topIdx--];
//}
//
//int main()
//{
//	Stack<char> stack(10);
//	stack.Push('A');
//	stack.Push('B');
//	stack.Push('C');
//
//	for (int i = 0; i < 3; i++)
//		cout << stack.Pop() << endl;
//
//	Stack<int> stack2(10);
//	stack2.Push(10);
//	stack2.Push(20);
//	stack2.Push(30);
//
//	for (int i = 0; i < 3; i++)
//		cout << stack2.Pop() << endl;
//}


//문제1
//#include <iostream>
//using namespace std;
//
//template <typename T>
//T Add(T a, T b)
//{
//	return a + b;
//}
//
//class Point 
//{
//private:
//	int x, y;
//public:
//	Point(int _x = 0, int _y = 0) : x(_x), y(_y) {}
//	void ShowPosition();
//
//	Point& operator+ (Point& a)
//	{
//		x += a.x;
//		y += a.y;
//		return *this;
//	}
//};
//
//void Point::ShowPosition() {
//	cout << x << " " << y << endl;
//}
//
//int main()
//{
//	Point p1(1,2);
//	Point p2(1,2);
//
//	Point p3 = Add(p1, p2);
//	p3.ShowPosition();
//}



//문제2
//#include <iostream>
//using namespace std;
//
//template <typename T>
//T Add(T a, T b)
//{
//	return a + b;
//}
//
//template <class T>
//void Swap(T& a, T& b)
//{
//	T tmp = b;
//	b = a;
//	a = tmp;
//}
//
//class Point
//{
//private:
//	int x, y;
//public:
//	Point(int _x = 0, int _y = 0) : x(_x), y(_y) {}
//	void ShowPosition();
//
//	Point& operator+ (Point& a)
//	{
//		x += a.x;
//		y += a.y;
//		return *this;
//	}
//};
//
//void Point::ShowPosition() {
//	cout << x << " " << y << endl;
//}
//
//int main()
//{
//	Point p1(1, 2);
//	Point p2(100, 200);
//
//	Swap(p1, p2);
//	p1.ShowPosition();
//	p2.ShowPosition();
//
//	int a = 10, b = 20;
//	Swap(a, b);
//	cout << a << ' ' << b << endl;
//
//}



#include <iostream>
using namespace std;

template <class T>
class A 
{
	T data;
public:
	A(T _data) : data(_data) {};
	void show(void)
	{
		cout << data<DATA<int>> << endl;
	}
};

template <class T>
class DATA
{
	T data;
public:
	DATA(T _data) : data(_data) {};
	
};

int main()
{
	A<int> a(10);
	A<DATA<int>> b(10);
	
}