#include <stdio.h>
#include <string.h>

class People
{
private:
	char name[12];

public:
	People()
	{
		name[0] = 0;
		printf("constructor\n");
	}
	explicit People(const char * _name)
	{
		strcpy(name, _name);
		printf("constructor(const char* )\n");
	}
	People(const People& people)
	{
		strcpy(name, people.name);
		printf("copy constructor \n");
	}
	People& operator=(const People& people)
	{
		strcpy(name, people.name);
		printf("assignment operator \n");
		return *this;
	}
	~People()
	{
		printf("destructor\n");
	}
	void PrintPeople()
	{
		printf("%s\n", &name);
	}
};

People GetPeople()
{
	return People("Marry");
}

void Test(People p)
{
	p.PrintPeople();
}

void main()
{
	// ex1
	/*People t;
	t = GetPeople();*/

	// ex2
	Test( People("Marry") );
}
