// 18.8.17

//------------------------------------------------------------------
//
//#include <stdio.h>
//#include <vector>
//
//template<typename T, typename U>
//int count_if(T first, T last, U predicate_)
//{
//	int ret = 0;
//	while (first != last)
//	{
//		if (predicate_(*first))
//		{
//			ret++;
//		}
//		++first;
//	}
//	return ret;
//}
//
//template<typename T>
//class less
//{
//public:
//	typedef const T			argument_type;
//	typedef bool			result_type;
//	result_type operator() (argument_type& left_, argument_type& right_) const
//	{
//		return left_ < right_;
//	}
//};
//
//template<typename P>
//class binder2nd
//{
//public:
//	typedef typename P::argument_type	argument_type;
//	typedef typename P::result_type		result_type;
//
//	binder2nd(const P& pred_, argument_type& right_)
//		: pred(pred_), right(right_)
//	{
//	}
//	result_type operator() (argument_type& left_) const
//	{
//		return pred(left_, right);
//	}
//
//private:
//	P					pred;
//	argument_type		right;
//};
//
//void main()
//{
//	int v[]{ 5, 4, 3, 2, 1 };
//	int count = 0;
//
//	count = count_if(&v[0], &v[_countof(v)], binder2nd<less<int>> (less<int>(), 4) );
//	printf("%d\n", count);
//}

//------------------------------------------------------------------

//#include <stdio.h>
//#include <vector>
//
//template<typename T, typename U>
//int count_if(T first, T last, U predicate_)
//{
//	int ret = 0;
//	while (first != last)
//	{
//		if (predicate_(*first))
//		{
//			ret++;
//		}
//		++first;
//	}
//	return ret;
//}
//
//template<typename T>
//class less
//{
//public:
//	typedef const T			argument_type;
//	typedef bool			result_type;
//	result_type operator() (argument_type& left_, argument_type& right_) const
//	{
//		return left_ < right_;
//	}
//};
//
//template<typename P>
//class binder2nd
//{
//public:
//	typedef typename P::argument_type	argument_type;
//	typedef typename P::result_type		result_type;
//
//	binder2nd(const P& pred_, argument_type& right_)
//		: pred(pred_), right(right_)
//	{
//	}
//	result_type operator() (argument_type& left_) const
//	{
//		return pred(left_, right);
//	}
//
//private:
//	P					pred;
//	argument_type		right;
//};
//
//template<typename P>
//binder2nd<P> bind2nd(const P& pred_, typename P::argument_type& right_)
//{
//	return binder2nd<P>(pred_, right_);
//}
//
//void main()
//{
//	int v[]{ 5, 4, 3, 2, 1 };
//	int count = 0;
//
//	count = count_if(&v[0], &v[_countof(v)], bind2nd(less<int>(), 4));
//	printf("%d\n", count);
//}

//-----------------------------------------------------------------------------


//#include <stdio.h>
//#include <vector>
//
//template<typename T, typename U>
//int count_if(T first, T last, U predicate_)
//{
//	int ret = 0;
//	while (first != last)
//	{
//		if (predicate_(*first))
//		{
//			ret++;
//		}
//		++first;
//	}
//	return ret;
//}
//
//template<typename T>
//class less
//{
//public:
//	typedef const T			argument_type;
//	typedef bool			result_type;
//	result_type operator() (argument_type& left_, argument_type& right_) const
//	{
//		return left_ < right_;
//	}
//};
//
//template<typename P>
//class binder2nd
//{
//public:
//	typedef typename P::argument_type	argument_type;
//	typedef typename P::result_type		result_type;
//
//	binder2nd(const P& pred_, argument_type& right_)
//		: pred(pred_), right(right_)
//	{
//	}
//	result_type operator() (argument_type& left_) const
//	{
//		return pred(left_, right);
//	}
//
//private:
//	P					pred;
//	argument_type		right;
//};
//
//template<typename P>
//binder2nd<P> bind2nd(const P& pred_, typename P::argument_type& right_)
//{
//	return binder2nd<P>(pred_, right_);
//}
//
//void main()
//{
//	int v[]{ 5, 4, 3, 2, 1 };
//	int count = 0;
//
//	count = count_if(&v[0], &v[_countof(v)], bind2nd(less<int>(), 4));
//	printf("%d\n", count);
//}

//-------------------------------------------------------------------------------------------

//------------------------------------------------------------------
//
//#include <stdio.h>
//#include <vector>
//
//template<typename T, typename U>
//int count_if(T first, T last, U predicate_)
//{
//	int ret = 0;
//	while (first != last)
//	{
//		if (predicate_(*first))
//		{
//			ret++;
//		}
//		++first;
//	}
//	return ret;
//}
//
//template<typename T>
//class less
//{
//public:
//	typedef const T			argument_type;
//	typedef bool			result_type;
//	result_type operator() (argument_type& left_, argument_type& right_) const
//	{
//		return left_ < right_;
//	}
//};
//
//template<typename P>
//class binder2nd
//{
//public:
//	typedef typename P::argument_type	argument_type;
//	typedef typename P::result_type		result_type;
//
//	binder2nd(const P& pred_, argument_type& right_)
//		: pred(pred_), right(right_)
//	{
//	}
//	result_type operator() (argument_type& left_) const
//	{
//		return pred(left_, right);
//	}
//
//private:
//	P					pred;
//	argument_type		right;
//};
//
//void main()
//{
//	int v[]{ 5, 4, 3, 2, 1 };
//	int count = 0;
//
//	count = count_if(&v[0], &v[_countof(v)], binder2nd<less<int>> (less<int>(), 4) );
//	printf("%d\n", count);
//}

//------------------------------------------------------------------

//#include <stdio.h>
//#include <vector>
//
//template<typename T, typename U>
//int count_if(T first, T last, U predicate_)
//{
//	int ret = 0;
//	while (first != last)
//	{
//		if (predicate_(*first))
//		{
//			ret++;
//		}
//		++first;
//	}
//	return ret;
//}
//
//template<typename T>
//class less
//{
//public:
//	typedef const T			argument_type;
//	typedef bool			result_type;
//	result_type operator() (argument_type& left_, argument_type& right_) const
//	{
//		return left_ < right_;
//	}
//};
//
//template<typename P>
//class binder2nd
//{
//public:
//	typedef typename P::argument_type	argument_type;
//	typedef typename P::result_type		result_type;
//
//	binder2nd(const P& pred_, argument_type& right_)
//		: pred(pred_), right(right_)
//	{
//	}
//	result_type operator() (argument_type& left_) const
//	{
//		return pred(left_, right);
//	}
//
//private:
//	P					pred;
//	argument_type		right;
//};
//
//template<typename P>
//binder2nd<P> bind2nd(const P& pred_, typename P::argument_type& right_)
//{
//	return binder2nd<P>(pred_, right_);
//}
//
//void main()
//{
//	int v[]{ 5, 4, 3, 2, 1 };
//	int count = 0;
//
//	count = count_if(&v[0], &v[_countof(v)], bind2nd(less<int>(), 4));
//	printf("%d\n", count);
//}

//-----------------------------------------------------------------------------


#include <stdio.h>
#include <vector>
#include <functional> // bind2nd
#include <algorithm> // count_if, less

void main()
{
	int v[]{ 5, 4, 3, 2, 1 };
	int count = 0;

	count = std::count_if(&v[0], &v[_countof(v)], 
		std::binder2nd<std::less<int>>( std::less<int>(), 4) );
	printf("%d\n", count);

	count = std::count_if(&v[0], &v[_countof(v)], 
		std::bind2nd( std::less<int>(), 4) );
	printf("%d\n", count);
}