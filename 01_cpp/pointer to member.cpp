// 18.9.7

#include <stdio.h>

class CTEST
{
public:
	CTEST() : data() {};
	int data;
};

int main()
{
	CTEST	test;

	// pointer to member (멤버변수포인터)
	int		CTEST::*pmd = &CTEST::data;

	test.data = 10000;			// 멤버변수에 직접 접근
	printf("%d\n", test.data);
	test.*pmd = 20000;			// 멤버변수에 pointer to memer를 써서 접근 (우회적)
	printf("%d\n", test.*pmd);
}