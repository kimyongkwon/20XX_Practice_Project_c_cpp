//-----------------------------------------------

//p78
//nullptr이 나온 이유?

//#include <iostream>
//
//void f(char * str) { std::cout << "void f(char * str)" << std::endl;}
//void f(int i) { std::cout << "void f(int i)" << std::endl; }
//
//int main()
//{
//    //f(NULL);
//    f(nullptr);
//}

//-----------------------------------------------

//p79
//스마트 포인터

//#include <iostream>
//#include <memory>
//
//int main()
//{
//    auto a = std::make_unique<
//}
//
//// cl.exe /analyze /EHsc /W4  
//#include <iostream>  

//-----------------------------------------------

////85p
////auto는 표현식의 타입이 자동으로 연역(삭제)된다.
//
//#include <iostream>
//using namespace std;
//
//int main()
//{
//    int count = 10;
//    int& countRef = count;
//    auto myAuto = countRef;
//
//    countRef = 11;
//    cout << count << " ";
//
//    myAuto = 12;
//    cout << count << endl;
//}

//-----------------------------------------------

//85p
//연역되는 문제를 없애기 위해 decltype(auto)을 쓴다.
//#include <iostream>
//using namespace std;
//
//int main()
//{
//    int count = 10;
//    int& countRef = count;
//    decltype(auto) myAuto = countRef;
//
//    countRef = 11;
//    cout << count << " ";
//
//    myAuto = 12;
//    cout << count << endl;
//}

//------------------------------------------------

//p264
//복사생성자를 프로그래머가 정의하면 디폴트복사생성자 그리고 디폴트생성자는 자동 생성되지않는다.
//#include <iostream>
//using namespace std;
//
//class A
//{
//    int num;
//public:
//    A( const A& a )
//    {
//        num = a.num;
//    }
//};
//
//int main()
//{
//    A a;
//}

//-------------------------------------------------

////p257
////얕은 복사생성자
//
//#include <iostream>
//using namespace std;
//
//class A
//{
//	int a;
//public:
//	A() = default;			// A() {};
//	A(int n) : a(n)
//	{
//		cout << a << endl;
//	};
//	A(const A& src)
//	{
//		a = src.a;
//		cout << a << endl;
//	}
//
//	~A()
//	{
//
//	}
//};
//
//int main()
//{
//	A a1(5);
//
//	//기존 객체를 이용해서 새로운 객체를 초기화하기때문에 복사생성자가 호출된다.
//	A a2 = a1;			
//}

//--------------------------------------------------
//18.5.2
//p257
//깊은 복사생성자

//#include <iostream>
//using namespace std;
//
//class A
//{
//	int * p = nullptr;
//public:
//	A() = default;			// A() {};
//
//	A(int n)
//	{
//		cout << "기본 생성자 호출" << endl;
//		p = new int(n);
//		cout << p << endl;
//		cout << *p << endl;
//	};
//
//	A(const A& src)
//	{
//		cout << "복사 생성자 호출" << endl;
//		p = new int;
//		*p = *(src.p);
//		cout << p << endl;
//		cout << *p << endl;
//	}
//
//	~A()
//	{
//		delete p;
//	}
//};
//
//int main()
//{
//	A a1(5);
//	
//	printf("--------\n");
//
//	A a2 = a1;
//}


//--------------------------------------------------
//18.5.2
//p257
//깊은 복사생성자

//#include <iostream>
//using namespace std;
//
//class A
//{
//	int ** p = nullptr;
//public:
//	A() = default;			// A() {};
//
//	A(const int n)
//	{
//		p = new int * [n];
//
//		cout << "p포인터 주소값 : " << p << endl;
//
//		for (int i = 1; i <= n; i++)
//		{ 
//			p[i-1] = new int(i);
//			cout << "p" << i-1 << "의 주소값 : " << p[i - 1] << endl;
//		}
//
//		//값 출력
//		for (int i = 0; i < n; i++)
//			printf("%d ", *p[i]);
//		
//		printf("\n");
//	}
//
//	A(const A& src)
//	{
//		p = new int *[5];
//
//		cout << "p포인터 주소값 : " << p << endl;
//
//		for (int i = 1; i <= 5; i++)
//		{
//			p[i - 1] = new int(*src.p[i - 1]);
//			cout << "p" << i - 1 << "의 주소값 : " << p[i - 1] << endl;
//		}
//		
//		//값 출력
//		for (int i = 0; i < 5; i++)
//			printf("%d ", *p[i]);
//
//		printf("\n");
//	}
//
//	~A()
//	{
//		if (p)
//		{
//			for (int i = 0; i < 5; i++)
//			{
//				cout << "p" << i << "의 주소값 : " << p[i] << endl;
//				delete p[i];
//			}
//
//			cout << "p포인터 주소값 : " << p << endl;
//			delete [] p;
//			p = nullptr;
//		}
//
//		printf("--------\n");
//	}
//};
//
//int main()
//{
//	A a1(5);
//
//	printf("--------\n");
//
//	A a2 = a1;
//
//	printf("--------\n");
//}


//----------------------------------------------------------
//18.5.2
//p296
//const 메서드

//#include <iostream>
//
//using namespace std;
//
//class A
//{
//public:
//	A() = default;
//	void func(void) const			//TEST클래스안에 const 포인터변수 a가 A객체의 메서드를 호출하려면 반드시 그 메서드가 const로 되어있어야한다.  
//	{
//		cout << "func" << endl;
//	}
//};
//
//class TEST
//{
//	
//public:
//	const A * a;
//
//	TEST() : a() {};
//};
//
//int main()
//{
//	TEST test;
//
//	test.a->func();
//}


//-----------------------------------------------------------
//18.5.2
//p334
//상속 기본 (virtual)

//#include <iostream>
//using namespace std;
//
//class Super
//{
//public:
//	void func(void)
//	{
//		cout << "Super" << endl;
//	}
//};
//
//class Sub : public Super
//{
//public:
//	void func(void)
//	{
//		cout << "Sub" << endl;
//	}
//};
//
//int main()
//{
//	Sub sub;
//	Super &super = sub;
//	super.func();
//}

//------------------------------------------------------------
//18.5.9
//override 키워드

//프로그래머는 suoer클래스의 print라는 함수를 sub 클래스에 오버라이딩하고 싶었다.
//하지만 실수로 매개변수를 잘못해서 오버라이딩 되지 않고 super클래스 print함수와는 완전 별개의
//print함수를 sub클래스에 생성하게 되었다.

//class super
//{
//public:
//	super() {};
//	virtual void print() {
//		cout << "super class" << endl;
//	}
//};
//
//class sub : public super
//{
//public:
//	sub() {};
//	virtual void print(int i) {
//		cout << "sub class" << endl;
//	}
//};

//이런 실수를 방지하기 위해 override라는 키워드를 써주면
//"프로그래머 너는 override선언을 했으니 해당클래스는 부모클래스에 있는 함수를 오버라이딩해야돼"라는 의미를 갖게 된다.
//만약 오버라이딩 하지않고 위와 같은 실수를 발생시키면 컴파일 에러가 난다.

//#include <iostream>
//using namespace std;
//
//class super
//{
//public:
//	super() {};
//	virtual void print() {
//		cout << "super class" << endl;
//	}
//};
//
//class sub : public super
//{
//public:
//	sub() {};
//	virtual void print() override{
//		cout << "sub class" << endl;
//	}
//};
//
//int main()
//{
//	super * b = new sub;
//	b->print();
//
//}


//------------------------------------------------------------
//18.5.12
//포인터에 대한 참조형 변수

//#include <iostream>
//using namespace std;
//
//int main()
//{
//	//int* x = nullptr;
//	//int* &ref = x;
//	//ref = new int;
//	//*ref = 5;
//	//cout << *ref << endl;
//
//	int x = 3;
//	int& xRef = x;
//	int* xPtr = &xRef;
//	*xPtr = 100;
//}


//-------------------------------------------------------------
//18.5.12
//포인터 대신 참조형을 쓰면 다음과 같이 코드가 훨씬 깔끔해질 수 있다

//#include <iostream>
//using namespace std;
//
////void separateOddsAndEvens(const int arr[], int size, int **odds, int *numOdds, int **even, int *numEvens)
////{
////	*numOdds = *numEvens = 0;
////	for (int i = 0; i < size; ++i) 
////	{
////		if (arr[i] % 2 == 1)
////			++(*numOdds);
////		else
////			++(*numEvens);
////	}
////
////	*odds = new int[*numOdds];
////	*even = new int[*numEvens];
////	int oddsPos = 0, evenPos = 0;
////	for (int i = 0; i < size; ++i)
////	{
////		if (arr[i] % 2 == 1)
////			(*odds)[oddsPos++] = arr[i];
////		else
////			(*odds)[evenPos++] = arr[i];
////	}
////
////}
//
//void separateOddsAndEvens(const int arr[], int size, int*& odds, int& numOdds, int*& even, int& numEvens)
//{
//	numOdds = numEvens = 0;
//	for (int i = 0; i < size; ++i)
//	{
//		if (arr[i] % 2 == 1)
//			++(numOdds);
//		else
//			++(numEvens);
//	}
//
//	odds = new int[numOdds];
//	even = new int[numEvens];
//	int oddsPos = 0, evenPos = 0;
//	for (int i = 0; i < size; ++i)
//	{
//		if (arr[i] % 2 == 1)
//			(odds)[oddsPos++] = arr[i];
//		else
//			(odds)[evenPos++] = arr[i];
//	}
//
//}
//
//int main()
//{
//	int unSplit[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int *oddNums, *evenNums;
//	int numOdds, numEvens;
//	separateOddsAndEvens(unSplit, 10, &oddNums, &numOdds, &evenNums, &numEvens);
//}


//-------------------------------------------------------------
//18.5.24
//참조형은 한번 초기화된 이후에는 가리키는 영역을 변경 할 수 없다.
//헷깔리면 안돼는게 영역을 변경할 수 없다는 것이지, 영역에 있는 값은 변경 시킬 수 있다.
//#include <cstdio>
//#include <iostream>
//using namespace std;
//
//int main()
//{
//	int a = 5;
//	int &ref = a;
//
//	cout << "a : " << &a << endl;
//	cout << "ref : " << &ref << endl;
//
//	cout << "-------------------------------------" << endl;
//
//	int b = 10;
//	ref = b;
//
//
//	cout << "a : " << &a << endl;
//	cout << "b : " << &b << endl;
//	cout << "ref : " << &ref << endl;
//}


//-------------------------------------------------------------
//18.5.24
//constexpr
//다음과 같은 코드는 컴파일 에러
//배열의 크기를 선언할때 상수가 아닌 구문이 삽입되었기때문에.
//#include <iostream>
//using namespace std;
//
//int getArraySize()
//{
//	return 100;
//}
//
//int main()
//{
//	int arr[getArraySize()];
//}

//하지만 getArraySize앞에 constexpr를 붙이면 해결가능
//#include <iostream>
//using namespace std;
//
//constexpr int getArraySize()
//{
//	return 100;
//}
//
//int main()
//{
//	int arr[getArraySize()];
//}


//-------------------------------------------------------------
//18.5.25
//typedef와 타입 에일리어스(using)

////typedef를 안쓴 함수 포인터
//#include <iostream>
//using namespace std;
//
//void fun(int a)
//{
//	cout << a << " func" << endl;
//}
//
//void fun2(int b)
//{
//	cout << b << " func2" << endl;
//}
//// fptr이라는 함수포인터 정의
//void (*fptr) (int a);
//
//int main()
//{
//	fun(1);
//	//fptr이라는 함수포인터를 최가화 
//	fptr = fun;
//	fptr(2);		//함수 포인터 사용
//	fptr(3);		
//}


//typedef를 사용한 함수 포인터
//#include <iostream>
//using namespace std;
//
//void fun(int a)
//{
//	cout << a << " func" << endl;
//}
//
//void fun2(int b)
//{
//	cout << b << " func2" << endl;
//}
//
//// void (*) (int a)함수형을 typedef를 사용해서 fptr라는 새로운 이름을 만들어줬음. 
//typedef void(*fptr) (int a);
//
//
//int main()
//{
//	//typedef을 사용했기 때문에 void (*) (int a) 함수형을 자료형 처럼 사용할 수 있다.
//	//그렇기 떄문에 다음과 같이 동일한 형의 다른 함수를 다른 포인터로 만들어서 사용할 수 있다.
//
//	fptr ptr = fun;			
//	fptr ptr2 = fun2;
//
//	ptr(5);
//	ptr(10);
//}


//타입 에일리어스를 사용
//#include <iostream>
//using namespace std;
//
//void fun(int a)
//{
//	cout << a << " func" << endl;
//}
//
//void fun2(int b)
//{
//	cout << b << " func2" << endl;
//}
//
//using INT = int;
//using fptr = void(*) (int);
//
//int main()
//{
//	INT a = 5;
//	cout << a << endl;
//
//	fptr p = fun;
//	p(10);
//
//	fptr p2 = fun2;
//	p(20);
//}


//-------------------------------------------------------------
//18.5.25
//초기화 리스트
//#include <iostream>
//#include <initializer_list>
//using namespace std;
//
//int makesum(initializer_list<int> list)
//{
//	int sum = 0;
//	for ( auto elem : list )
//		sum += elem;
//	return sum;
//};
//
//int main()
//{
//	int sum = makesum({ 1,2,3,4,5 });
//	cout << sum << endl;
//}


//--------------------------------------------------------------
//18.5.25
//explicit을 사용하면 꼭 명시적으로 변환해야한다.
//#include <iostream>
//using namespace std;
//
//class test
//{
//	int data;
//public:
//	test(int n) : data(n) {};
//	explicit operator int() { return data; };
//};
//
//int main()
//{
//	test a(10);
//
//	int k = static_cast<int>(a);
//	//int k = (int)a;
//
//	cout << k << endl;
//}


//--------------------------------------------------------------
//스마트포인터
//18.5.25
//unique_ptr
//unique_ptr은 하나의 포인터가 하나의 데이터만 가질 수 있다.
//즉, 데이터는 하나의 포인터가 소유권을 갖는다.
//이 소유권을 넘겨주려면 move함수를 써야함
//#include <iostream>
//#include <memory>
//using namespace std;
//
//int main()
//{
//	unique_ptr<int> p = make_unique<int>(10);
//	cout << *p << endl;
//
//	//unique_ptr<int> p2 = p;		//컴파일 에러난다. p와 p2가 동시에 같은 데이터롤 소유하려하기 때문에
//	unique_ptr<int> p2 = move(p);	//데이터에 대한 p포인터의 소유권을 p2로 넘기기위해서는 이렇게 move함수를 써야한다.
//}


//--------------------------------------------------------------
//스마트포인터
//18.5.25
//shared_ptr
//shared_ptr은 여러개의 포인터가 하나의 데이터를 공유할 수 있다.
//해당 데이터를 공유하는 참조 카운트로 관리한다.
//참조 카운트가 0일때 실제 데이터를 해제한다.
//#include <iostream>
//#include <memory>
//using namespace std;
//
//int main()
//{
//	shared_ptr<int> p = make_shared<int>(10);
//	shared_ptr<int> p2 = p;
//
//	//p와 p2는 현재 동시에 한 데이터를 공유하고 있다.
//	//그러므로 참조카운트는 2
//
//	cout << p2.use_count() << endl;		//2
//	p.reset();							//p의 공유를 해제
//	cout << p2.use_count() << endl;		//1
//	p2.reset();							//p2공유를 해제
//	cout << p2.use_count() << endl;		//0
//	//cout << *p << endl;					//런타임 에러 (왜? 위에서 참조카운트가 0이 되었으므로 데이터가 해제되었으므로)
//
//}
